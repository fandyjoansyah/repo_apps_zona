package com.zonamediagroup.zonamahasiswa;

public class MimeType {
    public static String getMimeType(String ekstensi) {
        //mendapatkan MIME type berdasarkan ekstensi yang ditentukan, agar pengguna bisa langsung membuka ketika mengklik notifikasi sudah download
        switch(ekstensi)
        {
            case ".docx":
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case ".pptx":
                return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            case ".pdf":
                return "application/pdf";
            case ".ppt":
                return"application/vnd.ms-powerpoint";
            case ".doc":
                return "";
            case ".xls":
                return "application/vnd.ms-excel";
            case ".xlsx":
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case ".png":
                return "image/png";
            case ".jpg":
                return "image/jpeg";
            case ".jpeg":
                return "image/jpeg";
            case ".mp4":
                return "video/mp4";
            case ".mkv":
                return "video/x-matroska";
            case ".ts":
                return "video/MP2T";
            case ".m3u8":
                return "video/application/x-mpegURL";
            case ".3gp":
                return "video/3gpp";
            case ".mov":
                return "video/quicktime";
            case ".avi":
                return "video/x-msvideo";
            case ".wmv":
                return "video/x-ms-wmv";
            case ".flv":
                return "video/x-flv";
            default:
                return "";
        }
    }
}

