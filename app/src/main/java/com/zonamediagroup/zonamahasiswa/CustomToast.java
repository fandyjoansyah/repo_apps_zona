package com.zonamediagroup.zonamahasiswa;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

public class CustomToast {

    public static final String JENIS_TOAST_WARNING = "warning";
    public static final String JENIS_TOAST_ERROR = "error";
    public static final String JENIS_TOAST_INFORMATION = "information";
    public static final String JENIS_TOAST_SUCCESS = "success";


    public static void s(Context context,String pesan){
        Toast.makeText(context,pesan,Toast.LENGTH_SHORT).show();
    }

    public static void l(Context context, String pesan){
        Toast.makeText(context,pesan,Toast.LENGTH_LONG).show();
    }
    public static void sD(Context context,String pesan,String debug){
        Toast.makeText(context,pesan+": "+debug,Toast.LENGTH_SHORT).show();
    }

    public static void lD(Context context, String pesan,String debug){
        Toast.makeText(context,pesan+": "+debug,Toast.LENGTH_LONG).show();
    }


    public static void informasiTanpaIkon(Context context,String message, int duration){
        Toast toast = new Toast(context);

        View view = LayoutInflater.from(context)
                .inflate(R.layout.custom_toast_layout, null);

        TextView tvMessage = view.findViewById(R.id.tvMessage);
        tvMessage.setText(message);
        toast.setView(view);
        toast.setDuration(duration);
        toast.show();
    }


    public static void makeCustomToast(Context context,String message, String jenisToast, int duration, boolean denganIcon)
    {
        Toast toast = new Toast(context);

        View view = LayoutInflater.from(context)
                .inflate(R.layout.custom_toast_layout, null);

        TextView tvMessage = view.findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        ImageView img_icon_toast = view.findViewById(R.id.img_icon_toast);
        CardView cardview_toast = view.findViewById(R.id.cardview_toast);

        switch(jenisToast){
            case JENIS_TOAST_SUCCESS:
                cardview_toast.setCardBackgroundColor(context.getColor(R.color.color_toast_success));
                img_icon_toast.setImageDrawable(context.getDrawable(R.drawable.success_toast));
                break;
            case JENIS_TOAST_INFORMATION:
                cardview_toast.setCardBackgroundColor(context.getColor(R.color.color_toast_information));
                img_icon_toast.setImageDrawable(context.getDrawable(R.drawable.information_toast));
                break;
            case JENIS_TOAST_WARNING:
                cardview_toast.setCardBackgroundColor(context.getColor(R.color.color_toast_warning));
                img_icon_toast.setImageDrawable(context.getDrawable(R.drawable.warning_toast));
                break;
            case JENIS_TOAST_ERROR:
                cardview_toast.setCardBackgroundColor(context.getColor(R.color.color_toast_error));
                img_icon_toast.setImageDrawable(context.getDrawable(R.drawable.error_toast));
                break;
        }
        if(denganIcon)
        {
            img_icon_toast.setVisibility(View.VISIBLE);
        }
        else
        {
            img_icon_toast.setVisibility(View.GONE);
        }


        toast.setView(view);
        toast.setDuration(duration);
        toast.show();

    }


    public static void makeBlackCustomToast(Context context,String message, int duration)
    {
        Toast toast = new Toast(context);

        View view = LayoutInflater.from(context)
                .inflate(R.layout.black_toast_layout, null);

        TextView tvMessage = view.findViewById(R.id.tvMessage);
        tvMessage.setText(message);


        toast.setView(view);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.setDuration(duration);
        toast.show();

    }

}
