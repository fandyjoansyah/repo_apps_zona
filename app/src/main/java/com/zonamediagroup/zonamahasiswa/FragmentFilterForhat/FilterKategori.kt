package com.zonamediagroup.zonamahasiswa.FragmentFilterForhat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.zonamediagroup.zonamahasiswa.R

class FilterKategori : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.include_filter_forhat_kategori,container,false)
    }


}