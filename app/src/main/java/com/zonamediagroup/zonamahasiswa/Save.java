package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Westlist_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Westlist_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Save extends AppCompatActivity implements Westlist_Adapter.ContactsAdapterListener {

    RecyclerView receler_westlist;

    private List<Westlist_Model> tigaModels;
    private Westlist_Adapter tigaAdapter;

    private static final String TAG = Detail.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";
//    private String URL = Server.URL_REAL + "Get_save_like";
    private String URL = Server.URL_REAL + "Get_save_like_dengan_waktu";

    private String URL_hapus_save = Server.URL_REAL + "Delete_westlist";

    String id_user_sv;

    static final int LAUNCH_SECOND_ACTIVITY = 1;

    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    LinearLayout layar_artikel_kososng;

    // parameter retrun
    Boolean param_return = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_save);

        id_user_sv = SharedPrefManagerZona.getInstance(this).getid_user();


        receler_westlist = findViewById(R.id.receler_westlist);
        tigaModels = new ArrayList<>();

        tigaAdapter = new Westlist_Adapter(Save.this, tigaModels, this);

        RecyclerView.LayoutManager mLayoutManagerkategoriv = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        receler_westlist.setLayoutManager(mLayoutManagerkategoriv);
        receler_westlist.setItemAnimator(new DefaultItemAnimator());
        receler_westlist.setAdapter(tigaAdapter);

//        Artikel kosong
        layar_artikel_kososng = findViewById(R.id.layar_artikel_kososng);


//        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);

//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();

                get_weslis(id_user_sv);

            }
        });

        artikel_kosong_dismis();
        eror_dismiss();
        loading_show();

        get_weslis(id_user_sv);

    }

    private void get_weslis(final String id_user) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "TAG Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);


                    if (jObj.getString("status").equals("true")) {

                        JSONArray dataArray3 = jObj.getJSONArray("data");
                        System.out.println("Data Array2 " + dataArray3);


                        List<Westlist_Model> itemstiga = new Gson().fromJson(dataArray3.toString(), new TypeToken<List<Westlist_Model>>() {
                        }.getType());


                        tigaModels.clear();
                        // adding contacts to  list
                        tigaModels.addAll(itemstiga);
                        // refreshing recycler view
                        tigaAdapter.notifyDataSetChanged();

                        loading_dismiss();

                    } else {

                        artike_kosong_show();
                        tigaModels.clear();
                        tigaAdapter.notifyDataSetChanged();

                        loading_dismiss();
                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    eror_show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());

                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);


                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public void onContactSelected(Westlist_Model itemsatu) {

        Intent intent = new Intent(this, Detail.class);
        intent.putExtra("id_post", itemsatu.getID());
        intent.putExtra("post_content", itemsatu.getPost_content());
        intent.putExtra("humnile_post", itemsatu.getMeta_value());
        intent.putExtra("title_post", itemsatu.getPost_title());
        intent.putExtra("name_categori", itemsatu.getName());
        intent.putExtra("date_post", itemsatu.getPost_date());
        intent.putExtra("name_auth", itemsatu.getUser_login());
        intent.putExtra("img_author", itemsatu.getGambar_user_wp());
        intent.putExtra("link", itemsatu.getGuid());
        intent.putExtra("id_kategori", itemsatu.getTerm_id());
        intent.putExtra("save", "1"); //pada halaman save, semua recycler view pasti merupakan artikel yang di like. maka dari itu value pasti 1
        intent.putExtra("id_author", itemsatu.getPost_author());
        startActivityForResult(intent,LAUNCH_SECOND_ACTIVITY);
    }

    @Override
    public void Save(Westlist_Model save) {

        loading_show();

        String id_post_sv = save.getID();
        save_like(id_user_sv, id_post_sv);

    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    private void save_like(String id_user, String id_post) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_save, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data SAVE LIKE");


                    if (jObj.getString("status").equals("true")) {

                        // reload
                        param_return = true;
//Call API
                        get_weslis(id_user_sv);

                    } else if (jObj.getString("status").equals("false")) {


                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());



            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_post", id_post);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void artike_kosong_show() {
        layar_artikel_kososng.setVisibility(View.VISIBLE);
    }

    public void artikel_kosong_dismis() {
        layar_artikel_kososng.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");

        if (param_return) { //jika ada aksi hapus simpan
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else { //jika tidak ada aksi hapus simpan
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }



    }


    // result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                //apabia ada perubahan simpan dari detail.class, maka reload activity save ini
                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }

            if (resultCode == Activity.RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    } //onActivityResult

}