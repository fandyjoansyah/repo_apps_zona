package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis.ForhatTulisStep1;
import com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis.ForhatTulisStep2;
import com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis.ForhatTulisStep3;
import com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis.ForhatTulisStep4;
import com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis.FragmentTulisCommunicator;
import com.zonamediagroup.zonamahasiswa.models.forhat_tulis_models.ForhatTulisStep1Model;
import com.zonamediagroup.zonamahasiswa.models.forhat_tulis_models.ForhatTulisStep2Model;

import java.util.ArrayList;

public class ForhatTulisActivity extends AppCompatActivity implements FragmentTulisCommunicator {

    Animation fade_in;
    ForhatTulisStep1 fragmentStep1;
    ForhatTulisStep2 fragmentStep2;
    ForhatTulisStep3 fragmentStep3;
    ForhatTulisStep4 fragmentStep4;
    FragmentManager fragmentManager;
    boolean flagStep1Finished,flagStep2Finished,flagStep3Finished,flagStep4Finished = false;
    ImageView checkpoint_step_1,checkpoint_step_2,checkpoint_step_3,checkpoint_step_4;
    ProgressBar pb_1_to_2,pb_2_to_3,pb_3_to_4;
    Resources rs;
    TextView title_step,mini_title_step;
    int currentPage = 0;

    /* START BIKIN MODEL UNTUK MENYIMPAN TULISAN CURHATAN DARI SETIAP STEPNYA */
    ForhatTulisStep1Model modelStep1;
    ForhatTulisStep2Model modelStep2;
    /* END BIKIN MODEL UNTUK MENYIMPAN TULISAN CURHATAN DARI SETIAP STEPNYA */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_forhat_tulis_activity);
        initAllTulisCurhatanModel();
        initAnimation();
        initResource();
        initAllFragmentStep();
        initFragmentManager();
        findViewByIdAllComponent();
        //load fragment awal
        mekanismeLoadNextPage(1);
    }

    private void initAllTulisCurhatanModel(){
        modelStep1 = new ForhatTulisStep1Model();
        modelStep2 = new ForhatTulisStep2Model();
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(ForhatTulisActivity.this,R.anim.fade_in);
    }

    private void initResource(){
        rs = getResources();
    }

    private void findViewByIdAllComponent() {
        checkpoint_step_1 = findViewById(R.id.checkpoint_step_1);
        checkpoint_step_2 = findViewById(R.id.checkpoint_step_2);
        checkpoint_step_3 = findViewById(R.id.checkpoint_step_3);
        checkpoint_step_4 = findViewById(R.id.checkpoint_step_4);
        pb_1_to_2 = findViewById(R.id.pb_1_to_2);
        pb_2_to_3 = findViewById(R.id.pb_2_to_3);
        pb_3_to_4 = findViewById(R.id.pb_3_to_4);
        title_step = findViewById(R.id.title_step);
        mini_title_step = findViewById(R.id.mini_title_step);
    }

    private void setTextTitleStep(String text){
        title_step.startAnimation(fade_in);
        title_step.setText(text);
    }

    private void setTextMiniTitleStep(String text)
    {
        mini_title_step.startAnimation(fade_in);
        mini_title_step.setText(text);
    }

    private boolean isMiniTitleStepVisible(){
        if(mini_title_step.getVisibility() == View.VISIBLE)
            return true;
        else
            return false;
    }

    private void hideMiniTitle()
    {
        mini_title_step.setVisibility(View.GONE);
    }

    private void showMiniTitle()
    {
        mini_title_step.setVisibility(View.VISIBLE);
    }

    private void initFragmentManager(){
        fragmentManager = getSupportFragmentManager();
    }



    private void initAllFragmentStep() {
        fragmentStep1 = new ForhatTulisStep1();
        fragmentStep2 = new ForhatTulisStep2();
        fragmentStep3 = new ForhatTulisStep3();
        fragmentStep4 = new ForhatTulisStep4();
    }

    private void mekanismeSetTextJudulStep(){
        switch(currentPage)
        {
            case 1:
            setTextTitleStep(rs.getString(R.string.TITLE_STEP_1));
            if(isMiniTitleStepVisible())
                hideMiniTitle();
            break;
            case 2:
                setTextTitleStep(rs.getString(R.string.TITLE_STEP_2));
                if(isMiniTitleStepVisible() == false)
                    showMiniTitle();
                setTextMiniTitleStep(rs.getString(R.string.MINI_TITLE_STEP_2));
                break;
            case 3:
                setTextTitleStep(rs.getString(R.string.TITLE_STEP_3));
                if(isMiniTitleStepVisible())
                    hideMiniTitle();
                break;
            case 4:
                setTextTitleStep(rs.getString(R.string.TITLE_STEP_4));
                if(isMiniTitleStepVisible())
                    hideMiniTitle();
                break;
        }
    }

    public boolean loadPage(Fragment fragment, int newPage) {
        /*jika new page lebih besar dari currentpage, maka animasi perpindahan fragment adalah dari kiri ke kanan*/
        /*jika new page lebih kecil dari currentpage, maka animasi perpindahan fragment adalah dari kanan ke kiri*/
        int beginningAnimation = 0;
        int endingAnimation = 0;
        /*Jika halaman yang dibuka adalah halaman selanjutnya (newPage lebih besar dari currentPage)*/
        if(newPage > currentPage)
        {
            beginningAnimation = R.anim.activity_transition_slide_from_right;
            endingAnimation = R.anim.activity_transition_slide_to_left;
        }
        /*Jika halaman yang dibuka adalah halaman sebelumnya (newPage lebih besar dari currentPage)*/
        else if(newPage < currentPage)
        {
            beginningAnimation = R.anim.activity_transition_slide_from_left;
            endingAnimation = R.anim.activity_transition_slide_to_right;
        }
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(beginningAnimation,endingAnimation)
                    .replace(R.id.container_fragment_tulis, fragment)
                    .commit();
            currentPage = newPage;
            mekanismeSetTextJudulStep();
            return true;
        }
        return false;
    }

    @Override
    public void communicatorGoToNextStep(int stepTujuan) {
        mekanismeLoadNextPage(stepTujuan);
    }

    @Override
    public void communicatorGoBackToPreviousStep(int stepTujuan) {
        switch(stepTujuan) {
            case 1:
                loadPage(fragmentStep1,1);
                goToPreviousStep(1);
                break;
            case 2:
                loadPage(fragmentStep2,2);
                goToPreviousStep(2);
                break;
            case 3:
                loadPage(fragmentStep3,3);
                goToPreviousStep(3);
                break;
            case 4:
                loadPage(fragmentStep4,4);
                goToPreviousStep(4);
                break;
        }
    }

    @Override
    public void communicatorSetLastStepDone(int lastStepDone) {

    }

    @Override
    public void finishAllStep() {

    }

    @Override
    public void saveStep1Data(String judul, String deskripsi, int idRadioCoverChecked, Uri uploadCoverUri,String namaFilePilihanUser) {
        modelStep1.setJudul(judul);
        modelStep1.setDeskripsi(deskripsi);
        modelStep1.setIdRadioCoverChecked(idRadioCoverChecked);
        modelStep1.setUriCoverPilihanUser(uploadCoverUri);
        modelStep1.setNamaFilePilihanUser(namaFilePilihanUser);
    }

    @Override
    public void saveStep2Data(ArrayList<Uri> dataUri) {
        modelStep2.setDataUri(dataUri);
    }

    private void mekanismeLoadNextPage(int stepTujuan)
    {
        switch(stepTujuan) {
            case 1:
                loadPage(fragmentStep1,1);
                goToNextStep(1);
                break;
            case 2:
                loadPage(fragmentStep2,2);
                goToNextStep(2);
                break;
            case 3:
                loadPage(fragmentStep3,3);
                goToNextStep(3);
                break;
            case 4:
                loadPage(fragmentStep4,4);
                goToNextStep(4);
                break;
        }
    }


    public void goToNextStep(int lastStepDone) {
        switch (lastStepDone)
        {
            case 1:
                changeCircleIndikatorForhatImage(checkpoint_step_1,checkpoint_step_1.getContext().getDrawable(R.drawable.ic_checkpoint_1_finished),true);
                flagStep1Finished = true;
                break;
            case 2:
                fillProgressBar(pb_1_to_2);
                flagStep2Finished = true;
                break;
            case 3:
                fillProgressBar(pb_2_to_3);
                flagStep3Finished = true;
                break;
            case 4:
                fillProgressBar(pb_3_to_4);
                flagStep4Finished = true;
                break;
        }
    }

    public void goToPreviousStep(int previousStep) {
        switch (previousStep)
        {
            case 1:
                changeCircleIndikatorForhatImage(checkpoint_step_1,checkpoint_step_1.getContext().getDrawable(R.drawable.ic_checkpoint_1_finished),false);
                Log.d("30_juli_2022","in goToPreviousStep() before entering changeCircleIndikatorForhatImage()");
                changeCircleIndikatorForhatImage(checkpoint_step_2,checkpoint_step_2.getContext().getDrawable(R.drawable.ic_checkpoint_2),false);
                changeCircleIndikatorForhatImage(checkpoint_step_3,checkpoint_step_3.getContext().getDrawable(R.drawable.ic_checkpoint_3),false);
                changeCircleIndikatorForhatImage(checkpoint_step_4,checkpoint_step_4.getContext().getDrawable(R.drawable.ic_checkpoint_4),false);
                emptyProgressbar(pb_1_to_2);
                emptyProgressbar(pb_2_to_3);
                emptyProgressbar(pb_3_to_4);
                break;
            case 2:
                changeCircleIndikatorForhatImage(checkpoint_step_1,checkpoint_step_1.getContext().getDrawable(R.drawable.ic_checkpoint_1_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_2,checkpoint_step_2.getContext().getDrawable(R.drawable.ic_checkpoint_2_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_3,checkpoint_step_3.getContext().getDrawable(R.drawable.ic_checkpoint_3),false);
                changeCircleIndikatorForhatImage(checkpoint_step_4,checkpoint_step_4.getContext().getDrawable(R.drawable.ic_checkpoint_4),false);
                emptyProgressbar(pb_2_to_3);
                emptyProgressbar(pb_3_to_4);
                break;
            case 3:
                changeCircleIndikatorForhatImage(checkpoint_step_1,checkpoint_step_1.getContext().getDrawable(R.drawable.ic_checkpoint_1_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_2,checkpoint_step_2.getContext().getDrawable(R.drawable.ic_checkpoint_2_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_3,checkpoint_step_3.getContext().getDrawable(R.drawable.ic_checkpoint_3_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_4,checkpoint_step_4.getContext().getDrawable(R.drawable.ic_checkpoint_4),false);
                emptyProgressbar(pb_3_to_4);
                break;
            case 4:
                changeCircleIndikatorForhatImage(checkpoint_step_1,checkpoint_step_1.getContext().getDrawable(R.drawable.ic_checkpoint_1_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_2,checkpoint_step_2.getContext().getDrawable(R.drawable.ic_checkpoint_2_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_3,checkpoint_step_3.getContext().getDrawable(R.drawable.ic_checkpoint_3_finished),false);
                changeCircleIndikatorForhatImage(checkpoint_step_4,checkpoint_step_4.getContext().getDrawable(R.drawable.ic_checkpoint_4_finished),false);
                break;
        }
    }

    private void fillProgressBar(ProgressBar pb)
    {
        int TIME_ANIM = 500;
        ObjectAnimator.ofInt(pb,"progress",1000).setDuration(TIME_ANIM).start();

        int id = pb.getId();
        switch(id)
        {
            case R.id.pb_1_to_2:
                new Delay().delayTask(TIME_ANIM, new Runnable() {
                    @Override
                    public void run() {
                        changeCircleIndikatorForhatImage(checkpoint_step_2,checkpoint_step_2.getContext().getDrawable(R.drawable.ic_checkpoint_2_finished),true);
                    }
                });
                break;
            case R.id.pb_2_to_3:
                new Delay().delayTask(TIME_ANIM, new Runnable() {
                    @Override
                    public void run() {
                        changeCircleIndikatorForhatImage(checkpoint_step_3,checkpoint_step_3.getContext().getDrawable(R.drawable.ic_checkpoint_3_finished),true);
                    }
                });
                break;
            case R.id.pb_3_to_4:
                new Delay().delayTask(TIME_ANIM, new Runnable() {
                    @Override
                    public void run() {
                        changeCircleIndikatorForhatImage(checkpoint_step_4,checkpoint_step_4.getContext().getDrawable(R.drawable.ic_checkpoint_4_finished),true);
                    }
                });
                break;
        }
    }

    private void emptyProgressbar(ProgressBar pb){
        pb.setProgress(0);
    }

    private void changeCircleIndikatorForhatImage(ImageView imgIndikatorForhat, Drawable newImage, boolean useAnimation)
    {
        Log.d("30_juli_2022","inside changeCircleIndikatorForhatImage()");
        if(!imgIndikatorForhat.getDrawable().getConstantState().equals(newImage.getConstantState()))
        {
            if(useAnimation) {
                Log.d("30_juli_2022","inside using animation changeCircleIndikatorForhatImage()");
                //start anim
                imgIndikatorForhat.animate()
                        .alpha(0.9f)
                        .setDuration(150)
                        .scaleX(0.7f)
                        .scaleY(0.7f)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                imgIndikatorForhat.setImageDrawable(newImage);
                                imgIndikatorForhat.animate().alpha(1).setDuration(150).scaleX(1).scaleY(1);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {
                            }
                        });
                //end anim
            }
            else
            {
                //start anim
                imgIndikatorForhat.animate()
                        .alpha(0.9f)
                        .setDuration(1)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                imgIndikatorForhat.setImageDrawable(newImage);
                                imgIndikatorForhat.animate().alpha(1).setDuration(1);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {
                            }
                        });
                //end anim
            }
        }
    }

   /* START METHOD RETREIVE DATA STEP 1 */
    public String retreiveJudul(){
        return modelStep1.getJudul();
    }
    public String retreiveDeskripsi(){
        return modelStep1.getDeskripsi();
    }
    public int retreiveIdRadioCoverChecked(){
        return modelStep1.getIdRadioCoverChecked();
    }
    public Uri retreiveUriCoverPilihanUser(){
        return modelStep1.getUriCoverPilihanUser();
    }

    public String retreiveNamaFilePilihanUser(){
        return modelStep1.getNamaFilePilihanUser();
    }
    /* END METHOD RETREIVE DATA STEP 1 */

    /* START METHOD RETREIVE DATA STEP 2 */
    public ArrayList<Uri> retreiveDataUri(){
        return modelStep2.getDataUri();
    }
    /* END METHOD RETREIVE DATA STEP 2 */




}