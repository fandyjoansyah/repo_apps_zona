package com.zonamediagroup.zonamahasiswa;

import java.util.HashMap;

public class MenuFilterForhatManager {

    /*start jenis2 page*/
    public static final String HOME = "home";
    public static final String TERPOPULER = "terpopuler";
    public static final String FILTER = "filter";
    public static final String KATEGORI = "kategori";
    /*end jenis2 page*/

    /*start jenis2 menu filter*/
    public static final String MENU_URUTAN = "urutan";
    public static final String MENU_JENIS = "jenis";
    public static final String MENU_POSTINGAN = "postingan";
    public static final String MENU_KATEGORI = "kategori";
    /*end jenis2 menu filter*/

    /*start jenis2 subFilter*/
    public static final String SUBFILTER_CURHATAN = "curhatan";
    public static final String SUBFILTER_TANGGAPAN = "tanggapan";
    public static final String SUBFILTER_SEMUA = "semua";
    public static final String SUBFILTER_TERBARU = "terbaru";
    public static final String SUBFILTER_TERPOPULER = "terpopuler";



    /*end jenis2 subFilter*/

    public static HashMap<String,String> mapFilterTerpilih;



    /*method getMenuFilterForhatAvailableByPage() merupakan method untuk mendapatkan menu filter apa yang boleh ditampilkan di halaman pencarian berdasarkan page yang mengaksesnya*/
    public static String[] getMenuFilterForhatAvailableByPage(String page){
        String[] result;
        switch(page)
        {
            case HOME:
            case FILTER:
                result = new String[]{MENU_URUTAN,MENU_JENIS,MENU_POSTINGAN,MENU_KATEGORI};
                break;
            case TERPOPULER:
                result = new String[]{MENU_JENIS,MENU_POSTINGAN,MENU_KATEGORI};
            break;

            case KATEGORI:
                result = new String[]{MENU_URUTAN,MENU_JENIS,MENU_POSTINGAN};
            break;
            default:
                result = null;
        }
        return result;
    }

    public static void initMapFilterTerpilih(){
        mapFilterTerpilih = new HashMap<>();
    }

    public static void nullifiedMapFilterTerpilih(){
        mapFilterTerpilih = null;
    }

    public static boolean isMapFilterNull(){
        if(mapFilterTerpilih == null)
        {
            return true;
        }
        else return false;
    }




}
