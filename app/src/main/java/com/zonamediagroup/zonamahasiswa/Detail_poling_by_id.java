package com.zonamediagroup.zonamahasiswa;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Detail_poling_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Detail_poling_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Detail_poling_by_id extends AppCompatActivity implements Detail_poling_Adapter.ContactsAdapterListener {


    private static final String TAG = Home.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";
    private String URL_post_view = Server.URL_REAL + "Post_view";

    RecyclerView receler_home;
    private List<Detail_poling_Model> receler_homeModels;
    private Detail_poling_Adapter receler_homeAdapter;
    private String URL_home = Server.URL_REAL + "Get_detail_poling";

    private String URL_save_poling = Server.URL_REAL + "Save_poling";

    String id_poling;

    CardView kirim_vote;

    EditText alasan_poling;

    String select_id_poling = "", select_id_detail_poling = "";

    String id_user_sv;

    String alasan;


    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok;

    boolean scrool = false;

    ImageView back;

    LinearLayout ly_notif_vote;

    //menampilkan pilihan user
    TextView txt_pilihan_user;

    WebView webview;



    TextView nama_kategori_polling;

    TextView keterangan_mengenai_polling;

    LinearLayout layout_keterangan_pilihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_detail_poling);
        Log.d("DebugZona","onCreate detail polling by id");
        id_user_sv = SharedPrefManagerZona.getInstance(Detail_poling_by_id.this).getid_user();

        ly_notif_vote = findViewById(R.id.ly_notif_vote);
        System.out.println("ID USER " + id_user_sv);
        terimadata();

        // rekomendasi
        receler_home = findViewById(R.id.receler_detail_poling);
        receler_homeModels = new ArrayList<>();
        receler_homeAdapter = new Detail_poling_Adapter(Detail_poling_by_id.this, receler_homeModels, this);
        RecyclerView.LayoutManager mLayoutManagerkategoriv = new GridLayoutManager(this, 2);
        receler_home.setLayoutManager(mLayoutManagerkategoriv);
        receler_home.setItemAnimator(new DefaultItemAnimator());
        receler_home.setAdapter(receler_homeAdapter);

        //layout keterangan sudah memilih siapa
        layout_keterangan_pilihan = findViewById(R.id.layout_keterangan_pilihan);

        //label keterangan polling
        keterangan_mengenai_polling = findViewById(R.id.keterangan_mengenai_polling);

        //menampilkan siapa yang dipilih pengguna
        txt_pilihan_user = findViewById(R.id.txt_pilihan_user);

        alasan_poling = findViewById(R.id.alasan_poling);


        //kirim vote
        kirim_vote = findViewById(R.id.kirim_vote);
        kirim_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                alasan = alasan_poling.getText().toString();

                if (select_id_detail_poling.trim().equals("")) {
                    Toast.makeText(Detail_poling_by_id.this, "Anda Belum Memilih", Toast.LENGTH_SHORT).show();
                } else if (alasan.trim().equals("")) {
                    Toast.makeText(Detail_poling_by_id.this, "Alasan Kosong", Toast.LENGTH_SHORT).show();
                } else {
                    loading_show();

                    save_poling(select_id_poling, select_id_detail_poling, id_user_sv, alasan);
                }
            }
        });


//        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);

        //webview grafik
        webview = findViewById(R.id.webview_chart_include_detail_poling);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                loading_dismiss();
            }
        });

//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();

                if (eror_param == 1) {
//                    Call API
                    get_detail_poling(id_poling, id_user_sv);
                } else if (eror_param == 2) {
//                    Call API
                    save_poling(select_id_poling, select_id_detail_poling, id_user_sv, alasan);
                }


            }
        });

        search = findViewById(R.id.search);
        saveok = findViewById(R.id.saveok);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Detail_poling_by_id.this, Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Detail_poling_by_id.this, Search.class);
                startActivity(intent);
            }
        });

        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                finish();

            }
        });

        eror_dismiss();
        loading_show();

// Call API
        get_detail_poling(id_poling, id_user_sv);
    }

    public void terimadata() {
        Bundle data = getIntent().getExtras();
        id_poling = data.getString("id_poling");

    }

    private void get_detail_poling(String id_poling, String id_user) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data POLING " + response.toString());

                    if (jObj.getString("status").equals("true")) {

                        // status
                        String status_user_poling = jObj.getString("status_user");

                        //label
                        String label = jObj.getString("label");
                        //jika label tidak kosong, maka rubah teks nya, jika kosong maka pakai text bawaan xml
                        if(!label.equals(""))
                        {
                            keterangan_mengenai_polling.setText("Yuk pilih "+label+" favoritmu sekarang!");
                        }
//                        else{
//                            keterangan_mengenai_polling.setText("else");
//
//                        }
//
//                        keterangan_mengenai_polling.setText(tiitle_poling);




                        if (status_user_poling.equals("1")) {
                            // user sudah poling
                            ly_notif_vote.setVisibility(View.VISIBLE);
                            receler_home.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    return true;
                                }
                            });
                            kirim_vote.setVisibility(View.GONE);
                            keterangan_mengenai_polling.setVisibility(View.GONE);
                            //tampilkan layout sudah memilih siapa
                            layout_keterangan_pilihan.setVisibility(View.VISIBLE);
                        } else {
                            // user belum poling
                        }

                        JSONArray dataArray = jObj.getJSONArray("data");

                        Log.d("DebugPolling","dataArray "+dataArray.getJSONObject(1).getString("nama_tokoh")+" ukuran "+dataArray.length());
                        ArrayList<Float> dataUntukChart = new ArrayList<>();
                        ArrayList<String> labelUntukChart = new ArrayList<>();
                        for(int i=0;i<dataArray.length();i++)
                        {
                            dataUntukChart.add(Float.parseFloat(dataArray.getJSONObject(i).getString("total_vote")));
                            labelUntukChart.add(dataArray.getJSONObject(i).getString("nama_tokoh"));
                        }


                        //funcInisialisasiChart(dataUntukChart,labelUntukChart);

                        List<Detail_poling_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Detail_poling_Model>>() {
                        }.getType());


                        // adding contacts to contacts list
                        receler_homeModels.clear();
                        receler_homeModels.addAll(items);

                        //idPolling untuk loading google chart
                        String idPolling = items.get(0).getId_poling();

                        //lakukan cek apakah total vote dari semua kategori adalah 0 semua atau sudah pernah dilakukan aksi vote
                        boolean nolSemua = true;
                        for(int i=0;i<receler_homeModels.size();i++){
                            Log.d("23september","getTotal_vote : "+receler_homeModels.get(i).getNama_tokoh());
                            if(Integer.valueOf(receler_homeModels.get(i).getTotal_vote()) > 0)
                            {
                                nolSemua = false;
                            }
                        }

                        //Load google chart by idPolling
                        if(nolSemua == true)
                        {
                            webview.loadUrl("http://api.zonamahasiswa.id/api/grafik_polling_data_nol/show/"+idPolling+"/");
                        }
                        else
                        {
                            webview.loadUrl("http://api.zonamahasiswa.id/api/grafik_polling/show/"+idPolling+"/");
                        }

                        //cari siapa yang dipilih pengguna
                        for(int i=0;i<receler_homeModels.size();i++)
                        {
                            if(receler_homeModels.get(i).getSelected() == true)
                            {
                                txt_pilihan_user.setText(receler_homeModels.get(i).getNama_tokoh());
                                alasan_poling.setText(receler_homeModels.get(i).getAlasan());
                                alasan_poling.setEnabled(false);
                            }
                        }


                        // refreshing recycler view
                        receler_homeAdapter.notifyDataSetChanged();



                        //loading_dismiss();


                    } else if (jObj.getString("status").equals("false")) {

                        loading_dismiss();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_param = 1;
                    eror_show();
                    Log.d("DebugPolling","error karena "+e.getMessage());
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                eror_param = 1;
                eror_show();

            }
        }) {


//            Parameter

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_poling", id_poling);
                params.put("id_user", id_user);
                Log.d("DebugPolling","id polling = "+id_poling);
                Log.d("DebugPolling","id user = "+id_user);
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public void onContactSelected(Detail_poling_Model poling) {

        select_id_poling = poling.getId_poling();
        select_id_detail_poling = poling.getId_detail_poling();

    }

    private void save_poling(String id_poling, String id_detail_poling, String id_user, String alasan) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_save_poling, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data REKOMENDASI");


                    if (jObj.getString("status").equals("true")) {


                        finish();

                    } else if (jObj.getString("status").equals("false")) {
                        finish();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error

                    eror_param = 2;
                    eror_show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());

                eror_param = 2;
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_poling", id_poling);
                params.put("id_detail_poling", id_detail_poling);
                params.put("id_user", id_user);
                params.put("alasan", alasan);

                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                eror_param = 2;
                eror_show();

                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }
}