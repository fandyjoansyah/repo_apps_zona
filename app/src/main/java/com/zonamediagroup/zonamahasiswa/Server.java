package com.zonamediagroup.zonamahasiswa;

/**
 * Created by Global Store on 10/10/2017.
 */

public class Server {

//    private static final  String URL_UTAMA = "http://api.zonamahasiswa.net/";
    private static final String URL_UTAMA = "http://api.zonamahasiswa.id/";

//    public static final String URL_REAL = URL_UTAMA+"api-v2/";

    public static final String URL_REAL = URL_UTAMA+"api-v3/";


    public static final String URL_REAL_VIDEO = URL_UTAMA+"api-video/";

    public static final String URL_REAL_NATIVE = URL_UTAMA+"api-v3/";


    //URL SERVER UNTUK MENDAPATKAN NAMA AUTHOR
    public static final String URL_WORDPRESS_NAMA_AUTHOR = "https://zonamahasiswa.id/wp-json/wp/v2/users/";

    //GET Tooltips Address
   // public static final String URL_GET_TOOLTIPS = "http://api.zonamahasiswa.net/api-v2/tooltips/index_post/";

    public static final String URL_HALAMAN_UTAMA_VIDEO = URL_REAL_VIDEO+"video/index_post";


    //start url untuk mekanisme daftar/masuk yang baru (mei 2022)
    public static final String URL_DAFTAR_MASUK = URL_UTAMA+"api-daftar-masuk/";
    public static final String URL_PROFIL_PENGGUNA = URL_UTAMA+"api-profil-pengguna/";
    //end url untuk mekanisme daftar masuk yang baru (mei 2022)
}
