package com.zonamediagroup.zonamahasiswa;

public class ServerForhat {

    public static final String URL_DOMAIN_UTAMA = "https://zonamahasiswa.net/";

    public static final String URL_API_FORHAT = URL_DOMAIN_UTAMA+"api/";

    public static final String URL_FORHAT_LIST = "forhat_list";

    public static final String URL_FORHAT_LIST_FINAL = URL_API_FORHAT+URL_FORHAT_LIST;

    public static final String URL_IMG_LOCATION = URL_DOMAIN_UTAMA+"assets/img/";

    public static final String URL_IMG_FOTO_PROFIL_LOCATION_PATEN = "https://zonamahasiswa.id/assets/img/";

    public static final String URL_POST_LIKE = URL_API_FORHAT+"like";

    public static final String URL_FORHAT_BACA = URL_DOMAIN_UTAMA+"baca/";


}
