package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

public class Tentang_zona extends AppCompatActivity {

    ImageView back;
    WebView p4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_tentang_zona);

        p4 = findViewById(R.id.p4);

        String x =
                "<p style=\"text-align: justify;\">Zona Mahasiswa adalah media online mahasiswa terbesar di Indonesia dengan tagline \"Tempat Nongkrongnya Mahasiswa\" yang memuat konten berita, tips, fakta, opini, politik dan kategori menarik lainnya yang bertujuan untuk memberikan informasi terupdate dan terlengkap kepada calon mahasiswa, mahasiswa serta masyarakat umum lainnya.<br /><br />Zona Mahasiswa didirikan pada bulan Agustus 2017 di kota Malang yang bermula dari akun Instagram hingga akhirnya bertransformasi menjadi media online mahasiswa dengan didirikannya website pada tahun 2020 di bawah badan hukum PT. Zona Media Indonesia. Dengan terus berkembangnya zaman dan teknologi, Zona Mahasiswa mengambil langkah pasti dalam menciptakan aplikasi pada bulan Agustus 2021 dan menjadikannya media online mahasiswa pertama yang memiliki aplikasi dalam bentuk tujuan memudahkan pengguna (Sobat Zona) untuk mengakses segala jenis informasi seputar mahasiswa.<br /><br />Zona Mahasiswa berkomitmen untuk terus memberikan informasi yang bermanfaat, terupdate, dan akurat kepada mahasiswa serta masyarakat umum lainnya. Zona Mahasiswa berkomitmen juga dalam pengembangan layanan dan fitur website serta aplikasi untuk menunjang kenyamanan Sobat Zona dalam mengakses segala jenis informasi dan fitur menarik lainnya di Zona Mahasiswa.<br /><br />Zona Mahasiswa<br />\"Tempat Nongkrongnya Mahasiswa\"</p>";
        p4.loadData(x, "text/html; charset=utf-8", "UTF-8");

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}