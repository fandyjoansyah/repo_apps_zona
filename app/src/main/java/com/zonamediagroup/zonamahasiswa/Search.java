package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Recyclerview_home_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Recyclerview_home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Search extends AppCompatActivity implements Recyclerview_home_Adapter.ContactsAdapterListener {

  SearchView input_pencarian;

  RecyclerView receler_home;
  private List<Recyclerview_home> receler_homeModels;
  private Recyclerview_home_Adapter receler_homeAdapter;

  NestedScrollView nestedscrol;
  int page = 1;
  String kataKunciYangSedangAktifDicari="";

  private static final String TAG = Home.class.getSimpleName();
  static String tag_json_obj = "json_obj_req";
  //start comment 25 april
//  private String URL_home = Server.URL_REAL + "Home_fix_dengan_waktu";
//end comment 25 april
  private String URL_home = Server.URL_REAL + "Home_fix_dengan_waktu_rnd_native";

  private String URL_hapus_save = Server.URL_REAL + "Delete_westlist";
  private String URL_search = Server.URL_REAL + "Search_fix";

  //start parameter untuk klik search
  private String halamanAktif = "";
  private String idUser = "";
  private String keyUntukCari = "";
  //end parameter untuk klik search

  private String URL_home_save = Server.URL_REAL + "Save_like";

  //    loading bar
  ProgressBar progressBar;
  Sprite doubleBounce;
  LinearLayout layoutloading_bar;

  static final int LAUNCH_SECOND_ACTIVITY = 1;


  // id_user from data sesion
  String id_user_sv;


  // loading
  ImageView image_animasi_loading;
  LinearLayout loding_layar;

  // eror layar
  LinearLayout eror_layar;

  //hasil pencarian notfound
  LinearLayout notfound_layar;
  TextView coba_lagi;
  ImageView iconCari;
  int eror_param = 0; // eror_param =1 => get_homedua | eror_param = 2 => get_key

  String key_search;

  // parameter retrun
  Boolean param_return = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.page_search);

    id_user_sv = SharedPrefManagerZona.getInstance(this).getid_user();

    //        Nested
    nestedscrol = findViewById(R.id.nestedscrol);

    //        Loading bar
    progressBar = (ProgressBar) findViewById(R.id.spin_kit_loading_search);
    doubleBounce = new ThreeBounce();
    progressBar.setIndeterminateDrawable(doubleBounce);


    layoutloading_bar = findViewById(R.id.layoutloading_bar);


    input_pencarian = findViewById(R.id.input_pencarian);

    iconCari = findViewById(R.id.icon_cari);

    input_pencarian.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        // filter recycler view when query submitted
        System.out.println("QUERY " + query);
        // post ke server
        key_search = query;
        dismissSemua();
        loading_show();

        receler_homeModels.clear();
        Log.d("ZonaSearch","page "+page);
        Log.d("ZonaSearch","id_user_sv "+id_user_sv);
        Log.d("ZonaSearch","key_search "+key_search);

        getkey(String.valueOf(page), id_user_sv, key_search);
//                jumlah_row();
        return false;
      }

      @Override
      public boolean onQueryTextChange(String query) {
        // filter recycler view when text is changed
        System.out.println("QUERY DUA " + query);

        return false;
      }


    });

    nestedscrol.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
      @Override
      public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        // on scroll change we are checking when users scroll as bottom.
        if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
          // in this method we are incrementing page number,
          // making progress bar visible and calling get data method.

//                    loading_show();
//          start komen 27 april
//          loading_bar_show();
//          end komen 27 april
//                    Toast.makeText(mActivity, "TOAS DATA ", Toast.LENGTH_SHORT).show();
//          start komen 27 april
          //page++;
//          end komen 27 april


          //lakukan cek apakah pengguna load more nya ketika sedang di halaman search dan masih belum ngetikan pencarian, atau load morenya itu dalam kondisi sudah mengetikan pencarian
          //pengguna masih belum aktif melakukan pencarian  (pengguna TIDAK memiliki kata kunci pencarian yang aktif )
          if(kataKunciYangSedangAktifDicari.equals(""))
          {
//            start komen 27 april
//            get_homedua(String.valueOf(page), id_user_sv);
//            end komen 27 april
          }
          //pengguna memiliki kata kunci pencarian yang aktif
          else
          {
//            start komen 27 april
            getkey(String.valueOf(page),id_user_sv,kataKunciYangSedangAktifDicari);
//            end komen 27 april
          }

        }
      }
    });


    receler_home = findViewById(R.id.receler_home_search);
    receler_homeModels = new ArrayList<>();

    receler_homeAdapter = new Recyclerview_home_Adapter(Search.this, receler_homeModels, this);

    RecyclerView.LayoutManager mLayoutManagerkategoriv = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
    receler_home.setLayoutManager(mLayoutManagerkategoriv);
    receler_home.setItemAnimator(new DefaultItemAnimator());
    receler_home.setAdapter(receler_homeAdapter);


//        Loading
    loding_layar = findViewById(R.id.loding_layar);
    image_animasi_loading = findViewById(R.id.image_animasi_loading);

//        eror
    eror_layar = findViewById(R.id.eror_layar);
    coba_lagi = findViewById(R.id.coba_lagi);

// pencarian notfound
    notfound_layar = findViewById(R.id.notfound_layar);

    coba_lagi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        eror_dismiss();
        loading_show();

        if (eror_param == 1) {
//                    Get Home
          get_homedua(String.valueOf(page), id_user_sv);

        } else if (eror_param == 2) {
//                    Get Key
          getkey(String.valueOf(page), id_user_sv, key_search);
        }


      }
    });

    eror_dismiss();
    loading_show();

//        Call API
    get_homedua(String.valueOf(page), id_user_sv);


    iconCari.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        dismissSemua();
        loading_show();
        receler_homeModels.clear();
        InputMethodManager imm = (InputMethodManager) getSystemService(Search.this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input_pencarian.getWindowToken(), 0);
        getkey(String.valueOf(page),id_user_sv,input_pencarian.getQuery().toString());
      }
    });

  }

  private void dismissSemua() {
    eror_dismiss();
    loading_dismiss();
    not_found_dismiss();
  }


  public void loading_bar_show() {
    layoutloading_bar.setVisibility(View.VISIBLE);
  }

  public void loading_bar_dismiss() {
    layoutloading_bar.setVisibility(View.INVISIBLE);
  }


  private void get_homedua(String halamanaktif, String id_user) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {



        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data Global");


          if (jObj.getString("status").equals("true")) {

            JSONArray dataArray = jObj.getJSONArray("item_terbaru");


            List<Recyclerview_home> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Recyclerview_home>>() {
            }.getType());


            // adding contacts to  list
            receler_homeModels.addAll(items);
            // refreshing recycler view
            receler_homeAdapter.notifyDataSetChanged();

            loading_dismiss();


          } else if (jObj.getString("status").equals("false")) {

            loading_dismiss();
          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error
          eror_param = 1;
          eror_show();

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());
        eror_param = 1;
        eror_show();
//                dismissdialog();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("halamanaktif", halamanaktif);
        params.put("id_user", id_user);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {


        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  @Override
  public void onContactSelected(Recyclerview_home itemsatu, int position) {

    Intent intent = new Intent(this, Detail.class);
    intent.putExtra("id_post", itemsatu.getID());
    intent.putExtra("post_content", itemsatu.getPost_content());
    intent.putExtra("humnile_post", itemsatu.getMeta_value());
    intent.putExtra("title_post", itemsatu.getPost_title());
    intent.putExtra("name_categori", itemsatu.getName());
    intent.putExtra("date_post", itemsatu.getPost_date());
    intent.putExtra("name_auth", itemsatu.getUser_login());
    intent.putExtra("img_author", itemsatu.getGambar_user_wp());
    intent.putExtra("save", itemsatu.getLike());
    intent.putExtra("link", itemsatu.getGuid());
    intent.putExtra("id_kategori", itemsatu.getTerm_id());
    intent.putExtra("id_author", itemsatu.getPost_author());
    startActivityForResult(intent,LAUNCH_SECOND_ACTIVITY);

  }

  @Override
  public void Save(Recyclerview_home save, int position) {

    String id_post_sv = save.getID();

    save_like(id_user_sv, id_post_sv);
    save.setLike("1");

  }

  @Override
  public void unSave(Recyclerview_home model, int position) {
    String id_post_sv = model.getID();

    delete_simpan(id_user_sv, id_post_sv);
    model.setLike("0");
  }

  private void delete_simpan(String id_user, String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data SAVE LIKE");


          if (jObj.getString("status").equals("true")) {


          } else if (jObj.getString("status").equals("false")) {


          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());



      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }



  private void save_like(String id_user, String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {



        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data SAVE LIKE");


          if (jObj.getString("status").equals("true")) {

            // parameter retrun
            param_return = true;

          } else if (jObj.getString("status").equals("false")) {


          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());


//                dismissdialog();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }



  private void getkey(String halamanaktif, String id_user, String key) {
    kataKunciYangSedangAktifDicari = key;
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_search, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          if (jObj.getString("status").equals("true")) {

            JSONArray dataArray = jObj.getJSONArray("item_terbaru");

            System.out.println("DATA JSON " + dataArray);

            List<Recyclerview_home> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Recyclerview_home>>() {
            }.getType());


            // adding contacts to  list
            if(page == 1) {
              receler_homeModels.clear();
            }
            receler_homeModels.addAll(items);
            // refreshing recycler view
            receler_homeAdapter.notifyDataSetChanged();

            loading_dismiss();


          } else if (jObj.getString("status").equals("false")) {
            loading_dismiss();
            Log.d("nfsx","nfs_1");
            not_found_show();
          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error
          Log.d("Search30okt","EP2-1");
          eror_param = 2;
          //not_found_show();
          Log.d("nfsx","nfs_2 "+e.getMessage());
          not_found_show();
          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {

        Log.e(TAG, "Register Error: " + error.getMessage());
        Log.d("Search30okt","EP2-2");
        eror_param = 2;
        eror_show();
//                dismissdialog();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("halamanaktif", halamanaktif);
        params.put("id_user", id_user);
        params.put("key", key);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {
        Log.d("Search30okt","EP2-3");
        eror_param = 2;
        eror_show();
        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }


  // loading dan eror dan pencarian notfound
  public void loading_show() {
    loding_layar.setVisibility(View.VISIBLE);
  }

  public void loading_dismiss() {

    loding_layar.setVisibility(View.GONE);
  }

  public void eror_show() {
    eror_layar.setVisibility(View.VISIBLE);
  }

  public void eror_dismiss() {

    eror_layar.setVisibility(View.GONE);
  }

  //pencarian gagal
  public void not_found_show() {
    notfound_layar.setVisibility(View.VISIBLE);
  }

  public void not_found_dismiss() {

    notfound_layar.setVisibility(View.GONE);
  }

  @Override
  public void onBackPressed() {
    Log.d("CDA", "onBackPressed Called");

    if (param_return) {
      Intent returnIntent = new Intent();
      setResult(Activity.RESULT_OK, returnIntent);
      finish();
    } else {
      Intent returnIntent = new Intent();
      setResult(Activity.RESULT_CANCELED, returnIntent);
      finish();
    }
  }

  // result
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == LAUNCH_SECOND_ACTIVITY) {
      if (resultCode == Activity.RESULT_OK) {
        //apabia ada perubahan simpan dari detail.class, maka reload activity ini
        Intent intent = getIntent();
        finish();
        startActivity(intent);
      }

      if (resultCode == Activity.RESULT_CANCELED) {
        // Write your code if there's no result
      }
    }
  } //onActivityResult
}