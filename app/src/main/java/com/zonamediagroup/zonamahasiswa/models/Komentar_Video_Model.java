package com.zonamediagroup.zonamahasiswa.models;

public class Komentar_Video_Model {
    String video_comment_id;
    String video_comment;
    String video_id;
    String video_comment_date_created;
    String video_comment_like_id;

    public String getVideo_comment_like_id() {
        return video_comment_like_id;
    }

    public void setVideo_comment_like_id(String video_comment_like_id) {
        this.video_comment_like_id = video_comment_like_id;
    }

    public String getVideo_comment_like() {
        return video_comment_like;
    }

    public void setVideo_comment_like(String video_comment_like) {
        this.video_comment_like = video_comment_like;
    }

    public String getVideo_comment_reply_id() {
        return video_comment_reply_id;
    }

    public void setVideo_comment_reply_id(String video_comment_reply_id) {
        this.video_comment_reply_id = video_comment_reply_id;
    }

    public String getVideo_comment_status() {
        return video_comment_status;
    }

    public void setVideo_comment_status(String video_comment_status) {
        this.video_comment_status = video_comment_status;
    }

    public String getUser_id_reply() {
        return user_id_reply;
    }

    public void setUser_id_reply(String user_id_reply) {
        this.user_id_reply = user_id_reply;
    }

    String user_id;
    String id_user;
    String person_id;
    String person_name;
    String person_given_name;
    String person_family_name;
    String person_photo;
    String video_comment_like;
    String video_comment_reply_id;
    String video_comment_status;
    String user_id_reply;
    String convert_time;

    public String getConvert_time() {
        return convert_time;
    }

    public void setConvert_time(String convert_time) {
        this.convert_time = convert_time;
    }

    public String getVideo_comment_id() {
        return video_comment_id;
    }

    public void setVideo_comment_id(String video_comment_id) {
        this.video_comment_id = video_comment_id;
    }

    public String getVideo_comment() {
        return video_comment;
    }

    public void setVideo_comment(String video_comment) {
        this.video_comment = video_comment;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_comment_date_created() {
        return video_comment_date_created;
    }

    public void setVideo_comment_date_created(String video_comment_date_created) {
        this.video_comment_date_created = video_comment_date_created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getPerson_given_name() {
        return person_given_name;
    }

    public void setPerson_given_name(String person_given_name) {
        this.person_given_name = person_given_name;
    }

    public String getPerson_family_name() {
        return person_family_name;
    }

    public void setPerson_family_name(String person_family_name) {
        this.person_family_name = person_family_name;
    }

    public String getPerson_photo() {
        return person_photo;
    }

    public void setPerson_photo(String person_photo) {
        this.person_photo = person_photo;
    }


}
