package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

public class NotifUpdateModel {
    @SerializedName("status_popup_update")
    private int statusUpdate;

    public int getStatusUpdate() {
        return statusUpdate;
    }

    public void setStatusUpdate(int statusUpdate) {
        this.statusUpdate = statusUpdate;
    }
}
