package com.zonamediagroup.zonamahasiswa.models;

public class Video_Notif_Model {
    String video_notification_id;
    String video_id;
    String person_id;
    String from_person_id;
    String video_notification_title;
    String video_notification_description;
    String video_notification_image;
    String video_notification_type;
    String video_notification_date;
    String id_user;
    String person_name;
    String person_given_name;
    String person_family_name;
    String person_email;
    String person_photo;
    String person_token;
    String video_title;
    String video_deskripsi;
    String video_url;
    String video_thumbnail;
    String convert_time;
    String comment_id;
    String youtube_video_id;
    String video_share;
    String video_like;
    String video_save;
    String video_view;
    String video_duration;
    String video_play_total;
    String video_comment;
    String video_date_created;
    String user_id;
    String video_publish_status;
    String video_share_url;
    String comment_reply_id;

    public String getComment_reply_id() {
        return comment_reply_id;
    }

    public void setComment_reply_id(String comment_reply_id) {
        this.comment_reply_id = comment_reply_id;
    }


    public String getVideo_play_total() {
        return video_play_total;
    }

    public void setVideo_play_total(String video_play_total) {
        this.video_play_total = video_play_total;
    }

    public String getVideo_comment() {
        return video_comment;
    }

    public void setVideo_comment(String video_comment) {
        this.video_comment = video_comment;
    }

    public String getVideo_date_created() {
        return video_date_created;
    }

    public void setVideo_date_created(String video_date_created) {
        this.video_date_created = video_date_created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVideo_publish_status() {
        return video_publish_status;
    }

    public void setVideo_publish_status(String video_publish_status) {
        this.video_publish_status = video_publish_status;
    }

    public String getVideo_share_url() {
        return video_share_url;
    }

    public void setVideo_share_url(String video_share_url) {
        this.video_share_url = video_share_url;
    }

    public String getVideo_rate() {
        return video_rate;
    }

    public void setVideo_rate(String video_rate) {
        this.video_rate = video_rate;
    }

    String video_rate;


    public String getVideo_duration() {
        return video_duration;
    }

    public void setVideo_duration(String video_duration) {
        this.video_duration = video_duration;
    }

    public String getVideo_view() {
        return video_view;
    }

    public void setVideo_view(String video_view) {
        this.video_view = video_view;
    }

    public String getVideo_save() {
        return video_save;
    }

    public void setVideo_save(String video_save) {
        this.video_save = video_save;
    }

    public String getVideo_like() {
        return video_like;
    }

    public void setVideo_like(String video_like) {
        this.video_like = video_like;
    }

    public String getVideo_share() {
        return video_share;
    }

    public void setVideo_share(String video_share) {
        this.video_share = video_share;
    }

    public String getYoutube_video_id() {
        return youtube_video_id;
    }

    public void setYoutube_video_id(String youtube_video_id) {
        this.youtube_video_id = youtube_video_id;
    }

    public String getVideo_category_sub_id() {
        return video_category_sub_id;
    }

    public void setVideo_category_sub_id(String video_category_sub_id) {
        this.video_category_sub_id = video_category_sub_id;
    }

    String video_category_sub_id;

    public String getVideo_category_id() {
        return video_category_id;
    }

    public void setVideo_category_id(String video_category_id) {
        this.video_category_id = video_category_id;
    }

    String video_category_id;

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getVideo_notification_id() {
        return video_notification_id;
    }

    public void setVideo_notification_id(String video_notification_id) {
        this.video_notification_id = video_notification_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getFrom_person_id() {
        return from_person_id;
    }

    public void setFrom_person_id(String from_person_id) {
        this.from_person_id = from_person_id;
    }

    public String getVideo_notification_title() {
        return video_notification_title;
    }

    public void setVideo_notification_title(String video_notification_title) {
        this.video_notification_title = video_notification_title;
    }

    public String getVideo_notification_description() {
        return video_notification_description;
    }

    public void setVideo_notification_description(String video_notification_description) {
        this.video_notification_description = video_notification_description;
    }

    public String getVideo_notification_image() {
        return video_notification_image;
    }

    public void setVideo_notification_image(String video_notification_image) {
        this.video_notification_image = video_notification_image;
    }

    public String getVideo_notification_type() {
        return video_notification_type;
    }

    public void setVideo_notification_type(String video_notification_type) {
        this.video_notification_type = video_notification_type;
    }

    public String getVideo_notification_date() {
        return video_notification_date;
    }

    public void setVideo_notification_date(String video_notification_date) {
        this.video_notification_date = video_notification_date;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getPerson_given_name() {
        return person_given_name;
    }

    public void setPerson_given_name(String person_given_name) {
        this.person_given_name = person_given_name;
    }

    public String getPerson_family_name() {
        return person_family_name;
    }

    public void setPerson_family_name(String person_family_name) {
        this.person_family_name = person_family_name;
    }

    public String getPerson_email() {
        return person_email;
    }

    public void setPerson_email(String person_email) {
        this.person_email = person_email;
    }

    public String getPerson_photo() {
        return person_photo;
    }

    public void setPerson_photo(String person_photo) {
        this.person_photo = person_photo;
    }

    public String getPerson_token() {
        return person_token;
    }

    public void setPerson_token(String person_token) {
        this.person_token = person_token;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getVideo_deskripsi() {
        return video_deskripsi;
    }

    public void setVideo_deskripsi(String video_deskripsi) {
        this.video_deskripsi = video_deskripsi;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getConvert_time() {
        return convert_time;
    }

    public void setConvert_time(String convert_time) {
        this.convert_time = convert_time;
    }
}
