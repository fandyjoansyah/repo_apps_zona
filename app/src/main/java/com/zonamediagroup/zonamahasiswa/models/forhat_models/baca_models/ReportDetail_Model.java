package com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models;

public class ReportDetail_Model {
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReport_name() {
        return report_name;
    }

    public void setReport_name(String report_name) {
        this.report_name = report_name;
    }

    String report_name;
}
