package com.zonamediagroup.zonamahasiswa.models;

public class Video_Save_Model {
    String video_save_id, video_id, video_save_date, video_title, video_deskripsi, video_url, video_thumbnail, video_category_nama, video_category_sub_nama;
    String video_category_id;
    String video_category_sub_id;

    public String getVideo_category_id() {
        return video_category_id;
    }

    public void setVideo_category_id(String video_category_id) {
        this.video_category_id = video_category_id;
    }

    public String getVideo_category_sub_id() {
        return video_category_sub_id;
    }

    public void setVideo_category_sub_id(String video_category_sub_id) {
        this.video_category_sub_id = video_category_sub_id;
    }

    public String getVideo_save_id() {
        return video_save_id;
    }

    public void setVideo_save_id(String video_save_id) {
        this.video_save_id = video_save_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_save_date() {
        return video_save_date;
    }

    public void setVideo_save_date(String video_save_date) {
        this.video_save_date = video_save_date;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getVideo_deskripsi() {
        return video_deskripsi;
    }

    public void setVideo_deskripsi(String video_deskripsi) {
        this.video_deskripsi = video_deskripsi;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getVideo_category_nama() {
        return video_category_nama;
    }

    public void setVideo_category_nama(String video_category_nama) {
        this.video_category_nama = video_category_nama;
    }

    public String getVideo_category_sub_nama() {
        return video_category_sub_nama;
    }

    public void setVideo_category_sub_nama(String video_category_sub_nama) {
        this.video_category_sub_nama = video_category_sub_nama;
    }
}
