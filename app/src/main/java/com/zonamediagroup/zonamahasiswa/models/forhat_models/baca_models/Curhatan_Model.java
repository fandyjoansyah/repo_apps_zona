package com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models;

public class Curhatan_Model {
    String id;
    String no;
    String id_curhatan;
    String id_user;
    String id_kategori;
    String nama_penulis;
    String nik_penulis;
    String universitas_penulis;
    String alamat_penulis;
    String jenis_kelamin;
    String slug;
    String visible_curhatan;
    String judul_curhatan;
    String isi_curhatan;
    String cover_select;
    String cover;
    String type_curhatan;
    String status_penerimaan;
    String status_publish;
    String status_report;
    String detail_report;
    String detail_banding;
    String toc;
    String anonymouse;

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    String nama_kategori;
    int total_komentar;
    int total_like;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getId_curhatan() {
        return id_curhatan;
    }

    public void setId_curhatan(String id_curhatan) {
        this.id_curhatan = id_curhatan;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getNama_penulis() {
        return nama_penulis;
    }

    public void setNama_penulis(String nama_penulis) {
        this.nama_penulis = nama_penulis;
    }

    public String getNik_penulis() {
        return nik_penulis;
    }

    public void setNik_penulis(String nik_penulis) {
        this.nik_penulis = nik_penulis;
    }

    public String getUniversitas_penulis() {
        return universitas_penulis;
    }

    public void setUniversitas_penulis(String universitas_penulis) {
        this.universitas_penulis = universitas_penulis;
    }

    public String getAlamat_penulis() {
        return alamat_penulis;
    }

    public void setAlamat_penulis(String alamat_penulis) {
        this.alamat_penulis = alamat_penulis;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getVisible_curhatan() {
        return visible_curhatan;
    }

    public void setVisible_curhatan(String visible_curhatan) {
        this.visible_curhatan = visible_curhatan;
    }

    public String getJudul_curhatan() {
        return judul_curhatan;
    }

    public void setJudul_curhatan(String judul_curhatan) {
        this.judul_curhatan = judul_curhatan;
    }

    public String getIsi_curhatan() {
        return isi_curhatan;
    }

    public void setIsi_curhatan(String isi_curhatan) {
        this.isi_curhatan = isi_curhatan;
    }

    public String getCover_select() {
        return cover_select;
    }

    public void setCover_select(String cover_select) {
        this.cover_select = cover_select;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getType_curhatan() {
        return type_curhatan;
    }

    public void setType_curhatan(String type_curhatan) {
        this.type_curhatan = type_curhatan;
    }

    public String getStatus_penerimaan() {
        return status_penerimaan;
    }

    public void setStatus_penerimaan(String status_penerimaan) {
        this.status_penerimaan = status_penerimaan;
    }

    public String getStatus_publish() {
        return status_publish;
    }

    public void setStatus_publish(String status_publish) {
        this.status_publish = status_publish;
    }

    public String getStatus_report() {
        return status_report;
    }

    public void setStatus_report(String status_report) {
        this.status_report = status_report;
    }

    public String getDetail_report() {
        return detail_report;
    }

    public void setDetail_report(String detail_report) {
        this.detail_report = detail_report;
    }

    public String getDetail_banding() {
        return detail_banding;
    }

    public void setDetail_banding(String detail_banding) {
        this.detail_banding = detail_banding;
    }

    public String getToc() {
        return toc;
    }

    public void setToc(String toc) {
        this.toc = toc;
    }

    public String getAnonymouse() {
        return anonymouse;
    }

    public void setAnonymouse(String anonymouse) {
        this.anonymouse = anonymouse;
    }

    public int getTotal_komentar() {
        return total_komentar;
    }

    public void setTotal_komentar(int total_komentar) {
        this.total_komentar = total_komentar;
    }

    public int getTotal_like() {
        return total_like;
    }

    public void setTotal_like(int total_like) {
        this.total_like = total_like;
    }

    public int getTotal_share() {
        return total_share;
    }

    public void setTotal_share(int total_share) {
        this.total_share = total_share;
    }

    public int getTotal_visitor() {
        return total_visitor;
    }

    public void setTotal_visitor(int total_visitor) {
        this.total_visitor = total_visitor;
    }

    public int getTotal_view() {
        return total_view;
    }

    public void setTotal_view(int total_view) {
        this.total_view = total_view;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDate_publish() {
        return date_publish;
    }

    public void setDate_publish(String date_publish) {
        this.date_publish = date_publish;
    }

    public String getFoto_user() {
        return foto_user;
    }

    public void setFoto_user(String foto_user) {
        this.foto_user = foto_user;
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }

    public String getId_avatar() {
        return id_avatar;
    }

    public void setId_avatar(String id_avatar) {
        this.id_avatar = id_avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_like() {
        return user_like;
    }

    public void setUser_like(String user_like) {
        this.user_like = user_like;
    }

    int total_share;
    int total_visitor;
    int total_view;
    int score;
    String created_at;
    String updated_at;
    String date_publish;
    String foto_user;
    String nama_user;
    String id_avatar;
    String avatar;
    String user_like;
}
