package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ravi on 16/11/17.
 */

public class Download_Model {

    @SerializedName("id_download")
    String id_download;
    @SerializedName("tittle_download")
    String tittle_download;

    @SerializedName("img_download")
    String img_download;
    @SerializedName("url_file")
    String url_file;


    public Download_Model() {
    }

    public String getId_download() {
        return id_download;
    }

    public void setId_download(String id_download) {
        this.id_download = id_download;
    }

    public String getTittle_download() {
        return tittle_download;
    }

    public void setTittle_download(String tittle_download) {
        this.tittle_download = tittle_download;
    }

    public String getImg_download() {
        return img_download;
    }

    public void setImg_download(String img_download) {
        this.img_download = img_download;
    }

    public String getUrl_file() {
        return url_file;
    }

    public void setUrl_file(String url_file) {
        this.url_file = url_file;
    }
}
