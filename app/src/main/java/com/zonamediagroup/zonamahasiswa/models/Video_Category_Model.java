package com.zonamediagroup.zonamahasiswa.models;

public class Video_Category_Model {
    String video_category_id;
    String video_category_nama;
    String video_category_image, video_category_image_click, video_category_image_name, video_category_image_name_click;

    public String getVideo_category_id() {
        return video_category_id;
    }

    public void setVideo_category_id(String video_category_id) {
        this.video_category_id = video_category_id;
    }

    public String getVideo_category_nama() {
        return video_category_nama;
    }

    public void setVideo_category_nama(String video_category_nama) {
        this.video_category_nama = video_category_nama;
    }

    public String getVideo_category_image() {
        return video_category_image;
    }

    public void setVideo_category_image(String video_category_image) {
        this.video_category_image = video_category_image;
    }

    public String getVideo_category_image_click() {
        return video_category_image_click;
    }

    public void setVideo_category_image_click(String video_category_image_click) {
        this.video_category_image_click = video_category_image_click;
    }

    public String getVideo_category_image_name() {
        return video_category_image_name;
    }

    public void setVideo_category_image_name(String video_category_image_name) {
        this.video_category_image_name = video_category_image_name;
    }

    public String getVideo_category_image_name_click() {
        return video_category_image_name_click;
    }

    public void setVideo_category_image_name_click(String video_category_image_name_click) {
        this.video_category_image_name_click = video_category_image_name_click;
    }
}
