package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ravi on 16/11/17.
 */

public class Detail_poling_Model {

    @SerializedName("id_detail_poling")
    String id_detail_poling;
    @SerializedName("id_poling")
    String id_poling;

    @SerializedName("nama_tokoh")
    String nama_tokoh;
    @SerializedName("img_tokoh")
    String img_tokoh;
    @SerializedName("total_vote")
    String total_vote;

    @SerializedName("selected")
    boolean selected;

    @SerializedName("alasan")
    String alasan;

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Detail_poling_Model() {
    }

    public String getId_detail_poling() {
        return id_detail_poling;
    }

    public void setId_detail_poling(String id_detail_poling) {
        this.id_detail_poling = id_detail_poling;
    }

    public String getId_poling() {
        return id_poling;
    }

    public void setId_poling(String id_poling) {
        this.id_poling = id_poling;
    }

    public String getNama_tokoh() {
        return nama_tokoh;
    }

    public void setNama_tokoh(String nama_tokoh) {
        this.nama_tokoh = nama_tokoh;
    }

    public String getImg_tokoh() {
        return img_tokoh;
    }

    public void setImg_tokoh(String img_tokoh) {
        this.img_tokoh = img_tokoh;
    }

    public String getTotal_vote() {
        return total_vote;
    }

    public void setTotal_vote(String total_vote) {
        this.total_vote = total_vote;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }
}
