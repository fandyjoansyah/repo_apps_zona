package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ravi on 16/11/17.
 */

public class Notif_Model {

    @SerializedName("id_notifikasi")
    String id_notifikasi;
    @SerializedName("title_notifikasi")
    String title_notifikasi;
    @SerializedName("img_notifikasi")
    String img_notifikasi;
    @SerializedName("jenis_notifikasi")
    String jenis_notifikasi;
    @SerializedName("id_reverensi")
    String id_reverensi;


    public Notif_Model() {
    }

    public String getId_notifikasi() {
        return id_notifikasi;
    }

    public void setId_notifikasi(String id_notifikasi) {
        this.id_notifikasi = id_notifikasi;
    }

    public String getTitle_notifikasi() {
        return title_notifikasi;
    }

    public void setTitle_notifikasi(String title_notifikasi) {
        this.title_notifikasi = title_notifikasi;
    }

    public String getImg_notifikasi() {
        return img_notifikasi;
    }

    public void setImg_notifikasi(String img_notifikasi) {
        this.img_notifikasi = img_notifikasi;
    }

    public String getJenis_notifikasi() {
        return jenis_notifikasi;
    }

    public void setJenis_notifikasi(String jenis_notifikasi) {
        this.jenis_notifikasi = jenis_notifikasi;
    }

    public String getId_reverensi() {
        return id_reverensi;
    }

    public void setId_reverensi(String id_reverensi) {
        this.id_reverensi = id_reverensi;
    }
}
