package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ravi on 16/11/17.
 */

public class Komentar_Model {

    @SerializedName("komentar")
    String komentar;
    @SerializedName("person_photo")
    String person_photo;
    @SerializedName("person_name")
    String person_name;
    @SerializedName("jumlah_like")
    String jumlah_like;
    @SerializedName("ada_like_current_user")
    boolean ada_like_current_user;
    List<Komentar_Model> sub_komentar;

    public String getId_sub_komentar() {
        return id_sub_komentar;
    }

    public void setId_sub_komentar(String id_sub_komentar) {
        this.id_sub_komentar = id_sub_komentar;
    }

    String id_sub_komentar;

    public boolean isAda_like_current_user() {
        return ada_like_current_user;
    }

    public List<Komentar_Model> getSub_komentar() {
        return sub_komentar;
    }

    public void setSub_komentar(List<Komentar_Model> sub_komentar) {
        this.sub_komentar = sub_komentar;
    }

    public String getWaktu_komentar() {
        return waktu_komentar;
    }

    public void setWaktu_komentar(String waktu_komentar) {
        this.waktu_komentar = waktu_komentar;
    }

    @SerializedName("waktu_komentar")
    String waktu_komentar;

    public String getJumlah_like() {
        return jumlah_like;
    }

    public void setJumlah_like(String jumlah_like) {
        this.jumlah_like = jumlah_like;
    }

    public boolean getAda_like_current_user() {
        return ada_like_current_user;
    }

    public void setAda_like_current_user(boolean ada_like_current_user) {
        this.ada_like_current_user = ada_like_current_user;
    }

    public String getId_komentar() {
        return id_komentar;
    }

    public void setId_komentar(String id_komentar) {
        this.id_komentar = id_komentar;
    }

    @SerializedName("id_komentar")
    String id_komentar;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    @SerializedName("id_user")
    String id_user;




    public Komentar_Model() {
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getPerson_photo() {
        return person_photo;
    }

    public void setPerson_photo(String person_photo) {
        this.person_photo = person_photo;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }
}
