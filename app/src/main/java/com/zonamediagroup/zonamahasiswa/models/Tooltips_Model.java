package com.zonamediagroup.zonamahasiswa.models;

public class Tooltips_Model {
    String tooltips_id;
    String tooltips_nama;
    String tooltips_status;
    String tooltips_order;
    String fragment;

    public String getTooltips_id() {
        return tooltips_id;
    }

    public void setTooltips_id(String tooltips_id) {
        this.tooltips_id = tooltips_id;
    }

    public String getTooltips_nama() {
        return tooltips_nama;
    }

    public void setTooltips_nama(String tooltips_nama) {
        this.tooltips_nama = tooltips_nama;
    }

    public String getTooltips_status() {
        return tooltips_status;
    }

    public void setTooltips_status(String tooltips_status) {
        this.tooltips_status = tooltips_status;
    }

    public String getTooltips_order() {
        return tooltips_order;
    }

    public void setTooltips_order(String tooltips_order) {
        this.tooltips_order = tooltips_order;
    }

    public String getFragment() {
        return fragment;
    }

    public void setFragment(String fragment) {
        this.fragment = fragment;
    }

    public String getSudah_klik() {
        return sudah_klik;
    }

    public void setSudah_klik(String sudah_klik) {
        this.sudah_klik = sudah_klik;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    String sudah_klik;
    String user_id;

}
