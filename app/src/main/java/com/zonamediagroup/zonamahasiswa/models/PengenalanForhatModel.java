package com.zonamediagroup.zonamahasiswa.models;

public class PengenalanForhatModel {
    String textPengenalan;

    public String getTextPengenalan() {
        return textPengenalan;
    }

    public void setTextPengenalan(String textPengenalan) {
        this.textPengenalan = textPengenalan;
    }

    public boolean isTombolCloseSudahDiKlikPengguna() {
        return tombolCloseSudahDiKlikPengguna;
    }

    public void setTombolCloseSudahDiKlikPengguna(boolean tombolCloseSudahDiKlikPengguna) {
        this.tombolCloseSudahDiKlikPengguna = tombolCloseSudahDiKlikPengguna;
    }

    boolean tombolCloseSudahDiKlikPengguna = false;
}
