package com.zonamediagroup.zonamahasiswa.models;

public class PopUp_Halaman_Utama_Model {
    String id;
    String nama_popup;
    String jenis_popup;
    String url_gambar;
    String param_tambahan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_popup() {
        return nama_popup;
    }

    public void setNama_popup(String nama_popup) {
        this.nama_popup = nama_popup;
    }

    public String getJenis_popup() {
        return jenis_popup;
    }

    public void setJenis_popup(String jenis_popup) {
        this.jenis_popup = jenis_popup;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }

    public String getParam_tambahan() {
        return param_tambahan;
    }

    public void setParam_tambahan(String param_tambahan) {
        this.param_tambahan = param_tambahan;
    }

    public String getText_tombol_aksi() {
        return text_tombol_aksi;
    }

    public void setText_tombol_aksi(String text_tombol_aksi) {
        this.text_tombol_aksi = text_tombol_aksi;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }

    String text_tombol_aksi;
    String aktif;
}
