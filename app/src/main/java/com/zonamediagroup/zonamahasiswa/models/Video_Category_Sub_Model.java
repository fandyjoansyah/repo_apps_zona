package com.zonamediagroup.zonamahasiswa.models;

public class Video_Category_Sub_Model {
    String video_category_sub_id, video_category_id, video_category_sub_nama, video_category_sub_image;

    public String getVideo_category_sub_id() {
        return video_category_sub_id;
    }

    public void setVideo_category_sub_id(String video_category_sub_id) {
        this.video_category_sub_id = video_category_sub_id;
    }

    public String getVideo_category_id() {
        return video_category_id;
    }

    public void setVideo_category_id(String video_category_id) {
        this.video_category_id = video_category_id;
    }

    public String getVideo_category_sub_nama() {
        return video_category_sub_nama;
    }

    public void setVideo_category_sub_nama(String video_category_sub_nama) {
        this.video_category_sub_nama = video_category_sub_nama;
    }

    public String getVideo_category_sub_image() {
        return video_category_sub_image;
    }

    public void setVideo_category_sub_image(String video_category_sub_image) {
        this.video_category_sub_image = video_category_sub_image;
    }


}
