package com.zonamediagroup.zonamahasiswa.models;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

public class CustomShareModel {

    CharSequence label;
    Drawable icon;


    public CharSequence getLabel() {
        return label;
    }

    public void setLabel(CharSequence label) {
        this.label = label;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }


}
