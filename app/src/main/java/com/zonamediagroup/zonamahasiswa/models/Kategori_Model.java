package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ravi on 16/11/17.
 */

public class Kategori_Model {

    @SerializedName("id_kategori")
    String id_kategori;
    @SerializedName("id_categori_wp")
    String id_categori_wp;
    @SerializedName("nama_kategori")
    String nama_kategori;
    @SerializedName("gambar_kategori")
    String gambar_kategori;
    @SerializedName("warna_kategori")
    String warna_kategori;




    public Kategori_Model() {
    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getId_categori_wp() {
        return id_categori_wp;
    }

    public void setId_categori_wp(String id_categori_wp) {
        this.id_categori_wp = id_categori_wp;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getGambar_kategori() {
        return gambar_kategori;
    }

    public void setGambar_kategori(String gambar_kategori) {
        this.gambar_kategori = gambar_kategori;
    }

    public String getWarna_kategori() {
        return warna_kategori;
    }

    public void setWarna_kategori(String warna_kategori) {
        this.warna_kategori = warna_kategori;
    }
}
