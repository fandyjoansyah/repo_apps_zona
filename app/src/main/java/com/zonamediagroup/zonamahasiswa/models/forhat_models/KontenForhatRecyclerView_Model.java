package com.zonamediagroup.zonamahasiswa.models.forhat_models;

public class KontenForhatRecyclerView_Model {
    String id;
    String no;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getJudul_curhatan() {
        return judul_curhatan;
    }

    public void setJudul_curhatan(String judul_curhatan) {
        this.judul_curhatan = judul_curhatan;
    }

    public String getIsi_curhatan() {
        return isi_curhatan;
    }

    public void setIsi_curhatan(String isi_curhatan) {
        this.isi_curhatan = isi_curhatan;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getTotal_like() {
        return total_like;
    }

    public void setTotal_like(int total_like) {
        this.total_like = total_like;
    }

    public int getTotal_komentar() {
        return total_komentar;
    }

    public void setTotal_komentar(int total_komentar) {
        this.total_komentar = total_komentar;
    }

    public int getTotal_share() {
        return total_share;
    }

    public void setTotal_share(int total_share) {
        this.total_share = total_share;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDate_publish() {
        return date_publish;
    }

    public void setDate_publish(String date_publish) {
        this.date_publish = date_publish;
    }

    public String getId_avatar() {
        return id_avatar;
    }

    public void setId_avatar(String id_avatar) {
        this.id_avatar = id_avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFoto_user() {
        return foto_user;
    }

    public void setFoto_user(String foto_user) {
        this.foto_user = foto_user;
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }

    int score;
    String judul_curhatan;
    String isi_curhatan;
    String slug;
    int total_like;
    int total_komentar;
    int total_share;
    String cover;
    String created_at;
    String date_publish;
    String id_avatar;
    String avatar;
    String foto_user;
    String nama_user;

    public String getUser_like() {
        return user_like;
    }

    public void setUser_like(String user_like) {
        this.user_like = user_like;
    }

    public String getTotal_tampil() {
        return total_tampil;
    }

    public void setTotal_tampil(String total_tampil) {
        this.total_tampil = total_tampil;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    String user_like;
    String total_tampil;
    String verified;

}
