package com.zonamediagroup.zonamahasiswa.models.forhat_tulis_models;

import android.net.Uri;

public class ForhatTulisStep1Model {
    private String judul;
    private String deskripsi;

    public String getNamaFilePilihanUser() {
        return namaFilePilihanUser;
    }

    public void setNamaFilePilihanUser(String namaFilePilihanUser) {
        this.namaFilePilihanUser = namaFilePilihanUser;
    }

    private String namaFilePilihanUser;

    public int getIdRadioCoverChecked() {
        return idRadioCoverChecked;
    }

    public void setIdRadioCoverChecked(int idRadioCoverChecked) {
        this.idRadioCoverChecked = idRadioCoverChecked;
    }

    private int idRadioCoverChecked;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }


    public Uri getUriCoverPilihanUser() {
        return uriCoverPilihanUser;
    }

    public void setUriCoverPilihanUser(Uri uriCoverPilihanUser) {
        this.uriCoverPilihanUser = uriCoverPilihanUser;
    }

    private Uri uriCoverPilihanUser;
}
