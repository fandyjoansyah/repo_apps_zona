package com.zonamediagroup.zonamahasiswa.models;

public class Data_ViewPager_Model {
    String id_poling;
    String jumlah_vote;
    String tiitle_poling;
    String poling_start;
    String poling_end;

    public String getWaktu_sekarang() {
        return waktu_sekarang;
    }

    public void setWaktu_sekarang(String waktu_sekarang) {
        this.waktu_sekarang = waktu_sekarang;
    }

    String waktu_sekarang;

    public String getPoling_start() {
        return poling_start;
    }

    public void setPoling_start(String poling_start) {
        this.poling_start = poling_start;
    }

    public String getPoling_end() {
        return poling_end;
    }

    public void setPoling_end(String poling_end) {
        this.poling_end = poling_end;
    }

    public String getTiitle_poling() {
        return tiitle_poling;
    }

    public void setTiitle_poling(String tiitle_poling) {
        this.tiitle_poling = tiitle_poling;
    }

    public String getId_poling() {
        return id_poling;
    }

    public void setId_poling(String id_poling) {
        this.id_poling = id_poling;
    }

    public String getJumlah_vote() {
        return jumlah_vote;
    }

    public void setJumlah_vote(String jumlah_vote) {
        this.jumlah_vote = jumlah_vote;
    }

    public String getImg_poling() {
        return img_poling;
    }

    public void setImg_poling(String img_poling) {
        this.img_poling = img_poling;
    }

    String img_poling;
}
