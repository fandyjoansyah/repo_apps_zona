package com.zonamediagroup.zonamahasiswa.models.forhat_tulis_models;

import android.net.Uri;

import java.util.ArrayList;

public class ForhatTulisStep2Model {
    public ArrayList<Uri> getDataUri() {
        return dataUri;
    }

    public void setDataUri(ArrayList<Uri> dataUri) {
        this.dataUri = dataUri;
    }

    ArrayList<Uri> dataUri;

}
