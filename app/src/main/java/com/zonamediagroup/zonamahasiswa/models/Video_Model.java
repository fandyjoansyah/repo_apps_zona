package com.zonamediagroup.zonamahasiswa.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Video_Model implements Parcelable {
    @SerializedName("video_id")
    String video_id;
    @SerializedName("video_title")
    String video_title;
    @SerializedName("video_deskripsi")
    String video_deskripsi;
    @SerializedName("video_category_id")
    String video_category_id;
    @SerializedName("video_category_sub_id")
    String video_category_sub_id;
    @SerializedName("youtube_video_id")
    String youtube_video_id;
    @SerializedName("video_url")
    String video_url;
    @SerializedName("video_thumbnail")
    String video_thumbnail;
    @SerializedName("video_share")
    String video_share;
    @SerializedName("video_like")
    String video_like;
    @SerializedName("video_save")
    String video_save;
    @SerializedName("video_comment")
    String video_comment;
    @SerializedName("video_date_created")
    String video_date_created;
    @SerializedName("user_id")
    String user_id;
    @SerializedName("video_publish_status")
    String video_publish_status;
    @SerializedName("video_share_url")
    String video_share_url;
    @SerializedName("id_user")
    String id_user;
    @SerializedName("person_id")
    String person_id;
    @SerializedName("person_name")
    String person_name;
    @SerializedName("person_given_name")
    String person_given_name;
    @SerializedName("person_family_name")
    String person_family_name;
    @SerializedName("person_email")
    String person_email;
    @SerializedName("person_photo")
    String person_photo;
    @SerializedName("person_token")
    String person_token;
    @SerializedName("video_like_id")
    String video_like_id;
    @SerializedName("video_save_id")
    String video_save_id;
    @SerializedName("comment")
    ArrayList<Komentar_Video_Model> comment;
    @SerializedName("convert_time")
    String convert_time;
    String video_category_sub_nama;

    public String getVideo_category_sub_nama() {
        return video_category_sub_nama;
    }

    public void setVideo_category_sub_nama(String video_category_sub_nama) {
        this.video_category_sub_nama = video_category_sub_nama;
    }

    public String getVideo_view() {
        return video_view;
    }

    public void setVideo_view(String video_view) {
        this.video_view = video_view;
    }

    @SerializedName("video_view")
    String video_view;


    public String getConvert_time() {
        return convert_time;
    }

    public void setConvert_time(String convert_time) {
        this.convert_time = convert_time;
    }

    public ArrayList<Komentar_Video_Model> getComment() {
        return comment;
    }

    public void setComment(ArrayList<Komentar_Video_Model> comment) {
        this.comment = comment;
    }

//    public String[] getComment() {
//        return comment;
//    }
//
//    public void setComment(String[] comment) {
//        this.comment = comment;
//    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getVideo_deskripsi() {
        return video_deskripsi;
    }

    public void setVideo_deskripsi(String video_deskripsi) {
        this.video_deskripsi = video_deskripsi;
    }

    public String getVideo_category_id() {
        return video_category_id;
    }

    public void setVideo_category_id(String video_category_id) {
        this.video_category_id = video_category_id;
    }

    public String getVideo_category_sub_id() {
        return video_category_sub_id;
    }

    public void setVideo_category_sub_id(String video_category_sub_id) {
        this.video_category_sub_id = video_category_sub_id;
    }

    public String getYoutube_video_id() {
        return youtube_video_id;
    }

    public void setYoutube_video_id(String youtube_video_id) {
        this.youtube_video_id = youtube_video_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getVideo_share() {
        return video_share;
    }

    public void setVideo_share(String video_share) {
        this.video_share = video_share;
    }

    public String getVideo_like() {
        return video_like;
    }

    public void setVideo_like(String video_like) {
        this.video_like = video_like;
    }

    public String getVideo_save() {
        return video_save;
    }

    public void setVideo_save(String video_save) {
        this.video_save = video_save;
    }

    public String getVideo_comment() {
        return video_comment;
    }

    public void setVideo_comment(String video_comment) {
        this.video_comment = video_comment;
    }

    public String getVideo_date_created() {
        return video_date_created;
    }

    public void setVideo_date_created(String video_date_created) {
        this.video_date_created = video_date_created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVideo_publish_status() {
        return video_publish_status;
    }

    public void setVideo_publish_status(String video_publish_status) {
        this.video_publish_status = video_publish_status;
    }

    public String getVideo_share_url() {
        return video_share_url;
    }

    public void setVideo_share_url(String video_share_url) {
        this.video_share_url = video_share_url;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getPerson_given_name() {
        return person_given_name;
    }

    public void setPerson_given_name(String person_given_name) {
        this.person_given_name = person_given_name;
    }

    public String getPerson_family_name() {
        return person_family_name;
    }

    public void setPerson_family_name(String person_family_name) {
        this.person_family_name = person_family_name;
    }

    public String getPerson_email() {
        return person_email;
    }

    public void setPerson_email(String person_email) {
        this.person_email = person_email;
    }

    public String getPerson_photo() {
        return person_photo;
    }

    public void setPerson_photo(String person_photo) {
        this.person_photo = person_photo;
    }

    public String getPerson_token() {
        return person_token;
    }

    public void setPerson_token(String person_token) {
        this.person_token = person_token;
    }

    public String getVideo_like_id() {
        return video_like_id;
    }

    public void setVideo_like_id(String video_like_id) {
        this.video_like_id = video_like_id;
    }

    public String getVideo_save_id() {
        return video_save_id;
    }

    public void setVideo_save_id(String video_save_id) {
        this.video_save_id = video_save_id;
    }

    public String getVideo_category_nama() {
        return video_category_nama;
    }

    public void setVideo_category_nama(String video_category_nama) {
        this.video_category_nama = video_category_nama;
    }

    @SerializedName("video_category_nama")
    String video_category_nama;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
