package com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models;

public class Komentar_Model {
    String id, id_curhatan, komentar, created_at, nama,anonim,nama_anonim,foto_user,user_like,avatar,is_user,is_user_komentar,verified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_curhatan() {
        return id_curhatan;
    }

    public void setId_curhatan(String id_curhatan) {
        this.id_curhatan = id_curhatan;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAnonim() {
        return anonim;
    }

    public void setAnonim(String anonim) {
        this.anonim = anonim;
    }

    public String getNama_anonim() {
        return nama_anonim;
    }

    public void setNama_anonim(String nama_anonim) {
        this.nama_anonim = nama_anonim;
    }

    public String getFoto_user() {
        return foto_user;
    }

    public void setFoto_user(String foto_user) {
        this.foto_user = foto_user;
    }

    public String getUser_like() {
        return user_like;
    }

    public void setUser_like(String user_like) {
        this.user_like = user_like;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIs_user() {
        return is_user;
    }

    public void setIs_user(String is_user) {
        this.is_user = is_user;
    }

    public String getIs_user_komentar() {
        return is_user_komentar;
    }

    public void setIs_user_komentar(String is_user_komentar) {
        this.is_user_komentar = is_user_komentar;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public int getTotal_balasan() {
        return total_balasan;
    }

    public void setTotal_balasan(int total_balasan) {
        this.total_balasan = total_balasan;
    }

    int total_balasan;
}
