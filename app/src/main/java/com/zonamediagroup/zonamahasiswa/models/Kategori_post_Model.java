package com.zonamediagroup.zonamahasiswa.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ravi on 16/11/17.
 */

public class Kategori_post_Model {

  @SerializedName("ID")
  String ID;
  @SerializedName("post_author")
  String post_author;
  @SerializedName("post_date")
  String post_date;
  @SerializedName("post_title")
  String post_title;
  @SerializedName("guid")
  String guid;
  @SerializedName("post_content")
  String post_content;
  @SerializedName("meta_value")
  String meta_value;
  @SerializedName("user_login")
  String user_login;
  @SerializedName("name")
  String name;
  @SerializedName("term_id")
  String term_id;
  @SerializedName("gambar_user_wp")
  String gambar_user_wp;

  @SerializedName("like")
  String like;




  public Kategori_post_Model() {
  }

  public String getID() {
    return ID;
  }

  public void setID(String ID) {
    this.ID = ID;
  }

  public String getPost_author() {
    return post_author;
  }

  public void setPost_author(String post_author) {
    this.post_author = post_author;
  }

  public String getPost_date() {
    return post_date;
  }

  public void setPost_date(String post_date) {
    this.post_date = post_date;
  }

  public String getPost_title() {
    return post_title;
  }

  public void setPost_title(String post_title) {
    this.post_title = post_title;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getPost_content() {
    return post_content;
  }

  public void setPost_content(String post_content) {
    this.post_content = post_content;
  }

  public String getMeta_value() {
    return meta_value;
  }

  public void setMeta_value(String meta_value) {
    this.meta_value = meta_value;
  }

  public String getUser_login() {
    return user_login;
  }

  public void setUser_login(String user_login) {
    this.user_login = user_login;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTerm_id() {
    return term_id;
  }

  public void setTerm_id(String term_id) {
    this.term_id = term_id;
  }

  public String getGambar_user_wp() {
    return gambar_user_wp;
  }

  public void setGambar_user_wp(String gambar_user_wp) {
    this.gambar_user_wp = gambar_user_wp;
  }

  public String getLike() {
    return like;
  }

  public void setLike(String like) {
    this.like = like;
  }
}
