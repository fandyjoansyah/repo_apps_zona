package com.zonamediagroup.zonamahasiswa;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Belal on 03/11/16.
 */

public class SharedPrefManagerZona {
    private static final String SHARED_PREF_NAME = "zonamhs";

    public static final String session_status = "session_status";

    public final static String TAG_ID_APPS = "id_user";
    public final static String TAG_ID = "person_id";
    public final static String TAG_URL_VOTO = "person_photo";
    public final static String TAG_USERNAME = "person_name";
    public final static String TAG_GIVEN_NAME = "person_given_name";
    public final static String TAG_FAMILY_NAME = "person_family_name";
    public final static String TAG_EMAIL = "person_email";
    public final static String TAG_GENDER = "person_gender";
    public final static String TAG_USERNAME_NATIVE = "username";
    public final static String TAG_NAMA_ANONIM = "nama_anonim";




    private static final String TAG_TOKEN = "tagtoken";

    private static SharedPrefManagerZona mInstance;
    private static Context mCtx;

    SharedPreferences sharedpreferences;
    Boolean session = false;
    String  person_id, url_profile, person_email, person_name, person_gender,username,nama_anonim;

    private SharedPrefManagerZona(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManagerZona getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManagerZona(context);
        }
        return mInstance;
    }

    //this method will save the device token to shared preferences
    public boolean saveData(String json_id_user, String json_person_id, String json_person_name, String json_person_given_name,
                            String json_person_family_name, String json_person_email, String json_person_photo, String json_person_gender, String json_username,String json_nama_anonim) {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);

        sharedPreferences.edit().clear().commit();

        //                    Save perverance
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(session_status, true);

        editor.putString(TAG_ID_APPS, json_id_user);
        editor.putString(TAG_ID, json_person_id);
        editor.putString(TAG_USERNAME, json_person_name);
        editor.putString(TAG_GIVEN_NAME, json_person_given_name);
        editor.putString(TAG_FAMILY_NAME, json_person_family_name);
        editor.putString(TAG_EMAIL, json_person_email);
        editor.putString(TAG_URL_VOTO, json_person_photo);
        editor.putString(TAG_GENDER,json_person_gender);
        editor.putString(TAG_USERNAME_NATIVE,json_username);
        editor.putString(TAG_NAMA_ANONIM,json_nama_anonim);




        editor.commit();


        return true;
    }

    public boolean getsesion() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return session = sharedpreferences.getBoolean(session_status, false);
    }

    public boolean removesesion() {

        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.clear().commit();
        return true;
    }

    public String getid_user() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        person_id = sharedpreferences.getString(TAG_ID, null);
        return person_id;
    }

    public String getnama_anonim() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        nama_anonim = sharedpreferences.getString(TAG_NAMA_ANONIM, null);
        return nama_anonim;
    }

    public String geturl_voto() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        url_profile = sharedpreferences.getString(TAG_URL_VOTO, null);
        return url_profile;
    }

    public String getperson_name() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        person_name = sharedpreferences.getString(TAG_USERNAME, null);
        return person_name;
    }

    public String getperson_email() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        person_email = sharedpreferences.getString(TAG_EMAIL, null);
        return person_email;
    }

    public String getperson_gender() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        person_gender = sharedpreferences.getString(TAG_GENDER, null);
        return person_gender;
    }

    public String get_username() {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        username = sharedpreferences.getString(TAG_USERNAME_NATIVE, null);
        return username;
    }

    //untuk simpan sharedpref tooltips
    public boolean saveToolTips(String namaToolTip, int statusSudahLihat) {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);

        sharedPreferences.edit().clear().commit();

        //                    Save perverance
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(namaToolTip,statusSudahLihat);

        editor.commit();


        return true;
    }

    public int get_tooltips(String nama) {
        sharedpreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedpreferences.getInt(nama, 0);
    }

    public void updateSingleSharedPreferencesColumn(String key, String value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(key, value);

        editor.apply();
    }
}
