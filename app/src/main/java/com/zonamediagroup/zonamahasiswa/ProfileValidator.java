package com.zonamediagroup.zonamahasiswa;

import android.util.Patterns;
import android.widget.EditText;
import android.widget.RadioGroup;

public class ProfileValidator {
    public static boolean checkValidasiInputKosong(EditText edt){
        if(edt.getText().toString().trim().length() == 0)
        {
            return false;
        }
        return true;
    }

    public static boolean checkValidasiInputEmail(String target){
        if(Patterns.EMAIL_ADDRESS.matcher(target).matches())
            return true;
        else
            return false;
    }

    public static boolean checkValidasiMinimalKarakter(String input, int panjangMinimal)
    {
        if(input.length() < panjangMinimal)
        {
            return false;
        }
        else{
            return true;
        }
    }

    public static boolean checkValidasiInputPhone(String target){
        if(Patterns.PHONE.matcher(target).matches())
            return true;
        else
            return false;
    }

    public static boolean checkValidasiMengandungSpasi(String input) {
        if(input.contains(" "))
        {
            return false;
        }
        else{
            return true;
        }
    }

    public static boolean checkValidasiTitikDiawalDanDiakhirKata(EditText edt){
        String inputan = edt.getText().toString();
        if(inputan.charAt(0)=='.' || inputan.charAt(inputan.length()-1)=='.')
        {
            return false;
        }
        return true;
    }

    public static boolean checkValidasiKesamaanKataSandiDenganKonfirmasiKataSandi(String kataSandi, String konfirmasiKataSandi){
        if(!kataSandi.equals(konfirmasiKataSandi))
        {
            return false;
        }
        else{
            return true;
        }
    }

    public static boolean cekApakahRadioJeniskelaminSudahTerpilih(RadioGroup radioGroup){
        if(radioGroup.getCheckedRadioButtonId() == -1)
        {
            return false;
        }
        else {
            return true;
        }
    }
}
