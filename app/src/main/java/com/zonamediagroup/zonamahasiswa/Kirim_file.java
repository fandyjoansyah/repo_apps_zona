



package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Kirim_file extends AppCompatActivity {

    LinearLayout layout_rubrik_1;

    CardView kirim_file_ok;
    // komponen upload file

    //    private String upload_URL = Server.URL_REAL + "Upload";
    private String upload_URL = "http://api.zonamahasiswa.id/api/Upload/";
    private RequestQueue rQueue;
    private ArrayList<HashMap<String, String>> arraylist;
    String url = "https://www.google.com";

    Uri uri;
    String uriString;
    File myFile;
    String path;
    String displayName = null;
    ProgressDialog progressDialog;


    //    iklan


//    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/5224354917";
private static String AD_UNIT_ID = "";
    private static final String AD_UNIT_ID2 = "ca-app-pub-3100669952331846/5680095416";
    private static final long COUNTER_TIME = 10;
    private static final int GAME_OVER_REWARD = 0;

    private int coinCount;

    private CountDownTimer countDownTimer;
    private boolean gameOver;
    private boolean gamePaused;

    private RewardedAd rewardedAd;

    private long timeRemaining;
    boolean isLoading;


//    iklan end

    // end komponen upload file

    //true = ada file. false = belum ada file
    boolean fileSudahTerpilih = false;

    String id_user_sv;

    WebView p4;

    TextView keterangan_file_upload;

    ImageView back;


    AlertDialog.Builder builderDialog;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_kirim_file);

        AD_UNIT_ID = getResources().getString(R.string.ad_unit_id_interstitial);

        id_user_sv = SharedPrefManagerZona.getInstance(this).getid_user();

        System.out.println(" USER ID " + id_user_sv);

        layout_rubrik_1 = findViewById(R.id.layout_rubrik_1);



        kirim_file_ok = findViewById(R.id.kirim_file_ok);
        keterangan_file_upload = findViewById(R.id.keterangan_file_upload);

        back=findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        layout_rubrik_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

// Open file
                try {
                    Log.e("debug_1_oktober","try 150");
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

                    intent.setType("*/*");
                    String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                    startActivityForResult(intent, 1);
                }
                catch(Exception e)
                {
                    Log.e("debug_1_oktober","catch 159 "+e.getMessage());
                }
            }
        });

        kirim_file_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(fileSudahTerpilih == false)
                {
                    Toast.makeText(getApplicationContext(),"Pilih file mu terlebih dahulu",Toast.LENGTH_SHORT).show();
                }
                else {
                    progressDialog = new ProgressDialog(Kirim_file.this);
                    progressDialog.setMessage("Mengirim...");
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    progressDialog.show();


                    //loadRewardedAd();
                    InterstitialAd intersAd = IklanInterstitial.iklanVideo;
                    if(intersAd.isLoaded()) {
                        intersAd.show();
                        intersAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                //load another ad
                                intersAd.loadAd(new AdRequest.Builder().build());
                                uploadPDF(displayName, uri);
                            }
                        });
                    }
                    else
                    {
                        uploadPDF(displayName, uri);
                    }
                    Log.d("Kirim_file","step satu");
                }

            }
        });

        p4 = findViewById(R.id.p4);
        String x =
                "<p style=\"text-align: center;\"><strong>Syarat &amp; Ketentuan Rubrik Sobat Zona</strong></p>\n" + "<p>1. Panjang artikel minimal 700 kata dengan panjang setiap paragraf 200-300 karakter. <br /> <br />2. Artikel yang ditulis harus berdasarkan kategori yang ada di aplikasi Zona Mahasiswa, dikecualikan beasiswa, cerbung, dan berita. <br/><br/>3. Sobat Zona harus menyertakan identitas pada berkas yang dikirim, meliputi:<br />Nama lengkap:<br />Kampus:<br />Email:<br />Nomor WhatsApp: <br /> <br />4. Naskah yang dikirimkan sudah disertai gambar dengan ketentuan:<br />- Gambar untuk cover resolusi minimal 700-800 pixel dan bebas dari watermark.<br />- Gambar untuk subheading (penajukan) resolusi minimal 700-1000 pixel dan bebas dari watermark.<br />- Menyertakan keterangan gambar dan sumber gambar. <br /><br />5. Artikel belum pernah dipublikasikan dimanapun dan bukan hasil plagiasi dari orang lain. <br/><br/>6. Bagi karya Sobat Zona yang diterima akan mendapatkan hadiah/reward.</p>";
        p4.loadData(x, "text/html; charset=utf-8", "UTF-8");
    }

    private void showAlertDialog(int myLayout) {
        builderDialog = new AlertDialog.Builder(this);
        View layoutView = getLayoutInflater().inflate(myLayout,null);

        builderDialog.setView(layoutView);
        alertDialog = builderDialog.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.WRAP_CONTENT);

        //close setelah 3 detik
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                alertDialog.dismiss();
            }
        },2500);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // Get the Uri of the selected file
            uri = data.getData();
            //Toast.makeText(getApplicationContext(),"URI : "+uri,Toast.LENGTH_SHORT).show();
            uriString = uri.toString();
            myFile = new File(uriString);
            path = myFile.getAbsolutePath();
            displayName = null;

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    Log.e("debug_1_oktober","try 226");
                    cursor = this.getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        Log.d("nameeeeeX1>>>>  ", displayName);

//                        Toast.makeText(this, "NAMA FILE " + displayName, Toast.LENGTH_SHORT).show();
                        layout_rubrik_1.setVisibility(View.INVISIBLE);
                        keterangan_file_upload.setText(displayName);
                        fileSudahTerpilih = true;
//                        uploadPDF(displayName, uri);
                    }

                }
                catch(Exception e)
                {
                    Log.e("debug_1_oktober","catch 242");
                }
                finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                try {
                    Log.e("debug_1_oktober","try 249 ");
                    displayName = myFile.getName().replace("%20"," ");
                    Log.d("nameeeeeX2>>>>  ", displayName);
                    layout_rubrik_1.setVisibility(View.INVISIBLE);
                    keterangan_file_upload.setText(displayName);
                    fileSudahTerpilih = true;
                }
                catch(Exception e){
                    Log.e("debug_1_oktober","catch 256 "+e.getMessage());
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    private void uploadPDF(final String pdfname, Uri pdffile) {


        Log.d("debug_1_oktober","uploadPDF terpanggil");
        InputStream iStream = null;
        try {
            iStream = getContentResolver().openInputStream(pdffile);
            final byte[] inputData = getBytes(iStream);

            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, upload_URL,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.d("ressssssoo", new String(response.data));
                            rQueue.getCache().clear();
                            try {
                                progressDialog.dismiss();
                                showAlertDialog(R.layout.my_success_dialog);
                                JSONObject jsonObject = new JSONObject(new String(response.data));
//                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                                jsonObject.toString().replace("\\\\", "");

                                if (jsonObject.getString("status").equals("true")) {
                                    Log.d("come::: >>>  ", "yessssss");
//                                    arraylist = new ArrayList<HashMap<String, String>>();
//                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    // post Insert sukses
                                    System.out.println("FILE MOVE OK");
                                } else {
                                    System.out.println("FILE MOVE FALSE");
                                }
                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                showAlertDialog(R.layout.my_failed_dialog);
                                e.printStackTrace();
                                System.out.println("EROR FILE " + e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            showAlertDialog(R.layout.my_failed_dialog);
                            System.out.println("EROR FILE DUA " + error);
//                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 * */
//                Parameter
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    System.out.println(" USER ID " + id_user_sv);

                    Map<String, String> params = new HashMap<>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", id_user_sv);
                    // params.put("tags", "ccccc");  add string parameters
                    return params;
                }

                /*
                 *pass files using below method
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("filename", new DataPart(pdfname, inputData));

                    return params;
                }
            };


            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rQueue = Volley.newRequestQueue(Kirim_file.this);
            rQueue.add(volleyMultipartRequest);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


    // iklan
    private void loadRewardedAd() {
        if (rewardedAd == null || !rewardedAd.isLoaded()) {
            rewardedAd = new RewardedAd(Kirim_file.this, AD_UNIT_ID);
            isLoading = true;
            rewardedAd.loadAd(
                    new AdRequest.Builder().build(),
                    new RewardedAdLoadCallback() {
                        @Override
                        public void onRewardedAdLoaded() {
                            // Ad successfully loaded.
                            isLoading = false;
                            //Toast.makeText(Point.this, "onRewardedAdLoaded", Toast.LENGTH_SHORT).show();
                            startGame();
                        }

                        @Override
                        public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
                            // Ad failed to load.
                            progressDialog.dismiss();
                            isLoading = false;
                            //Toast.makeText(Point.this, "on Rewarded Ad FailedTo Load", Toast.LENGTH_SHORT).show();
                            Toast.makeText(Kirim_file.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                            System.out.println("error load " + loadAdError);
                        }
                    });
        }
    }

    private void startGame() {
// Run Coll down
        if (!rewardedAd.isLoaded() && !isLoading) {
            loadRewardedAd();
        }
        createTimer(COUNTER_TIME);
        gamePaused = false;
        gameOver = false;
    }

    private void createTimer(long time) {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer =
                new CountDownTimer(time * 100, 50) {
                    @Override
                    public void onTick(long millisUnitFinished) {
                        timeRemaining = ((millisUnitFinished / 1000) + 1);
                        //Toast.makeText(Point.this, "Remaining " + timeRemaining, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFinish() {
                        if (rewardedAd.isLoaded()) {

                            showRewardedVideo();
                        }

                        gameOver = true;
                    }
                };
        countDownTimer.start();
    }

    private void showRewardedVideo() {
        //    showVideoButton.setVisibility(View.INVISIBLE);
        if (rewardedAd.isLoaded()) {
            RewardedAdCallback adCallback =
                    new RewardedAdCallback() {
                        @Override
                        public void onRewardedAdOpened() {
                            // Ad opened.

                        }

                        @Override
                        public void onRewardedAdClosed() {
                            // Ad closed.
                            uploadPDF(displayName, uri);
                        }

                        @Override
                        public void onUserEarnedReward(RewardItem rewardItem) {
                            // User earned reward.

                            // download disini ya
                            // Call API
                            //start setup progress dialog

                            //end setup progress dialog
                           // uploadPDF(displayName, uri);

                        }

                        @Override
                        public void onRewardedAdFailedToShow(AdError adError) {
                            // Ad failed to display
                            Log.e("Kirim_file","onRewardedAdFailedToShow "+adError.getMessage());
                        }
                    };
            rewardedAd.show(Kirim_file.this, adCallback);

        }
    }

    // END IKLAN

}