package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class KelolaAkun extends AppCompatActivity {
    ImageView back_kelola_akun;
    TextView menu_rubah_nama,menu_rubah_nama_pengguna,menu_domisili,menu_email,menu_no_handphone,menu_universitas_atau_instansi,menu_jenis_kelamin,menu_rubah_foto_profil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_kelola_akun);
        findViewByIdAllComponent();
        setListenerAllComponent();

    }
    private void findViewByIdAllComponent(){
        back_kelola_akun = findViewById(R.id.back_kelola_akun);
        menu_rubah_nama = findViewById(R.id.menu_rubah_nama);
        menu_rubah_nama_pengguna = findViewById(R.id.menu_rubah_nama_pengguna);
        menu_domisili = findViewById(R.id.menu_domisili);
        menu_email = findViewById(R.id.menu_email);
        menu_no_handphone = findViewById(R.id.menu_no_handphone);
        menu_universitas_atau_instansi = findViewById(R.id.menu_universitas_atau_instansi);
        menu_jenis_kelamin = findViewById(R.id.menu_jenis_kelamin);
        menu_rubah_foto_profil = findViewById(R.id.menu_rubah_foto_profil);
    }

    private void setListenerAllComponent(){
        back_kelola_akun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu_rubah_nama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KelolaAkun.this,RubahNama.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menu_rubah_nama_pengguna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KelolaAkun.this,RubahNamaPengguna.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menu_domisili.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KelolaAkun.this,RubahDomisili.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menu_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KelolaAkun.this,RubahEmail.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menu_no_handphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KelolaAkun.this,RubahNomorTelepon.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);

            }
        });
        menu_universitas_atau_instansi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KelolaAkun.this,RubahUniversitasAtauInstansi.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menu_jenis_kelamin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KelolaAkun.this,RubahJenisKelamin.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);

            }
        });
        menu_rubah_foto_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KelolaAkun.this,RubahFotoProfil.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_transition_slide_from_left,R.anim.activity_transition_slide_to_right);
    }
}