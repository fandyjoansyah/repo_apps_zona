package com.zonamediagroup.zonamahasiswa;

import com.zonamediagroup.zonamahasiswa.models.Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Save_Model;

import java.util.ArrayList;
import java.util.List;

public class CurrentActiveVideoData {
    public static ArrayList<Video_Model> dataVideos;
    public static ArrayList<String> dataSavesDeleted;
    public static String targetKomentarInduk;
    public static String targetKomentarAnak;


    public static ArrayList<Video_Model> getDataVideos() {
        return dataVideos;
    }

    public static void setDataVideos(ArrayList<Video_Model> dataVideos) {
        CurrentActiveVideoData.dataVideos = dataVideos;
    }


    public static void initDataVideos(){
        dataVideos = new ArrayList<>();
    }

    public static void initDataVideosSavesDeleted(){
        dataSavesDeleted = new ArrayList<>();
    }

}
