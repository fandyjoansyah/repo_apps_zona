package com.zonamediagroup.zonamahasiswa.ForhatFragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.ActivityForhat.PertanyaanActivity;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.Delay;
import com.zonamediagroup.zonamahasiswa.ForhatBaca;
import com.zonamediagroup.zonamahasiswa.ForhatSearchActivity;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.ServerForhat;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.KontenForhatAdapter;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClientForhat;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi;
import com.zonamediagroup.zonamahasiswa.models.KeteranganKontenHabisModel;
import com.zonamediagroup.zonamahasiswa.models.PengenalanForhatModel;
import com.zonamediagroup.zonamahasiswa.models.TulisCurhatanModel;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KontenForhatRecyclerView_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class ForhatHome extends Fragment implements KontenForhatAdapter.ListKontenForhatListener {
    /* HARAP DIBACA
     Mekanisme Get konten forhat dari server adalah sbb
     1.Request token dari http://zonamahasiswa.net/api/get_token_forhat_list
     2.Ketika request konten, sertakan token yang sebelumnya di request
     */

    // loading
    private static final String URL_GET_TOKEN = ServerForhat.URL_API_FORHAT+"get_token_forhat_list";

    public static final int ITEMS_PER_AD = 5;

    ImageView btn_search_forhat;
    String keteranganForhat = "<b>forHat</b> atau forum curHat merupakan media yang digunakan untuk menyalurkan curhatan atau keluh kesah terkait perkuliahan, dosen, asmara, belanja, hingga permasalahan lainnya.";
    ImageView btn_icon_tulis;
    RelativeLayout nested_forhat_home;
    Resources rs;
    RecyclerView.LayoutManager layoutManagerRecyclerViewKontenForhat;

    boolean isLoadingShimmerActive = false;

    SwipeRefreshLayout swipe_refresh;
    boolean semuaKontenSudahDitampilkan = false;

    String idUser;

    ShimmerFrameLayout shimmer_framelayout;

    LinearLayout ly_keterangan_forhat;

    public void setForhatToken(String forhatToken) {
        this.forhatToken = forhatToken;
    }

    String forhatToken;

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    String pageNum;
    ProgressBar loadingLoadMore;
    String kode;
    FrameLayout layout_include_forhat_home;
    Animation fade_in;
     private static final String FILTER = "Home";

    // start section init recycler view
    RecyclerView recycler_kontenforhat_home;
    ArrayList<Object> kontenForhat_RecyclerView_model;
    KontenForhatAdapter kontenForhatadapter;
    // end section init recycler view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.page_forhat_home, container, false);
        init(root);
        return root;
    }

    public void setIdUser(String idUser){
        this.idUser = idUser;
    }

    public String getIdUserFromSharedPreferences(){
        return SharedPrefManagerZona.getInstance(getActivity()).getid_user();
    }

    private void findViewByIdFromRoot(View root)
    {
        btn_icon_tulis = root.findViewById(R.id.btn_icon_tulis);
        btn_search_forhat = root.findViewById(R.id.btn_search_forhat);
        recycler_kontenforhat_home = root.findViewById(R.id.recycler_kontenforhat_home);
        swipe_refresh = root.findViewById(R.id.swipe_refresh);
        ly_keterangan_forhat = root.findViewById(R.id.ly_keterangan_forhat);
        shimmer_framelayout = root.findViewById(R.id.shimmer_framelayout);
        layout_include_forhat_home = root.findViewById(R.id.layout_include_forhat_home);
        nested_forhat_home = root.findViewById(R.id.nested_forhat_home);
        loadingLoadMore = (ProgressBar) root.findViewById(R.id.loadingLoadMore);
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
    }


    private void setRs(Resources rs){
        this.rs = rs;
    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setListenerAllComponent(){
        btn_search_forhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ForhatSearchActivity.class));
            }
        });

        btn_icon_tulis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTambahPostingan();
            }
        });

        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mekanismeRefresh();
                new Delay().delayTask(500, new Runnable() {
                    @Override
                    public void run() {
                        swipe_refresh.setRefreshing(false);
                    }
                });

            }
        });
        recycler_kontenforhat_home.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d("9_juli_2022","RecyclerView has reached bottom");
                    mekanismeLoadMore();
                }


            }
        });
    }

    private void dialogTambahPostingan() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.getActivity());
        View layoutView = getLayoutInflater().inflate(R.layout.dialog_tambah_postingan, null);

        dialogBuilder.setView(layoutView);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        Window window = alertDialog.getWindow();

        // Background dialog transparant
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // Ukuran dialog
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);
        window.setLayout(width, height);

        // Posisi dialog
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.y = 1000;
        window.setAttributes(wlp);

        /* Listener yang ada di dialog */
        ImageView btnCLose = layoutView.findViewById(R.id.btn_close_dialog_tambah);
        btnCLose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        CardView cardPertanyaan = layoutView.findViewById(R.id.card_pertanyaan);
        cardPertanyaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PertanyaanActivity.class));
            }
        });
    }

    private void mekanismeRefresh(){
        clearDataRecyclerView();
        addPengenalanForhat(0);
        addTulisCurhatan(1);
        getForhatTokenFromServer();
    }

    private void clearDataRecyclerView(){
        kontenForhat_RecyclerView_model.clear();
    }

    private void mekanismeLoadMore(){
        if(semuaKontenSudahDitampilkan == false)
        {
            Log.d("7_juli_2022","mekanismeLoadMore()");
            showLoadingLoadMore();
            getHomeKonten(forhatToken,pageNum,FILTER,true);
        }
    }


    //start section init recycler view
    private void initRecyclerKontenForhat(){
        kontenForhat_RecyclerView_model = new ArrayList<>();
        //start temporary set data dummy
        //kontenForhat_model = generateDataDummy();
        //end temporary set data dummy
        kontenForhatadapter = new KontenForhatAdapter(getContext(), kontenForhat_RecyclerView_model,this);
        layoutManagerRecyclerViewKontenForhat = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        recycler_kontenforhat_home.setLayoutManager(layoutManagerRecyclerViewKontenForhat);
        recycler_kontenforhat_home.setItemAnimator(new DefaultItemAnimator());
        recycler_kontenforhat_home.setAdapter(kontenForhatadapter);

        /*start optimization*/
        recycler_kontenforhat_home.setHasFixedSize(true);
        recycler_kontenforhat_home.setItemViewCacheSize(20);
        recycler_kontenforhat_home.setDrawingCacheEnabled(true);
        recycler_kontenforhat_home.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        /*end optimization*/
    }
    //end section init recycler view

    private void init(View root) {
        findViewByIdFromRoot(root);
        initAnimation();
        initAdMobAdsSDK();
        initRecyclerKontenForhat();
        setListenerAllComponent();
        setIdUser(getIdUserFromSharedPreferences());
        setRs(getResources());
        addPengenalanForhat(0);
        addTulisCurhatan(1);
        getForhatTokenFromServer();
        startLoadingShimmer();
    }

    public void startLoadingShimmer(){
        isLoadingShimmerActive = true;
        shimmer_framelayout.startShimmerAnimation();
        shimmer_framelayout.setVisibility(View.VISIBLE);
        layout_include_forhat_home.setVisibility(View.GONE);
        isLoadingShimmerActive = true;
    }

    public void endLoadingShimmer(){
        isLoadingShimmerActive = false;
        shimmer_framelayout.stopShimmerAnimation();
        shimmer_framelayout.setVisibility(View.GONE);
        layout_include_forhat_home.startAnimation(fade_in);
        layout_include_forhat_home.setVisibility(View.VISIBLE);
    }

    private List<KontenForhatRecyclerView_Model> generateDataDummy(){
        List<KontenForhatRecyclerView_Model> listData = new ArrayList<>();
        KontenForhatRecyclerView_Model data1 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data2 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data3 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data4 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data5 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data6 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data7 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data8 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data9 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data10 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data11 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data12 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data13 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data14 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data15 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data16 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data17 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data18 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data19 = new KontenForhatRecyclerView_Model();
        KontenForhatRecyclerView_Model data20 = new KontenForhatRecyclerView_Model();
        listData.add(data1);
        listData.add(data2);
        listData.add(data3);
        listData.add(data4);
        listData.add(data5);
        listData.add(data6);
        listData.add(data7);
        listData.add(data8);
        listData.add(data9);
        listData.add(data10);
        listData.add(data11);
        listData.add(data12);
        listData.add(data13);
        listData.add(data14);
        listData.add(data15);
        listData.add(data16);
        listData.add(data17);
        listData.add(data18);
        listData.add(data19);
        listData.add(data20);

        return listData;
    }


    private void getForhatTokenFromServer(){
        InterfaceApi api = ApiClientForhat.getClient().create(InterfaceApi.class);
        Call<JsonObject> call = api.getForhatToken();
       call.enqueue(new Callback<JsonObject>() {
           @Override
           public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> responseRetrofit) {
               try{
                   JSONObject response = new JSONObject(responseRetrofit.body().toString());
                   setForhatToken(response.getString("token"));
                   /*cek apakah pageNum = null*/
                   if(response.isNull("pageNum") == true)
                   {

                       setPageNum("");
                   }
                   else
                   {
                       setPageNum(response.getString("pageNum"));
                   }

                   /*request pertama get data konten*/

                   getHomeKonten(forhatToken,pageNum,FILTER,false);

               }
               catch (NullPointerException e)
               {
                   endLoadingShimmer();
                   Log.d("9_juli_2022","getForhatTokenFromServer() onResponse catch NullPointerException: "+e.getMessage());
               }
               catch(Exception e)
               {
                   endLoadingShimmer();
                   Log.d("9_juli_2022","getForhatTokenFromServer() onResponse catch Exception: "+e.getMessage());
                   CustomToast.makeBlackCustomToast(getContext(),rs.getString(R.string.terjadi_kesalahan_coba_lagi_nanti), Toast.LENGTH_SHORT);
               }
           }

           @Override
           public void onFailure(Call<JsonObject> call, Throwable t) {

           }
       });
    }

    private void getForhatTokenFromServerVolley(){
        startLoadingShimmer();
        //start request forhat token
        StringRequest strReq = new StringRequest(Request.Method.GET, URL_GET_TOKEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String responseString) {
                try{
                    JSONObject response = new JSONObject(responseString);
                    Log.d("9_juli_2022","getForhatTokenFromServer() onResponse try");
                    Log.d("9_juli_2022","getForhatTokenFromServer() onResponse try. Response token="+response.getString("token"));
                    //Log.d("27_juni_2022","token forhat: "+response.getString("token"));
                    setForhatToken(response.getString("token"));

                    /*cek apakah pageNum = null*/
                    if(response.isNull("pageNum") == true)
                    {
                        Log.d("9_juli_2022","getForhatTokenFromServer() onResponse try if pageNum is null");
                        setPageNum("");
                    }
                    else
                    {
                        Log.d("9_juli_2022","getForhatTokenFromServer() onResponse try else pageNum is null(pageNum is not null)");
                        setPageNum(response.getString("pageNum"));
                    }


                    /*request pertama get data konten*/
                    Log.d("9_juli_2022","getForhatTokenFromServer() before calling getHomeKonten()");
                    getHomeKonten(forhatToken,pageNum,FILTER,false);

                }
                catch (NullPointerException e)
                {
                    endLoadingShimmer();
                    Log.d("9_juli_2022","getForhatTokenFromServer() onResponse catch NullPointerException: "+e.getMessage());
                }
                catch(Exception e)
                {
                    endLoadingShimmer();
                    Log.d("9_juli_2022","getForhatTokenFromServer() onResponse catch Exception: "+e.getMessage());
                    CustomToast.makeBlackCustomToast(getContext(),rs.getString(R.string.terjadi_kesalahan_coba_lagi_nanti), Toast.LENGTH_SHORT);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {


        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end request forhat token
    }



    private void notifyDataSetChangeHomeAdapter(){
        kontenForhatadapter.notifyDataSetChanged();
    }

    private void showLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.VISIBLE);
    }

    private boolean isLoadingLoadMoreActive(){
        if(loadingLoadMore.getVisibility() == View.VISIBLE)
            return true;
        else
            return false;
    }

    private void hideLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.GONE);
    }


    private void setDataKonten(JSONArray arrayData){
        Log.d("6_juli_2022","setDataKonten()");
        List<KontenForhatRecyclerView_Model> dataForhat = new Gson().fromJson(arrayData.toString(), new TypeToken<List<KontenForhatRecyclerView_Model>>() {
        }.getType());
        kontenForhat_RecyclerView_model.addAll(dataForhat);
        addAdMobBannerAds();
        notifyDataSetChangeHomeAdapter();
    }

    private void showToastSemuaKontenSudahDitampilkan(){
        CustomToast.makeBlackCustomToast(getContext(),rs.getString(R.string.kamu_sudah_menampilkan_seluruh_konten),Toast.LENGTH_SHORT);
    }

    private void getHomeKonten2(String kode,String pageNum,String filter,boolean isLoadMore){
        //jika merupakan load more, maka tampilkan loading loadmore ketika sedang mengambil data
        Log.d("30_juni_2022","getHomeKonten()");
        Log.d("27_juni_2022","kode: "+kode);
        Log.d("27_juni_2022","pageNum: "+pageNum);
        Log.d("27_juni_2022","filter: "+filter);
        AndroidNetworking.post(ServerForhat.URL_FORHAT_LIST_FINAL)
                .addBodyParameter("kode", kode)
                .addBodyParameter("pageNum", pageNum)
                .addBodyParameter("filter", filter)
                .addHeaders("id",idUser)
                .setTag("get home konten")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(isLoadingShimmerActive==true)
                        endLoadingShimmer();
                        try{
                            Log.d("30_juni_2022","getHomeKonten() onResponse");
                            Log.d("28_juni_2022","get home konten berhasil. message: "+response.getString("message"));
                            //cek apakah data adalah jsonArray
                            if(response.get("data") instanceof JSONArray)
                            {
                                JSONArray arrayData = response.getJSONArray("data");
                                //cek apakah array data ada isinya
                                if(arrayData.length()>0)
                                {
                                    Log.d("30_juni_2022","getHomeKonten() onResponse kondisiOk");
                                    Log.d("27_juni_2022","ada data");
                                    Log.d("27_juni_2022","size: "+arrayData);
                                    Log.d("27_juni_2022","judul curhatan: "+arrayData.getJSONObject(2).getString("judul_curhatan"));
                                    setDataKonten(arrayData);

                                }
                                //dapatkan data konten(array)
                            }
                            else
                            {
                                if(semuaKontenSudahDitampilkan == false) {
                                    semuaKontenSudahDitampilkan = true;
                                    addKeteranganKontenHabis();
                                    notifyDataSetChangeHomeAdapter();
                                }
                                showToastSemuaKontenSudahDitampilkan();
                            }

                        }
                        catch(Exception e)
                        {
                            //Log.d("27_juni_2022","ghk catch: "+e.getMessage());
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        if(isLoadingShimmerActive==true)
                        endLoadingShimmer();
                        if(isLoadMore == true)
                            hideLoadingLoadMore();
                        Log.d("27_juni_2022","ghk error: "+error.getMessage());
                    }
                });
    }

    private void getHomeKonten(String kode,String pageNum,String filter,boolean isLoadMore)
    {
        InterfaceApi api = ApiClientForhat.getClient().create(InterfaceApi.class);
        Call<JsonObject> call = api.getHomeKontenRetrofit(idUser,kode,filter,pageNum);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> responseRetrofit) {
                Log.d("9_juli_2022","getHomeKontenRetrofit onResponse");
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
                if(isLoadingShimmerActive==true)
                    endLoadingShimmer();
                try{
                    Log.d("9_juli_2022","getHomeKontenRetrofit onResponse try");
                    Log.d("9_juli_2022","getHomeKontenRetrofit onResponse try");
                    if(responseRetrofit.isSuccessful())
                    {
                        Log.d("9_juli_2022","getHomeKontenRetrofit onResponse isSuccesful");
                    }
                    else
                    {
                        Log.d("9_juli_2022","getHomeKontenRetrofit onResponse isNotSuccesful (empty)");
                    }
                    JSONObject response =new JSONObject(responseRetrofit.body().toString());

                    if(response.getInt("status") == 0)
                    {
                        //jika status = 0 maka konten sudah habis
                        if(semuaKontenSudahDitampilkan == false) {
                            semuaKontenSudahDitampilkan = true;
                            addKeteranganKontenHabis();
                            notifyDataSetChangeHomeAdapter();
                        }
                        showToastSemuaKontenSudahDitampilkan();
                    }
                    else if (response.getInt("status") == 1)
                    {
                        //jika status = 1 maka ada konten diterima
                        Log.d("9_juli_2022","getHomeKontenRetrofit() onResponse try. response message="+response.getString("message"));
                        //cek apakah data adalah jsonArray
                        if(response.get("data") instanceof JSONArray) {
                            Log.d("9_juli_2022","getHomeKontenRetrofit() onResponse try if data instance of Array = true");
                            JSONArray arrayData = response.getJSONArray("data");
                            setDataKonten(arrayData);
                        }

                    }
                    //dapatkan data konten(array)
                } catch (JSONException e) {
                    Log.d("9_juli_2022","getHomeKontenRetrofit onResponse catch: "+e.getMessage());
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("9_juli_2022","getHomeKontenRetrofit onFailure "+t.getMessage());
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
            }
        });
    }

    private void getHomeKontenVolley(String kode,String pageNum,String filter,boolean isLoadMore){
        Log.d("9_juli_2022","getHomeKontenVolley()");
        Log.d("9_juli_2022","getHomeKontenVolley()  param kode: "+kode);
        Log.d("9_juli_2022","getHomeKontenVolley()  param pageNum: "+pageNum);
        Log.d("9_juli_2022","getHomeKontenVolley()  param filter: "+filter);
        Log.d("9_juli_2022","getHomeKontenVolley()  param isLoadMore: "+isLoadMore);
        StringRequest strReq = new StringRequest(Request.Method.POST, ServerForhat.URL_FORHAT_LIST_FINAL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
                Log.d("9_juli_2022","getHomeKontenVolley() onResponse");
                if(isLoadingShimmerActive==true)
                    endLoadingShimmer();
                try{
                    Log.d("9_juli_2022","getHomeKontenVolley() onResponse try");
                    JSONObject jObjResponse = new JSONObject(response);
                    if(jObjResponse.getInt("status") == 0)
                    {
                        //jika status = 0 maka konten sudah habis
                        if(semuaKontenSudahDitampilkan == false) {
                            semuaKontenSudahDitampilkan = true;
                            addKeteranganKontenHabis();
                            notifyDataSetChangeHomeAdapter();
                        }
                        showToastSemuaKontenSudahDitampilkan();
                    }
                    else if (jObjResponse.getInt("status") == 1)
                    {
                        //jika status = 1 maka ada konten diterima
                        Log.d("9_juli_2022","getHomeKontenVolley() onResponse try. response message="+jObjResponse.getString("message"));
                        //cek apakah data adalah jsonArray
                        if(jObjResponse.get("data") instanceof JSONArray) {
                            Log.d("9_juli_2022","getHomeKontenVolley() onResponse try if data instance of Array = true");
                            JSONArray arrayData = jObjResponse.getJSONArray("data");
                            setDataKonten(arrayData);
                    }

                    }
                        //dapatkan data konten(array)
                } catch (JSONException e) {
                    Log.d("9_juli_2022","getHomeKontenVolley() onResponse catch: "+e.getMessage());
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
                Log.d("9_juli_2022","getHomeKontenVolley() onErrorResponse "+error.getMessage());
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("kode", kode);
                params.put("filter", filter);
                params.put("pageNum",pageNum);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("id",idUser);
                return headers;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end bukaBlokir Pengguna
    }


    @Override
    public void like(String idUser, String idKonten) {
        Log.d("29_juni_2022","method like idUser: "+idUser);
        Log.d("29_juni_2022","method like idKonten: "+idKonten);
        AndroidNetworking.post(ServerForhat.URL_POST_LIKE)
                .addBodyParameter("id_curhatan", idKonten)
                .addHeaders("id",idUser)
                .setTag("toggle_like")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public void openBacaKonten(String slug) {
        Intent i = new Intent(getActivity(), ForhatBaca.class);
        i.putExtra("slug",slug);
        startActivity(i);
    }

    private void initAdMobAdsSDK()
    {
        MobileAds.initialize(getContext(), new OnInitializationCompleteListener()
        {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus)
            {
            }
        });
    }

    /*menambahkan object PengenalanForhatModel ke recycler view*/
    private void addPengenalanForhat(int posisi){
        PengenalanForhatModel pFM = new PengenalanForhatModel();
        kontenForhat_RecyclerView_model.add(posisi,pFM);
    }

    private void addKeteranganKontenHabis()
    {
        KeteranganKontenHabisModel kKHM = new KeteranganKontenHabisModel();
        kontenForhat_RecyclerView_model.add(kKHM);
    }

    /*menambahkan object tulis curhatan ke recycler view*/
    private void addTulisCurhatan(int posisi){
        TulisCurhatanModel tCM = new TulisCurhatanModel();
        kontenForhat_RecyclerView_model.add(posisi,tCM);
    }

    private void addAdMobBannerAds()
    {
        Log.d("6_juli_2022","size awal: "+kontenForhat_RecyclerView_model.size());
        for (int i = ITEMS_PER_AD; i < kontenForhat_RecyclerView_model.size(); i += ITEMS_PER_AD)
        {
            //skip jika item berupa iklan
            if(kontenForhat_RecyclerView_model.get(i) instanceof  AdView){
                continue;
            }
            else{

                final AdView adView = new AdView(getContext());
                adView.setAdSize(AdSize.LEADERBOARD);
                adView.setAdUnitId(getResources().getString(R.string.ad_unit_id_banner));
                kontenForhat_RecyclerView_model.add(i, adView);
                Log.d("6_juli_2022","item ads added at: "+i);
                Log.d("6_juli_2022","Structure Data After insert ads: ");
                for(int j=0;j<kontenForhat_RecyclerView_model.size();j++)
                {
                    if(kontenForhat_RecyclerView_model.get(j) instanceof KontenForhatRecyclerView_Model)
                    {
                        Log.d("6_juli_2022","<<data>>");
                    }
                    else if(kontenForhat_RecyclerView_model.get(j) instanceof AdView)
                    {
                        Log.d("6_juli_2022","<<adView>>");
                    }
                }

            }
        }

        loadBannerAds();
    }

    private void loadBannerAds()
    {
        //Load the first banner ad in the items list (subsequent ads will be loaded automatically in sequence).
        loadBannerAd(ITEMS_PER_AD);
    }

    private void loadBannerAd(final int index)
    {
        if (index >= kontenForhat_RecyclerView_model.size())
        {
            return;
        }

        Object item = kontenForhat_RecyclerView_model.get(index);
        if (!(item instanceof AdView))
        {
            throw new ClassCastException("Expected item at index " + index + " to be a banner ad" + " ad.");
        }

        final AdView adView = (AdView) item;

        // Set an AdListener on the AdView to wait for the previous banner ad
        // to finish loading before loading the next ad in the items list.
        adView.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                super.onAdLoaded();
                // The previous banner ad loaded successfully, call this method again to
                // load the next ad in the items list.
                loadBannerAd(index + ITEMS_PER_AD);
            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                // The previous banner ad failed to load. Call this method again to load
                // the next ad in the items list.
                Log.e("MainActivity", "The previous banner ad failed to load. Attempting to"
                        + " load the next banner ad in the items list.");
                loadBannerAd(index + ITEMS_PER_AD);
            }
        });

        // Load the banner ad.
        adView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onResume() {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.resume();
            }
        }
        super.onResume();
    }

    @Override
    public void onPause()
    {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.pause();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.destroy();
            }
        }
        super.onDestroy();
    }
}
