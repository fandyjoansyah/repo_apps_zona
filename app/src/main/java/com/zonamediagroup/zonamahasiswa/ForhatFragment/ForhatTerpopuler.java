package com.zonamediagroup.zonamahasiswa.ForhatFragment;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.Delay;
import com.zonamediagroup.zonamahasiswa.ForhatBaca;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.ServerForhat;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.KontenForhatAdapter;
import com.zonamediagroup.zonamahasiswa.models.KeteranganKontenHabisModel;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KontenForhatRecyclerView_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForhatTerpopuler extends Fragment implements View.OnClickListener, KontenForhatAdapter.ListKontenForhatListener{

    Animation fade_in;
    ArrayList<Object> kontenForhat_RecyclerView_model;
    boolean flagTabCurhatanAktif, flagTabTanggapanAktif, isLoadingShimmerActive, semuaKontenSudahDitampilkan = false;
    FrameLayout layout_include_forhat_terpopuler;
    String pageNum = "null";
    KontenForhatAdapter kontenForhatadapter;
    ProgressBar loadingLoadMore;
    RadioButton btn_jenis_curhatan, btn_jenis_tanggapan;
    RecyclerView recycler_kontenforhat_terpopuler;
    RecyclerView.LayoutManager layoutManagerRecyclerViewKontenForhat;
    RelativeLayout btn_search_forhat,nested_forhat_terpopuler;
    Resources rs;
    ShimmerFrameLayout shimmer_framelayout;
    String idUser, forhatToken;
    SwipeRefreshLayout swipe_refresh;


    private static final String URL_GET_TOKEN = ServerForhat.URL_API_FORHAT+"get_token_forhat_list";
    public static final int ITEMS_PER_AD = 5;

    private static final String FILTER_TERPOPULER = "Terpopuler";
    private static final String JENIS_TANGGAPAN = "Tanggapan";
    private static final String JENIS_CURHATAN = "Curhatan";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.page_forhat_terpopuler, container, false);
        init(root);
        /*aktivasi tab curhatan ketika awal page di load */
        return root;
    }

    private void init(View root) {
        findViewByIdFromRoot(root);
        initAnimation();
        initAdMobAdsSDK();
        initRecyclerKontenForhat();
        setPageNumToNull();
        setListenerAllComponent();
        setIdUser(getIdUserFromSharedPreferences());
        setRs(getResources());
        mekanismeToggleBtnJenis(R.id.btn_jenis_curhatan);
        //==================masih belum pasti pakai token atau tidak
        //getForhatTokenFromServer();
    }

    public void startLoadingShimmer(){
        isLoadingShimmerActive = true;
        shimmer_framelayout.startShimmerAnimation();
        shimmer_framelayout.setVisibility(View.VISIBLE);
        layout_include_forhat_terpopuler.setVisibility(View.GONE);
        isLoadingShimmerActive = true;
    }



    public void endLoadingShimmer(){
        isLoadingShimmerActive = false;
        shimmer_framelayout.stopShimmerAnimation();
        shimmer_framelayout.setVisibility(View.GONE);
        layout_include_forhat_terpopuler.startAnimation(fade_in);
        layout_include_forhat_terpopuler.setVisibility(View.VISIBLE);
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public void setForhatToken(String forhatToken) {
        this.forhatToken = forhatToken;
    }

    private void setRs(Resources rs){
        this.rs = rs;
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
    }



    public String getIdUserFromSharedPreferences(){
        return SharedPrefManagerZona.getInstance(getActivity()).getid_user();
    }

    private void findViewByIdFromRoot(View root){
        btn_jenis_curhatan = root.findViewById(R.id.btn_jenis_curhatan);
        btn_jenis_tanggapan = root.findViewById(R.id.btn_jenis_tanggapan);
        btn_search_forhat = root.findViewById(R.id.btn_search_forhat);
        recycler_kontenforhat_terpopuler = root.findViewById(R.id.recycler_kontenforhat_terpopuler);
        swipe_refresh = root.findViewById(R.id.swipe_refresh);
        shimmer_framelayout = root.findViewById(R.id.shimmer_framelayout);
        layout_include_forhat_terpopuler = root.findViewById(R.id.layout_include_forhat_terpopuler);
        nested_forhat_terpopuler = root.findViewById(R.id.nested_forhat_terpopuler);
        loadingLoadMore = (ProgressBar) root.findViewById(R.id.loadingLoadMore);

    }

    private void setListenerAllComponent(){
        btn_jenis_curhatan.setOnClickListener(this);
        btn_jenis_tanggapan.setOnClickListener(this);
        recycler_kontenforhat_terpopuler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    mekanismeLoadMore();
                }


            }
        });
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startLoadingShimmer();
                clearItemInRecyclerView();
                setPageNumToNull();
                semuaKontenSudahDitampilkan = false;
                String jenis="";
                if(flagTabTanggapanAktif == true)
                    jenis = JENIS_TANGGAPAN;
                if(flagTabCurhatanAktif == true)
                    jenis = JENIS_CURHATAN;

                getKontenCurhatanOrTanggapan("",pageNum,FILTER_TERPOPULER,false,jenis);

                new Delay().delayTask(500, new Runnable() {
                    @Override
                    public void run() {
                        swipe_refresh.setRefreshing(false);
                    }
                });
            }
        });
    }

    private void activateTabCurhatan()
    {
        btn_jenis_curhatan.setChecked(true);
    }

    private void activateTabTanggapan()
    {
        btn_jenis_tanggapan.setChecked(true);
    }

    private void deactivateTabCurhatan()
    {
        btn_jenis_curhatan.setChecked(false);
    }

    private void deactivateTabTanggapan()
    {
        btn_jenis_tanggapan.setChecked(false);
    }

    private void clearItemInRecyclerView(){
        kontenForhat_RecyclerView_model.clear();
        notifyDataSetChangeAdapter();
    }

    private void setPageNumToNull(){
        pageNum = "null";
    }

    private void mekanismeToggleBtnJenis(int idButtonYangDiKlik){
        switch(idButtonYangDiKlik)
        {
            case R.id.btn_jenis_curhatan:
                if(flagTabCurhatanAktif == true)
                {

                }
                else {
                    setPageNumToNull();
                    semuaKontenSudahDitampilkan = false;
                    clearItemInRecyclerView();
                    startLoadingShimmer();
                    activateTabCurhatan();
                    deactivateTabTanggapan();
                    flagTabCurhatanAktif = true;
                    flagTabTanggapanAktif = false;
                    loadKontenCurhatan(false);
                }
                break;
            case R.id.btn_jenis_tanggapan:
                if(flagTabTanggapanAktif == true) {

                }
                else
                {
                    setPageNumToNull();
                    semuaKontenSudahDitampilkan = false;
                    clearItemInRecyclerView();
                    startLoadingShimmer();
                    deactivateTabCurhatan();
                    activateTabTanggapan();
                    flagTabCurhatanAktif = false;
                    flagTabTanggapanAktif = true;
                    loadKontenTanggapan(false);
                }
                break;
        }
    }

    private void loadKontenCurhatan(boolean loadMore){
        getKontenCurhatanOrTanggapan("",pageNum,FILTER_TERPOPULER,loadMore,JENIS_CURHATAN);
    }

    private void loadKontenTanggapan(boolean loadMore){
        getKontenCurhatanOrTanggapan("",pageNum,FILTER_TERPOPULER,loadMore,JENIS_TANGGAPAN);
    }


    //start section init recycler view
    private void initRecyclerKontenForhat(){
        kontenForhat_RecyclerView_model = new ArrayList<>();
        //start temporary set data dummy
        //kontenForhat_model = generateDataDummy();
        //end temporary set data dummy
        kontenForhatadapter = new KontenForhatAdapter(getContext(), kontenForhat_RecyclerView_model,this);
        layoutManagerRecyclerViewKontenForhat = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false);
        recycler_kontenforhat_terpopuler.setLayoutManager(layoutManagerRecyclerViewKontenForhat);
        recycler_kontenforhat_terpopuler.setItemAnimator(new DefaultItemAnimator());
        recycler_kontenforhat_terpopuler.setAdapter(kontenForhatadapter);
    }
    //end section init recycler view

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id)
        {
            case R.id.btn_jenis_curhatan:
                mekanismeToggleBtnJenis(R.id.btn_jenis_curhatan);
                break;
            case R.id.btn_jenis_tanggapan:
                mekanismeToggleBtnJenis(R.id.btn_jenis_tanggapan);
        }
    }

    @Override
    public void like(String idUser, String idKonten) {

    }

    @Override
    public void openBacaKonten(String slug) {
        Intent i = new Intent(getActivity(), ForhatBaca.class);
        i.putExtra("slug",slug);
        startActivity(i);
    }

    private void initAdMobAdsSDK()
    {
        MobileAds.initialize(getContext(), new OnInitializationCompleteListener()
        {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus)
            {
            }
        });
    }

    public void setIdUser(String idUser){
        this.idUser = idUser;
    }

    private boolean isLoadingLoadMoreActive(){
        if(loadingLoadMore.getVisibility() == View.VISIBLE)
            return true;
        else
            return false;
    }

    private void mekanismeLoadMore(){

        if(semuaKontenSudahDitampilkan == false)
        {


            showLoadingLoadMore();
            String jenis = "";

            /* deteksi flag button apa yang sedang aktif, maka itu yang menjadi jenis*/
            if(flagTabTanggapanAktif == true)
                jenis = JENIS_TANGGAPAN;
            if(flagTabCurhatanAktif == true)
                jenis = JENIS_CURHATAN;

            getKontenCurhatanOrTanggapan("",pageNum,FILTER_TERPOPULER,true,jenis);
        }
    }

    private void showLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.VISIBLE);
    }

    private void hideLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.GONE);
    }

    private void addKeteranganKontenHabis()
    {
        KeteranganKontenHabisModel kKHM = new KeteranganKontenHabisModel();
        kontenForhat_RecyclerView_model.add(kKHM);
    }

    private void notifyDataSetChangeAdapter(){
        kontenForhatadapter.notifyDataSetChanged();
    }

    private void showToastSemuaKontenSudahDitampilkan(){
        CustomToast.makeBlackCustomToast(getContext(),rs.getString(R.string.kamu_sudah_menampilkan_seluruh_konten),Toast.LENGTH_SHORT);
    }

    private void setDataKonten(JSONArray arrayData){

        List<KontenForhatRecyclerView_Model> dataForhat = new Gson().fromJson(arrayData.toString(), new TypeToken<List<KontenForhatRecyclerView_Model>>() {
        }.getType());
        kontenForhat_RecyclerView_model.addAll(dataForhat);

        /* codingan dibawah (getActivity != null digunakan untuk antisipasi force close ketika pengguna dengan cepat berpindah bottom navigation dari terpopuler ke lainnya) part 2*/
        if(getActivity()!=null)
        {
            addAdMobBannerAds();
        }
        notifyDataSetChangeAdapter();
    }

    private void loadBannerAds()
    {
        //Load the first banner ad in the items list (subsequent ads will be loaded automatically in sequence).
        loadBannerAd(ITEMS_PER_AD);
    }

    private void loadBannerAd(final int index)
    {
        if (index >= kontenForhat_RecyclerView_model.size())
        {
            return;
        }

        Object item = kontenForhat_RecyclerView_model.get(index);
        if (!(item instanceof AdView))
        {
            throw new ClassCastException("Expected item at index " + index + " to be a banner ad" + " ad.");
        }

        final AdView adView = (AdView) item;

        // Set an AdListener on the AdView to wait for the previous banner ad
        // to finish loading before loading the next ad in the items list.
        adView.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                super.onAdLoaded();
                // The previous banner ad loaded successfully, call this method again to
                // load the next ad in the items list.
                loadBannerAd(index + ITEMS_PER_AD);
            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                // The previous banner ad failed to load. Call this method again to load
                // the next ad in the items list.
                Log.e("MainActivity", "The previous banner ad failed to load. Attempting to"
                        + " load the next banner ad in the items list.");
                loadBannerAd(index + ITEMS_PER_AD);
            }
        });

        // Load the banner ad.
        adView.loadAd(new AdRequest.Builder().build());
    }

    private void addAdMobBannerAds()
    {
        for (int i = ITEMS_PER_AD; i < kontenForhat_RecyclerView_model.size(); i += ITEMS_PER_AD)
        {
            //skip jika item berupa iklan
            if(kontenForhat_RecyclerView_model.get(i) instanceof AdView){
                continue;
            }
            else{
                /* codingan dibawah (getActivity != null digunakan untuk antisipasi force close ketika pengguna dengan cepat berpindah bottom navigation dari terpopuler ke lainnya) part1*/
                if(getActivity() != null) {
                    final AdView adView = new AdView(getActivity());
                    adView.setAdSize(AdSize.LEADERBOARD);
                    adView.setAdUnitId(getResources().getString(R.string.ad_unit_id_banner));
                    kontenForhat_RecyclerView_model.add(i, adView);
                    Log.d("6_juli_2022", "item ads added at: " + i);
                    Log.d("6_juli_2022", "Structure Data After insert ads: ");
                    for (int j = 0; j < kontenForhat_RecyclerView_model.size(); j++) {
                        if (kontenForhat_RecyclerView_model.get(j) instanceof KontenForhatRecyclerView_Model) {
                            Log.d("6_juli_2022", "<<data>>");
                        } else if (kontenForhat_RecyclerView_model.get(j) instanceof AdView) {
                            Log.d("6_juli_2022", "<<adView>>");
                        }
                    }
                }
            }
        }

        loadBannerAds();
    }


    private void getKontenCurhatanOrTanggapan(String kode,String pageNum,String filter,boolean isLoadMore,String jenis){
        Log.d("19_juli_2022","========================================START PARAM BLOCK===============================");
            Log.d("19_juli_2022","kode: "+kode);
            Log.d("19_juli_2022","pageNum: "+pageNum);
            Log.d("19_juli_2022","filter: "+filter);
        Log.d("19_juli_2022","isLoadMore: "+isLoadMore);
        Log.d("19_juli_2022","jenis: "+jenis);
        Log.d("19_juli_2022","========================================END PARAM BLOCK===============================");

        Log.d("19_juli_2022","getKontenCurhatanOrTanggapan()");
        Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() jenis: "+jenis);
        StringRequest strReq = new StringRequest(Request.Method.POST, ServerForhat.URL_FORHAT_LIST_FINAL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
                Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse");
                if(isLoadingShimmerActive==true)
                    endLoadingShimmer();
                try{
                    Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse try");
                    JSONObject jObjResponse = new JSONObject(response);
                    if(jObjResponse.getInt("status") == 0)
                    {
                        Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse try if");
                        //jika status = 0 maka konten sudah habis
                        if(semuaKontenSudahDitampilkan == false) {
                            Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse try if if");
                            semuaKontenSudahDitampilkan = true;
                            addKeteranganKontenHabis();
                            notifyDataSetChangeAdapter();
                        }
                        showToastSemuaKontenSudahDitampilkan();
                    }
                    else if (jObjResponse.getInt("status") == 1)
                    {
                        Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse try elseif");
                        //jika status = 1 maka ada konten diterima
                        Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse try. response message="+jObjResponse.getString("message"));
                        //cek apakah data adalah jsonArray
                        if(jObjResponse.get("data") instanceof JSONArray) {
                            Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse try elseif if");
                            JSONArray arrayData = jObjResponse.getJSONArray("data");
                                setDataKonten(arrayData);
                        }
                        setPageNum(jObjResponse.getString("page"));

                    }
                    //dapatkan data konten(array)
                } catch (JSONException e) {

                    if(isLoadingShimmerActive==true)
                        endLoadingShimmer();

                    CustomToast.makeBlackCustomToast(getContext(),"Terjadi Kesalahan",Toast.LENGTH_SHORT);
                    Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onResponse catch: "+e.getMessage());
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();

                if(isLoadingShimmerActive==true)
                    endLoadingShimmer();
                Log.d("19_juli_2022","getKontenCurhatanOrTanggapan() onErrorResponse "+error.getMessage());
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("kode", kode);
                params.put("filter", filter);
                params.put("pageNum",pageNum);
                params.put("jenis",jenis);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("id",idUser);
                return headers;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end bukaBlokir Pengguna
    }

    @Override
    public void onResume() {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.resume();
            }
        }
        super.onResume();
    }

    @Override
    public void onPause()
    {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.pause();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.destroy();
            }
        }
        super.onDestroy();
    }
}
