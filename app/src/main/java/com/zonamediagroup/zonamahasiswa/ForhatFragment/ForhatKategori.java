package com.zonamediagroup.zonamahasiswa.ForhatFragment;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.gravity.IChildGravityResolver;
import com.beloo.widget.chipslayoutmanager.layouter.breaker.IRowBreaker;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.TampilListKontenForhatByKategori;
import com.zonamediagroup.zonamahasiswa.adapters.KategoriForhatAdapter;
import com.zonamediagroup.zonamahasiswa.adapters.KontenForhatAdapter;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClientForhat;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KategoriForhatModel;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KontenForhatRecyclerView_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ForhatKategori#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForhatKategori extends Fragment implements KategoriForhatAdapter.KategoriForhatAdapterListener, View.OnClickListener {

    Animation fade_in;
    ArrayList<KategoriForhatModel> listKategoriForhat;
    boolean isLoadingShimmerActive, isBtnLanjutkanEnabled = false;
    FrameLayout layout_include_forhat_kategori;
    ImageView icon_chevron_kategori;
    KategoriForhatAdapter kategoriForhatAdapter;
    RecyclerView recycler_kategori_forhat;
    RecyclerView.LayoutManager layoutManagerRecyclerViewKontenForhat;
    RelativeLayout btn_lanjutkan;
    Resources rs;
    ShimmerFrameLayout shimmer_framelayout;
    String idUser;
    TextView txt_btn_kategori;
    ArrayList<KategoriForhatModel> listKategoriForhatSelected;

    private static final int MAXIMUM_CATEGORY_ALLOWED = 5;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.page_forhat_kategori, container, false);
        init(root);
        return root;
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
    }

    private void init(View root){
        initArrayListKontenForhatSelected();
        initAnimation();
        findViewByIdFromRoot(root);
        initRecyclerKategoriForhat();
        setListenerAllComponent();
        setIdUser(getIdUserFromSharedPreferences());
        getKategoriForhat();
        setRs(getResources());
        startLoadingShimmer();
    }

    private void initArrayListKontenForhatSelected() {
        listKategoriForhatSelected = new ArrayList<>();
    }

    public void startLoadingShimmer(){
        isLoadingShimmerActive = true;
        shimmer_framelayout.startShimmerAnimation();
        shimmer_framelayout.setVisibility(View.VISIBLE);
        layout_include_forhat_kategori.setVisibility(View.GONE);
        isLoadingShimmerActive = true;
    }

    private void setRs(Resources rs){
        this.rs = rs;
    }

    private void setListenerAllComponent(){
        btn_lanjutkan.setOnClickListener(this);
    }

    public void setIdUser(String idUser){
        this.idUser = idUser;
    }

    public String getIdUserFromSharedPreferences(){
        return SharedPrefManagerZona.getInstance(getActivity()).getid_user();
    }

    private void findViewByIdFromRoot(View root){
        recycler_kategori_forhat = root.findViewById(R.id.recycler_kategori_forhat);
        shimmer_framelayout = root.findViewById(R.id.shimmer_framelayout);
        layout_include_forhat_kategori = root.findViewById(R.id.layout_include_forhat_kategori);
        btn_lanjutkan = root.findViewById(R.id.btn_lanjutkan);
        icon_chevron_kategori = root.findViewById(R.id.icon_chevron_kategori);
        txt_btn_kategori = root.findViewById(R.id.txt_btn_kategori);
    }

    private void initRecyclerKategoriForhat(){
        listKategoriForhat = new ArrayList<>();
        kategoriForhatAdapter = new KategoriForhatAdapter(getContext(), listKategoriForhat,this);
        RecyclerView.LayoutManager mLayoutManagerkategoriForhat = new GridLayoutManager(getActivity(), 2);
        recycler_kategori_forhat.setLayoutManager(mLayoutManagerkategoriForhat);
        recycler_kategori_forhat.setItemAnimator(new DefaultItemAnimator());
        recycler_kategori_forhat.setAdapter(kategoriForhatAdapter);

        /*start optimization*/
        recycler_kategori_forhat.setHasFixedSize(true);
        recycler_kategori_forhat.setItemViewCacheSize(20);
        recycler_kategori_forhat.setDrawingCacheEnabled(true);
        recycler_kategori_forhat.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        /*end optimization*/
    }


    public void endLoadingShimmer(){
        isLoadingShimmerActive = false;
        shimmer_framelayout.stopShimmerAnimation();
        shimmer_framelayout.setVisibility(View.GONE);
        layout_include_forhat_kategori.startAnimation(fade_in);
        layout_include_forhat_kategori.setVisibility(View.VISIBLE);
    }


    private void getKategoriForhat(){
        InterfaceApi api = ApiClientForhat.getClient().create(InterfaceApi.class);
        Call<JsonObject> call = api.getKategoriForhat();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> responseRetrofit) {
                if(isLoadingShimmerActive==true)
                    endLoadingShimmer();
                try{
                    JSONObject response =new JSONObject(responseRetrofit.body().toString());


                    if (response.getInt("status") == 1)
                    {
                        //cek apakah data adalah jsonArray
                        if(response.get("data") instanceof JSONArray) {
                            JSONArray arrayData = response.getJSONArray("data");
                            setDataKonten(arrayData);
                        }

                    }
                    //dapatkan data konten(array)
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void setDataKonten(JSONArray arrayData){
        listKategoriForhat.addAll(new Gson().fromJson(arrayData.toString(), new TypeToken<List<KategoriForhatModel>>() {}.getType()));
        //notifyDataSetChangeHomeAdapter();
    }

    private void notifyDataSetChangeHomeAdapter(){
        kategoriForhatAdapter.notifyDataSetChanged();
    }






    private void mekanismeCalculateCurrentActiveCategory(){
        int totalActiveCategory = listKategoriForhatSelected.size();
        if(totalActiveCategory>0 && totalActiveCategory<=MAXIMUM_CATEGORY_ALLOWED)
            setStateEnableBtnLanjutkan();
        else
            setStateDisableBtnLanjutkan();
    }


    private void setStateEnableBtnLanjutkan(){
        if(isBtnLanjutkanEnabled == false) {
            isBtnLanjutkanEnabled = true;
            btn_lanjutkan.startAnimation(fade_in);
            btn_lanjutkan.setBackgroundTintList(getContext().getResources().getColorStateList(R.color.color_btn_aksi_utama));
            icon_chevron_kategori.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));
            txt_btn_kategori.setTextColor(getContext().getResources().getColorStateList(R.color.white));
        }
    }


    private void setStateDisableBtnLanjutkan(){
        if(isBtnLanjutkanEnabled == true) {
            isBtnLanjutkanEnabled = false;
            btn_lanjutkan.setBackgroundTintList(null);
            icon_chevron_kategori.setColorFilter(ContextCompat.getColor(getContext(), R.color.color3));
            txt_btn_kategori.setTextColor(getContext().getResources().getColorStateList(R.color.color3));
        }
    }

    private void mekanismeKlikNext(){
        if(isBtnLanjutkanEnabled == true)
        {
            openActivityTampilListKontenForhatByKategori(getFormatParamKategori(listKategoriForhatSelected));
        }
    }

    private String getFormatParamKategori(ArrayList<KategoriForhatModel> listKategoriForhatSelected){
        String hasil = "";
        for(int i=0;i<listKategoriForhatSelected.size();i++){
            int lastDataIndex = listKategoriForhatSelected.size()-1;
            if(i != lastDataIndex)
            {
                hasil = hasil + listKategoriForhatSelected.get(i).getId()+ ",";
            }
            else
            {
                hasil = hasil + listKategoriForhatSelected.get(i).getId();
            }
        }
        return hasil;
    }

    private void openActivityTampilListKontenForhatByKategori(String paramKategori){
       Intent i = new Intent(getContext(), TampilListKontenForhatByKategori.class);
       i.putExtra("kategori",paramKategori);
       startActivity(i);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id)
        {
            case R.id.btn_lanjutkan:
                mekanismeKlikNext();
                break;
        }
    }

    @Override
    public void addCategorySelected(KategoriForhatModel kategoriForhatModel) {
        listKategoriForhatSelected.add(kategoriForhatModel);
        mekanismeCalculateCurrentActiveCategory();
    }

    @Override
    public void removeCategoryUnSelected(KategoriForhatModel kategoriForhatModel) {
        listKategoriForhatSelected.remove(kategoriForhatModel);
        mekanismeCalculateCurrentActiveCategory();
    }
}