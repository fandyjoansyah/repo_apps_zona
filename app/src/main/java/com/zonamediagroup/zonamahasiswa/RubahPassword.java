package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class RubahPassword extends AppCompatActivity {
    RelativeLayout btn_back_lupa_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_rubah_password);
        findViewByIdAllComponent();
        setListenerAllComponent();
    }

    private void findViewByIdAllComponent(){
        btn_back_lupa_password = findViewById(R.id.btn_back_lupa_password);
    }

    private void setListenerAllComponent(){
        btn_back_lupa_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}