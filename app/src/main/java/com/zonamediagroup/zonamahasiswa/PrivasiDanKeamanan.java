package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PrivasiDanKeamanan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_activity_privasi_dan_keamanan);
        TextView daftarBlokir;
        daftarBlokir = findViewById(R.id.daftar_blokir);
        daftarBlokir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DaftarBlokir.class);
                startActivity(intent);
            }
        });
    }
}