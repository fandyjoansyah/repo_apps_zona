package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class WebViewZona extends AppCompatActivity {
    private WebView myWebView;
    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;
    LinearLayout eror_layar;
    boolean redirect = false;
    String link = "";
    String judul="Zona Mahasiswa"; //default = zona mahasiswa apabila tidak extra dari activity lain
    TextView tvJudul;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        terimadata();
        setContentView(R.layout.page_web_view_zona);
        //        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);
        tvJudul = findViewById(R.id.webview_txt_judul);
        tvJudul.setText(judul);


        myWebView = findViewById(R.id.webview_zona);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl(link);
        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                if (loding_layar.getVisibility() == View.VISIBLE) {
                    redirect = true;
                }

                loading_show();
                myWebView.loadUrl(urlNewString);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loading_show();
            }



            @Override
            public void onPageFinished(WebView view, String url) {
                if (!redirect) {
                    loading_dismiss();
                    //HIDE LOADING IT HAS FINISHED
                } else {
                    redirect = false;
                }
            }
        });

        back = findViewById(R.id.back_web_view_zona);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void terimadata() {
        Bundle data = getIntent().getExtras();
        link = data.getString("link");
        judul = data.getString("pageTitle");
    }

    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if(myWebView.canGoBack()){
            myWebView.goBack();
        } else {
            super.onBackPressed();
        }

    }


}