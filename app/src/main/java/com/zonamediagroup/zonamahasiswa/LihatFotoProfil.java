package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class LihatFotoProfil extends AppCompatActivity {
    String urlFotoProfil;
    ImageView img_foto_profil,logo_download;
    RelativeLayout btn_download_foto_profil;
    String fotoProfileSharedPref;
    ProgressBar loading_img_profile;
    private static final int STORAGE_PERMISSION_CODE = 19;
    Resources rs;
    ProgressBar pb_loading_download;
    String urlDownloadFotoProfil;
    private static final String URL_REQUEST_DEFAULT_FOTO_PROFIL = Server.URL_REAL + "get_default_foto_profil";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_lihat_foto_profil);
        findViewByIdAllComponent();
        setListenerAllComponent();
        getAndroidResources();
        setFotoProfilPengguna();
        stateBtnDownloadTidakSedangDownload();
    }

    private void getAndroidResources(){
        rs = getResources();
    }

    public void setFotoProfilPengguna(){
      fotoProfileSharedPref = getCurrentProfilePictureFromSharedPreferences();
      if(fotoProfileSharedPref.equals("") || fotoProfileSharedPref == null)
        {
            getDefaultFotoProfilFromServer();
        }
        else
        {
            setProfilePictureToImageView(fotoProfileSharedPref);
        }
    }

    private String getCurrentProfilePictureFromSharedPreferences(){
        return  SharedPrefManagerZona.getInstance(LihatFotoProfil.this).geturl_voto();
    }
    public void setProfilePictureToImageView(String url)
    {
        Glide.with(LihatFotoProfil.this)
                .load(url)
                .apply(RequestOptions.fitCenterTransform())
                .placeholder(R.drawable.ic_no_image_available)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        hideButtonDownload();
                        hideLoadingImgProfile();
                        CustomToast.makeBlackCustomToast(LihatFotoProfil.this,rs.getString(R.string.terjadi_kesalahan_saat_mencoba_mendapatkan_foto_profil), Toast.LENGTH_SHORT);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        showButtonDownload();
                        hideLoadingImgProfile();
                        return false;
                    }
                })
                .into(img_foto_profil);
    }

    /* get default foto profil pengguna ketika tidak ditemukan foto profil */
    private void getDefaultFotoProfilFromServer(){
        AndroidNetworking.get(URL_REQUEST_DEFAULT_FOTO_PROFIL)
                .addQueryParameter("TOKEN", "qwerty")
                .setTag("get default foto profil")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String url =  response.getString("url_foto_profil");
                            setProfilePictureToImageView(url);
                        } catch (JSONException e) {

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideLoadingImgProfile();
                        CustomToast.makeBlackCustomToast(LihatFotoProfil.this,rs.getString(R.string.permintaan_gagal_dieksekusi), Toast.LENGTH_SHORT);
                        // handle error
                    }
                });
    }


    public void findViewByIdAllComponent(){
        img_foto_profil = findViewById(R.id.img_foto_profil);
        btn_download_foto_profil = findViewById(R.id.btn_download_foto_profil);
        loading_img_profile = findViewById(R.id.loading_img_profile);
        logo_download = findViewById(R.id.logo_download);
        pb_loading_download = findViewById(R.id.pb_loading_download);
    }

    public void setListenerAllComponent(){
        btn_download_foto_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismeDownloadFotoProfil(fotoProfileSharedPref);
            }
        });
    }

    public void mekanismeDownloadFotoProfil(String url) {
        urlDownloadFotoProfil = url;
        boolean permissionDownloadGranted = checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,STORAGE_PERMISSION_CODE);
        /* apabila permission sudah diberikan, maka jalankan proses download */
        if(permissionDownloadGranted == true)
        {
                downloadFotoProfil(urlDownloadFotoProfil);
        }
        /* apabila permission belum diberikan, maka request permission */
        else
        {
            requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,STORAGE_PERMISSION_CODE);
        }
    }



    /* fungsi untuk cek runtime permission sudah diberikan atau belum */
    public boolean checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(LihatFotoProfil.this, permission) == PackageManager.PERMISSION_DENIED)
            return false;
        else
            return true;
    }

    /* fungsi untuk meminta runtime permission */
    public void requestPermission(String permission, int permissionCode){
        requestPermissions(new String[]{
                permission}, permissionCode);
    }





    public void hideLoadingImgProfile(){
        loading_img_profile.setVisibility(View.GONE);
    }

    public String getFilePath(String url){
        return getAbsolutePathDCIMDirectory()+"/"+getLastSegmentForName(url);
    }

    public String getAbsolutePathDCIMDirectory(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
    }

    public String getLastSegmentForName(String url){
        return Uri.parse(url).getLastPathSegment();
    }



    public boolean cekFotoProfilSudahAda(String urlDownloadFile){
        File f = new File(getFilePath(urlDownloadFile));
        boolean gambarSudahAda = false;
        if(f.exists())
        {
            gambarSudahAda = true;
        }
        else
        {
            gambarSudahAda = false;
        }

        return gambarSudahAda;
    }

    public void downloadFotoProfil(String url){
        Log.d("11_juni_2022","url: "+url);
        if(cekFotoProfilSudahAda(url)!=true)
        {
            stateBtnDownloadSedangDownload();
            new DownloadFileFromURL().execute(url);
        }
        else
        {
            CustomToast.makeBlackCustomToast(LihatFotoProfil.this,rs.getString(R.string.foto_profil_sudah_ada_di_galerimu),Toast.LENGTH_LONG);
        }


//        String dirPath =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
//        String fileName = "fan_fandy.jpg";
//        AndroidNetworking.download(url,dirPath,fileName)
//                .setTag("downloadTest")
//                .setPriority(Priority.MEDIUM)
//                .build()
//                .setDownloadProgressListener(new DownloadProgressListener() {
//                    @Override
//                    public void onProgress(long bytesDownloaded, long totalBytes) {
//                        // do anything with progress
//                    }
//                })
//                .startDownload(new DownloadListener() {
//                    @Override
//                    public void onDownloadComplete() {
//                        // do anything after completion
//                        Log.d("11_juni_2022","onDownloadComplete()");
//                    }
//                    @Override
//                    public void onError(ANError error) {
//                        Log.d("11_juni_2022","onError() errorBody: "+error.getErrorBody());
//                        Log.d("11_juni_2022","onError() errorDetail: "+error.getErrorDetail());
//                        Log.d("11_juni_2022","onError() message: "+error.getMessage());
//
//                    }
//                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadFotoProfil(urlDownloadFotoProfil);
            } else {
                //gagal memberikan permisi
                CustomToast.makeBlackCustomToast(LihatFotoProfil.this,rs.getString(R.string.aktifkan_izin_penyimpananmu_terlebih_dahulu),Toast.LENGTH_LONG);
            }
        }
    }


    // Download from URL
    class DownloadFileFromURL extends AsyncTask<String, Integer, String> {
        String namaFile;
        boolean adaError = false;

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... arrStringParam) {

            try {
                URL url = new URL(arrStringParam[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // download the file
                InputStream input =connection.getInputStream();


                // Create File directo
//                    Write file
                String nama_dan_lokasi_file_download_tujuan = getFilePath(arrStringParam[0]);
                OutputStream output = new FileOutputStream(nama_dan_lokasi_file_download_tujuan);

                byte data[] = new byte[1024];
                int count;
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }


                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();



            } catch (Exception e) {
                Log.d("11_juni_2022","Exception async: "+e.getMessage());
                adaError = true;
            }


            return null;
        }








        @Override
        protected void onPostExecute(String file_url) {
            Log.d("11_juni_2022","onPostExecute");

            if(adaError == true)
                CustomToast.makeBlackCustomToast(LihatFotoProfil.this,rs.getString(R.string.permintaan_gagal_dieksekusi),Toast.LENGTH_LONG);
            else
                CustomToast.makeBlackCustomToast(LihatFotoProfil.this,rs.getString(R.string.download_selesai),Toast.LENGTH_LONG);


            stateBtnDownloadTidakSedangDownload();
        }


    }

    private void hideButtonDownload(){
        btn_download_foto_profil.setVisibility(View.GONE);
    }

    private void showButtonDownload(){
        btn_download_foto_profil.setVisibility(View.VISIBLE);
    }

    private void showProgressBarBtnDownload(){
        pb_loading_download.setVisibility(View.VISIBLE);
    }

    private void hideProgressBarBtnDownload(){
        pb_loading_download.setVisibility(View.GONE);
    }

    private void showLogoBtnDownload(){
        logo_download.setVisibility(View.VISIBLE);
    }

    private void hideLogoBtnDownload(){
        logo_download.setVisibility(View.GONE);
    }

    private void stateBtnDownloadTidakSedangDownload(){
        showLogoBtnDownload();
        hideProgressBarBtnDownload();
    }
    private void stateBtnDownloadSedangDownload(){
        hideLogoBtnDownload();
        showProgressBarBtnDownload();
    }

}