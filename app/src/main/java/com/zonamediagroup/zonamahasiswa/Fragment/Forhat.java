package com.zonamediagroup.zonamahasiswa.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.zonamediagroup.zonamahasiswa.R;


public class Forhat extends Fragment {
    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.page_fragment_forhat, container, false);
        init(root);
        return root;

    }

    private void init(View root) {
//        Loading
        loding_layar = root.findViewById(R.id.loding_layar);
        image_animasi_loading = root.findViewById(R.id.image_animasi_loading);


//        eror
        eror_layar = root.findViewById(R.id.eror_layar);
        coba_lagi = root.findViewById(R.id.coba_lagi);
        loading_dismiss();
    }

    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }
}
