package com.zonamediagroup.zonamahasiswa.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.Download_file;
import com.zonamediagroup.zonamahasiswa.IklanInterstitial;
import com.zonamediagroup.zonamahasiswa.Kirim_file;
import com.zonamediagroup.zonamahasiswa.LihatFotoProfil;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.PendaftaranAkun;
import com.zonamediagroup.zonamahasiswa.PollingActivity;
import com.zonamediagroup.zonamahasiswa.Post_by_kategori;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.RubahFotoProfil;
import com.zonamediagroup.zonamahasiswa.RuleValueAplikasi;
import com.zonamediagroup.zonamahasiswa.Save;
import com.zonamediagroup.zonamahasiswa.Search;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Kategori_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Kategori_Model;
import com.zonamediagroup.zonamahasiswa.tooltip.OverlayView;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltip;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltipUtils;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZonaController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Kategori extends Fragment implements Kategori_Adapter.ContactsAdapterListener {

    private String URL_home = Server.URL_REAL + "Kategori_fix/?TOKEN=qwerty";

    private static final String TAG = Kategori.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";

    // item kedua
    RecyclerView receler_dua;
    private List<Kategori_Model> duaModels;
    private Kategori_Adapter duaAdapter;

    LinearLayout menu_polling,menu_rubrik,menu_template;

    ShapeableImageView img_template;
    ImageView img_class_program;
    CardView cardview_template, cardview_polling, cardview_rubrik;

    Animation fade_in;

            // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok;

    private ToolTipsZona[] arrToolTipsKategori;

    private InterstitialAd iklan;


    //    iklan


    //    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/5224354917";
    private static String AD_UNIT_ID = "";
    private static final long COUNTER_TIME = 10;

    private int coinCount;

    private CountDownTimer countDownTimer;
    private boolean gameOver;
    private boolean gamePaused;

    private RewardedAd rewardedAd;

    private long timeRemaining;
    boolean isLoading;
    Kategori_Model item_dua;



    String id;

    int cGet;
    private ArrayList<ToolTipsZona> listToolTipsZona;
    SharedPreferences pref;

    AlphaAnimation buttonClickAnimation;

    BottomSheetBehavior mBottomSheetBehavior;

    LinearLayout layout_bottomSheet_kategori;

    View bottomSheet;

    RelativeLayout area_biru;
//    iklan end

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.page_kategori, container, false);
        AD_UNIT_ID = getResources().getString(R.string.ad_unit_id_interstitial);

        init(root);

        return root;
    }

    private int getTinggiLayar(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        return height;
    }


    void init(View root) {
            Log.d(MainActivity.MYTAG,"init kategori");
            cGet = Request.Method.GET;
//        item KATEGORI
        receler_dua = root.findViewById(R.id.receler_dua);
        area_biru = root.findViewById(R.id.area_biru);
        img_class_program = root.findViewById(R.id.img_class_program);
        duaModels = new ArrayList<>();
        duaAdapter = new Kategori_Adapter(getContext(), duaModels, this,getActivity());
        RecyclerView.LayoutManager mLayoutManagerkategorix = new GridLayoutManager(getActivity(), 4);
        receler_dua.setLayoutManager(mLayoutManagerkategorix);
        receler_dua.setItemAnimator(new DefaultItemAnimator());

        //init animation
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_out_to_04);
         buttonClickAnimation = new AlphaAnimation(1F, 0.4F);

         showBottomSheet(root);
        // setTinggiBottomSheet();

        receler_dua.setItemViewCacheSize(30);
        receler_dua.setDrawingCacheEnabled(true);
        receler_dua.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        receler_dua.setAdapter(duaAdapter);

        pref = getContext().getSharedPreferences("zonamhs", 0); // 0 - for private mode


        setShowTimeForShowIklanVideoOnBottomNavigationKategori();

       // img_template = root.findViewById(R.id.img_template);
      //  cardview_template = root.findViewById(R.id.cardview_template);
//        cardview_polling = root.findViewById(R.id.cardview_polling);
//        cardview_rubrik = root.findViewById(R.id.cardview_rubrik);

        menu_polling = root.findViewById(R.id.menu_polling);
        menu_template = root.findViewById(R.id.menu_template);
        menu_rubrik = root.findViewById(R.id.menu_rubrik);


//        Loading
        loding_layar = root.findViewById(R.id.loding_layar);
        image_animasi_loading = root.findViewById(R.id.image_animasi_loading);

//        eror
        eror_layar = root.findViewById(R.id.eror_layar);
        coba_lagi = root.findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
            loading_show();
                get_kategori();

            }
        });

        search = root.findViewById(R.id.search);
        saveok = root.findViewById(R.id.saveok);


        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Search.class);
                startActivity(intent);
            }
        });

        eror_dismiss();
     loading_show();

//        Call API
         get_kategori();

        setMenuUtama();

    }

    private void showBottomSheet(View root){
     bottomSheet = root.findViewById(R.id.bottom_sheet);
    mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(getTinggiLayar()/3*2);
    }



    public void setMenuUtama(){
        initTemplate();
        initRubrik();
        initPolling();
        initClassProgram();
    }

    public void initTemplate(){


        menu_template.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_template.startAnimation(buttonClickAnimation);
                Intent intent = new Intent(getActivity(), Download_file.class);
                startActivity(intent);
            }
        });
    }

    public void initRubrik(){


       menu_rubrik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_rubrik.startAnimation(buttonClickAnimation);
                Intent intent = new Intent(getActivity(), Kirim_file.class);
                startActivity(intent);
            }
        });
    }

    public void initPolling(){


        menu_polling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_polling.startAnimation(buttonClickAnimation);
                Intent intent = new Intent(getActivity(), PollingActivity.class);
                startActivity(intent);
            }
        });
    }

    public void initClassProgram(){
        //set gambar template
        Glide.with(getContext())
                .load("https://zonamahasiswa.id/wp-content/uploads/apps_banner/btn_cp_gradient.png")
                .apply(RequestOptions.fitCenterTransform())
                .into(img_class_program);

        img_class_program.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_class_program.startAnimation(buttonClickAnimation);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://classprogram.id/"));
                startActivity(intent);
            }
        });
    }




    //atur waktu tampil iklan agar tampil hanya setelah x menit, lalu show iklan
    public void setShowTimeForShowIklanVideoOnBottomNavigationKategori(){
        Log.d("fandy_29_april","ssT bottom navigation kategori");
        //start iklan
        int intervalMenit = RuleValueAplikasi.durasiIklan;
        String labelSharedPref = "waktu_iklan_bottom_navigation_kategori_terakhir";
        //start pref
        if (pref.contains(labelSharedPref) == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
        {
            //jika tidak ada sharedpref iklan tampil terakhir, maka sharedpref iklan tampil terakhir = now
            pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
            showIklanVideoOnBottomNavigationKategori();
        }
        else //jika sudah ada waktu iklan terakhir
        {
            String waktuTerakhirIklanTampil = pref.getString(labelSharedPref,"");
            String waktuSekarang = IklanInterstitial.getCurrentDateTime();
            long selisihWaktu = IklanInterstitial.substractTwoDateTimes(waktuTerakhirIklanTampil,waktuSekarang);
            Log.d("2 Februari","2feb selisih waktu: "+selisihWaktu);
            if(selisihWaktu > intervalMenit)
            {
                Log.d("fandy_29_april","ssT  if 237 visited");
                showIklanVideoOnBottomNavigationKategori();
                pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
                Log.d("2 Februari","2feb ifku: ");
            }
            else {
                Log.d("fandy_29_april","ssT  else 243 visited");
                Log.d("2 Februari","2feb elseku: ");
            }
        }
        //end pref
        //end iklan

    }

    //show iklan bottomNavigationKategori
    public void showIklanVideoOnBottomNavigationKategori(){
        IklanInterstitial.iklanVideo.show();
    }








    private void get_kategori() {


        StringRequest strReq = new StringRequest(cGet, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {


                        JSONArray dataArray2 = jObj.getJSONArray("item_kedua");

                        List<Kategori_Model> itemsdua = new Gson().fromJson(dataArray2.toString(), new TypeToken<List<Kategori_Model>>() {
                        }.getType());


                        duaModels.clear();
                        duaModels.addAll(itemsdua);


                        // add one index last

                        String dataArray = new JSONArray()
                                .put(new JSONObject()
                                        .put("id_kategori", "yogi")
                                        .put("id_categori_wp", "yogi")
                                        .put("nama_kategori", "Template Makalah, Skripsi, PPT")
                                        .put("gambar_kategori", "https://zonamahasiswa.id/wp-content/uploads/apps_icon/v1/templatemakalah.png")
                                        .put("warna_kategori", "#5286EC")
                                )

                                .toString();



                        List<Kategori_Model> items_dobel = new Gson().fromJson(dataArray, new TypeToken<List<Kategori_Model>>() {
                        }.getType());

    /* start comment template di recycler view
                        duaModels.addAll(items_dobel);


                        //start swap position
                        Collections.swap(duaModels,(duaModels.size()-1),(duaModels.size()-2));
                        //end swap position
end comment template di recycler view*/
                        duaAdapter.notifyDataSetChanged();

                        loading_dismiss();
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();

                    }
//                    doToolTipsKategori();
//                    initToolTipsKategori();
//                    runToolTipsKategori();

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");

                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());
//                eror_show();
            }
        });

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    @Override
    public void onContactSelected(Kategori_Model itemdua) {
        item_dua = itemdua;
        loading_show();
//      loadRewardedAd();
        setShowTimeForshowIklanVideoOnKategori();


    }



    public void setShowTimeForshowIklanVideoOnKategori(){
        //start iklan
        int intervalMenit = RuleValueAplikasi.durasiIklan;
        String labelSharedPref = "waktu_iklan_kategori_terakhir";
        //start pref
        if (pref.contains(labelSharedPref) == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
        {
            //jika tidak ada sharedpref iklan tampil terakhir, maka sharedpref iklan tampil terakhir = now
            pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
            showIklanVideoOnKategori();
        }
        else //jika sudah ada waktu iklan terakhir
        {
            String waktuTerakhirIklanTampil = pref.getString(labelSharedPref,"");
            String waktuSekarang = IklanInterstitial.getCurrentDateTime();
            long selisihWaktu = IklanInterstitial.substractTwoDateTimes(waktuTerakhirIklanTampil,waktuSekarang);
            Log.d("2 Februari","2feb selisih waktu: "+selisihWaktu);
            if(selisihWaktu > intervalMenit)
            {
                showIklanVideoOnKategori();
                pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
                Log.d("2 Februari","2feb ifku: ");
            }
            else {
                Log.d("2 Februari","2feb elseku: ");
                bukaMenu();
            }
        }
        //end pref
        //end iklan


    }

    //show iklan untuk icon kategori artikel ketika di klik
    public void showIklanVideoOnKategori(){
        //start coding iklan
        //tampilkan iklan full screen
        InterstitialAd intersAd = IklanInterstitial.iklanVideo;
        if(intersAd.isLoaded()) {
            Log.d("fandy_28april","if 363");
            intersAd.show();
            intersAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    //load another ad
                    intersAd.loadAd(new AdRequest.Builder().build());

                    //pindah activity
                    bukaMenu();
                }
            });
        }
        else
        {
            Log.d("fandy_28april","else_378");
            bukaMenu();
        }
        //end coding iklan
    }


//    @Override
    public void doToolTipsKategori(View anchorView, Context ctxtx)
    {
        if(getContext() != null) {
            if (pref.contains("kategoriToolTips") == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
            {
                initToolTipsKategori(anchorView, ctxtx);
                runToolTipsKategori();
                pref.edit().putInt("kategoriToolTips",1).commit();
            }
        }
    }

    public void initToolTipsKategori(View anchorView,Context ctxtx)
    {
//        Log.d(MainActivity.MYTAG,"init di kategori");
        listToolTipsZona = new ArrayList<>();
        String[] pesanToolTips = {"Download template, makalah, skripsi & PPT di sini"};
        String[] textButtonToolTips = {"Selesai"};
        int[] gravityToolTips = {Gravity.TOP};
        //jCOunter yang akan dipakai sebagai index oleh local array
        int jCounter=0;
        for(int i = 0; i< 1;i++) {
//            if(MainActivity.allToolTips.get(i).getFragment().equals("kategori")) {
                listToolTipsZona.add(new ToolTipsZona(ctxtx,
                        pesanToolTips[jCounter],
                        textButtonToolTips[jCounter],
                        anchorView,
                        true,
                        R.layout.tooltip_small,
                        R.id.tv_text,
                        R.id.btn_next,
                        gravityToolTips[jCounter],
                        "Kategori",
                        "kategori"
                ));
                jCounter++;
            receler_dua.scrollTo(0, -500);
//            }
        }
    }

    private void runToolTipsKategori()
    {
        new ToolTipsZonaController().setMultipleToolTipsAnchorView(listToolTipsZona);
    }


    private void bukaMenu(){
        id = item_dua.getId_categori_wp();

        if (id.equals("yogi")) {
            Intent intent = new Intent(getActivity(), Download_file.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), Post_by_kategori.class);
            intent.putExtra("id_kategori", item_dua.getId_kategori());
            intent.putExtra("id_categori_wp", item_dua.getId_categori_wp());
            intent.putExtra("nama_kategori", item_dua.getNama_kategori());
            intent.putExtra("gambar_kategori", item_dua.getGambar_kategori());
            intent.putExtra("warna_kategori", item_dua.getWarna_kategori());

            startActivity(intent);
        }
        loading_dismiss();
    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    // iklan
    private void loadRewardedAd() {
        if (rewardedAd == null || !rewardedAd.isLoaded()) {
            rewardedAd = new RewardedAd(getContext(), AD_UNIT_ID);
            isLoading = true;
            rewardedAd.loadAd(
                    new AdRequest.Builder().build(),
                    new RewardedAdLoadCallback() {
                        @Override
                        public void onRewardedAdLoaded() {
                            // Ad successfully loaded.
                            isLoading = false;
                            //Toast.makeText(Point.this, "onRewardedAdLoaded", Toast.LENGTH_SHORT).show();
                            //startGame();
                           showRewardedVideo();
                        }

                        @Override
                        public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
                            // Ad failed to load.
                            isLoading = false;
                            //Toast.makeText(Point.this, "on Rewarded Ad FailedTo Load", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(Kirim_file.this, "Failed To Load", Toast.LENGTH_SHORT).show();
                            System.out.println("error load " + loadAdError);
                        }
                    });
        }
    }

    private void startGame() {
// Run Coll down
        if (!rewardedAd.isLoaded() && !isLoading) {
            loadRewardedAd();

        }
        createTimer(COUNTER_TIME);
        gamePaused = false;
        gameOver = false;

    }

    private void createTimer(long time) {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer =
                new CountDownTimer(time * 100, 1) {
                    @Override
                    public void onTick(long millisUnitFinished) {
                        timeRemaining = ((millisUnitFinished / 1000) + 1);
                        //Toast.makeText(Point.this, "Remaining " + timeRemaining, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFinish() {
                        if (rewardedAd.isLoaded()) {

                            showRewardedVideo();
                        }

                        gameOver = true;
                    }
                };
        countDownTimer.start();
    }

    private void showRewardedVideo() {
        //    showVideoButton.setVisibility(View.INVISIBLE);
        if (rewardedAd.isLoaded()) {
            RewardedAdCallback adCallback =
                    new RewardedAdCallback() {
                        @Override
                        public void onRewardedAdOpened() {
                            // Ad opened.

                        }

                        @Override
                        public void onRewardedAdClosed() {
                            // Ad closed.

                            id = item_dua.getId_categori_wp();

                            if (id.equals("yogi")) {
                                Intent intent = new Intent(getActivity(), Download_file.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(getActivity(), Post_by_kategori.class);
                                intent.putExtra("id_kategori", item_dua.getId_kategori());
                                intent.putExtra("id_categori_wp", item_dua.getId_categori_wp());
                                intent.putExtra("nama_kategori", item_dua.getNama_kategori());

                                intent.putExtra("gambar_kategori", item_dua.getGambar_kategori());
                                intent.putExtra("warna_kategori", item_dua.getWarna_kategori());

                                startActivity(intent);
                            }
                            loading_dismiss();
                        }

                        @Override
                        public void onUserEarnedReward(RewardItem rewardItem) {
                            // User earned reward.

                            // download disini ya
                            // Call API
                            //start setup progress dialog

                            //end setup progress dialog
                            // uploadPDF(displayName, uri);

                        }

                        @Override
                        public void onRewardedAdFailedToShow(AdError adError) {
                            // Ad failed to display
                            //  Log.e("Kirim_file","onRewardedAdFailedToShow "+adError.getMessage());
                        }
                    };
            rewardedAd.show(getActivity(), adCallback);
        }
    }

    // END IKLAN
//    private void initToolTipsKategori()
//    {
//        ToolTipsZona tooltipsKategori1 =  new ToolTipsZona(getActivity(),"Download template makalah, skripsi dan lainnya disini!","Selesai",R.id.receler_dua,true,R.layout.tooltip_small,R.id.tv_text,R.id.btn_next, Gravity.TOP,"Kategori","");
//        arrToolTipsKategori = new ToolTipsZona[]{tooltipsKategori1};
//    }
//
//
//
//    private void runToolTipsKategori()
//    {
//        new ToolTipsZonaController().setMultipleToolTips(arrToolTipsKategori);
//    }
}


