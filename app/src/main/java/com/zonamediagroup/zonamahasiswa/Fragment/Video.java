package com.zonamediagroup.zonamahasiswa.Fragment;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.CurrentActiveVideoData;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.Download_file;
import com.zonamediagroup.zonamahasiswa.IklanInterstitial;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.MimeType;
import com.zonamediagroup.zonamahasiswa.PaginationListener;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.RuleValueAplikasi;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.SwipeVideo;
import com.zonamediagroup.zonamahasiswa.Video_Notif;
import com.zonamediagroup.zonamahasiswa.Video_save;
import com.zonamediagroup.zonamahasiswa.adapters.Notif_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.SwipeVideo_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Category_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Category_Sub_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Rekomendasi_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Category_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Category_Sub_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Model;
import com.zonamediagroup.zonamahasiswa.tooltip.OverlayView;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltip;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltipUtils;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZonaController;
import com.zonamediagroup.zonamahasiswa.utils.ExoPlayerRecyclerViewAUTOPREVIEW;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Video newInstance} factory method to
 * create an instance of this fragment.
 */
public class Video extends Fragment implements Video_Adapter.VideoListener,Video_Category_Adapter.VideoListener , Video_Category_Sub_Adapter.VideoListener, Komentar_Video_Level1_Adapter.KomentarLevel1{

    LinearLayout loding_layar;
    RecyclerView recyclerCategory, recyclerCategorySub;
    ExoPlayerRecyclerViewAUTOPREVIEW recyclerVideo;
    ArrayList<Video_Model>dataVideo;
    List<Video_Category_Model>dataCategory;
    List<Video_Category_Sub_Model>dataCategorySub;
    Video_Adapter adapterVideo;
    Video_Category_Adapter adapterCategory;
    Video_Category_Sub_Adapter adapterCategorySub;

    LinearLayout layoutloading_bar;

    //folder lokasi video
    String lokasiFolder = "Video Zona Mahasiswa";

    //untuk video
    String url_file;
    String nama_file;
    String nextIdVideo="";
    String categoryId="";
    String categorySubId="";
    int jCounter=0;

    private ArrayList<ToolTipsZona> listToolTipsVideo;

    LinearLayoutManager mLayoutManagerVideo;

    //ProgressBar (spinkit)
    ProgressBar loadingBola;

    //Sprite
    Sprite animasiSprite;

    String nama_dan_lokasi_file_download="";

    // eror layar
    LinearLayout eror_layar;

    TextView coba_lagi;

    TextView ket_list_video_habis;


    //variabel untuk digunakan ketika permisi baru diberikan di runtime
    String urlVideoGlobal = "";
    String namaVideoGlobal = "";
    View viewCurrentRecycler;
    VideoView videoCurrentRecycler;
    NestedScrollView nestedScrollView;
    public static int targetPosition;

    //boolean pengatur anim load more video
    boolean isShowBallLoading = false;

    private static final String URL_HALAMAN_UTAMA_VIDEO = Server.URL_HALAMAN_UTAMA_VIDEO;

    private static final String URL_GET_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment/index_post";

    private static final String URL_LIKE_VIDEO = Server.URL_REAL_VIDEO+"Video_like/index_post";

    private static final String URL_TAMBAH_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_create/index_post";

    private static final String URL_BALAS_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_reply_create/index_post";


    private static final String URL_SAVE_VIDEO = Server.URL_REAL_VIDEO+"Video_save_create/index_post";

    private static final String URL_KATEGORI_VIDEO = Server.URL_REAL_VIDEO+"Video_category/index_post";

    private static final String URL_SUB_KATEGORI_VIDEO = Server.URL_REAL_VIDEO+"Video_category_sub/index_post";

    private static final String URL_NOTIF_COUNT_VIDEO = Server.URL_REAL_VIDEO+"Video_notification_count/index_post";

    private static final String TAG = "VideoFragment";

    Activity mActivity;

    private static final int STORAGE_PERMISSION_CODE = 19;

    Komentar_Video_Level1_Adapter commentAdapterLevel1;

    RecyclerView recyclerCommentInBottomSheet;

    BottomSheetDialog bottomSheetDialog,bottomSheetDialogBalasKomentar;
    ProgressBar pbBottomSheet;

    EditText txt_komentar_video;

    TextView txt_bottomsheet_komentar_kosong, text_count_notif;

    ImageView send_button, gambar_komentator_video;

    ImageView gambar_komentator;

    ArrayList<Komentar_Video_Model> dataRecyclerKomentarBottomSheet;

    ImageView icon_video_notif, icon_video_save;

    int progressMax = 100;

    private int videoSurfaceDefaultHeight = 0;
    private int screenDefaultHeight = 0;
    private boolean isVideoViewAdded;

    Integer ShowHideNavbar = 1;
    LinearLayout linear_kategori;
    float scale = 0;
    SharedPreferences pref;

    private Handler handler = new Handler();

    Button btnDownload;
    public static final String CHANNEL_2_ID = "channel2";
    private NotificationManagerCompat notificationManager;
    int currentIdNotif = 1;

    Runnable runnable;

    private static final int REQUEST_CODE_OPEN_SAVE = 1;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.page_fragment_video, container, false);

        init(root);
        checkIfNull();
        return root;

    }

    private void init(View root) {
        //inisialisasi widget
        recyclerVideo = root.findViewById(R.id.recycler_list_video);


        //komponen recycler view
        dataVideo = new ArrayList<>();
        adapterVideo = new Video_Adapter(getContext(), dataVideo, this);

        linear_kategori = root.findViewById(R.id.linear_kategori);
        pref = getContext().getSharedPreferences("zonamhs", 0); // 0 - for private mode
        //setup recycler view
        mLayoutManagerVideo = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recyclerVideo.setLayoutManager(mLayoutManagerVideo);
        recyclerVideo.setNestedScrollingEnabled(false);
        recyclerVideo.setItemAnimator(new DefaultItemAnimator());
        //start autoplay video

        scale = getActivity().getResources().getDisplayMetrics().density;
        recyclerVideo.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                //lakukan sesuatu ketika berhenti scroll
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//
//                    // There's a special case when the end of the list has been reached.
//                    // Need to handle that with this bit of logic
//                    if(!recyclerView.canScrollVertically(1)){
//                        playVideo(true);
//                    }
//                    else{
//                        playVideo(false);
//                    }
//                }
//            }
//


            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

//                LinearLayoutManager mLayoutManagerVideo = (LinearLayoutManager) recyclerVideo.getLayoutManager();
//                int pos = mLayoutManagerVideo.findFirstVisibleItemPosition();
//                if(mLayoutManagerVideo.findViewByPosition(pos).getTop()==0 && pos==0){
//                    slideDown();
//                }

                setShowTimeForShowIklanVideoOnBottomNavigationVideo();
                // To check if at the top of recycler view
                if(isRecyclerViewAtTop()){
                    Integer number = categorySubId.equals("") || categorySubId.equals("0") ? 115 : 155;
                    int atas = (int) (number * scale + 0.5f);
                    recyclerVideo.setPadding(0,atas,0,0);
                    slideDown();
                }
                if (dy > 20) {
                    // Scrolling up
//                    Log.d("cobascroll", "upupup "+dy);
                    slideUp();
                } else if(dy < -20){
                    // Scrolling down
//                    Log.d("cobascroll", "downdowndown "+dy);
                    slideDown();
                }
            }
        });
        //end autoplay video
        recyclerVideo.setVideo_Models(dataVideo);
        recyclerVideo.setAdapter(adapterVideo);

        //inisialisasi widget Category
        recyclerCategory = root.findViewById(R.id.recycler_list_Category);

        //komponen recycler view Category
        dataCategory = new ArrayList<>();
        adapterCategory = new Video_Category_Adapter(getContext(), dataCategory, this);

        //setup recycler view Category
        LinearLayoutManager mLayoutManagerVideoCategory;
        mLayoutManagerVideoCategory = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        recyclerCategory.setLayoutManager(mLayoutManagerVideoCategory);
        recyclerCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerCategory.setAdapter(adapterCategory);

        //inisialisasi widget Category Sub
        recyclerCategorySub = root.findViewById(R.id.recycler_list_Category_sub);

        //komponen recycler view Category Sub
        dataCategorySub = new ArrayList<>();
        adapterCategorySub = new Video_Category_Sub_Adapter(getContext(), dataCategorySub, this);

        //setup recycler view Category Sub
        LinearLayoutManager mLayoutManagerVideoCategorySub;
        mLayoutManagerVideoCategorySub = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        recyclerCategorySub.setLayoutManager(mLayoutManagerVideoCategorySub);
        recyclerCategorySub.setItemAnimator(new DefaultItemAnimator());
        recyclerCategorySub.setAdapter(adapterCategorySub);

        loding_layar = root.findViewById(R.id.loding_layar);
        eror_layar = root.findViewById(R.id.eror_layar);

        //   nestedScrollView = root.findViewById(R.id.nested_scroll_halaman_utama_video);

        layoutloading_bar = root.findViewById(R.id.layoutloading_bar);

        coba_lagi = root.findViewById(R.id.coba_lagi);

        loadingBola = (ProgressBar)root.findViewById(R.id.spin_kit_loading);

        ket_list_video_habis = root.findViewById(R.id.ket_list_video_habis);

        animasiSprite = new ThreeBounce();

        loadingBola.setIndeterminateDrawable(animasiSprite);
        text_count_notif = root.findViewById(R.id.text_count_notif);


        getAllCategory("","");
//        countNotif();
        startNotif();
//        //pagination nested scroll
//        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
//                    ket_list_video_habis.setVisibility(View.GONE);
//                    loading_bar_show();
//                    //jika scroll mentok, load item selanjutnya

//                }
//            }
//        });

        //start recycler view pagination
        recyclerVideo.addOnScrollListener(new PaginationListener(mLayoutManagerVideo) {
            @Override
            protected void loadMoreItems() {

            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
        //end recycler view pagination

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eror_dismiss();
                loading_show();
               //fx com getAllVideo("all","",categoryId, categorySubId,nextIdVideo);
                getAllVideo("all","",categoryId, categorySubId,nextIdVideo);
                Log.e(MainActivity.MYTAG,"GGcategory id: "+categoryId);
                Log.e(MainActivity.MYTAG,"GGcategory sub id: "+categorySubId);
                Log.e(MainActivity.MYTAG,"GGnextIdVideo: "+nextIdVideo);
                ket_list_video_habis.setVisibility(View.GONE);
            }
        });

        icon_video_notif = root.findViewById(R.id.icon_video_notif);

        icon_video_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                countNotif();
                text_count_notif.setText("0");
                text_count_notif.setVisibility(View.GONE);
                Intent i = new Intent(getContext(), Video_Notif.class);
                getActivity().startActivity(i);
            }
        });

        icon_video_save = root.findViewById(R.id.icon_video_save);

        icon_video_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), Video_save.class);
                startActivityForResult(i,REQUEST_CODE_OPEN_SAVE);
            }
        });
        initCommentBottomSheet();
    }

    //    CHECK RECYCLER VIDEO ON TOP
    private boolean isRecyclerViewAtTop()   {
        if(recyclerVideo.getChildCount() == 0)
            return true;
        return recyclerVideo.getChildAt(0).getTop() == 0;
    }

    //SLIDE UP AND DOWN KATEGORI
    private void slideUp() {
        if(ShowHideNavbar == 1) {
            ShowHideNavbar = 0;
            recyclerVideo.setPadding(0,0,0,0);
            recyclerVideo.setNestedScrollingEnabled(false);
            linear_kategori.clearAnimation();

            linear_kategori.animate()
                    .translationY(-linear_kategori.getHeight())
                    .alpha(0.0f)
                    .setDuration(900)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            linear_kategori.setVisibility(View.GONE);
                            recyclerVideo.setNestedScrollingEnabled(true);
                        }
                    });
        }
    }

    private void slideDown() {
        if(ShowHideNavbar == 0) {
            ShowHideNavbar = 1;
            recyclerVideo.setNestedScrollingEnabled(false);
            Integer number = categorySubId.equals("") || categorySubId.equals("0") ? 115 : 155;
            int atas = (int) (number * scale + 0.5f);
            recyclerVideo.setPadding(0,atas,0,0);
            linear_kategori.clearAnimation();
            linear_kategori.animate()
                    .translationY(0)
                    .alpha(1.0f)
                    .setDuration(200)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            linear_kategori.setVisibility(View.VISIBLE);
                            recyclerVideo.setNestedScrollingEnabled(true);
                        }
                    });
        }
    }

    //SHARE URL
//    @Override
    public void shareVideo(String url){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        // shareIntent.putExtra(Intent.EXTRA_SUBJECT, " https://play.google.com/store/apps/details?id=com.zonamediagroup.zonamahasiswa.app");
        String app_url =  url;
        shareIntent.putExtra(Intent.EXTRA_TEXT, app_url);
        startActivity(Intent.createChooser(shareIntent, "Bagikan ke"));
    }

    public void initCommentBottomSheet()
    {
        bottomSheetDialog = new BottomSheetDialog(getContext());
        View view= LayoutInflater.from(getContext()).inflate(R.layout.sheet_dialog_komentar,null);

        recyclerCommentInBottomSheet = view.findViewById(R.id.recycler_bottomsheet_komen_video);
        LinearLayoutManager layoutLevel1BottomSheet = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1BottomSheet);

        pbBottomSheet = view.findViewById(R.id.progress_bottom_video);
        gambar_komentator_video = view.findViewById(R.id.gambar_komentator_video);

        txt_komentar_video = view.findViewById(R.id.txt_komentar_video);
        send_button = view.findViewById(R.id.send_button);

        dataRecyclerKomentarBottomSheet = new ArrayList<>();

        txt_bottomsheet_komentar_kosong = view.findViewById(R.id.txt_bottomsheet_komentar_kosong);

        gambar_komentator = view.findViewById(R.id.gambar_komentator_video);

        Glide.with(getContext())
                .load(SharedPrefManagerZona.getInstance(getActivity()).geturl_voto())
                .apply(RequestOptions.fitCenterTransform())
                .into(gambar_komentator);

        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                commentAdapterLevel1 = null;
            }
        });
    }

    public void tampilDialogBalasKomentar(String idKomentarYangDibalas)
    {

        bottomSheetDialogBalasKomentar = new BottomSheetDialog(getContext());
        View view= LayoutInflater.from(getContext()).inflate(R.layout.sheet_balas_komentar,null);



        bottomSheetDialogBalasKomentar.setContentView(view);
        bottomSheetDialogBalasKomentar.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
        bottomSheetDialogBalasKomentar.show();
    }

    @Override
    public void showAllComment(String idVideo) {
        Log.d(MainActivity.MYTAG,"CALLER FROM Video. idVideo: "+idVideo);
        initCommentBottomSheet();
        bottomSheetDialog.show();
        refreshRecyclerViewKomentarSetelahAksiLevel1(idVideo,"awal_buka");
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahKomentar(MainActivity.idUserLogin, idVideo, txt_komentar_video.getText().toString());
                txt_komentar_video.setText("");
                txt_komentar_video.clearFocus();
                //  CustomToast.s(getContext(),"Komentarmu berhasil ditambah");
            }
        });
    }

    public void refreshRecyclerViewKomentarSetelahAksiLevel1(String idVideo,String jenisAksi)
    {
        //start code getCommentLevel1
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONArray dataArray = jObj.getJSONArray("data");
                        ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());

                        dataRecyclerKomentarBottomSheet.clear();
                        dataRecyclerKomentarBottomSheet.addAll(items);
                        commentAdapterLevel1 = new Komentar_Video_Level1_Adapter(getContext(),dataRecyclerKomentarBottomSheet,Video.this);
                        recyclerCommentInBottomSheet.setAdapter(commentAdapterLevel1);
                        commentAdapterLevel1.notifyDataSetChanged();
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"komen pertama: "+items.get(0).getVideo_comment());
                        pbBottomSheet.setVisibility(View.GONE);
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");
                        txt_bottomsheet_komentar_kosong.setVisibility(View.VISIBLE);
                        pbBottomSheet.setVisibility(View.GONE);
                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catch");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end code getCommentLevel1
    }

    public  void  addDataToRecyclerViewComment(RecyclerView.Adapter adapter, List<Komentar_Video_Model> allData, Komentar_Video_Model newData, int indeksPosisiBaru, RecyclerView recyclerView){
        Log.d(MainActivity.MYTAG,"sakjane terpanggil");
        txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
        allData.add(indeksPosisiBaru,newData);
        adapter.notifyItemInserted(indeksPosisiBaru);
        // adapter.notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(indeksPosisiBaru);
    }



    @Override
    public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {
        bottomSheetDialog.show();
        //start code getCommentLevel1
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONArray dataArray = jObj.getJSONArray("data");
                        ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());

                        dataRecyclerKomentarBottomSheet.clear();
                        dataRecyclerKomentarBottomSheet.addAll(items);
                        commentAdapterLevel1 = new Komentar_Video_Level1_Adapter(getContext(),dataRecyclerKomentarBottomSheet,Video.this);
                        recyclerCommentInBottomSheet.setAdapter(commentAdapterLevel1);
                        commentAdapterLevel1.notifyDataSetChanged();
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"komen pertama: "+items.get(0).getVideo_comment());
                        pbBottomSheet.setVisibility(View.GONE);
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");
                        pbBottomSheet.setVisibility(View.GONE);
                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catch");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end code getCommentLevel1
        txt_komentar_video.setHint("Balas ke "+namaKomentatorYangDibalas);
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                balasKomentar(MainActivity.idUserLogin,idVideo,idKomentarYangDibalas,txt_komentar_video.getText().toString(),idUserGoogleYangDibalas);
            }
        });
    }

    @Override
    public void balasKomentarLevel1(String idKomentarYangDibalas) {
        tampilDialogBalasKomentar(idKomentarYangDibalas);
    }

    @Override
    public void passingShowAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {
        this.showAllCommentForReply(idVideo, idKomentarYangDibalas, namaKomentatorYangDibalas,idUserGoogleYangDibalas);
    }

    public void tambahKomentar(String idUser, String idVideo, String komentar){
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_TAMBAH_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONObject dataObject = jObj.getJSONObject("data");
                        Komentar_Video_Model items = new Gson().fromJson(dataObject.toString(), new TypeToken<Komentar_Video_Model>() {
                        }.getType());

                        Log.d(MainActivity.MYTAG,"tambahKomentar() -> onResponse() -> if -> if");
                        if(commentAdapterLevel1!=null) {
                            addDataToRecyclerViewComment(commentAdapterLevel1, dataRecyclerKomentarBottomSheet, items, 0, recyclerCommentInBottomSheet);
                        }
                        else {
                            initDataRecyclerViewCommentOnFirstAdd(items);
                        }
                        //lanjut disini. refresh recycler sehabis menambahkan komentar
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");

                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catchX");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUser);
                params.put("id_video",idVideo);
                params.put("comment",komentar);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    private void initDataRecyclerViewCommentOnFirstAdd(Komentar_Video_Model items) {
        recyclerCommentInBottomSheet.setAdapter(null);
        txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
        List<Komentar_Video_Model> dataKomentar = new ArrayList<>();
        dataKomentar.add(items);
        commentAdapterLevel1 = new Komentar_Video_Level1_Adapter(getContext(),dataKomentar,this);
        recyclerCommentInBottomSheet.setAdapter(commentAdapterLevel1);
        recyclerCommentInBottomSheet.setVisibility(View.VISIBLE);
    }

    private void balasKomentar(String idUser, String idVideo, String idKomentarYangDibalas, String komentar, String idUserTargetForReply){
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_BALAS_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONObject dataObject = jObj.getJSONObject("data");
                        Komentar_Video_Model items = new Gson().fromJson(dataObject.toString(), new TypeToken<Komentar_Video_Model>() {
                        }.getType());


                        //commentAdapterLevel1.notifyItemInserted(0);
                        commentAdapterLevel1.notifyDataSetChanged();
                        //lanjut disini. refresh recycler sehabis menambahkan komentar
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");

                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catchX");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUser);
                params.put("id_video",idVideo);
                params.put("id_comment",idKomentarYangDibalas);
                params.put("id_user_reply",idUserTargetForReply);
                params.put("comment",komentar);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }






    @Override
    public void persiapanDownloadVideo(int position,String urlVideo,String namaVideo) {
        urlVideoGlobal = urlVideo;
        namaVideoGlobal = namaVideo;
        boolean izinSimpanDownload =  checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,STORAGE_PERMISSION_CODE);
        if(izinSimpanDownload == false) //izin belum diberikan. request dulu
            requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,STORAGE_PERMISSION_CODE);
        else //izin telah diberikan dari awal. lanjut download
            prosesDownloadVideo(urlVideo,namaVideo);
    }



    private String getExtension(String url){
        return  url.substring(url.lastIndexOf("."));
    }


    public void prosesDownloadVideo(String urlVideo, String namaVideo){

        if(cekPernahDownload(urlVideo,namaVideo) == false) {
            createNotificationChannels();
            new DownloadFileFromURL(currentIdNotif).execute(urlVideo, namaVideo);
            //currentIdNotif adalah id pembeda notifikasi, agar ketika pengguna mendownload beberapa video sekaligus, videonya punya notif sendiri2 dan tidak tumpang tindih notifikasinya
            currentIdNotif++;
        }
        else
            CustomToast.s(getContext(),"Yey, video sudah ada di HP mu");
    }

    private String showTextOrTrim(String content)
    {
        int indexAtWhichTrimSupposedToBegins = 30;
        if(content.length() <= indexAtWhichTrimSupposedToBegins)
        {
            return content;
        }
        else{
            return formatOffset(content,indexAtWhichTrimSupposedToBegins);
        }
    }

    private String formatOffset(String content,int indexAtWhichTrimSupposedToBegins)
    {
        int maximumOffset = 40;
        int gap = maximumOffset - indexAtWhichTrimSupposedToBegins;
        String replacer="...";
        //skenario 1. karakter ke 30 adalah " "

        if(content.charAt(30)==' ')
        {
            return content+replacer;
        }
        //skenario 2. karakter ke 30 bukan " "
        else{
            String contentNext = content.substring(indexAtWhichTrimSupposedToBegins);
            int indexOfnextOccurenceOfWhiteSpace = contentNext.indexOf(" ");
            //skenario 3. karakter " " ditemukan di kata selanjutnya sebelum maximumOffset
            if(indexOfnextOccurenceOfWhiteSpace <= maximumOffset )
            {
                content.substring(0,indexOfnextOccurenceOfWhiteSpace);
            }
            //skenario 3 gagal. karakter " " TIDAK ditemukan di kata selanjutnya sebelum maximumOffset
            else
            {
                String contentPrevious = content.substring(0,indexAtWhichTrimSupposedToBegins);
                int indexOfPreviousOccurenceOfWhiteSpace = contentPrevious.indexOf(" ");
                //skenario 4. cari karakter " " di kata sebelumnya indexAtWhichTrimBegin
                if(indexOfPreviousOccurenceOfWhiteSpace <= indexAtWhichTrimSupposedToBegins)
                {
                    return contentPrevious+replacer;
                }
                //skenario 4 gagal. maka judul tidak memiliki spasi. cut di indexOfNextOccurenceOfWhiteSpace
                else
                {
                    return content.substring(0,indexAtWhichTrimSupposedToBegins)+replacer;
                }
            }
        }
        return content.substring(0,indexAtWhichTrimSupposedToBegins)+replacer;
    }

    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,
                    "Channel 2",
                    NotificationManager.IMPORTANCE_LOW
            );
            channel2.setDescription("This is Channel 2");

            NotificationManager manager = getContext().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel2);
        }
        notificationManager = NotificationManagerCompat.from(getContext());
    }



    // loading dan eror

    private void getAllVideo(String kategori,String id_video,String video_category_id, String video_category_sub_id, String startItemSelanjutnya) {
        Log.d("fandydebug","id_videoX: "+id_video);
        Log.d("fandydebug","video category id: "+video_category_id);
        Log.d("fandydebug","video category sub id: "+video_category_sub_id);
        ket_list_video_habis.setVisibility(View.GONE);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_HALAMAN_UTAMA_VIDEO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    Log.d(MainActivity.MYTAG,"try response getALl");


                    if (jObj.getString("status").equals("true")) {
                        //set nextVideo yang akan diload awal di next page
                        nextIdVideo = jObj.getString("next_item");
                        JSONArray dataArray = jObj.getJSONArray("data");

                        //Log.d("Response VideoX","vbgvbg: "+dataArray.getJSONObject(0).getJSONArray("comment").getJSONObject(0).get("video_comment"));
                        List<Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Video_Model>>() {
                        }.getType());

                        //jika awal load, clear adapter
                        if(startItemSelanjutnya.equals(""))
                            dataVideo.clear();

//                        if(items.size() <= 0)


                     dataVideo.addAll(items);
                        adapterVideo.notifyDataSetChanged();
                        loading_dismiss();
                        doToolTipsVideo();
                        Log.e(MainActivity.MYTAG,"terpanggil YYY");
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();
                        //item telah habis
                        Log.e(MainActivity.MYTAG,"terpanggil XXX");
                        ket_list_video_habis.setVisibility(View.VISIBLE);
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    eror_show();
                    e.printStackTrace();
                }
                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",id_video);
                params.put("video_category_id",video_category_id);
                params.put("video_category_sub_id",video_category_sub_id);
                params.put("halamanaktif","");
                Log.e("fandyndebug_25april","id_user: "+MainActivity.idUserLogin);
                Log.e("fandyndebug_25april","id_video: "+id_video);
                Log.e("fandyndebug_25april","video_category_id: "+video_category_id);
                Log.e("fandyndebug_25april","video_category_sub_id: "+video_category_sub_id);
                Log.e("fandyndebug_25april","halamanaktif: "+"");
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {
        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {
        eror_layar.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mActivity = (Activity) context;
    }

    // Download from URL
    class DownloadFileFromURL extends AsyncTask<String, Integer, String> {
        int idNotifikasi;
        String namaFile;
        String ekstensiFile;
        NotificationCompat.Builder notif;
        public DownloadFileFromURL(int idNotifikasi){
            this.idNotifikasi = idNotifikasi;
        }

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CustomToast.s(getContext(),"Mengunduh video...");
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {


            try {
                Log.d(MainActivity.MYTAG,"error download 24 desember. f_url[0]: "+f_url[0]);
                Log.d(MainActivity.MYTAG,"error download 24 desember. f_url[1]: "+f_url[1]);
                URL url = new URL(f_url[0]);
                namaFile = f_url[1];
                URLConnection connection = url.openConnection();
                connection.connect();
                ekstensiFile = getExtension(f_url[0]);
                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int maxSize = connection.getContentLength();
                initNotification(idNotifikasi,namaFile);

                // download the file
//                InputStream input = new BufferedInputStream(url.openStream(),
//                        8192);
                InputStream input =connection.getInputStream();


                // Create File directori
                String rootPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath() + "/"+lokasiFolder+"/";
                File root = new File(rootPath);
                if (!root.exists()) {
                    root.mkdirs();
                }

                System.out.println("Berjalan");
//                    Write file
                nama_dan_lokasi_file_download = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+lokasiFolder+"/" + namaFile + ekstensiFile;
                OutputStream output = new FileOutputStream(nama_dan_lokasi_file_download);

                byte data[] = new byte[1024];

                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"total: "+total);
                    Log.d(MainActivity.MYTAG,"progressMax: "+progressMax);
                    Log.d(MainActivity.MYTAG,"maxSize: "+maxSize);
                    Log.d(MainActivity.MYTAG,"total*100: "+total*100);
                    Log.d(MainActivity.MYTAG,"(total*100)/maxSize: "+((total * 100) / maxSize));
                    // publishing the progress....
                    int progress = (int) ((total * 100) / maxSize);
                    if(progress % 5 == 0) {
                        publishProgress(progress);
                    }
                    // After this onProgressUpdate will be called


                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();



            } catch (Exception e) {
                Log.d(MainActivity.MYTAG,"error vid: "+e.getMessage());
                System.out.println("EROR Download " + e.getMessage());

            }


            return null;
        }

        //agar tidak error ekspose
        public void setVmPolicy()
        {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }


        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
            notifDownloadOnProgress(progress[0],namaFile);
        }

        private void notifDownloadOnProgress(int currentProgress,String namaFile){
            Log.d(MainActivity.MYTAG,"notifDownloadOnProgress() "+currentProgress);
            notif.setContentText(currentProgress+" %");
            notif.setProgress(100, currentProgress,false);
            notificationManager.notify(idNotifikasi,notif.build());
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            setVmPolicy();
            notifDownloadSelesai();
            //  openFolder();

        }

        private void notifDownloadSelesai()
        {
            notificationManager.cancel(idNotifikasi);
            Intent intent1 = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(nama_dan_lokasi_file_download));
            intent1.setDataAndType(uri, MimeType.getMimeType(ekstensiFile));
            Log.d("fandy_progress","Download Selesai");

            notificationManager.cancel(idNotifikasi);
            if(notif!=null) {
                notif.setContentText("Video Berhasil didownload")
                        .setProgress(0, 0, false)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo))
                        .setOngoing(false)
                        .setContentIntent(PendingIntent.getActivity(getContext(), 0, intent1, PendingIntent.FLAG_CANCEL_CURRENT));

                notificationManager.notify(idNotifikasi, notif.build());
            }
        }


        private void initNotification(int idNotif,String namaFile){
            Log.d(MainActivity.MYTAG,"initNotification()");
            notif = new NotificationCompat.Builder(getContext(),CHANNEL_2_ID).setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle(showTextOrTrim(namaFile))
                    .setContentText("0 %")
                    .setSmallIcon(R.drawable.ic_logo)
                    .setProgress(100,50, false)
                    .setOngoing(true)
                    .setOnlyAlertOnce(true)
                    .setPriority(NotificationCompat.PRIORITY_LOW);

            notificationManager.notify(idNotif,notif.build());
        }

    }

    //cek file sudah ada atau belum
    public boolean cekPernahDownload(String urlFile, String namaFile){
        String ekstensiFile = urlFile.substring(urlFile.lastIndexOf("."));
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+lokasiFolder+"/" + namaFile + ekstensiFile);
        if(file.exists())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    //check permission
    public boolean checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_DENIED)
            return false;
        else
            return true;
    }

    public void requestPermission(String permission, int requestCode){
//        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        requestPermissions(new String[]{
                permission}, STORAGE_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                prosesDownloadVideo(urlVideoGlobal,namaVideoGlobal);
            } else {
                //gagal memberikan permisi
                Toast.makeText(getContext(),"Aktifkan izin penyimpananmu terlebih dahulu",Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void loading_bar_show() {
        layoutloading_bar.setVisibility(View.VISIBLE);
    }

    public void loading_bar_dismiss() {
        layoutloading_bar.setVisibility(View.GONE);
    }

    @Override
    public void storeLike(String idVideo, String like)
    {
        int likeAngka = Integer.valueOf(like);
        int videoIdAngka = Integer.valueOf(idVideo);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();
                    }

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
//                    eror_show();
                    e.printStackTrace();
                }
                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALlXXz "+error.getMessage());
//                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                params.put("like",like);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void storeSave(String idVideo, String save)
    {
        int saveAngka = Integer.valueOf(save);
        int videoIdAngka = Integer.valueOf(idVideo);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_SAVE_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();
                    }

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    eror_show();
                    e.printStackTrace();
                }
                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                params.put("save",save);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }


    public void changeCategoryVideo(int position, String Type, String video_category_id, String video_category_sub_id){
        ket_list_video_habis.setVisibility(View.GONE);
        loading_bar_show();
        if(video_category_sub_id.equals("")){
            getSubCategory(video_category_id);
            int atas = (int) (115 * scale + 0.5f);
            recyclerVideo.setPadding(0,atas,0,0);
        }else{
            int atas = (int) (155 * scale + 0.5f);
            recyclerVideo.setPadding(0,atas,0,0);
        }
        nextIdVideo="";
        categoryId=video_category_id;
        categorySubId=video_category_sub_id;
        dataVideo.clear();
        getAllVideo("all","",video_category_id, video_category_sub_id,nextIdVideo);
        adapterCategory.notifyDataSetChanged();
        adapterVideo.notifyDataSetChanged();
    }

    private void getAllCategory(String idCategory, String idCategorySub) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_KATEGORI_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    Log.d(MainActivity.MYTAG,"kateg try");


                    if (jObj.getString("status").equals("true")) {
                        //set nextVideo yang akan diload awal di next page
//                        nextIdVideo = jObj.getString("next_item");
                        JSONArray dataArray = jObj.getJSONArray("data");

                        List<Video_Category_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Video_Category_Model>>() {
                        }.getType());

                        dataCategory.addAll(items);
                        adapterCategory.notifyDataSetChanged();
                        loading_dismiss();
                        loading_bar_show();
                        getSubCategory(idCategory);
                        getAllVideo("all","","", "",nextIdVideo);

//                        if(!dataArray.getJSONObject(0).getJSONArray("sub_category").equals(null)) {
//                            JSONArray dataArraySub = dataArray.getJSONObject(0).getJSONArray("sub_category");
//                            Log.d(MainActivity.MYTAG,"getAllCategory() -> response -> try -> if -> if");
//                            List<Video_Category_Sub_Model> itemsCategory = new Gson().fromJson(dataArraySub.toString(), new TypeToken<List<Video_Category_Sub_Model>>() {
//                            }.getType());
//                            dataCategorySub.addAll(itemsCategory);
//                            adapterCategorySub.notifyDataSetChanged();
//                            categoryId=dataArray.getJSONObject(0).getString("video_category_id");
//                            categorySubId=dataArraySub.getJSONObject(0).getString("video_category_sub_id");
//                        }else{
//                            Log.d(MainActivity.MYTAG,"getAllCategory() -> response -> try -> if -> else");
//                            recyclerCategorySub.setVisibility(View.GONE);
//                            categoryId=dataArray.getJSONObject(0).getString("video_category_id");
//                            categorySubId="";
//                        }


                    } else if (jObj.getString("status").equals("false")) {
                        Log.d(MainActivity.MYTAG,"kateg try else if");
                        loading_dismiss();
                        //item telah habis
                        ket_list_video_habis.setVisibility(View.VISIBLE);
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    eror_show();
                    e.printStackTrace();
                    Log.d(MainActivity.MYTAG,"kateg cach "+e.getMessage());
                }
                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                eror_show();
                Log.d(MainActivity.MYTAG,"kateg onErrorRsponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    private void getSubCategory(String idCategory) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_SUB_KATEGORI_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    Log.d(MainActivity.MYTAG,"kateg try");


                    if (jObj.getString("status").equals("true")) {
                        //set nextVideo yang akan diload awal di next page
//                        nextIdVideo = jObj.getString("next_item");
                        JSONArray dataArraySub = jObj.getJSONArray("data");
                        dataCategorySub.clear();

                        List<Video_Category_Sub_Model> itemsCategory = new Gson().fromJson(dataArraySub.toString(), new TypeToken<List<Video_Category_Sub_Model>>() {
                        }.getType());

                        dataCategorySub.addAll(itemsCategory);
                        adapterCategorySub.notifyDataSetChanged();

                        loading_dismiss();
                        if(dataArraySub != null) {
                            recyclerCategorySub.setVisibility(View.VISIBLE);
                            categoryId=idCategory;
                            categorySubId=dataArraySub.getJSONObject(0).getString("video_category_sub_id");
                            int atas = (int) (155 * scale + 0.5f);
                            recyclerVideo.setPadding(0,atas,0,0);
                        }else{
                            recyclerCategorySub.setVisibility(View.GONE);
                            categoryId=idCategory;
                            categorySubId="";
                            int atas = (int) (115 * scale + 0.5f);
                            recyclerVideo.setPadding(0,atas,0,0);
                        }


                    } else if (jObj.getString("status").equals("false")) {
                        recyclerCategorySub.setVisibility(View.GONE);
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    eror_show();
                    e.printStackTrace();
                    Log.d(MainActivity.MYTAG,"kateg cach "+e.getMessage());
                }
                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                eror_show();
                Log.d(MainActivity.MYTAG,"kateg onErrorRsponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("video_category_id", idCategory);
                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }


    public void playVideo(boolean isEndOfList) {



        if(!isEndOfList){
            int startPosition = ((LinearLayoutManager) recyclerVideo.getLayoutManager()).findFirstVisibleItemPosition();
            int endPosition = ((LinearLayoutManager) recyclerVideo.getLayoutManager()).findLastVisibleItemPosition();

            // if there is more than 2 list-items on the screen, set the difference to be 1
            if (endPosition - startPosition > 1) {
                endPosition = startPosition + 1;
            }

            // something is wrong. return.
            if (startPosition < 0 || endPosition < 0) {
                return;
            }

            // if there is more than 1 list-item on the screen
            if (startPosition != endPosition) {
                int startPositionVideoHeight = getVisibleVideoSurfaceHeight(startPosition);
                int endPositionVideoHeight = getVisibleVideoSurfaceHeight(endPosition);

                targetPosition = startPositionVideoHeight > endPositionVideoHeight ? startPosition : endPosition;
            }
            else {
                targetPosition = startPosition;
            }
        }
        else{
            //    targetPosition = mediaObjects.size() - 1;
        }
        viewCurrentRecycler =recyclerVideo.findViewHolderForAdapterPosition(targetPosition).itemView;

        videoCurrentRecycler.start();


    }

    private int getVisibleVideoSurfaceHeight(int playPosition) {
        int at = playPosition - ((LinearLayoutManager) recyclerVideo.getLayoutManager()).findFirstVisibleItemPosition();
        Log.d(TAG, "getVisibleVideoSurfaceHeight: at: " + at);

        View child = recyclerVideo.getChildAt(at);
        if (child == null) {
            return 0;
        }

        int[] location = new int[2];
        child.getLocationInWindow(location);

        if (location[1] < 0) {
            return location[1] + videoSurfaceDefaultHeight;
        } else {
            return screenDefaultHeight - location[1];
        }
    }

    public  void startNotif(){
        runnable = new Runnable() {
            @Override
            public void run() {
                countNotif();
                Log.d("debagpesan", "okoko");
                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(runnable, 5000);
    }
    public void countNotif()
    {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_NOTIF_COUNT_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        if(jObj.getInt("data") > 0) {
                            text_count_notif.setVisibility(View.VISIBLE);
                            if (jObj.getInt("data") >= 99) {
                                text_count_notif.setText("99+");
                            } else {
                                text_count_notif.setText(jObj.getString("data"));
                            }
                        }
                    } else if (jObj.getString("status").equals("false")) {
                        text_count_notif.setText("0");
                        text_count_notif.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    // JSON error
//                    Log.d(MainActivity.MYTAG,"catch response getALl");
//                    eror_show();
//                    e.printStackTrace();
                }
                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    @Override
    public void onStop() {
        super.onStop();
        recyclerVideo.onPausePlayer();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkIfNull();
        recyclerVideo.onPlayPlayer();
        //jika ada data
        if(CurrentActiveVideoData.dataVideos != null) {
            if (CurrentActiveVideoData.dataVideos.size() > 0) {
                dataVideo.clear();
                dataVideo.addAll(CurrentActiveVideoData.dataVideos);
                adapterVideo.notifyDataSetChanged();
                CurrentActiveVideoData.dataVideos = null;
            }
        }
        //jika ada data unsave
        if(CurrentActiveVideoData.dataSavesDeleted != null) {
            if (CurrentActiveVideoData.dataSavesDeleted.size() > 0) {
                setUnSaveFromUnSaveListToCurrentVideoData();
            }
        }
    }

    private void setUnSaveFromUnSaveListToCurrentVideoData(){
        for(int indeksUnsave=0;indeksUnsave<CurrentActiveVideoData.dataSavesDeleted.size();indeksUnsave++){
            for(int indeksDataVideo=0;indeksDataVideo<dataVideo.size();indeksDataVideo++){
                if(CurrentActiveVideoData.dataSavesDeleted.get(indeksUnsave).equals(dataVideo.get(indeksDataVideo).getVideo_id())) //jika sama, maka ada di dataVideo sekarang, hapus save nya
                    dataVideo.get(indeksDataVideo).setVideo_save_id(null);
            }
        }
        adapterVideo.notifyDataSetChanged();
        CurrentActiveVideoData.dataSavesDeleted = null;
    }


    private void doToolTipsVideo()
    {
        if(getActivity() != null) {
            if (pref.contains("videoToolTips") == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
            {
                initToolTipsVideo();
//            runToolTipsVideo();
                pref.edit().putInt("videoToolTips", 1).commit();
            }
        }
    }

    private void initToolTipsVideo()
    {
        listToolTipsVideo = new ArrayList<>();
        String[] pesanToolTips = {"Pilih kategori kesukaanmu","Pilih video yang ingin kamu lihat","Klik tombol play untuk melihat video","Kamu juga bisa mengunduh video yang kamu suka","Simpan video untuk ditonton nanti","Lihat video yang kamu simpan di sini"};
        String[] textButtonToolTips = {"Lanjutkan","Lanjutkan","Lanjutkan","Lanjutkan","Lanjutkan","Selesai"};
        int[] gravityToolTips = {Gravity.BOTTOM,Gravity.TOP,Gravity.TOP,Gravity.TOP,Gravity.TOP,Gravity.BOTTOM};
        int[] targetToolTips = {R.id.recycler_list_Category,R.id.linear_item_video,R.id.icon_play,R.id.download_video,R.id.save_video,R.id.icon_video_save};

//        List<Tooltips_Model> allToolTips = MainActivity.allToolTips;
        //jCOunter yang akan dipakai sebagai index oleh local array
        for(int i=0;i<6;i++)
        {
            listToolTipsVideo.add(new ToolTipsZona(getActivity(),
                    pesanToolTips[i],
                    textButtonToolTips[i],
                    targetToolTips[i],
                    true,
                    R.layout.tooltip_small,
                    R.id.tv_text,
                    R.id.btn_next,
                    gravityToolTips[i],
                    "video",
                    "video"));
        }
        jalankanToolTips(jCounter);
    }

    public  void jalankanToolTips(int indeks){
        final SimpleTooltip tooltip = new SimpleTooltip.Builder(getActivity())
                .anchorView(getActivity().findViewById(listToolTipsVideo.get(indeks).idViewAnchor))
                .text(listToolTipsVideo.get(indeks).pesan)
                .gravity(listToolTipsVideo.get(indeks).gravity)
                .showArrow(listToolTipsVideo.get(indeks).arrow)
                .dismissOnOutsideTouch(false)
                .dismissOnInsideTouch(false)
                .modal(true)
                .animated(true)
                .transparentOverlay(false)
                .overlayMatchParent(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR_RADIUS)
                .overlayOffset(0)
                .animationDuration(2000)
                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
                .contentView(listToolTipsVideo.get(indeks).resLayoutToolTips, R.id.tv_text)
                .focusable(true)
                .build();
        if(indeks==2){
            mLayoutManagerVideo.scrollToPositionWithOffset(0, -250);
        }
        if(indeks==3){
            mLayoutManagerVideo.scrollToPositionWithOffset(0, -900);
        }
        if(indeks==4){
            mLayoutManagerVideo.scrollToPositionWithOffset(0, -1000);
        }
        Button btn = tooltip.findViewById(R.id.btn_next);
        btn.setText(listToolTipsVideo.get(indeks).textTombol);
        tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tooltip.isShowing())
                {
                    tooltip.dismiss();
                    if(indeks + 1 < listToolTipsVideo.size())
                    {
                        jCounter++;
                        //jalankan tooltips array selanjutnya
                        jalankanToolTips(jCounter);
                    }

                }
            }
        });

        tooltip.show();
    }

    private void runToolTipsVideo()
    {
        new ToolTipsZonaController().setMultipleToolTips(listToolTipsVideo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_OPEN_SAVE)
        {
            if(resultCode == Activity.RESULT_OK){

            }

            else if (resultCode == Activity.RESULT_CANCELED){

            }

        }
    }

    public void checkIfNull() {
        if (CurrentActiveVideoData.getDataVideos() == null) {
            // CustomToast.s(getContext(), "data masih null");
            CurrentActiveVideoData.initDataVideos();
        }
        if (CurrentActiveVideoData.dataSavesDeleted == null) {
            // CustomToast.s(getContext(), "data masih null");
            CurrentActiveVideoData.initDataVideosSavesDeleted();
        }
        else {
            //  CustomToast.s(getContext(),"data tidak lagi null");
        }
    }



    //    start iklan bottom navigation per x menit
//atur waktu tampil iklan agar tampil hanya setelah x menit, lalu show iklan
    public void setShowTimeForShowIklanVideoOnBottomNavigationVideo(){
        Log.d("fandy_29_april","ssT bottom navigation video");
        //start iklan
        int intervalMenit = RuleValueAplikasi.durasiIklan;
        String labelSharedPref = "waktu_iklan_bottom_navigation_video_terakhir";
        //start pref
        if (pref.contains(labelSharedPref) == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
        {
            //jika tidak ada sharedpref iklan tampil terakhir, maka sharedpref iklan tampil terakhir = now
            pref.edit().putString(labelSharedPref, IklanInterstitial.getCurrentDateTime()).commit();
            showIklanVideoOnBottomNavigationPolling();
        }
        else //jika sudah ada waktu iklan terakhir
        {
            String waktuTerakhirIklanTampil = pref.getString(labelSharedPref,"");
            String waktuSekarang = IklanInterstitial.getCurrentDateTime();
            long selisihWaktu = IklanInterstitial.substractTwoDateTimes(waktuTerakhirIklanTampil,waktuSekarang);
            Log.d("2 Februari","2feb selisih waktu: "+selisihWaktu);
            if(selisihWaktu > intervalMenit)
            {
                Log.d("fandy_29_april","ssT  if 237 visited");
                showIklanVideoOnBottomNavigationPolling();
                pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
                Log.d("2 Februari","2feb ifku: ");
            }
            else {
                Log.d("fandy_29_april","ssT  else 243 visited");
                Log.d("2 Februari","2feb elseku: ");
            }
        }
        //end pref
        //end iklan

    }

    //show iklan bottomNavigationKategori
    public void showIklanVideoOnBottomNavigationPolling(){
        IklanInterstitial.iklanVideo.show();
    }
//    end iklan bottom navigation per x menit


}