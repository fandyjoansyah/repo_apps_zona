package com.zonamediagroup.zonamahasiswa.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.DaftarBlokir;
import com.zonamediagroup.zonamahasiswa.Delay;
import com.zonamediagroup.zonamahasiswa.Iklan;
import com.zonamediagroup.zonamahasiswa.Keamanan;
import com.zonamediagroup.zonamahasiswa.KelolaAkun;
import com.zonamediagroup.zonamahasiswa.Kirim_file;
import com.zonamediagroup.zonamahasiswa.LihatFotoProfil;
import com.zonamediagroup.zonamahasiswa.Login;
import com.zonamediagroup.zonamahasiswa.LoginDenganKredensial;
import com.zonamediagroup.zonamahasiswa.Mimin_zona;
import com.zonamediagroup.zonamahasiswa.NotifikasiActivity;
import com.zonamediagroup.zonamahasiswa.PendaftaranAkun;
import com.zonamediagroup.zonamahasiswa.PrivasiDanKeamanan;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.RubahPassword;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.WebViewZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZonaController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class Profile extends Fragment {
    private static final String TAG = Login.class.getSimpleName();


    private static final int RC_SIGN_IN = 9001;
    private static final String URL_REQUEST_DEFAULT_FOTO_PROFIL = Server.URL_REAL + "get_default_foto_profil";
    private GoogleSignInClient mGoogleSignInClient;

    BottomSheetDialog bottomSheetKeluar;

    String URL_NOMOR_WA = Server.URL_REAL + "get_nomor_wa?TOKEN=qwerty";

    TextView cara_mengirim_tulisan, menu_kata_sandi_activity;

    LinearLayout menuKebijakanPrivasi, menuAkunYangDiblokir, menuTentangKami, menuIklan, menuMiminZona, menuKeluar, menuKelolaAkun,menuKeamanan;

    RelativeLayout menuBottomSheetKeluar, menuBottomSheetBatal;

    RelativeLayout menuDialogKeluar, menuDialogBatal;

    static String tag_json_obj = "json_obj_req";


    String id_user_sv, username_native, user_img, user_name, user_gender;

    CircleImageView img_email;
    TextView tx_person_name, tx_username_native, versi_apps;

    TextView linkClassProgram;

    LinearLayout menuNotifikasi;

    String nomorWa;

    ImageView sos_whatapps, sos_facebook, sos_instagram, sos_twiter, sos_tiktok, sos_youtube;
    private ToolTipsZona[] arrToolTipsProfil;
    private ArrayList<ToolTipsZona> listToolTipsProfile;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.page_profile, container, false);

        init(root);
        return root;
    }

    void init(View root) {

        id_user_sv = SharedPrefManagerZona.getInstance(getActivity()).getid_user();
        Log.d("2_agustus_2022","foto pengguna: "+SharedPrefManagerZona.getInstance(getActivity()).geturl_voto());
        SharedPrefManagerZona.getInstance(getActivity()).geturl_voto();
        Log.d("28_juni_2022","id_user: "+id_user_sv);
        username_native = SharedPrefManagerZona.getInstance(getActivity()).get_username();
        user_img = SharedPrefManagerZona.getInstance(getActivity()).geturl_voto();
        user_name = SharedPrefManagerZona.getInstance(getActivity()).getperson_name();
       user_gender = SharedPrefManagerZona.getInstance(getActivity()).getperson_gender();
       username_native = SharedPrefManagerZona.getInstance(getActivity()).get_username();

        pref = getContext().getSharedPreferences("zonamhs", 0); // 0 - for private mode
//        deklarasi google login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        menuKeluar = root.findViewById(R.id.menu_keluar);
        cara_mengirim_tulisan = root.findViewById(R.id.cara_mengirim_tulisan);
        menuTentangKami = root.findViewById(R.id.menu_tentang_kami);
        menuAkunYangDiblokir = root.findViewById(R.id.menu_akun_yang_diblokir);
        menuKebijakanPrivasi = root.findViewById(R.id.menuKebijakanPrivasi);
        menuKelolaAkun = root.findViewById(R.id.menu_kelola_akun);
        menuKeamanan = root.findViewById(R.id.menu_keamanan);
        menuMiminZona = root.findViewById(R.id.menu_mimin_zona);
        menuIklan=root.findViewById(R.id.menu_iklan);
        menuNotifikasi = root.findViewById(R.id.menu_notifikasi);
        linkClassProgram = root.findViewById(R.id.profile_class_program);
        menu_kata_sandi_activity = root.findViewById(R.id.menu_kata_sandi_activity);

        initBottomSheetKeluar();

        menu_kata_sandi_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), RubahPassword.class);
                startActivity(i);
            }
        });
        linkClassProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://classprogram.id/"));
                startActivity(intent);
            }
        });
        menuNotifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotifikasiActivity.class);
                startActivity(intent);
            }
        });
        menuIklan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Iklan.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });

        menuMiminZona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Mimin_zona.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });

        menuTentangKami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //tentang lama
                //Intent intent = new Intent(getActivity(), Tentang_zona.class);
                //startActivity(intent);
                Intent intent = new Intent(getActivity(), WebViewZona.class);
                intent.putExtra("link","https://zonamahasiswa.id/about-us-zona-mahasiswa/");
                intent.putExtra("pageTitle","Tentang");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });

        menuAkunYangDiblokir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DaftarBlokir.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menuKelolaAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), KelolaAkun.class));
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menuKeamanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Keamanan.class));
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        menuKebijakanPrivasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //kebijakan privasi lama
//                Intent intent = new Intent(getActivity(), KebijakanPrivasi.class);
//                startActivity(intent);
                Intent intent = new Intent(getActivity(), WebViewZona.class);
                intent.putExtra("link","https://zonamahasiswa.id/privacy-policy/");
                intent.putExtra("pageTitle","Kebijakan Privasi");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });

        menuKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showBottomSheetKeluar();
                showPopUpLogout();
            }
        });

        cara_mengirim_tulisan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Kirim_file.class);
                startActivity(intent);
            }
        });

        img_email = root.findViewById(R.id.img_email);
        tx_person_name = root.findViewById(R.id.tx_person_name);
        tx_username_native = root.findViewById(R.id.tx_username_native);
      //  versi_apps = root.findViewById(R.id.versi_apps);

        //useraname pengguna
        tx_username_native.setText(username_native);

        //nama pengguna
        tx_person_name.setText(user_name);

        /* jika user_img = null atau user_img = "", maka cek jenis kelamin pengguna, dan pasang male_placeholder untuk laki, atau female_placeholder untuk perempuan*/

        if(user_img == null || user_img.equals("") || user_img.equals("null")){
            getDefaultFotoProfilFromServer();
        }
        else{
            setFotoProfil(user_img);
        }


       // versi_apps.setText("Version " + BuildConfig.VERSION_NAME);

        sos_whatapps=root.findViewById(R.id.sos_whatapps);
        sos_facebook=root.findViewById(R.id.sos_facebook);
        sos_instagram=root.findViewById(R.id.sos_instagram);
        sos_twiter=root.findViewById(R.id.sos_twiter);
        sos_tiktok=root.findViewById(R.id.sos_tiktok);
        sos_youtube=root.findViewById(R.id.sos_youtube);

//        on click

        sos_whatapps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //start volley get nomor wa
                StringRequest strReq = new StringRequest(Request.Method.GET, URL_NOMOR_WA, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jObj = new JSONObject(response);

                            if (jObj.getString("status").equals("true")) {
                                nomorWa =  jObj.getString("nomor_wa");
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://wa.me/"+nomorWa));
                                startActivity(intent);
                            }


                        } catch (JSONException e) {
                            // JSON error
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Register Error: " + error.getMessage());


                    }
                }) {


//            parameter

                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to login url
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("TOKEN", "qwerty");

                        return params;
                    }
                };


//        ini heandling requestimeout
                strReq.setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 10000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 10000;
                    }

                    @Override
                    public void retry(VolleyError error) throws VolleyError {

                        Log.e(TAG, "VolleyError Error: " + error.getMessage());
//                eror_show();
                    }
                });

                // Adding request to request queue
                MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
                //end volley get nomor wa

            }
        });
        sos_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.facebook.com/zonamahasiswa.id"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        sos_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.instagram.com/zonamahasiswa.id/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        sos_twiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://twitter.com/zonamahasiswaid"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        sos_tiktok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.tiktok.com/@zonamahasiswa.id"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        sos_youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.youtube.com/channel/UCbTM5NE-LV0V-JearwgFh4Q"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

    }

    private void showBottomSheetKeluar() {
        initBottomSheetKeluar();
        bottomSheetKeluar.show();
    }

    private void dismissBottomSheetKeluar(){
        bottomSheetKeluar.dismiss();
    }

    private void initBottomSheetKeluar(){
        bottomSheetKeluar = new BottomSheetDialog(getContext());
        View vBottomSheet= LayoutInflater.from(getContext()).inflate(R.layout.sheet_dialog_keluar,null);
        bottomSheetKeluar.setContentView(vBottomSheet);
        findViewByIdBottomSheetKeluar(vBottomSheet);
        setOnClickListenerBottomSheetKeluar();
    }
    private void findViewByIdBottomSheetKeluar(View vBottomSheet){
        menuBottomSheetKeluar = vBottomSheet.findViewById(R.id.menu_bottomsheet_keluar);
        menuBottomSheetBatal = vBottomSheet.findViewById(R.id.menu_bottomsheet_batal);
    }

    private void setOnClickListenerBottomSheetKeluar(){
        menuBottomSheetKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });
        menuBottomSheetBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissBottomSheetKeluar();
            }
        });
    }

    private void setFotoProfil(String url)
    {
        Glide.with(getActivity())
                .load(url)
                .apply(RequestOptions.fitCenterTransform())
                .placeholder(R.drawable.ic_no_image_available)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        CustomToast.makeBlackCustomToast(getContext(),getResources().getString(R.string.terjadi_kesalahan_saat_mencoba_mendapatkan_foto_profil), Toast.LENGTH_SHORT);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(img_email);
    }

    private void getDefaultFotoProfilFromServer(){
        AndroidNetworking.get(URL_REQUEST_DEFAULT_FOTO_PROFIL)
                .addQueryParameter("TOKEN", "qwerty")
                .setTag("get default foto profil")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                          String url =  response.getString("url_foto_profil");
                            setFotoProfil(url);
                        } catch (JSONException e) {

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }


    @Override
    public void onStart() {
        super.onStart();

        // [START on_start_sign_in]
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.

        //start komen by fandy 7 mei 2022 karena pergantian mekanisme login ke mekanisme baru
        //GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this.getActivity());
        //updateUI(account);
        //end komen by fandy 7 mei 2022 karena pergantian mekanisme login ke mekanisme baru

     //komen 16 mei 2022 karena menu rubrik pindah ke kategori doToolTipsProfil();

//        Log.w(TAG, "sudah masuk" );
        // [END on_start_sign_in]
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshDataProfil();
    }

    public void refreshDataProfil(){
        username_native = SharedPrefManagerZona.getInstance(getActivity()).get_username();

        user_name = SharedPrefManagerZona.getInstance(getActivity()).getperson_name();
        //useraname pengguna
        tx_username_native.setText(username_native);

        //nama pengguna
        tx_person_name.setText(user_name);

        //start refresh foto profil
        user_img = SharedPrefManagerZona.getInstance(getActivity()).geturl_voto();
        if(user_img == null || user_img.equals("") || user_img.equals("null")){
            getDefaultFotoProfilFromServer();
        }
        else{
            setFotoProfil(user_img);
        }
        //end refresh foto profil
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }


    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    // [START signOut]
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this.getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
//                        updateUI(null);
                        revokeAccess();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this.getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {


        } else {
//            hapuscache();
            Boolean remove = SharedPrefManagerZona.getInstance(getContext().getApplicationContext()).removesesion();

            if (remove) {
                Intent intent = new Intent(this.getActivity(), LoginDenganKredensial.class);
                startActivity(intent);
                getActivity().finish();
            } else {

            }


        }
    }

    public String getNomorWa()
    {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL_NOMOR_WA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                         nomorWa =  jObj.getString("nomor_wa");
                    }


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());


            }
        }) {
//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");

                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());
//                eror_show();
            }
        });

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
        return nomorWa;
    }

    private void doToolTipsProfil()
    {
        if(getActivity() != null) {
            if (pref.contains("profilToolTips") == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
            {
                initToolTipsProfile();
                runToolTipsProfile();
                pref.edit().putInt("profilToolTips", 1).commit();
            }
        }
    }

private void initToolTipsProfile()
{
    String[] pesanToolTips = {"Kamu bisa mengupload karya tulismu di sini", "Menu notifikasi dipindah di sini"};
    String[] textButtonToolTips = {"Lanjutkan","Selesai"};
    int[] gravityToolTips = {Gravity.TOP, Gravity.BOTTOM};
    int[] targetToolTips = {R.id.cara_mengirim_tulisan, R.id.menu_notifikasi};

    listToolTipsProfile = new ArrayList<ToolTipsZona>();
//    List<Tooltips_Model> allToolTips = MainActivity.allToolTips;
    //jCOunter yang akan dipakai sebagai index oleh local array
    int jCounter=0;
    for(int i=0;i<2;i++)
    {
//        if( allToolTips.get(i).getFragment().equals("profil"))
//        {
            listToolTipsProfile.add(new ToolTipsZona(getActivity(),
                    pesanToolTips[jCounter],
                    textButtonToolTips[jCounter],
                    targetToolTips[jCounter],
                    true,
                    R.layout.tooltip_small,
                    R.id.tv_text,
                    R.id.btn_next,
                    gravityToolTips[jCounter],
                    "profil",
                    "profile"));
            jCounter++;
//        }
    }
}

    private void runToolTipsProfile()
    {
        new ToolTipsZonaController().setMultipleToolTips(listToolTipsProfile);
    }


//    start popup logout
private void showPopUpLogout(){
    AlertDialog.Builder dialogBuilder;
    AlertDialog dialog;
    TextView popup_logout_keluar, popup_logout_batal, popup_logout_username;
    dialogBuilder = new AlertDialog.Builder(getContext());
    final View vPopUp = getLayoutInflater().inflate(R.layout.popup_logout,null);



    popup_logout_keluar = vPopUp.findViewById(R.id.popup_logout_keluar);
    popup_logout_batal = vPopUp.findViewById(R.id.popup_logout_batal);



    dialogBuilder.setView(vPopUp);
    dialog = dialogBuilder.create();
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    dialog.show();

    popup_logout_keluar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dialog.dismiss();
            signOut();
        }
    });

    popup_logout_batal.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dialog.dismiss();
        }
    });



}
//    end popup logout

}

