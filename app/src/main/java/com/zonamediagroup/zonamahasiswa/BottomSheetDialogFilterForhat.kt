package com.zonamediagroup.zonamahasiswa

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.zonamediagroup.zonamahasiswa.Fragment.Home
import com.zonamediagroup.zonamahasiswa.FragmentFilterForhat.FilterKategori
import com.zonamediagroup.zonamahasiswa.FragmentFilterForhat.FilterWaktu

class BottomSheetDialogFilterForhat : BottomSheetDialogFragment() {

    lateinit var menu_filter_kategori : RadioButton
    lateinit var menu_filter_waktu : RadioButton
    lateinit var contextBottomSheet : Context
    lateinit var mListener: BottomSheetDialogFilterForHatLama.BottomSheetFilterForhatListener
    lateinit var viewUtama : View

    fun findViewByIdAllComponent(view: View){
        viewUtama = view
        menu_filter_kategori = viewUtama.findViewById(R.id.menu_filter_kategori)
        menu_filter_waktu = viewUtama.findViewById(R.id.menu_filter_waktu)
    }

    fun setlistenerAllComponent(){
        menu_filter_kategori.setOnClickListener {
            loadPage(FilterKategori())
        }
        menu_filter_waktu.setOnClickListener {
            loadPage(FilterWaktu())
        }
    }

    fun loadPage(fragment: Fragment):Boolean{
        fragment?.let {
            childFragmentManager.beginTransaction().replace(R.id.container_fragment_filter_forhat,fragment)
                .commit()
            return true
        }
        return false
    }

    fun selectRadioButtonInFirstLaunch()
    {
        menu_filter_kategori.isChecked = true
        loadPage(FilterKategori())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottomsheet_filter_pencarian_forhat,container,false)
    }

    /*Main Method*/
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findViewByIdAllComponent(view)
        setlistenerAllComponent()
        selectRadioButtonInFirstLaunch()
    }

    interface BottomSheetFilterForhatListener :
        BottomSheetDialogFilterForHatLama.BottomSheetFilterForhatListener {
        fun sendDataBottomSheetToForhatSearchActivity()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        contextBottomSheet = context
        try{
            mListener = context as BottomSheetDialogFilterForhat.BottomSheetFilterForhatListener
        }
        catch(e: ClassCastException)
        {
            throw java.lang.ClassCastException("${context.toString()} must implements BottomSheetListener")
        }
    }
}