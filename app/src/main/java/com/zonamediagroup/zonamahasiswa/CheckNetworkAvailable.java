package com.zonamediagroup.zonamahasiswa;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import java.net.InetAddress;

public class CheckNetworkAvailable {
    public static boolean check(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        boolean result = connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
        if(!result)
        Toast.makeText(context,context.getString(R.string.no_connection),Toast.LENGTH_SHORT).show();
        return result;
    }
}
