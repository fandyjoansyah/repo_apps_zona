package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Kategori_post_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Kategori_post_Model;
import com.zonamediagroup.zonamahasiswa.models.Recyclerview_home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Post_by_kategori extends AppCompatActivity implements Kategori_post_Adapter.ContactsAdapterListener {

    private static final String TAG = Home.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";
    //private String URL_home = Server.URL_REAL + "Post_by_id_categori";
    private String URL_home = Server.URL_REAL + "Post_by_id_categori_dengan_waktu";
    //URL untuk mendapatkan nama author
    private String URL_nama_author = Server.URL_WORDPRESS_NAMA_AUTHOR;

    private String URL_home_save = Server.URL_REAL + "Save_like";

    private String URL_hapus_save = Server.URL_REAL + "Delete_westlist";




    String id_kategori;
    String id_categori_wp;
    String nama_kategori;
    String gambar_kategori;
    String warna_kategori;

    String id_user_sv;

    NestedScrollView nestedscrol_post_by_kategori;
    int page = 1;


    //    loading bar
    ProgressBar progressBar;
    Sprite doubleBounce;
    LinearLayout layoutloading_bar;

    // receler_bykategori
    RecyclerView receler_bykategori;
    private List<Kategori_post_Model> receler_kategori_postModels;
    private Kategori_post_Adapter receler_kategori_postAdapter;

    TextView nama_kategorix;


    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok;

    boolean scrool = false;

    ImageView back;

    //nama author yang tulisannya benar
    String nameAuthor;

    // parameter result
    int LAUNCH_SECOND_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_post_by_kategori);
        terimadata();
        id_user_sv = SharedPrefManagerZona.getInstance(Post_by_kategori.this).getid_user();

        //        Loading bar pagination
        progressBar = (ProgressBar) findViewById(R.id.spin_kit_loading);
        doubleBounce = new ThreeBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);
        layoutloading_bar = findViewById(R.id.layoutloading_bar);


        nestedscrol_post_by_kategori = findViewById(R.id.nestedscrol_post_by_kategori);

        nestedscrol_post_by_kategori.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                // on scroll change we are checking when users scroll as bottom.
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    // in this method we are incrementing page number,
                    // making progress bar visible and calling get data method.

                    scrool = true;
//                    loading_show();
                    loading_bar_show();
//                    Toast.makeText(mActivity, "TOAS DATA ", Toast.LENGTH_SHORT).show();
                    page++;
//                    Call API
                    get_kategori_post(String.valueOf(page), id_user_sv, id_categori_wp);

                }
            }
        });

        // recelerview
        receler_bykategori = findViewById(R.id.receler_bykategori);

        receler_kategori_postModels = new ArrayList<>();
        receler_kategori_postAdapter = new Kategori_post_Adapter(this, receler_kategori_postModels, this);
        LinearLayoutManager mLayoutManagerkategori;
        mLayoutManagerkategori = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        receler_bykategori.setLayoutManager(mLayoutManagerkategori);
        receler_bykategori.setItemAnimator(new DefaultItemAnimator());
        receler_bykategori.setAdapter(receler_kategori_postAdapter);


        nama_kategorix = findViewById(R.id.nama_kategorix);

        nama_kategorix.setText(nama_kategori);


//        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);
//        check apakah loading layar tampil, apabila tampil maka sembunyikan

//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();

                get_kategori_post(String.valueOf(page), id_user_sv, id_categori_wp);

            }
        });

        search = findViewById(R.id.search);
        saveok = findViewById(R.id.saveok);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Post_by_kategori.this, Save.class);

                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Post_by_kategori.this, Search.class);

                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);


            }
        });

        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                finish();

            }
        });
        eror_dismiss();
        loading_show();


        get_kategori_post(String.valueOf(page), id_user_sv, id_categori_wp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //jika resume tapi ternyata masih loading (karena backpress dari stack activity selanjutnya), maka sembunyikan iklan tersebut
        if(loding_layar.getVisibility() == View.VISIBLE)
        {
           // loading_dismiss();
        }
    }

    public void terimadata() {
        Bundle data = getIntent().getExtras();
        id_kategori = data.getString("id_kategori");
        id_categori_wp = data.getString("id_categori_wp");
        nama_kategori = data.getString("nama_kategori");
        gambar_kategori = data.getString("gambar_kategori");
        warna_kategori = data.getString("warna_kategori");
    }

    public void loading_bar_dismiss() {
        layoutloading_bar.setVisibility(View.INVISIBLE);
    }

    public void loading_bar_show() {
        layoutloading_bar.setVisibility(View.VISIBLE);
    }

    private void get_kategori_post(String halamanaktif, String id_user, String id_categori_wp) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data Global");


                    if (jObj.getString("status").equals("true")) {

                        if (page == 1) {
                            receler_kategori_postModels.clear();
                            receler_kategori_postAdapter.notifyDataSetChanged();
                        }

                        JSONArray dataArray = jObj.getJSONArray("item_terbaru");

                        List<Kategori_post_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Kategori_post_Model>>() {
                        }.getType());


                        // adding contacts to contacts list
                        receler_kategori_postModels.addAll(items);
                        // refreshing recycler view
                        receler_kategori_postAdapter.notifyDataSetChanged();

                        loading_dismiss();

                        if (scrool) {
                            loading_bar_dismiss();
                            scrool = false;
                        }


                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();

                        if (scrool) {
                            loading_bar_dismiss();
                            scrool = false;
                        }

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_show();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                eror_show();


            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("halamanaktif", halamanaktif);
                Log.d("DEBUGZONA_POSTKATEGORI","halaman aktif "+halamanaktif);
                params.put("id_user", id_user);
                Log.d("DEBUGZONA_POSTKATEGORI","id_user"+id_user);
                params.put("id_categori", id_categori_wp);
                Log.d("DEBUGZONA_POSTKATEGORI","id_categori"+id_categori_wp);
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public void onContactSelected(Kategori_post_Model itemsatu) {
//        String namaAuthor = funcDapatkanNamaAuthor(itemsatu.getPost_author(), itemsatu);
                            funcTampilkanArtikel(itemsatu);

    }


    //fungsi ini untuk memanggil artikel yang akan dibaca
    public boolean funcTampilkanArtikel(Kategori_post_Model itemsatu)
    {
        Intent intent = new Intent(Post_by_kategori.this, Detail.class);
        intent.putExtra("id_post", itemsatu.getID());
        intent.putExtra("post_content", itemsatu.getPost_content());
        intent.putExtra("humnile_post", itemsatu.getMeta_value());
        intent.putExtra("title_post", itemsatu.getPost_title());
        intent.putExtra("name_categori", itemsatu.getName());
        intent.putExtra("date_post", itemsatu.getPost_date());
        intent.putExtra("name_auth", itemsatu.getUser_login());
        intent.putExtra("id_author", itemsatu.getPost_author());
        //Log.d("DEBUGZONA_VOLLEY_POST_BY_KATEGORI",itemsatu.getPost_author());

        intent.putExtra("img_author", itemsatu.getGambar_user_wp());
        intent.putExtra("link", itemsatu.getGuid());
            intent.putExtra("save", itemsatu.getLike());
        intent.putExtra("id_kategori", itemsatu.getTerm_id());
        //startActivity(intent);
        startActivityForResult(intent,LAUNCH_SECOND_ACTIVITY);
        return true;
    }


    @Override
    public void Save(Kategori_post_Model save, int position) {


        String id_post_sv = save.getID();

        save_like(id_user_sv, id_post_sv);


        //codingan agar ketika icon simpan di klik lalu load artikel lebih banyak, icon simpan tadi tidak berubah menjadi semula
        //buat postModels ke position menjadi ada simpan
        save.setLike("1");

    }

    @Override
    public void unSave(Kategori_post_Model save, int position) {
        String id_post_sv = save.getID();

        delete_simpan(id_user_sv, id_post_sv);


        //codingan agar ketika icon simpan di klik lalu load artikel lebih banyak, icon simpan tadi tidak berubah menjadi semula
        //buat postModels ke position menjadi tidak ada simpan
       save.setLike("0");
    }

    private void save_like(String id_user, String id_post) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_home_save, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data SAVE LIKE");


                    if (jObj.getString("status").equals("true")) {


                    } else if (jObj.getString("status").equals("false")) {


                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());


            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_post", id_post);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void delete_simpan(String id_user, String id_post) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_save, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data SAVE LIKE");


                    if (jObj.getString("status").equals("true")) {

                    } else if (jObj.getString("status").equals("false")) {

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());



            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_post", id_post);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    // result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {


                page = 1;
                loading_show();
                get_kategori_post(String.valueOf(page), id_user_sv, id_categori_wp);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    } //onActivityResult
}