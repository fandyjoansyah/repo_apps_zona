package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.messaging.FirebaseMessaging;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class PendaftaranAkun extends AppCompatActivity implements View.OnFocusChangeListener, View.OnClickListener {
    private static final String URL_DAFTAR_PENGGUNA = Server.URL_DAFTAR_MASUK+"registrasi_pengguna_baru";
    private static final String URL_VALIDASI_INPUT =  Server.URL_DAFTAR_MASUK+"Validasi_input_unik/";
    EditText edt_email,edt_no_handphone,edt_univertitas_instansi,edt_nama,edt_nama_pengguna,edt_domisili;
    TextInputEditText edt_kata_sandi,edt_konfirmasi_kata_sandi;
    TextView error_for_radio_group_jenis_kelamin, error_for_edt_email, error_for_edt_no_handphone,error_for_edt_univertitas_instansi,error_for_edt_nama_pengguna,error_for_edt_domisili,error_for_ly_kata_sandi,error_for_ly_konfirmasi_kata_sandi, error_for_edt_nama,error_for_checkbox_kebijakan_privasi;
    TextView tv_kebijakan_privasi, tv_keterangan_btn_daftar, txt_himbauan;
    RelativeLayout btn_daftar;
    Button btn_kembali;
    ArrayList<TextView> semuaPesanError;
    ArrayList<EditText> semuaEditText;
    RadioGroup radio_group_jenis_kelamin;
    RadioButton radio_laki, radio_perempuan;
    CheckBox checkbox_kebijakan_privasi;
    ProgressBar progress_btn_daftar;
    Animation fade_in, fade_out_to_04, fade_out_permanent;
    RelativeLayout relative_popup_berhasil_daftar;
    ImageView img_eye,konfirmasi_img_eye, img_centang_daftar;
    CircleImageView img_profil_gmail;
    ScrollView scrollview_pendaftaran;


    private static final String URL_CEK_UNIK_EMAIL = "validasi_email_unik";
    private static final String URL_CEK_UNIK_USERNAME = "validasi_username_unik";
    private static final String URL_CEK_UNIK_NOMOR_TELEPON = "validasi_nomor_telepon_unik";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_pendaftaran_akun);
        initArrayListSemuaPesanError();
        initArrayListSemuaEditText();
        findViewByIdAllComponent();
        setListenerAllComponent();
        checkDaftarGmailAtauManual();


    }

    private void checkDaftarGmailAtauManual(){
        boolean test = checkApakahAdaDataIntentGmail();
        if(test)
        {
            siapkanDataUntukPendaftaranMelaluiGmail();
        }
    }

    private void siapkanDataUntukPendaftaranMelaluiGmail()
    {
        setGambarToImgProfileGmail(getIntent().getStringExtra("personPhoto"));
        showImgProfileGmail();
        setGmailToEditTextEmail( getIntent().getStringExtra("gmail"));
        disableEditTextEmail();
        makeEditTextGmailGrey();
    }

    private void setGambarToImgProfileGmail(String urlGambar){
        Glide.with(PendaftaranAkun.this)
                .load(urlGambar)
                .apply(RequestOptions.fitCenterTransform())
                .into(img_profil_gmail);
    }

    private void showImgProfileGmail(){
        img_profil_gmail.setVisibility(View.VISIBLE);
    }

    private void setGmailToEditTextEmail(String namaEmail){
        edt_email.setText(namaEmail);
    }

    private void makeEditTextGmailGrey(){
        edt_email.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#E7DEDE")));
    }

    private void disableEditTextEmail(){
        edt_email.setEnabled(false);
    }


    private void initArrayListSemuaPesanError(){
        semuaPesanError = new ArrayList<>();
    }

    private void initArrayListSemuaEditText(){
        semuaEditText = new ArrayList<>();
    }

    private void addTextViewErrorToArrayListSemuaPesanError(TextView tv){
        semuaPesanError.add(tv);
    }

    private void addEditTextToArrayListSemuaEditText(EditText edt){
        semuaEditText.add(edt);
    }

    private boolean checkApakahAdaDataIntentGmail(){
        if(getIntent().getStringExtra("gmail")==null && getIntent().getStringExtra("personName")==null)
        {
            return false;
        }
        else
            return true;
    }

    public void findViewByIdAllComponent(){

        scrollview_pendaftaran = findViewById(R.id.scrollview_pendaftaran);

        tv_kebijakan_privasi = findViewById(R.id.tv_kebijakan_privasi);
        txt_himbauan = findViewById(R.id.txt_himbauan);

        edt_email = findViewById(R.id.edt_email);
        addEditTextToArrayListSemuaEditText(edt_email);

        edt_no_handphone = findViewById(R.id.edt_no_handphone);
        addEditTextToArrayListSemuaEditText(edt_no_handphone);

        edt_univertitas_instansi = findViewById(R.id.edt_universitas_instansi);
        addEditTextToArrayListSemuaEditText(edt_univertitas_instansi);

        edt_nama = findViewById(R.id.edt_nama);
        addEditTextToArrayListSemuaEditText(edt_nama);

        edt_nama_pengguna = findViewById(R.id.edt_nama_pengguna);
        addEditTextToArrayListSemuaEditText(edt_nama_pengguna);

        edt_domisili = findViewById(R.id.edt_domisili);
        addEditTextToArrayListSemuaEditText(edt_domisili);

        edt_kata_sandi = findViewById(R.id.edt_kata_sandi);
        addEditTextToArrayListSemuaEditText(edt_kata_sandi);

        edt_konfirmasi_kata_sandi = findViewById(R.id.edt_konfirmasi_kata_sandi);
        addEditTextToArrayListSemuaEditText(edt_konfirmasi_kata_sandi);

        error_for_edt_email = findViewById(R.id.error_for_edt_email);
        addTextViewErrorToArrayListSemuaPesanError(error_for_edt_email);

        error_for_edt_no_handphone = findViewById(R.id.error_for_edt_no_handphone);
        addTextViewErrorToArrayListSemuaPesanError(error_for_edt_no_handphone);

        error_for_edt_univertitas_instansi = findViewById(R.id.error_for_universitas_instansi);
        addTextViewErrorToArrayListSemuaPesanError(error_for_edt_univertitas_instansi);


        error_for_edt_nama_pengguna = findViewById(R.id.error_for_edt_nama_pengguna);
        addTextViewErrorToArrayListSemuaPesanError(error_for_edt_nama_pengguna);

        error_for_edt_domisili = findViewById(R.id.error_for_edt_domisili);
        addTextViewErrorToArrayListSemuaPesanError(error_for_edt_domisili);

        error_for_ly_kata_sandi = findViewById(R.id.error_for_ly_kata_sandi);
        addTextViewErrorToArrayListSemuaPesanError(error_for_ly_kata_sandi);

        error_for_ly_konfirmasi_kata_sandi= findViewById(R.id.error_for_ly_konfirmasi_kata_sandi);
        addTextViewErrorToArrayListSemuaPesanError(error_for_ly_konfirmasi_kata_sandi);

        error_for_edt_nama = findViewById(R.id.error_for_edt_nama);
        addTextViewErrorToArrayListSemuaPesanError(error_for_edt_nama);

        error_for_radio_group_jenis_kelamin = findViewById(R.id.error_for_radio_group_jenis_kelamin);
        addTextViewErrorToArrayListSemuaPesanError(error_for_radio_group_jenis_kelamin);

        error_for_checkbox_kebijakan_privasi = findViewById(R.id.error_for_checkbox_kebijakan_privasi);
        addTextViewErrorToArrayListSemuaPesanError(error_for_checkbox_kebijakan_privasi);

        btn_daftar = findViewById(R.id.btn_daftar);
        btn_kembali = findViewById(R.id.btn_kembali);

        radio_group_jenis_kelamin = findViewById(R.id.radio_group_jenis_kelamin);

        checkbox_kebijakan_privasi = findViewById(R.id.checkbox_kebijakan_privasi);


        tv_keterangan_btn_daftar = findViewById(R.id.tv_keterangan_btn_daftar);
        progress_btn_daftar = findViewById(R.id.progress_btn_daftar);

        radio_laki = findViewById(R.id.radio_laki);
        radio_perempuan = findViewById(R.id.radio_perempuan);

        img_centang_daftar = findViewById(R.id.img_centang_daftar);

        //init animation
        fade_in = AnimationUtils.loadAnimation(PendaftaranAkun.this,R.anim.fade_in);
        fade_out_to_04 = AnimationUtils.loadAnimation(PendaftaranAkun.this,R.anim.fade_out_to_04);
        fade_out_permanent = AnimationUtils.loadAnimation(PendaftaranAkun.this,R.anim.fade_out_permanent);

        img_profil_gmail = findViewById(R.id.img_profil_gmail);

    }

    private void emptyEdtKonfirmasiKataSandi(){
        hideErrorTextView(error_for_ly_konfirmasi_kata_sandi);
        edt_konfirmasi_kata_sandi.setText("");

    }

    public void setListenerAllComponent(){

        //listener textview kebijakan privasi
//        tv_kebijakan_privasi.setClickable(true);
//        tv_kebijakan_privasi.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "Silahkan menyetujui <u><a style='color:yellow;' href='javascript:'> syarat dan ketentuan </a></u> untuk mendaftar";
        tv_kebijakan_privasi.setText(Html.fromHtml(text));
            tv_kebijakan_privasi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(PendaftaranAkun.this,WebViewZona.class);
                    i.putExtra("link","https://zonamahasiswa.id");
                    i.putExtra("pageTitle","Syarat dan Ketentuan");
                    startActivity(i);
                }
            });

        //listener onFocusChange (lost focus)
        edt_email.setOnFocusChangeListener(this);
        edt_no_handphone.setOnFocusChangeListener(this);
        edt_univertitas_instansi.setOnFocusChangeListener(this);
        edt_nama.setOnFocusChangeListener(this);
        edt_nama_pengguna.setOnFocusChangeListener(this);
        edt_domisili.setOnFocusChangeListener(this);
        edt_kata_sandi.setOnFocusChangeListener(this);
        edt_konfirmasi_kata_sandi.setOnFocusChangeListener(this);

        //listener onTextChanged
        edt_kata_sandi.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

           }

           @Override
           public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               emptyEdtKonfirmasiKataSandi();
           }

           @Override
           public void afterTextChanged(Editable editable) {

           }
       });

        edt_nama_pengguna.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!checkValidasiTitikDiawalDanDiakhirKata(edt_nama_pengguna) && (error_for_edt_nama_pengguna.getVisibility() == View.GONE || error_for_edt_nama_pengguna.getVisibility() == View.INVISIBLE))
                {
                    showErrorTextViewWithoutAnimation(error_for_edt_nama_pengguna,"Titik hanya bisa di tengah kalimat");
                }
                else if((checkValidasiTitikDiawalDanDiakhirKata(edt_nama_pengguna) || edt_nama_pengguna.getText().toString().length() == 0) == true && error_for_edt_nama_pengguna.getVisibility() == View.VISIBLE){
                    hideErrorTextView(error_for_edt_nama_pengguna);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });





        //listener button onclick
        btn_daftar.setOnClickListener(this);
        btn_kembali.setOnClickListener(this);

        //listener radio onclick
        radio_laki.setOnClickListener(this);
        radio_perempuan.setOnClickListener(this);

        //listener checkbox
        checkbox_kebijakan_privasi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cekValidasiKomponenInput(R.id.checkbox_kebijakan_privasi);
            }
        });

    }


    private boolean cekApakahCheckBoxKebijakanPrivasiDisetujui(){
        if(checkbox_kebijakan_privasi.isChecked())
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    private boolean checkValidasiInputKosong(EditText edt){
        if(edt.getText().toString().trim().length() == 0)
        {
            return false;
        }
        return true;
    }

    private boolean checkValidasiTitikDiawalDanDiakhirKata(EditText edt){
        String inputan = edt.getText().toString();
        if(inputan.length() > 0) {
            if (inputan.charAt(0) == '.' || inputan.charAt(inputan.length() - 1) == '.') {
                return false;
            }
        }
        return true;
    }

    private boolean checkValidasiInputEmail(String target){
        if(Patterns.EMAIL_ADDRESS.matcher(target).matches())
        return true;
        else
            return false;
    }

    private boolean checkValidasiInputPhone(String target){
        if(Patterns.PHONE.matcher(target).matches())
            return true;
        else
            return false;
    }

    private boolean checkValidasiMinimalKarakter(String input, int panjangMinimal)
    {
        if(input.length() < panjangMinimal)
        {
            return false;
        }
        else{
            return true;
        }
    }

    private boolean checkValidasiKesamaanKataSandiDenganKonfirmasiKataSandi(String kataSandi, String konfirmasiKataSandi){
        if(!kataSandi.equals(konfirmasiKataSandi))
        {
            return false;
        }
        else{
            return true;
        }
    }

    private void tampilkanErrorMessageUntukValidasiServer(String urlEndPoint,String pesanError)
    {
        switch(urlEndPoint){
            case URL_CEK_UNIK_USERNAME:
                showErrorTextView(error_for_edt_nama_pengguna,pesanError);
                break;
            case URL_CEK_UNIK_EMAIL:
                showErrorTextView(error_for_edt_email,pesanError);
                break;
            case URL_CEK_UNIK_NOMOR_TELEPON:
                showErrorTextView(error_for_edt_no_handphone,pesanError);
                break;
        }
    }

    private void showToastTerjadiKesalahan(){
        CustomToast.l(getApplicationContext(),"Terjadi kesalahan. Coba lagi nanti");
    }

    private void checkValidasiSudahAdaDiServer(String urlEndPoint, String value)
    {

        String tokenApi="qwerty";
        String url=URL_VALIDASI_INPUT+urlEndPoint+"?TOKEN="+tokenApi+"&value_param="+value;
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean status = jObj.getBoolean("status");
                    //jika status false maka di server sudah ada
                    if (status == false) {
                        tampilkanErrorMessageUntukValidasiServer(urlEndPoint, jObj.getString("message"));
                    }
                }
                catch (JSONException e)
                {
                    showToastTerjadiKesalahan();
                    nonaktifkanButtonLoading();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showToastTerjadiKesalahan();
                nonaktifkanButtonLoading();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("PendaftaranAKun", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, RuleValueAplikasi.tag_json_obj);

    }

    private void cekValidasiKomponenInput(int idKomponenInput){
        int minCharPassword = 8;
        switch(idKomponenInput)
        {
            case R.id.edt_email:

                    hideErrorTextView(error_for_edt_email);
                    if(!checkValidasiInputKosong(edt_email))
                    {
                        showErrorTextView(error_for_edt_email,"Email tidak boleh kosong");
                    }
                    else if(!checkValidasiInputEmail(edt_email.getText().toString())){
                        showErrorTextView(error_for_edt_email,"Format penulisan email salah");
                    }
                    else if(!EmailValidator.checkValidasiDomainAllowed(edt_email.getText().toString()))
                    {
                        showErrorTextView(error_for_edt_email,"Format penulisan email salah");
                    }


                break;
            case R.id.edt_no_handphone:

                    int minCharHandphone = 10;
                    hideErrorTextView(error_for_edt_no_handphone);
                    if(!checkValidasiInputKosong(edt_no_handphone))
                    {
                        showErrorTextView(error_for_edt_no_handphone,"Nomor handphone tidak boleh kosong");
                    }
                    else if(!checkValidasiMinimalKarakter(edt_no_handphone.getText().toString(),minCharHandphone)){
                        showErrorTextView(error_for_edt_no_handphone,"Masukan minimal "+minCharHandphone+" digit nomor telepon");
                    }
                    else if(!checkValidasiInputPhone(edt_no_handphone.getText().toString())){
                        showErrorTextView(error_for_edt_no_handphone,"Format penulisan nomor handphone salah");
                    }

                break;
            case R.id.edt_nama:
                    int minCharNama = 4;
                    hideErrorTextView(error_for_edt_nama);
                    if(!checkValidasiInputKosong(edt_nama))
                    {
                        showErrorTextView(error_for_edt_nama,"Nama tidak boleh kosong");
                    }
                    else if(!checkValidasiMinimalKarakter(edt_nama.getText().toString(),minCharNama)){
                        showErrorTextView(error_for_edt_nama,"Nama minimal "+minCharNama+" karakter");
                    }

                break;
            case R.id.edt_nama_pengguna:

                    int minCharNamaPengguna = 4;
                    hideErrorTextView(error_for_edt_nama_pengguna);
                    if(!checkValidasiInputKosong(edt_nama_pengguna))
                    {
                        showErrorTextView(error_for_edt_nama_pengguna,"Nama pengguna tidak boleh kosong");
                    }
                    else if(!checkValidasiMinimalKarakter(edt_nama_pengguna.getText().toString(),minCharNamaPengguna)){
                        showErrorTextView(error_for_edt_nama_pengguna,"Nama pengguna minimal "+minCharNamaPengguna+" karakter");
                    }
                    else if(!checkValidasiMengandungSpasi(edt_nama_pengguna.getText().toString())){
                        showErrorTextView(error_for_edt_nama_pengguna,"Nama pengguna tidak boleh mengandung spasi ");
                    }
                    else if(!checkValidasiTitikDiawalDanDiakhirKata(edt_nama_pengguna))
                    {
                        showErrorTextView(error_for_edt_nama_pengguna,"Titik hanya bisa di tengah kalimat");
                    }
                break;
            case R.id.edt_kata_sandi:


                    hideErrorTextView(error_for_ly_kata_sandi);
                    if(!checkValidasiInputKosong(edt_kata_sandi))
                    {
                        showErrorTextView(error_for_ly_kata_sandi,"Kata sandi tidak boleh kosong");
                    }
                    else if(!checkValidasiMinimalKarakter(edt_kata_sandi.getText().toString(),minCharPassword)){
                        showErrorTextView(error_for_ly_kata_sandi,"Kata sandi minimal "+minCharPassword+" karakter");
                    }

                break;
            case R.id.edt_konfirmasi_kata_sandi:

                    hideErrorTextView(error_for_ly_konfirmasi_kata_sandi);
                    if(!checkValidasiInputKosong(edt_konfirmasi_kata_sandi))
                    {
                        showErrorTextView(error_for_ly_konfirmasi_kata_sandi,"Konfirmasi kata sandi tidak boleh kosong");
                    }
                    else if(!checkValidasiMinimalKarakter(edt_konfirmasi_kata_sandi.getText().toString(),minCharPassword)){
                        showErrorTextView(error_for_ly_konfirmasi_kata_sandi,"Konfirmasi kata sandi minimal "+minCharPassword+" karakter");
                    }
                    else if(!checkValidasiKesamaanKataSandiDenganKonfirmasiKataSandi(edt_kata_sandi.getText().toString(),edt_konfirmasi_kata_sandi.getText().toString())){
                        showErrorTextView(error_for_ly_konfirmasi_kata_sandi,"Password yang kamu masukkan tidak sama");
                    }
                break;
            case R.id.radio_group_jenis_kelamin:
            case R.id.radio_laki:
            case R.id.radio_perempuan:
                hideErrorTextView(error_for_radio_group_jenis_kelamin);
                if(!cekApakahRadioJeniskelaminSudahTerpilih(radio_group_jenis_kelamin))
                {
                    showErrorTextView(error_for_radio_group_jenis_kelamin,"Jenis kelamin belum dipilih");
                }
                break;
            case R.id.checkbox_kebijakan_privasi:
                hideErrorTextView(error_for_checkbox_kebijakan_privasi);
                if(!cekApakahCheckBoxKebijakanPrivasiDisetujui())
                {
                    showErrorTextView(error_for_checkbox_kebijakan_privasi,"Kamu belum menyetujui syarat dan ketentuan");
                }
                break;
            case R.id.edt_universitas_instansi:
                int minCharUniversitasInstansi = 4;
                hideErrorTextView(error_for_edt_univertitas_instansi);

                if(!checkValidasiMinimalKarakter(edt_univertitas_instansi.getText().toString(),minCharUniversitasInstansi)){
                    showErrorTextView(error_for_edt_univertitas_instansi,"Universitas atau instansi minimal "+minCharUniversitasInstansi+" karakter");
                }

                break;
        }
    }

    private boolean checkValidasiMengandungSpasi(String input) {
    if(input.contains(" "))
    {
        return false;
    }
    else{
        return true;
    }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        int idCurrentComponent = view.getId();

        if(!hasFocus)
        {
            cekValidasiKomponenInput(idCurrentComponent);
        }

    }

    private void showErrorTextView(TextView tv,String pesanError){
        tv.startAnimation(fade_in);
        tv.setVisibility(View.VISIBLE);
        tv.setText(pesanError);
    }

    private void showErrorTextViewWithoutAnimation(TextView tv,String pesanError){
        tv.setVisibility(View.VISIBLE);
        tv.setText(pesanError);
    }

    private void hideErrorTextView(TextView tv){
        tv.setVisibility(View.INVISIBLE);
    }

    private boolean cekApakahAdaPesanErrorTampil(){

        for(int i=0;i<semuaPesanError.size();i++){
            Log.d("fandy_debug_11_mei","++++++++++++++++++++++++++++++");
            Log.d("fandy_debug_11_mei","pesan error komponennya: "+semuaPesanError.get(i).getText());

            int visibility = semuaPesanError.get(i).getVisibility();
            if(visibility == View.VISIBLE)
            {
                Log.d("fandy_debug_11_mei","view visible ");
            }
            else if(visibility == View.INVISIBLE)
            {
                Log.d("fandy_debug_11_mei","view invisible ");
            }
            else if(visibility == View.GONE)
            {
                Log.d("fandy_debug_11_mei","view gone ");
            }
            else{
                Log.d("fandy_debug_11_mei","view else ");
            }
            Log.d("fandy_debug_11_mei","visibility: "+semuaPesanError.get(i).getText());
            Log.d("fandy_debug_11_mei","++++++++++++++++++++++++++++++");

            if(semuaPesanError.get(i).getVisibility() == View.VISIBLE)
            {
                Log.d("fandy_debug_11_mei","===============================");
                Log.d("fandy_debug_11_mei","pesan error komponennya: "+semuaPesanError.get(i).getText());

                Log.d("fandy_debug_11_mei","visibility: "+semuaPesanError.get(i).getText());
                Log.d("fandy_debug_11_mei","===============================");

                return true; //true berarti ada pesan error yang tampil
            }
        }
        return false; //false berarti tidak ada pesan error yang tampil
    }

    private String getSelectedRadioButtonJenisKelamin(){
        int idRadioButtonTerpilih = radio_group_jenis_kelamin.getCheckedRadioButtonId();
        RadioButton radioButtonTerpilih = (RadioButton)findViewById(idRadioButtonTerpilih);
        String valueRadio = radioButtonTerpilih.getText().toString();
        return valueRadio.toLowerCase();
    }

    private void scrollToFirstErrorEncountered()
    {
        for(int i=0;i< semuaPesanError.size();i++)
        {
            if(semuaPesanError.get(i).getVisibility() == View.VISIBLE)
            {
                TextView tvError = semuaPesanError.get(i);
                scrollview_pendaftaran.smoothScrollTo(0,tvError.getTop());
                break;
            }
        }

    }



    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_daftar)
        {
            //step pv1.cek input secara keseluruhan apakah ada yang salah
            cekSemuaInput();

            //step pv3.cek apakah ada error yang tampil, apabila ada yang tampil maka jangan perbolehkan daftar
            boolean bolehDaftar = !cekApakahAdaPesanErrorTampil();
            if(bolehDaftar)
            {
                aktifkanButtonLoading();
                String jenisKelamin = getSelectedRadioButtonJenisKelamin();
               insertDataPendaftaranKeDb(edt_email.getText().toString().trim(),edt_nama.getText().toString().trim(),edt_no_handphone.getText().toString().trim(),edt_univertitas_instansi.getText().toString().trim(),edt_domisili.getText().toString().trim(),jenisKelamin,edt_nama_pengguna.getText().toString(),edt_kata_sandi.getText().toString());
            }
            else
            {
                scrollToFirstErrorEncountered();
            }

        }
        if(view.getId() == R.id.btn_kembali)
        {
            onBackPressed();
//            CustomToast.zonaToast("zona toast itu memang keren karena itu adalah hal yang sangat tidak main main loh sam oyi kan sam oyi siap mantap djiwa",PendaftaranAkun.this);
        }

        //jika radio jenis kelamin di klik
        if(view.getId() == R.id.radio_laki)
        {
            cekValidasiKomponenInput(R.id.radio_laki);
        }
        if(view.getId() == R.id.radio_perempuan)
        {
            cekValidasiKomponenInput(R.id.radio_perempuan);
        }

    }



    private void showPopUpBerhasilDaftar(){
         AlertDialog.Builder dialogBuilder;
         AlertDialog dialog;
        Button btn_meluncur;
        RelativeLayout relative_popup_berhasil_daftar;
        dialogBuilder = new AlertDialog.Builder(PendaftaranAkun.this);
        final View vPopUp = getLayoutInflater().inflate(R.layout.popup_pendaftaran_berhasil,null);

        ProgressBar progressBarLoadingPopupDaftar = (ProgressBar)vPopUp.findViewById(R.id.spin_kit_popup_daftar);
        Sprite fadingCircle = new FadingCircle();
        progressBarLoadingPopupDaftar.setIndeterminateDrawable(fadingCircle);


        btn_meluncur = vPopUp.findViewById(R.id.btn_meluncur);
        relative_popup_berhasil_daftar = vPopUp.findViewById(R.id.relative_popup_berhasil_daftar);

        btn_meluncur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBarLoadingPopupDaftar.setVisibility(View.VISIBLE);
                relative_popup_berhasil_daftar.startAnimation(fade_out_permanent);

                new Delay().delayTask(2500, new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        goToMainActivity();
                    }
                });
            }
        });
        dialogBuilder.setView(vPopUp);
        dialog = dialogBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
        vPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    private void goToMainActivity() {
        Intent i = new Intent(PendaftaranAkun.this,MainActivity.class);
        startActivity(i);
        finish();
    }
    private boolean saveSession(String id_user,String person_id,String person_name,String person_given_name,String person_family_name,String person_email,String person_photo, String person_gender, String username, String nama_anonim){

        // sesion save
        Boolean a = SharedPrefManagerZona.getInstance(getApplicationContext()).saveData(id_user, person_id, person_name,person_given_name,
                person_family_name, person_email, person_photo, person_gender, username,nama_anonim);
        if(a)
        {
            return true;
        }
        else{
            return false;
        }

    }
    private void addRecentRegisteredAccountToSharedPreferences(JSONArray responseLastUserInserted){
        try {
            JSONObject jObj = responseLastUserInserted.getJSONObject(0);
            String id_user = jObj.getString("id_user");

            /*jika person_id = "", itu karena pengguna mendaftar manual, maka person_id = id_user*/
            String person_id="";
            if(jObj.getString("person_id").equals(""))
            {
                 person_id = jObj.getString("id_user");
            }
            else{
                 person_id = jObj.getString("person_id");
            }


            String person_name = jObj.getString("person_name");
            String person_given_name = jObj.getString("person_given_name");
            String person_family_name = jObj.getString("person_family_name");
            String person_email = jObj.getString("person_email");
            String person_photo = jObj.getString("person_photo");
            String person_gender = jObj.getString("person_gender");
            String username = jObj.getString("username");
            String nama_anonim = jObj.getString("nama_anonim");
            boolean resultSaveSession = saveSession(id_user,person_id,person_name,person_given_name,person_family_name,person_email,person_photo,person_gender,username,nama_anonim);
            if (resultSaveSession) {
                subscribeToTopicFirebase();
            }
            else
            {
                //terjadi kesalahan saat insert shared preferences
            }
        }
        catch(Exception e)
        {

        }



    }

        private void subscribeToTopicFirebase(){
//                        Subcribe ke topik. Toggle salah satu
            String topic = "notif-zona-develop";
//                            String topic = "notif-zona-production";
            FirebaseMessaging.getInstance().subscribeToTopic(topic);


//                        End Topic

        }

    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    private void toggleEyeIcon(){
        if(img_eye.getDrawable().getConstantState().equals(img_eye.getContext().getDrawable(R.drawable.eye).getConstantState()))
        {
            funcTransisiGambar(img_eye,img_eye.getContext().getDrawable(R.drawable.eye_slash),150,150);
        }
        else if(img_eye.getDrawable().getConstantState().equals(img_eye.getContext().getDrawable(R.drawable.eye_slash).getConstantState()))
        {

            funcTransisiGambar(img_eye,img_eye.getContext().getDrawable(R.drawable.eye),150,150);
        }
        else
        {
            funcTransisiGambar(img_eye,getApplicationContext().getDrawable(R.drawable.eye_slash),150,150);
        }
    }


    private void togglePassword(){

        if(edt_kata_sandi.getTransformationMethod() == null)
        {
            edt_kata_sandi.setTransformationMethod(new PasswordTransformationMethod());
        }
        else
        {
            edt_kata_sandi.setTransformationMethod(null);
        }
    }

    private void toggleEyeIconKonfirmasi(){
        if(konfirmasi_img_eye.getDrawable().getConstantState().equals(konfirmasi_img_eye.getContext().getDrawable(R.drawable.eye).getConstantState()))
        {
            funcTransisiGambar(konfirmasi_img_eye,konfirmasi_img_eye.getContext().getDrawable(R.drawable.eye_slash),150,150);
        }
        else if(konfirmasi_img_eye.getDrawable().getConstantState().equals(konfirmasi_img_eye.getContext().getDrawable(R.drawable.eye_slash).getConstantState()))
        {

            funcTransisiGambar(konfirmasi_img_eye,konfirmasi_img_eye.getContext().getDrawable(R.drawable.eye),150,150);
        }
        else
        {
            funcTransisiGambar(konfirmasi_img_eye,getApplicationContext().getDrawable(R.drawable.eye_slash),150,150);
        }
    }


    private void togglePasswordKonfirmasi(){

        if(edt_konfirmasi_kata_sandi.getTransformationMethod() == null)
        {
            edt_konfirmasi_kata_sandi.setTransformationMethod(new PasswordTransformationMethod());
        }
        else
        {
            edt_konfirmasi_kata_sandi.setTransformationMethod(null);
        }
    }

    private void insertDataPendaftaranKeDb(String email,String nama,String nomorTelepon,String universitas,String alamat,String jenis_kelamin,String username,String password_1) {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_DAFTAR_PENGGUNA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("fandy_13_mei","insertDaftar onResponse");
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean pendaftaranberhasil = jObj.getBoolean("status");
                    Log.d("fandy_13_mei","insertDaftar try");
                    if(pendaftaranberhasil == true){
                        // send request berhasil pendaftaran berhasil
                        Log.d("fandy_13_mei","insertDaftar if");


                        rubahButtonMenjadiBerhasilMendaftar();
                        closeAllEditTextSoftKeyboard();
                        showPopUpBerhasilDaftar();
                        addRecentRegisteredAccountToSharedPreferences(jObj.getJSONArray("responseLastUserInserted"));

                    }
                    else{
                        Log.d("fandy_13_mei","insertDaftar else");
                        //send request berhasil pendaftaran gagal
                        Log.d("fandy_12_mei","daftar gagal");
                        JSONObject jObjResponseInputError = jObj.getJSONObject("responseInputError");

                       boolean adaErrorUnik = checkApakahAdaReponseErrorUnique(jObjResponseInputError);
                        if(adaErrorUnik)
                        {
                            Log.d("fandy_13_mei","insertDaftar if adaErrorUnik");
                            CustomToast.l(getApplicationContext(),"Daftar gagal.Periksa lagi data yang kamu isi ya");
                            nonaktifkanButtonLoading();
                            scrollToFirstErrorEncountered();
                        }
                    }
                }
                catch (Exception e) {
                    Log.d("fandy_13_mei","insertDaftar catch: "+e.getMessage());
                    // JSON error
                    e.printStackTrace();
                    Log.d("fandy_12_mei","daftar exception: "+e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fandy_13_mei","insertDaftar onErrorResponse: "+error.getMessage());
//                dismissdialog();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("email",email);
                params.put("nama",nama);
                params.put("nim",nomorTelepon);
                params.put("universitas",universitas);
                params.put("alamat",alamat);
                params.put("jenis_kelamin",jenis_kelamin);
                params.put("username",username);
                params.put("password_1",password_1);
                params.put("password_2",password_1);

                /*start jika mendaftar melalui google */
                if(getIntent().getStringExtra("personPhoto")!=null)
                params.put("image_user",getIntent().getStringExtra("personPhoto"));

                if(getIntent().getStringExtra("personIdGoogle")!=null)
                params.put("id_ref_login_sosmed",getIntent().getStringExtra("personIdGoogle"));

                /*end jika mendaftar melalui google*/



                return params;
            }

        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("PendaftaranAKun:", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, RuleValueAplikasi.tag_json_obj);

    }

    private boolean checkApakahAdaReponseErrorUnique(JSONObject jObjResponseInputError) {
        boolean adaError = false;
        if(jObjResponseInputError.has("email_error")){
            // Log.d("fandy_12_mei","ada email error, yaitu "+jObjResponseInputError.getString("email_error"));
            try {
                showErrorTextView(error_for_edt_email,jObjResponseInputError.getString("email_error"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adaError = true;
        }


        if(jObjResponseInputError.has("telepon_error")){
            //Log.d("fandy_12_mei","ada telepon error, yaitu "+jObjResponseInputError.getString("telepon_error"));
            try {
                showErrorTextView(error_for_edt_no_handphone,jObjResponseInputError.getString("telepon_error"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adaError = true;
        }


        if(jObjResponseInputError.has("username_error")){
            // Log.d("fandy_12_mei","ada username error, yaitu "+jObjResponseInputError.getString("username_error"));
            try {
                showErrorTextView(error_for_edt_nama_pengguna,jObjResponseInputError.getString("username_error"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adaError = true;
        }
        return adaError;
    }


    private void cekSemuaInput(){
        //step 1, cek semua EditText
        for(int i=0;i<semuaEditText.size();i++)
        {
            cekValidasiKomponenInput(semuaEditText.get(i).getId());
        }

        //step2, cek radio group jenis kelamin apakah radio buttonnya sudah ada yang terpilih atau belum
            cekValidasiKomponenInput(R.id.radio_group_jenis_kelamin);

        //step3, cek apakah pengguna sudah mensetujui CB kebijakan privasi
        cekValidasiKomponenInput(R.id.checkbox_kebijakan_privasi);
    }

    private boolean cekApakahRadioJeniskelaminSudahTerpilih(RadioGroup radioGroup){
        if(radioGroup.getCheckedRadioButtonId() == -1)
        {
            return false;
        }
        else {
            return true;
        }
    }

    private void aktifkanButtonLoading(){
        progress_btn_daftar.startAnimation(fade_in);
        progress_btn_daftar.setVisibility(View.VISIBLE);
        tv_keterangan_btn_daftar.startAnimation(fade_in);
        tv_keterangan_btn_daftar.setText("Mendaftar akun...");
    }

    private void nonaktifkanButtonLoading(){
        progress_btn_daftar.startAnimation(fade_in);
        progress_btn_daftar.setVisibility(View.GONE);
        tv_keterangan_btn_daftar.startAnimation(fade_in);
        tv_keterangan_btn_daftar.setText("Daftar");
    }

    private void closeAllEditTextSoftKeyboard(){
        for(int i =0;i<semuaEditText.size();i++)
        {
            semuaEditText.get(i).onEditorAction(EditorInfo.IME_ACTION_DONE);
        }
    }

    private void rubahButtonMenjadiBerhasilMendaftar(){
        progress_btn_daftar.setVisibility(View.GONE);
        img_centang_daftar.startAnimation(fade_in);
        img_centang_daftar.setVisibility(View.VISIBLE);
        tv_keterangan_btn_daftar.startAnimation(fade_in);
        tv_keterangan_btn_daftar.setText("BERHASIL MENDAFTAR");
        btn_daftar.setBackgroundResource(R.drawable.ic_bg_button_berhasil_daftar);
        //disable setOnClickListener
        btn_daftar.setOnClickListener(null);
    }
}