package com.zonamediagroup.zonamahasiswa;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class BottomSheetDialogFilterForHatLama extends BottomSheetDialogFragment implements View.OnClickListener{
    public static final String CURHATAN = MenuFilterForhatManager.SUBFILTER_CURHATAN;
    public static final String TANGGAPAN = MenuFilterForhatManager.SUBFILTER_TANGGAPAN;
    public static final String SEMUA = MenuFilterForhatManager.SUBFILTER_SEMUA;
    public static final String TERBARU = MenuFilterForhatManager.SUBFILTER_TERBARU;
    public static final String TERPOPULER = MenuFilterForhatManager.SUBFILTER_TERPOPULER;
    Animation fade_out,fade_in;
    boolean flagJenisCurhatan = false;
    boolean flagJenisTanggapan = false;
    boolean flagPostinganSemua = false;
    boolean flagPostinganTerbaru = false;
    boolean flagPostinganTerpopuler = false;
    BottomSheetFilterForhatListener mListener;
    Context context;
    FlowLayout area_selected_filter_in_bottomsheet;
    ImageView close_tag_postingan_in_bottomsheet,close_tag_jenis_in_bottomsheet,close_tag_kategori_in_bottomsheet;
    LinearLayout layout_subfilter_forhat_jenis,layout_subfilter_forhat_kategori,layout_subfilter_forhat_postingan,ly_selected_filter_kategori_in_bottomsheet,ly_selected_filter_jenis_in_bottomsheet,ly_selected_filter_postingan_in_bottomsheet;
    String concatMonthYearPostingan, year, month;
    String postinganPrefix = "";
    String postinganSuffix = "";
    String bulanTerpilih = "";
    String tahunTerpilih = "";
    String[] menuFilterForhat;
    Spinner spinner_tahun;
    Spinner spinner_bulan;
    TextView tv_kategori_selected,tv_jenis_selected,tv_postingan_selected, btn_hapus_semua_bottomsheet, btn_simpan_bottomsheet;

    private static final int NUMBERS_OF_YEARS_TO_BACK = 3;

    /*RadioButton filter*/
    RadioButton menu_filter_jenis, menu_filter_kategori, menu_filter_postingan;

    /*RadioButton subfilter*/
    RadioButton pilihan_submenufilter_curhatan,pilihan_submenufilter_postingan_terbaru,pilihan_submenufilter_tanggapan,pilihan_submenufilter_postingan_semua,pilihan_submenufilter_postingan_terpopuler;

    RadioGroup radio_group_filter_forhat;
    View viewBottomSheet;
    FrameLayout subFilterContainer;
    ArrayList<View> allSubFilterLayout;
    HashMap<String,String> hashMapBottomSheet;
    String tahunTerpilihBottomSheet="";
    String bulanTerpilihBottomSheet="";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.bottomsheet_filter_pencarian_forhat_lama,container,false);
        init(root);
        return root;
    }

    public BottomSheetDialogFilterForHatLama(HashMap hashMapActivity, String bulanTerpilihActivity, String tahunTerpilihActivity){
        hashMapBottomSheet = hashMapActivity;
        tahunTerpilihBottomSheet = tahunTerpilihActivity;
        bulanTerpilihBottomSheet = bulanTerpilihActivity;
    }

    private void initHashMapBottomSheet(){
        if(hashMapBottomSheet == null)
        hashMapBottomSheet = new HashMap<>();
    }

    private void init(View root)
    {
            initHashMapBottomSheet();
            getMenuFilterForhatAvailable();
            initAnimation();
            viewBottomSheet = root;
            initArrayListSubFilterLayout();
            initMenuFilterForhatInBottomSheet();
            findViewByIdAllComponentInBottomSheetFilter();
            findViewByIdAllComponentInFlowLayoutBottomSheetFilter();
            setListenerAllComponentInFlowLayoutBottomSheetFilter();
            inflateAllSubFilter();
            findViewByIdAllContainerLayoutSubFilter();
            registerAllSubFilterLayoutToArray();
            setListenerAllComponentInBottomSheetFilter();
            findViewByIdAllSubFilter();
            findViewByIdSpinnerPostingan();
            setListenerSpinnerPostingan();
            prepareSpinnerPostingan();
            disableSpinnerOnPostinganPage();
            setListenerAllSubFilterInBottomSheetFilter();
            setFirstAvailableMenuFilterForhatToBeSelected();

    }

    private void simpanSemuaFilterTerpilih(){


        /*Masukan semua filter dan subfilter terpilih ke hashMap mapFilterTerpilih */

        /*check flag apa saja yang aktif dan masukan ke hashmap*/

        /*start flag Jenis*/
        if(flagJenisCurhatan == true)
            hashMapBottomSheet.put(MenuFilterForhatManager.MENU_JENIS,CURHATAN);
        else if(flagJenisTanggapan == true)
            hashMapBottomSheet.put(MenuFilterForhatManager.MENU_JENIS,TANGGAPAN);
        /*end flag jenis*/

        /*start flag Postingan*/
        if(flagPostinganSemua == true)
            hashMapBottomSheet.put(MenuFilterForhatManager.MENU_POSTINGAN,SEMUA);
        else if(flagPostinganTerbaru == true)
            hashMapBottomSheet.put(MenuFilterForhatManager.MENU_POSTINGAN,TERBARU);
        else if(flagPostinganTerpopuler == true)
            hashMapBottomSheet.put(MenuFilterForhatManager.MENU_POSTINGAN,TERPOPULER);
        /*end flag Postingan*/
        mListener.sendDataBottomSheetToForhatSearchActivity(hashMapBottomSheet,bulanTerpilih,tahunTerpilih);
        dismissBottomSheet();
    }

    private void dismissBottomSheet(){
        dismiss();
    }

    private void initAnimation(){
        fade_out = AnimationUtils.loadAnimation(context,R.anim.fade_out);
        fade_in = AnimationUtils.loadAnimation(context,R.anim.fade_in);
    }

    private void getMenuFilterForhatAvailable(){
        menuFilterForhat = MenuFilterForhatManager.getMenuFilterForhatAvailableByPage(MenuFilterForhatManager.HOME);
    }

    private void initMenuFilterForhatInBottomSheet(){
        if(Arrays.asList(menuFilterForhat).contains(MenuFilterForhatManager.MENU_JENIS) != true)
        {
            menu_filter_jenis.setVisibility(View.GONE);
        }
        if(Arrays.asList(menuFilterForhat).contains(MenuFilterForhatManager.MENU_KATEGORI) != true)
        {
            menu_filter_kategori.setVisibility(View.GONE);
        }
        if(Arrays.asList(menuFilterForhat).contains(MenuFilterForhatManager.MENU_POSTINGAN) != true)
        {
            menu_filter_postingan.setVisibility(View.GONE);
        }
    }

    private void removeAllFilterTag(){
        /*start section jenis*/
        hideTagFilterJenisInBottomSheet();
        unselectCurhatan();
        unselectTanggapan();
        /*end section jenis*/

        /*start section kategori*/
        hideTagFilterKategoriInBottomSheet();
        /*hide section kategori*/

        /*start section postingan*/
        hideTagFilterPostinganInBottomSheet();
        unSelectSemua();
        unSelectTerbaru();
        unSelectTerpopuler();
        disableSpinnerOnPostinganPage();
        /*hide section jenis*/
    }

    private void setFirstAvailableMenuFilterForhatToBeSelected(){
            menu_filter_kategori.setChecked(true);
            mekanismeOpenSubFilterBySelectedMenuFilter(menu_filter_kategori.getId());
    }



    private void mekanismeOpenSubFilterBySelectedMenuFilter(int idMenuFilterSelected){
        switch(idMenuFilterSelected)
        {
            case R.id.menu_filter_jenis:
                showCertainSubFilterLayoutAndHideAnother(R.id.layout_subfilter_forhat_jenis);
                break;
            case R.id.menu_filter_kategori:
                showCertainSubFilterLayoutAndHideAnother(R.id.layout_subfilter_forhat_kategori);
                break;
            case R.id.menu_filter_postingan:
                showCertainSubFilterLayoutAndHideAnother(R.id.layout_subfilter_forhat_postingan);
                break;
        }
    }



    public void inflateAllSubFilter(){
        subFilterContainer.addView(getLayoutInflater().inflate(R.layout.include_filter_forhat_jenis, null));
        subFilterContainer.addView(getLayoutInflater().inflate(R.layout.include_filter_forhat_postingan, null));
        subFilterContainer.addView(getLayoutInflater().inflate(R.layout.include_filter_forhat_kategori, null));
    }

    public void registerAllSubFilterLayoutToArray(){
        allSubFilterLayout.add(layout_subfilter_forhat_jenis);
        allSubFilterLayout.add(layout_subfilter_forhat_kategori);
        allSubFilterLayout.add(layout_subfilter_forhat_postingan);
    }

    public void initArrayListSubFilterLayout(){
        allSubFilterLayout = new ArrayList<View>();
    }



    public void showCertainSubFilterLayoutAndHideAnother(int idLayoutSubFilter){
        Log.d("20_juli_2022","showCertainSubFilterLayoutAndHideAnother()");
        for(int i=0;i<allSubFilterLayout.size();i++)
        {
            Log.d("20_juli_2022","showCertainSubFilterLayoutAndHideAnother() for");
            Log.d("20_juli_2022","showCertainSubFilterLayoutAndHideAnother() allSubFilterLayout size: "+allSubFilterLayout.size());
            if(allSubFilterLayout.get(i).getId() == idLayoutSubFilter)
            {
                Log.d("20_juli_2022","showCertainSubFilterLayoutAndHideAnother() for if");
                allSubFilterLayout.get(i).startAnimation(fade_in);
                allSubFilterLayout.get(i).setVisibility(View.VISIBLE);
            }
            else {
                Log.d("20_juli_2022","showCertainSubFilterLayoutAndHideAnother() for else");
                allSubFilterLayout.get(i).setVisibility(View.GONE);
            }
        }
    }

    public void removeViewInsideSubFilterContainer(){
        subFilterContainer.startAnimation(fade_out);
        subFilterContainer.removeAllViews();
    }











    private void findViewByIdAllComponentInBottomSheetFilter() {
        menu_filter_jenis = viewBottomSheet.findViewById(R.id.menu_filter_jenis);
        menu_filter_kategori = viewBottomSheet.findViewById(R.id.menu_filter_kategori);
        menu_filter_postingan = viewBottomSheet.findViewById(R.id.menu_filter_postingan);
        radio_group_filter_forhat = viewBottomSheet.findViewById(R.id.radio_group_filter_forhat);
        subFilterContainer = viewBottomSheet.findViewById(R.id.container_subfilter_forhat);
        btn_simpan_bottomsheet = viewBottomSheet.findViewById(R.id.btn_simpan_bottomsheet);
        btn_hapus_semua_bottomsheet = viewBottomSheet.findViewById(R.id.btn_hapus_semua_bottomsheet);
    }

    private void findViewByIdAllComponentInFlowLayoutBottomSheetFilter(){
        Log.d("20_juli_2022","findViewByIdAllComponentInFlowLayoutBottomSheetFilter()");
        area_selected_filter_in_bottomsheet = viewBottomSheet.findViewById(R.id.area_selected_filter_in_bottomsheet);
        close_tag_jenis_in_bottomsheet = viewBottomSheet.findViewById(R.id.close_tag_jenis_in_bottomsheet);
        close_tag_kategori_in_bottomsheet = viewBottomSheet.findViewById(R.id.close_tag_kategori_in_bottomsheet);
        close_tag_postingan_in_bottomsheet = viewBottomSheet.findViewById(R.id.close_tag_postingan_in_bottomsheet);
        ly_selected_filter_kategori_in_bottomsheet = viewBottomSheet.findViewById(R.id.ly_selected_filter_kategori_in_bottomsheet);
        ly_selected_filter_jenis_in_bottomsheet = viewBottomSheet.findViewById(R.id.ly_selected_filter_jenis_in_bottomsheet);
        ly_selected_filter_postingan_in_bottomsheet = viewBottomSheet.findViewById(R.id.ly_selected_filter_postingan_in_bottomsheet);
        tv_jenis_selected = viewBottomSheet.findViewById(R.id.tv_jenis_selected);
        tv_kategori_selected = viewBottomSheet.findViewById(R.id.tv_kategori_selected);
        tv_postingan_selected = viewBottomSheet.findViewById(R.id.tv_postingan_selected);
    }

    private void setTv_jenis(String s){
        tv_jenis_selected.setText(s);
    }

    private String getTv_jenis(){
        return tv_jenis_selected.getText().toString();
    }

    private void setTv_kategori(String s){
        tv_kategori_selected.setText(s);
    }

    private String getTv_kategori(){
        return tv_kategori_selected.getText().toString();
    }

    private void setTv_postingan(String s){
        tv_postingan_selected.setText(s);
    }

    private String getTv_postingan(){
        return tv_postingan_selected.getText().toString();
    }

    private void unselectSubFilter(RadioButton radioButton){
        radioButton.setChecked(false);
    }

    private void unselectCurhatan(){
        pilihan_submenufilter_curhatan.setChecked(false);
        flagJenisCurhatan = false;
    }

    private void unselectTanggapan(){
        pilihan_submenufilter_tanggapan.setChecked(false);
        flagJenisTanggapan = false;
    }

    private void unSelectSemua(){
        pilihan_submenufilter_postingan_semua.setChecked(false);
        flagPostinganSemua = false;
    }
    private void unSelectTerbaru(){
        pilihan_submenufilter_postingan_terbaru.setChecked(false);
        flagPostinganTerbaru = false;
    }
    private void unSelectTerpopuler(){
        pilihan_submenufilter_postingan_terpopuler.setChecked(false);
        flagPostinganTerpopuler = false;
    }


    private void setListenerAllComponentInFlowLayoutBottomSheetFilter(){
        close_tag_jenis_in_bottomsheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideTagFilterJenisInBottomSheet();
                unselectCurhatan();
                unselectTanggapan();
            }
        });
        close_tag_kategori_in_bottomsheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideTagFilterKategoriInBottomSheet();
            }
        });
        close_tag_postingan_in_bottomsheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideTagFilterPostinganInBottomSheet();
                unSelectSemua();
                unSelectTerbaru();
                unSelectTerpopuler();
                disableSpinnerOnPostinganPage();
            }
        });
    }

    private void findViewByIdAllContainerLayoutSubFilter(){
        /*start findViewById all container layout subFilter*/
        layout_subfilter_forhat_jenis = viewBottomSheet.findViewById(R.id.layout_subfilter_forhat_jenis);
        layout_subfilter_forhat_kategori = viewBottomSheet.findViewById(R.id.layout_subfilter_forhat_kategori);
        layout_subfilter_forhat_postingan = viewBottomSheet.findViewById(R.id.layout_subfilter_forhat_postingan);
        /*end findViewById all container layout subFilter*/
    }

    private void findViewByIdAllSubFilter(){
        /*start findViewById all subFilter Option*/
        pilihan_submenufilter_curhatan = viewBottomSheet.findViewById(R.id.pilihan_submenufilter_curhatan);
        pilihan_submenufilter_tanggapan = viewBottomSheet.findViewById(R.id.pilihan_submenufilter_tanggapan);

        pilihan_submenufilter_postingan_semua = viewBottomSheet.findViewById(R.id.pilihan_submenufilter_postingan_semua);
        pilihan_submenufilter_postingan_terbaru = viewBottomSheet.findViewById(R.id.pilihan_submenufilter_postingan_terbaru);
        pilihan_submenufilter_postingan_terpopuler = viewBottomSheet.findViewById(R.id.pilihan_submenufilter_postingan_terpopuler);
        /*end findViewById all subFilter Option*/
    }



    private void mekanismeSelectingSubFilterJenis(RadioButton radioYangDiKlik){
        switch(radioYangDiKlik.getId()) {
            case R.id.pilihan_submenufilter_curhatan:
                if (pilihan_submenufilter_curhatan.isChecked()) {
                    if (!flagJenisCurhatan) {
                        showTagFilterJenisInBottomSheet();
                        setTv_jenis(MenuFilterForhatManager.MENU_JENIS+ ": " + CURHATAN);
                        pilihan_submenufilter_curhatan.setChecked(true);
                        pilihan_submenufilter_tanggapan.setChecked(false);
                        flagJenisCurhatan = true;
                        flagJenisTanggapan = false;
                    } else {
                        hideTagFilterJenisInBottomSheet();
                        flagJenisCurhatan = false;
                        pilihan_submenufilter_curhatan.setChecked(false);
                        pilihan_submenufilter_tanggapan.setChecked(false);
                    }
                }
            case R.id.pilihan_submenufilter_tanggapan:
                if(pilihan_submenufilter_tanggapan.isChecked()){
                    if (!flagJenisTanggapan) {
                        showTagFilterJenisInBottomSheet();
                        setTv_jenis(MenuFilterForhatManager.MENU_JENIS+ ": " + TANGGAPAN);
                        pilihan_submenufilter_tanggapan.setChecked(true);
                        pilihan_submenufilter_curhatan.setChecked(false);
                        flagJenisTanggapan = true;
                        flagJenisCurhatan = false;
                    } else {
                        hideTagFilterJenisInBottomSheet();
                        flagJenisTanggapan = false;
                        pilihan_submenufilter_tanggapan.setChecked(false);
                        pilihan_submenufilter_curhatan.setChecked(false);
                    }
                }
        }
    }



    private void mekanismeSelectingSubFilterPostingan(RadioButton radioYangDiKlik){
        switch(radioYangDiKlik.getId()) {
            case R.id.pilihan_submenufilter_postingan_semua:
                if (pilihan_submenufilter_postingan_semua.isChecked()) {
                    if (!flagPostinganSemua) {
                        showTagFilterPostinganInBottomSheet();
                        postinganPrefix = MenuFilterForhatManager.MENU_POSTINGAN+ ": " + SEMUA;
                        setTv_postingan(postinganPrefix+postinganSuffix);
                        enableSpinnerOnPostinganPage();
                        Log.d("16_juli_2022","if(!flagPostinganSemua)");
                        pilihan_submenufilter_postingan_semua.setChecked(true);
                        pilihan_submenufilter_postingan_terbaru.setChecked(false);
                        pilihan_submenufilter_postingan_terpopuler.setChecked(false);
                        flagPostinganSemua = true;
                        flagPostinganTerbaru = false;
                        flagPostinganTerpopuler = false;
                    }
                    else {
                        hideTagFilterPostinganInBottomSheet();
                        disableSpinnerOnPostinganPage();
                        Log.d("16_juli_2022","else(!flagPostinganSemua)");
                        flagPostinganSemua = false;
                        pilihan_submenufilter_postingan_semua.setChecked(false);
                        pilihan_submenufilter_postingan_terbaru.setChecked(false);
                        pilihan_submenufilter_postingan_terpopuler.setChecked(false);
                    }
                }
                break;
            case R.id.pilihan_submenufilter_postingan_terbaru:
                if(pilihan_submenufilter_postingan_terbaru.isChecked()) {
                    if (!flagPostinganTerbaru) {
                        showTagFilterPostinganInBottomSheet();
                        postinganPrefix = MenuFilterForhatManager.MENU_POSTINGAN+ ": " + TERBARU;
                        setTv_postingan(postinganPrefix+postinganSuffix);
                        enableSpinnerOnPostinganPage();
                        pilihan_submenufilter_postingan_semua.setChecked(false);
                        pilihan_submenufilter_postingan_terbaru.setChecked(true);
                        pilihan_submenufilter_postingan_terpopuler.setChecked(false);
                        flagPostinganSemua = false;
                        flagPostinganTerbaru = true;
                        flagPostinganTerpopuler = false;
                    } else {
                        hideTagFilterPostinganInBottomSheet();
                        disableSpinnerOnPostinganPage();
                        flagPostinganTerbaru = false;
                        pilihan_submenufilter_postingan_semua.setChecked(false);
                        pilihan_submenufilter_postingan_terbaru.setChecked(false);
                        pilihan_submenufilter_postingan_terpopuler.setChecked(false);
                    }
                }
                break;
            case R.id.pilihan_submenufilter_postingan_terpopuler:
                if(pilihan_submenufilter_postingan_terpopuler.isChecked()) {
                    if (!flagPostinganTerpopuler) {
                        showTagFilterPostinganInBottomSheet();
                        postinganPrefix = MenuFilterForhatManager.MENU_POSTINGAN+ ": " + TERPOPULER;
                        setTv_postingan(postinganPrefix+postinganSuffix);
                        enableSpinnerOnPostinganPage();
                        Log.d("16_juli_2022","if(!flagPostinganTerpopuler)");
                        pilihan_submenufilter_postingan_semua.setChecked(false);
                        pilihan_submenufilter_postingan_terbaru.setChecked(false);
                        pilihan_submenufilter_postingan_terpopuler.setChecked(true);
                        flagPostinganSemua = false;
                        flagPostinganTerbaru = false;
                        flagPostinganTerpopuler = true;
                    } else {
                        hideTagFilterPostinganInBottomSheet();
                        disableSpinnerOnPostinganPage();
                        Log.d("16_juli_2022","else(!flagPostinganTerpopuler)");
                        flagPostinganTerpopuler = false;
                        pilihan_submenufilter_postingan_semua.setChecked(false);
                        pilihan_submenufilter_postingan_terbaru.setChecked(false);
                        pilihan_submenufilter_postingan_terpopuler.setChecked(false);
                    }
                }
                break;
        }
    }



    private void enableSpinnerOnPostinganPage(){
        enableSpinnerBulan();
        enableSpinnerTahun();
    }


    /*method untuk check postingan sudah punya suffix bulan tahun atau belum. true = sudah ada, false = belum ada*/
    private boolean checkIfPostinganAlreadyHasSuffix(){
        if(postinganSuffix != null || !postinganSuffix.equals(""))
            return false;
            else
                return true;
    }

    private void mekanismeConcatMonthAndYearToTextView(){
            /*jika spinner bulan aktif, & spinner bulan bukan dalam seleksi item pertama (bulan)
        dan
        jika spinner tahun aktif, & spinner tahun bukan dalam seleksi item pertama (tahun)
         */


        if((spinner_bulan.isEnabled() && spinner_bulan.getSelectedItemPosition()!=0) && (spinner_tahun.isEnabled() && spinner_tahun.getSelectedItemPosition()!=0))
        {
            postinganSuffix = getFormatConcatMonthAndYearFromSpinnerForTextView();
            /* set nilai tv_postingan dengan nilai tv_postingan sekarang plus di concat dengan bulan tahun dari spinner*/
            setTv_postingan(postinganPrefix+postinganSuffix);
        }
        else
        {
            /*user kembali memilih text bulan dan tahun di spinner*/
            /* set nilai tv_postingan dengan nilai tv_postingan sekarang minus concat dengan bulan tahun dari spinner(replace concat dengan "")*/
            Log.d("25_juli","Prefix: "+postinganPrefix);
            Log.d("25_juli","Suffix: "+postinganSuffix);
            Log.d("25_juli","getTvPostingan original: "+getTv_postingan());
            String replace = getTv_postingan().replace(postinganSuffix,"");
            Log.d("25_juli","getTvPostingan replace: "+replace);
            setTv_postingan(getTv_postingan().replace(postinganSuffix,""));
            postinganSuffix = "";
        }
    }


    private String getFormatConcatMonthAndYearFromSpinnerForTextView(){
        bulanTerpilih = spinner_bulan.getSelectedItem().toString();
        tahunTerpilih = spinner_tahun.getSelectedItem().toString();
        return ": "+bulanTerpilih+" "+tahunTerpilih;
    }

    private void disableSpinnerOnPostinganPage(){
        resetSeleksiSpinner();
        disableSpinnerBulan();
        disableSpinnerTahun();
    }



    private void resetSeleksiSpinner(){
        setSelectedItemSpinnerBulan(0);
        setSelectedItemSpinnerTahun(0);
    }

    private void setSelectedItemSpinnerBulan(int index){
        spinner_bulan.setSelection(index);
    }

    private void setSelectedItemSpinnerTahun(int index){
        spinner_tahun.setSelection(index);
    }






    private void setListenerAllSubFilterInBottomSheetFilter(){
        /*start setOnClickListener all subFilter Option*/
        //start sementara comment
        pilihan_submenufilter_curhatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  unCheckSubFilterIfAlreadyCheck(pilihan_submenufilter_curhatan.getId());
                mekanismeSelectingSubFilterJenis(pilihan_submenufilter_curhatan);
            }
        });
        pilihan_submenufilter_tanggapan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeSelectingSubFilterJenis(pilihan_submenufilter_tanggapan);
            }
        });
        //end sementara comment
        pilihan_submenufilter_postingan_terbaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeSelectingSubFilterPostingan(pilihan_submenufilter_postingan_terbaru);
            }
        });
        pilihan_submenufilter_postingan_semua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeSelectingSubFilterPostingan(pilihan_submenufilter_postingan_semua);
            }
        });
        pilihan_submenufilter_postingan_terpopuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeSelectingSubFilterPostingan(pilihan_submenufilter_postingan_terpopuler);
            }
        });

        /*end setOnClickListener all subFilter Option*/
    }

    private void setListenerAllComponentInBottomSheetFilter(){
        menu_filter_jenis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeOpenSubFilterBySelectedMenuFilter(menu_filter_jenis.getId());
            }
        });
        menu_filter_kategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeOpenSubFilterBySelectedMenuFilter(menu_filter_kategori.getId());
            }
        });
        menu_filter_postingan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeOpenSubFilterBySelectedMenuFilter(menu_filter_postingan.getId());
            }
        });
        btn_hapus_semua_bottomsheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_simpan_bottomsheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpanSemuaFilterTerpilih();
            }
        });
    }




    private void showTagFilterJenisInBottomSheet(){
        ly_selected_filter_jenis_in_bottomsheet.setVisibility(View.VISIBLE);
    }

    private void showTagFilterPostinganInBottomSheet(){
        ly_selected_filter_postingan_in_bottomsheet.setVisibility(View.VISIBLE);
    }

    private void showTagFilterKategoriInBottomSheet(){
        ly_selected_filter_kategori_in_bottomsheet.setVisibility(View.VISIBLE);
    }

    private void hideTagFilterJenisInBottomSheet(){
        if(ly_selected_filter_jenis_in_bottomsheet.getVisibility() == View.VISIBLE)
        {
            ly_selected_filter_jenis_in_bottomsheet.setVisibility(View.GONE);
        }
    }

    private void hideTagFilterPostinganInBottomSheet(){
        if(ly_selected_filter_postingan_in_bottomsheet.getVisibility() == View.VISIBLE)
        {
            ly_selected_filter_postingan_in_bottomsheet.setVisibility(View.GONE);
        }
    }

    private void hideTagFilterKategoriInBottomSheet(){
        if(ly_selected_filter_kategori_in_bottomsheet.getVisibility() == View.VISIBLE)
        {
            ly_selected_filter_kategori_in_bottomsheet.setVisibility(View.GONE);
        }
    }


    private void setTextTagFilterBottomSheetFilter(ViewGroup viewGroup,String kategoriSelectedName, String optionName)
    {
        for (int i = 0; i < viewGroup.getChildCount(); ++i) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof TextView) {
                ((TextView)view).setText(kategoriSelectedName+": "+optionName);//here it will be clear all the EditText field
            }
        }
    }

    private String getTextTagFilterBottomSheetFilter(ViewGroup viewGroup)
    {
        String result = "";
        for (int i = 0; i < viewGroup.getChildCount(); ++i) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof TextView) {
                result =  ((TextView)view).getText().toString();//here it will be clear all the EditText field
            }
        }
        return result;
    }




    private void findViewByIdSpinnerPostingan(){
        spinner_tahun = viewBottomSheet.findViewById(R.id.spinner_tahun);
        spinner_bulan = viewBottomSheet.findViewById(R.id.spinner_bulan);
    }

    private void setListenerSpinnerPostingan(){
        spinner_bulan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mekanismeConcatMonthAndYearToTextView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_tahun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mekanismeConcatMonthAndYearToTextView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void prepareSpinnerPostingan(){
        List<String> item = getYearsBack(getCurrentYear(), NUMBERS_OF_YEARS_TO_BACK);
        /*tambahkan teks "tahun" di awal list*/
        item.add(0,"Tahun");
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item,item);

       // spinner_tahun.setAdapter(adapter);
    }


    /*generate list beberapa tahun kebelakang untuk spinner*/
    private List<String> getYearsBack(int currentYear, int numbersOfYearsToBack) {
        List<String> listYears = new ArrayList<String>();
        int valueYears = currentYear;
        for(int i=0;i<numbersOfYearsToBack;i++)
        {
            listYears.add(String.valueOf(valueYears));
            valueYears= valueYears-1;
        }
        return listYears;
    }

    private int getCurrentYear(){
        int currentYear =  Calendar.getInstance().get(Calendar.YEAR);
        return currentYear;
    }


    private void disableSpinnerBulan(){
        spinner_bulan.setEnabled(false);
    }

    private void enableSpinnerBulan(){
        spinner_bulan.setEnabled(true);
    }

    private void disableSpinnerTahun(){
        spinner_tahun.setEnabled(false);
    }

    private void enableSpinnerTahun(){
        spinner_tahun.setEnabled(true);
    }

    @Override
    public void onClick(View view) {
        int id = viewBottomSheet.getId();
        switch(id) {
            case R.id.close_tag_postingan_in_bottomsheet:
                hideTagFilterPostinganInBottomSheet();
                break;
            case R.id.close_tag_jenis_in_bottomsheet:
                Log.d("20_juli_2022","close jenis");
        hideTagFilterJenisInBottomSheet();
                break;
            case R.id.close_tag_kategori_in_bottomsheet:
                hideTagFilterKategoriInBottomSheet();
                break;
        }
    }

    public interface BottomSheetFilterForhatListener{
        public void sendDataBottomSheetToForhatSearchActivity(HashMap hashMapBottomSheet,String bulanTerpilih, String tahunTerpilih);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        try{
            mListener = (BottomSheetFilterForhatListener) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement bottomSheetListener");
        }
    }



}
