package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.animation.Animator;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Kategori;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Download_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Download_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Download_file extends AppCompatActivity implements Download_Adapter.ContactsAdapterListener {


    //private String URL_home = Server.URL_REAL + "Get_file/?TOKEN=qwerty";
    private String URL_home = Server.URL_REAL + "Get_file_with_category/?TOKEN=qwerty";
    //private String URL_home = Server.URL_REAL + "Get_file_develop/?TOKEN=qwerty";

    private static final String TAG = Kategori.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";

    String titleDownload = "";

    private long downloadId;

    ProgressDialog progressDialog;

    // item kedua
    RecyclerView receler_dua;
    private List<Download_Model> duaModels;
    private Download_Adapter duaAdapter;


    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    Toolbar toolbar;

    TabLayout tabLayout;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok, back;

//    iklan


    private static String AD_UNIT_ID = "";
    private static final long COUNTER_TIME = 10;
    private static final int GAME_OVER_REWARD = 0;

    private int coinCount;

    private CountDownTimer countDownTimer;
    private boolean gameOver;
    private boolean gamePaused;

    private RewardedAd rewardedAd;

    JSONArray respon = null;

    private long timeRemaining;
    boolean isLoading;

    String lokasiFolder = "Zona Mahasiswa";


//    iklan end

    String url_file;
    String nama_file;

    String ekstensiFile;
//    DownloadManager manager;

    // permission Storage
    private static final int STORAGE_PERMISSION_CODE = 101;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int PERMISSION_STORAGE_CODE = 1000;

    String status;

    String nama_dan_lokasi_file_download="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AD_UNIT_ID = getResources().getString(R.string.ad_unit_id_interstitial);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_download_file);
        //catching download complete events from android download manager which broadcast message
//        Konfigurasi

        //start agar tidak error expose...
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        //end agar tidak error expose...

        // cek permission
        checkPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                STORAGE_PERMISSION_CODE);

//        item KATEGORI
        receler_dua = findViewById(R.id.receler_download);
        duaModels = new ArrayList<>();
        duaAdapter = new Download_Adapter(Download_file.this, duaModels, this);
        RecyclerView.LayoutManager mLayoutManagerkategorix = new GridLayoutManager(Download_file.this, 3);
        //RecyclerView.LayoutManager mLayoutManagerkategorix = new LinearLayoutManager(Download_file.this, RecyclerView.VERTICAL, false);
        receler_dua.setLayoutManager(mLayoutManagerkategorix);
        receler_dua.setItemAnimator(new DefaultItemAnimator());
        receler_dua.setAdapter(duaAdapter);


//        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);

//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();

                get_kategori();

            }
        });

        search = findViewById(R.id.search);
        saveok = findViewById(R.id.saveok);
        back = findViewById(R.id.back);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Download_file.this, Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Download_file.this, Search.class);
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        initProgressDialog();

        eror_dismiss();
        loading_show();

//        Call API
        get_kategori();

    }

    private void initializeTabLayout() {
        //start tab layout
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        tabLayout = findViewById(R.id.tablayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                setDataTemplate(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //end tab layout
    }

    private void get_kategori()
    {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    //set tab layout sesuai kategori
                    setTabTemplate(jObj);

                    // Check for error node in json

                } catch (JSONException e) {
                    eror_show();
                    Log.e("error_download","error 1:"+e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                eror_show();
                Log.e("error_download","error 2:"+error.getMessage());

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                return params;
            }
        };

        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });


        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void setTabTemplate(JSONObject jObj) {
        initializeTabLayout();
        try {
            respon = jObj.getJSONArray("respon");
            for(int i=0;i<respon.length();i++)
            {
            tabLayout.addTab(tabLayout.newTab().setText(respon.getJSONObject(i).getString("nama_kategori")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        loading_dismiss();
    }

    private void setDataTemplate(int indeks){
        //ambil respon berdasarkan indeks tertentu. respon indeks = tab indeks
        JSONArray templateArray = null;
        try {
            templateArray = respon.getJSONObject(indeks).getJSONArray("file_template");
            List<Download_Model> itemsdua = new Gson().fromJson(templateArray.toString(), new TypeToken<List<Download_Model>>() {
            }.getType());

            //start animate
            //start coba animasi
            receler_dua.animate()
                    .alpha(0f)
                    .setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) { }
                        @Override
                        public void onAnimationEnd(Animator animator) {
                            duaModels.clear();
                            duaModels.addAll(itemsdua);
                            duaAdapter.notifyDataSetChanged();
                            receler_dua.animate().alpha(1).setDuration(100).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    receler_dua.clearAnimation();
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });
                        }
                        @Override
                        public void onAnimationCancel(Animator animator) { }
                        @Override
                        public void onAnimationRepeat(Animator animator) { }
                    });

            //end coba animasi
            //end animate

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    
    private void get_kategoriLama() {


        StringRequest strReq = new StringRequest(Request.Method.GET, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "Produk Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
//                    System.out.println(jObj.getString("status"));

                    System.out.println("Data Global");


                    if (jObj.getString("status").equals("true")) {

//                        intro_dismis();
                        // item dua
                        JSONArray dataArray2 = jObj.getJSONArray("data");
//                        System.out.println("Data Array2 " + dataArray2);


                        List<Download_Model> itemsdua = new Gson().fromJson(dataArray2.toString(), new TypeToken<List<Download_Model>>() {
                        }.getType());




                        duaModels.clear();
                        // adding contacts to contacts list
                        //bukuList.clear();
                        duaModels.addAll(itemsdua);

                        // refreshing recycler view
                        duaAdapter.notifyDataSetChanged();


                        loading_dismiss();
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_show();
                    Log.e("error_download","error 3:"+e.getMessage());

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                eror_show();
                Log.e("error_download","error 4:"+error.getMessage());


            }
        }) {


//            reques

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");

                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());
//                eror_show();
            }
        });

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    @Override
    public void onContactSelected(Download_Model download) {

        url_file = download.getUrl_file();
        Log.d("sam_fandy_26april","url_file: "+url_file);
        nama_file = download.getTittle_download();
        loading_show();
        Toast.makeText(this, "Sedang menyiapkan...", Toast.LENGTH_SHORT).show();
        // Log.d("debugRnDDownloadManager","nama file: "+nama_file);
        if (ContextCompat.checkSelfPermission(Download_file.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            requestPermissions(permissions, PERMISSION_STORAGE_CODE);
            loading_dismiss();
            Toast.makeText(Download_file.this, "Aktifkan Izin Penyimpananmu Terlebih Dahulu", Toast.LENGTH_SHORT).show();

        } else {
            InterstitialAd intersAd = IklanInterstitial.iklanVideo;
            if(intersAd.isLoaded()) {
                intersAd.show();
                intersAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        //load another ad
                        intersAd.loadAd(new AdRequest.Builder().build());
                        new DownloadFileFromURL().execute(url_file);
                    }
                });
            }
            else
            {
                new DownloadFileFromURL().execute(url_file);
            }
        }

    }

    // iklan
    private void loadRewardedAd() {
        if (rewardedAd == null || !rewardedAd.isLoaded()) {
            rewardedAd = new RewardedAd(Download_file.this, AD_UNIT_ID);
            isLoading = true;
            rewardedAd.loadAd(
                    new AdRequest.Builder().build(),
                    new RewardedAdLoadCallback() {
                        @Override
                        public void onRewardedAdLoaded() {
                            //  Log.d("Download_file_debug","onRewardedAdLoaded");

                            // Ad successfully loaded.
                            isLoading = false;
                            //Toast.makeText(Point.this, "onRewardedAdLoaded", Toast.LENGTH_SHORT).show();
                            startGame();
                        }

                        @Override
                        public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
                            // Ad failed to load.
                            isLoading = false;
                            //    Log.d("Download_file_debug","onRewardedAdFailedToLoad cuz "+loadAdError.getMessage());
                            //Toast.makeText(Point.this, "on Rewarded Ad FailedTo Load", Toast.LENGTH_SHORT).show();
                            // Toast.makeText(Download_file.this, "Gagal Memuat "+loadAdError.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("error load " + loadAdError);
                            loading_dismiss();
                        }
                    });
        }
    }

    private void startGame() {
        //   Log.d("Download_file_debug","startGame");

// Run Coll down
        if (!rewardedAd.isLoaded() && !isLoading) {
            loadRewardedAd();
        }
        createTimer(COUNTER_TIME);
        gamePaused = false;
        gameOver = false;
    }

    private void createTimer(long time) {
        //    Log.d("Download_file_debug","createTimer");
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer =
                new CountDownTimer(time * 100, 50) {
                    @Override
                    public void onTick(long millisUnitFinished) {
                        timeRemaining = ((millisUnitFinished / 1000) + 1);
                        //Toast.makeText(Point.this, "Remaining " + timeRemaining, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFinish() {
                        //  Log.d("Download_file_debug","onFinish");

                        if (rewardedAd.isLoaded()) {

                            showRewardedVideo();
                        }

                        gameOver = true;
                    }
                };
        countDownTimer.start();
    }

    private void showRewardedVideo() {
        //  Log.d("Download_file_debug","showRewardedVideo");
        //    showVideoButton.setVisibility(View.INVISIBLE);
        if (rewardedAd.isLoaded()) {
            RewardedAdCallback adCallback =
                    new RewardedAdCallback() {
                        @Override
                        public void onRewardedAdOpened() {
                            // Ad opened.
                            //  Log.d("Download_file_debug","onRewardedAdOpened");
                        }

                        @Override
                        public void onRewardedAdClosed() {
                            // Ad closed.
                            //  Log.d("Download_file_debug","onRewardedAdClosed");
                            //downloadFile(url_file);



                                Toast.makeText(Download_file.this, "Pengunduhan berjalan !", Toast.LENGTH_SHORT).show();

                                //  new DownloadFileFromURL().execute(url_file);
                                new DownloadFileFromURL().execute(url_file);

                        }

                        @Override
                        public void onUserEarnedReward(RewardItem rewardItem) {
                            // User earned reward.
                            //     Log.d("Download_file_debug","onUserEarnedReward");
                            // download disini ya

                            if (ContextCompat.checkSelfPermission(Download_file.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                                //       Log.d("Download_file_debug","if line 448");
                                String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                requestPermissions(permissions, PERMISSION_STORAGE_CODE);
                            } else {
                                //    Log.d("Download_file_debug","else line 452");
                                Toast.makeText(Download_file.this, "Pengunduhan berjalan !", Toast.LENGTH_SHORT).show();

                                //download cara lama new DownloadFileFromURL().execute(url_file);
                                //downloadFile(url_file);

                            }

                        }

                        @Override
                        public void onRewardedAdFailedToShow(AdError adError) {
                            // Ad failed to display
                            Toast.makeText(Download_file.this, "Gagal Memuat Iklan", Toast.LENGTH_SHORT)
                                    .show();
                            //    Log.d("Download_file_debug","onRewardedAdFailedToShow cuz"+adError);
                        }
                    };
            rewardedAd.show(Download_file.this, adCallback);
        }
    }

    // END IKLAN

    // Function to check and request permission.
    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(Download_file.this, permission)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(Download_file.this,
                    new String[]{permission},
                    requestCode);
        } else {


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(Download_file.this,
//                        "Akses Kamera Diberikan",
//                        Toast.LENGTH_SHORT)
//                        .show();
            } else {
//                Toast.makeText(Download_file.this,
//                        "Akses Kamera Ditolak",
//                        Toast.LENGTH_SHORT)
//                        .show();
            }
        } else if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(Download_file.this,
//                        "Akses Penyimpanan Diberikan",
//                        Toast.LENGTH_SHORT)
//                        .show();


            } else {
//                Toast.makeText(Download_file.this,
//                        "Akses Penyimpanan Ditolak",
//                        Toast.LENGTH_SHORT)
//                        .show();
            }
        }
    }


    // Download from URL
    class DownloadFileFromURL extends AsyncTask<String, Integer, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading_show();
            //  Toast.makeText(Download_file.this, "DATA", Toast.LENGTH_SHORT).show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {

        Log.d(MainActivity.MYTAG,"url downloadnya: "+f_url[0]);
            try {
                Log.d(MainActivity.MYTAG,"try downloadnya:");
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                ekstensiFile = f_url[0].substring(f_url[0].lastIndexOf("."));
                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = connection.getContentLength();

                // download the file
//                InputStream input = new BufferedInputStream(url.openStream(),
//                        8192);
                InputStream input =connection.getInputStream();


                // Create File directori
                //start handle android 30 (red velvet) above
                String rootPath = "";
                if(android.os.Build.VERSION.SDK_INT>= Build.VERSION_CODES.Q)
                {
                     rootPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
                            .getAbsolutePath() + "/" + lokasiFolder + "/";
                }
                else {
                     rootPath = Environment.getExternalStorageDirectory()
                            .getAbsolutePath() + "/" + lokasiFolder + "/";
                }
                //end handle android 30 (red velvet) above
                File root = new File(rootPath);
                if (!root.exists()) {
                    //segera hapus
                  //  Log.d(MainActivity.MYTAG,"downloadnya. root doesnt exists");
                    root.mkdirs();
                }

                System.out.println("Berjalan");
//                    Write file
                Log.d(MainActivity.MYTAG,"downloadnya. versi android user: "+android.os.Build.VERSION.SDK_INT);
                    //nama_dan_lokasi_file_download = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+lokasiFolder+"/" + nama_file + ekstensiFile;
                //start handle android 30 (red velvet) above
                if(android.os.Build.VERSION.SDK_INT>= Build.VERSION_CODES.Q)
                {
                    nama_dan_lokasi_file_download = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
                            .getAbsolutePath() + "/"+lokasiFolder+"/" + nama_file + ekstensiFile;
                }
                else {
                    nama_dan_lokasi_file_download = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+lokasiFolder+"/" + nama_file + ekstensiFile;
                }
                //end handle android 30 (red velvet) above
                OutputStream output = new FileOutputStream(nama_dan_lokasi_file_download);
                byte data[] = new byte[1024];

                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called


                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();




            } catch (Exception e) {
                Log.d(MainActivity.MYTAG,"catch downloadnya: "+e.getMessage());
                System.out.println("EROR Download " + e.getMessage());

            }


            return null;
        }


        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
         progressDialog.setProgress(progress[0]);
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            loading_dismiss();
            Toast.makeText(Download_file.this, "Download selesai", Toast.LENGTH_SHORT).show();

            //  openFolder();
            showNotif();
        }


    }



    private void showNotif() {
        String NOTIFICATION_CHANNEL_ID = "chanelzona";
        Context context = this.getApplicationContext();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelName = "Download zona";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }



        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(new File(nama_dan_lokasi_file_download));
        intent1.setDataAndType(uri, getMimeType(ekstensiFile));
        //T.tsD(getApplicationContext(),"ekstensi file",ekstensiFile);
        builder.setContentIntent(PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_CANCEL_CURRENT))
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_logo)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_logo))
                .setTicker("notif starting")
                .setAutoCancel(true)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentTitle("Pengunduhan Selesai")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(nama_file + " tersimpan di dalam folder "+lokasiFolder))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        ;

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(115, builder.build());



    }

    private String getMimeType(String ekstensi) {
        //mendapatkan MIME type berdasarkan ekstensi yang ditentukan, agar pengguna bisa langsung membuka ketika mengklik notifikasi sudah download
        switch(ekstensi)
        {
            case ".docx":
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case ".pptx":
                return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            case ".pdf":
                return "application/pdf";
            case ".ppt":
                return"application/vnd.ms-powerpoint";
            case ".doc":
                return "";
            case ".xls":
                return "application/vnd.ms-excel";
            case ".xlsx":
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case ".png":
                return "image/png";
            case ".jpg":
                return "image/jpeg";
            case ".jpeg":
                return "image/jpeg";
            default:
                return "";
        }
    }


    private void initProgressDialog()
    {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sedang Mengunduh...");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(true);
    }


    private BroadcastReceiver onDownloadComplete=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id=intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,-1);
            if(downloadId==id){
                Toast.makeText(Download_file.this, "Download Selesai", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }
    };



}




