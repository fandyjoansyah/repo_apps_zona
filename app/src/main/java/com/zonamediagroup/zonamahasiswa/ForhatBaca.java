package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.models.Blokir_Model;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models.AnonimDetail_Model;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models.Curhatan_Model;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models.Komentar_Model;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models.ReportDetail_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ForhatBaca extends AppCompatActivity {
    private static final int PANJANG_NAMA_MAKSIMAL = 30;
    private boolean DUMMY_VALUE_VERIFY = false;
    private String DUMMY_CATEGORY = "";
    String URL_GET_KONTEN = ServerForhat.URL_API_FORHAT+"baca";
    String slug;
    String idUser;

    // dibawah merupakan variabel2 untuk menyimpan nilai dari hasil response server. Adapun beberapa JSON yang terdiri dari bagian printilan yang banyak, disimpan di dalam folder forhat_models/baca_models
    /*start variabel2 untuk menyimpan nilai hasil response server*/
    Curhatan_Model curhatan;
    String balasan;
    String page;
    List<ReportDetail_Model> reportDetail;
    List<AnonimDetail_Model> anonimDetail;
    List<Komentar_Model> komentar;
    int total_curhatan;
    int total_tanggapan;
    /*start variabel2 untuk menyimpan nilai hasil response server*/

    CircleImageView img_foto_profil;
    FrameLayout container_gambar;
    TextView txt_nama_pengguna,txt_waktu,judul_curhat,jumlah_like_forhat,jumlah_comment_forhat,jumlah_share_forhat,tv_kategori,txt_ikuti;
    ImageView img_lihat_identitas_asli,img_lainnya,img_utama,centang_biru;
    RelativeLayout btn_like,btn_comment,btn_share;
    WebView webview_baca_forhat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_forhat_baca);
        findViewByIdAllComponent();
        getSlugDataExtra();
        setIdUser(getIdUserFromSharedPreferences());
        getKontenFromServer(idUser,slug);
        mekanismeTampilGambar(4);
    }

    private void findViewByIdAllComponent(){
        img_foto_profil = findViewById(R.id.img_foto_profil);
        txt_nama_pengguna = findViewById(R.id.txt_nama_pengguna);
        txt_waktu = findViewById(R.id.txt_waktu);
        judul_curhat = findViewById(R.id.judul_curhat);
        img_lihat_identitas_asli = findViewById(R.id.img_lihat_identitas_asli);
        img_lainnya = findViewById(R.id.img_lainnya);
        btn_like = findViewById(R.id.btn_like);
        btn_comment = findViewById(R.id.btn_comment);
        btn_share = findViewById(R.id.btn_share);
        webview_baca_forhat = findViewById(R.id.webview_baca_forhat);
        jumlah_like_forhat = findViewById(R.id.jumlah_like_forhat);
        jumlah_comment_forhat = findViewById(R.id.jumlah_comment_forhat);
        jumlah_share_forhat = findViewById(R.id.jumlah_share_forhat);
        img_utama = findViewById(R.id.img_utama);
        container_gambar = findViewById(R.id.container_gambar);
        tv_kategori = findViewById(R.id.tv_kategori);
        txt_ikuti = findViewById(R.id.txt_ikuti);
        centang_biru = findViewById(R.id.centang_biru);
    }

    private void mekanismeTampilGambarAtauVideo(){

    }

    private void mekanismeTampilGambar(int jumlahGambar){
        //cek jumlah gambar
        switch(jumlahGambar)
        {
            case 1 :
                initShowSatuGambar();
                break;
            case 2 :
                initShowDuaGambar();
                break;
            case 3 :
                initShowTigaGambar();
                break;
            case 4 :
                initShowEmpatGambar();
                break;
        }
    }

    private void mekanismeTampilVideo(){

    }

    private void initShowSatuGambar(){
        View satuGambar = getLayoutInflater().inflate(R.layout.layout_forhat_one_image,null);
        container_gambar.addView(satuGambar);
    }

    private void initShowDuaGambar(){
        View duaGambar = getLayoutInflater().inflate(R.layout.layout_forhat_two_image,null);
        container_gambar.addView(duaGambar);
    }

    private void initShowTigaGambar(){
        View tigaGambar = getLayoutInflater().inflate(R.layout.layout_forhat_three_image,null);
        container_gambar.addView(tigaGambar);
    }

    private void initShowEmpatGambar(){
        View empatGambar = getLayoutInflater().inflate(R.layout.layout_forhat_four_image,null);
        container_gambar.addView(empatGambar);
    }

    public void setIdUser(String idUser){
        this.idUser = idUser;
    }

    public String getIdUserFromSharedPreferences(){
        return SharedPrefManagerZona.getInstance(ForhatBaca.this).getid_user();
    }



    private void getSlugDataExtra(){
        slug = getIntent().getStringExtra("slug");
    }

    private void getKontenFromServer(String idUser, String slug){
        Log.d("5_september_2022","gKFS");
        Log.d("5_september_2022","gKFS param.idUser: "+idUser+" slug: "+slug);
        AndroidNetworking.post(URL_GET_KONTEN)
                .addBodyParameter("slug", slug)
                .addHeaders("id",idUser)
                .setTag("get_konten")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("5_september_2022","gKFS try");
                            //check jika get konten berhasil
                            int status = response.getInt("status");
                            if(status == 1)
                            {
                                Log.d("5_september_2022","gKFS try if");
                                saveResponseDataToLocalData(response.getJSONObject("data"));
                                setDataToComponents();
                            }
                            Log.d("29_juni_2022","message: "+response.getString("message"));
                        } catch (JSONException e) {
                            Log.d("5_september_2022","gKFS catch: "+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.d("5_september_2022","gKFS onError: "+error.getErrorBody());
                        // handle error
                    }
                });
    }

    private void saveResponseDataToLocalData(JSONObject data){
        /*
        Struktur Response:

        JSONObject data
            JSONObject curhatan
                    ...
                    ...
                    ...
            String balasan
            String page
            JSONArray report_detail
                    String id
                    String report_name
            JSONArray anonim_detail
                    String id
                    String nama
            JSONArray komentar
                    ...
                    ...
                    ...
            int total_curhatan
            int total_tanggapan

        */

        try {
            saveCurhatanToLocal(data.getJSONObject("curhatan"));
            saveBalasanToLocal(data.getString("balasan"));
            savePageToLocal(data.getString("page"));
            saveReportDetailToLocal(data.getJSONArray("report_detail"));
            saveAnonimDetailToLocal(data.getJSONArray("anonim_detail"));
            saveKomentarToLocal(data.getJSONArray("komentar"));
            saveTotalCurhatanToLocal(data.getInt("total_curhatan"));
            saveTotalTanggapanToLocal(data.getInt("total_tanggapan"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void saveCurhatanToLocal(JSONObject curhatan){
        this.curhatan = new Gson().fromJson(curhatan.toString(), new TypeToken<Curhatan_Model>() {
        }.getType());
    }

    private void saveBalasanToLocal(String balasan){
        this.balasan = balasan;
    }

    private void savePageToLocal(String page){
        this.page = page;
    }

    private void saveReportDetailToLocal(JSONArray report_detail){
        this.reportDetail = new Gson().fromJson(report_detail.toString(), new TypeToken<List<ReportDetail_Model>>() {
        }.getType());
        Log.d("30_juni_2022","report detail ke 2: "+this.reportDetail.get(2).getReport_name());
    }

    private void saveAnonimDetailToLocal(JSONArray anonim_detail){
        this.anonimDetail = new Gson().fromJson(anonim_detail.toString(), new TypeToken<List<ReportDetail_Model>>() {
        }.getType());
    }

    private void saveKomentarToLocal(JSONArray komentar){
        this.komentar = new Gson().fromJson(komentar.toString(), new TypeToken<List<com.zonamediagroup.zonamahasiswa.models.forhat_models.baca_models.Komentar_Model>>() {
        }.getType());
    }

    private void saveTotalCurhatanToLocal(int totalCurhatan){
        this.total_curhatan = totalCurhatan;
    }

    private void saveTotalTanggapanToLocal(int totalTanggapan){
        this.total_tanggapan = totalTanggapan;
    }


    private void setDataToComponents(){
        Log.d("5_september_2022","judul curhatan: "+curhatan.getJudul_curhatan());
        Log.d("5_september_2022","total komentar: "+curhatan.getTotal_komentar());
        Log.d("5_september_2022","total like: "+curhatan.getTotal_like());

        /*set data dari hasil response ke view*/
        set_img_foto_profil(curhatan.getFoto_user(),curhatan.getAvatar());

        /*check apakah anonymous null atau tidak */
        if(curhatan.getAnonymouse()!=null) {
            /*check apakah curhatan berupa anonim atau tidak*/
            if (curhatan.getAnonymouse().equals("ya")) {
                img_lihat_identitas_asli.setVisibility(View.VISIBLE);
                txt_ikuti.setVisibility(View.GONE);
            }
        }
        /*check apakah akun verified atau tidak */
        boolean verify = DUMMY_VALUE_VERIFY;
        if(verify)
        {
            centang_biru.setVisibility(View.VISIBLE);
        }






        mekanismeSetTextnamaPengguna(curhatan.getNama_user());
        txt_waktu.setText(curhatan.getCreated_at());
        jumlah_like_forhat.setText(String.valueOf(curhatan.getTotal_like()));
        jumlah_comment_forhat.setText(String.valueOf(curhatan.getTotal_komentar()));
        jumlah_share_forhat.setText(String.valueOf(curhatan.getTotal_share()));
        set_img_utama(ServerForhat.URL_IMG_LOCATION+curhatan.getCover());
        judul_curhat.setText(curhatan.getJudul_curhatan());
        loadDataToWebView(getContentWithCss(curhatan.getIsi_curhatan()));

        /* jika ada kategori, maka set ke btn kategori, jika tidak(curhatan awal2 forHat berdiri yang tanpa kategori) maka sembunyikan btn kategori*/
        if(curhatan.getNama_kategori()!=null)
        {
            tv_kategori.setText(curhatan.getNama_kategori());
        }
        else
        {
            tv_kategori.setVisibility(View.GONE);
        }

    }



    /*method untuk menghandle semisal karakter nama pengguna terlalu banyak*/
    private void mekanismeSetTextnamaPengguna(String nama)
    {
        if(cekPanjangString(nama,PANJANG_NAMA_MAKSIMAL))
        {
            txt_nama_pengguna.setText(nama);
        }
        else
        {
            nama = nama.substring(0,Math.min(nama.length(),PANJANG_NAMA_MAKSIMAL));
            txt_nama_pengguna.setText(nama+"...");
        }
    }

    /*fungsi untuk mengecek panjang String apakah lebih dari limit atau tidak
    true = panjang belum melebihi
    false = panjang sudah melebihi
    * */
    private boolean cekPanjangString(String nama, int panjangLimit)
    {
        if(nama.length() > panjangLimit)
            return false;
        else
            return true;
    }

    private void set_img_utama(String value){
        Glide.with(ForhatBaca.this)
                .load(value)
                .apply(RequestOptions.fitCenterTransform())
                .into(img_utama);
    }

    private void loadDataToWebView(String htmlContent)
    {
        Log.d("5_september_2022","load data to webview. htmlContent: "+htmlContent);
        webview_baca_forhat.loadData(htmlContent, "text/html; charset=utf-8", "UTF-8");
    }



    private void set_img_foto_profil(String fotoProfile,String avatar){
        String imgDestination;
        if(fotoProfile != null)
        {
            imgDestination = fotoProfile;
        }
        else
        {
            imgDestination = avatar;
        }
        Log.d("28_juni_2022","ifp: "+fotoProfile);
        Glide.with(ForhatBaca.this)
                .load(imgDestination)
                .apply(RequestOptions.fitCenterTransform())
                .into(img_foto_profil);
    }

    private String getContentWithCss(String content){
        String css = "    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
                "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
                "<link href=\"https://fonts.googleapis.com/css2?family=Poppins&display=swap\" rel=\"stylesheet\">\n" +
                "<style>\n" +
                "    *{\n" +
                "        font-family: 'Poppins', sans-serif;\n" +
                "        padding:inherit 0;\n" +
                "        margin:inherit 0;\n" +
                "    }\n" +
                "    p{\n" +
                "        font-size: 16px;\n" +
                "    }\n" +
                "    img {\n" +
                "        width:100%;\n" +
                "        height:auto;\n" +
                "    }\n" +
                "</style>";
        return css+content;
    }


}