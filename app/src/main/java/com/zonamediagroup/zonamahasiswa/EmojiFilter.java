package com.zonamediagroup.zonamahasiswa;

import android.text.InputFilter;
import android.text.Spanned;

public class EmojiFilter {
    public static InputFilter[] getFilter(){
        InputFilter EMOJI_FILTER = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence charSequence, int start, int end, Spanned spanned, int i2, int i3) {
                for(int index=start;index<end;index++)
                {
                    int type = Character.getType(charSequence.charAt(index));

                    if(type == Character.SURROGATE || type == Character.NON_SPACING_MARK || type==Character.OTHER_SYMBOL){
                        return "";
                    }
                }
                return null;
            }
        };
        return new InputFilter[]{EMOJI_FILTER};
    }
}
