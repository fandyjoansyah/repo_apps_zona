package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.ForhatFragment.ForhatFilter;
import com.zonamediagroup.zonamahasiswa.ForhatFragment.ForhatHome;
import com.zonamediagroup.zonamahasiswa.ForhatFragment.ForhatKategori;
import com.zonamediagroup.zonamahasiswa.ForhatFragment.ForhatProfile;
import com.zonamediagroup.zonamahasiswa.ForhatFragment.ForhatTerpopuler;
import com.zonamediagroup.zonamahasiswa.Fragment.Forhat;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Fragment.Kategori;
import com.zonamediagroup.zonamahasiswa.Fragment.Notifikasi;
import com.zonamediagroup.zonamahasiswa.Fragment.Poling;
import com.zonamediagroup.zonamahasiswa.Fragment.Profile;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClient;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi;
import com.zonamediagroup.zonamahasiswa.models.Kategori_Model;
import com.zonamediagroup.zonamahasiswa.models.NotifUpdateModel;
import com.zonamediagroup.zonamahasiswa.models.Poling_Model;
import com.zonamediagroup.zonamahasiswa.models.PopUp_Halaman_Utama_Model;
import com.zonamediagroup.zonamahasiswa.models.Recyclerview_home;
import com.zonamediagroup.zonamahasiswa.models.Tooltips_Model;
import com.zonamediagroup.zonamahasiswa.tooltip.OverlayView;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltip;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltipUtils;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZonaController;

import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForhatActivity extends AppCompatActivity {


    BottomNavigationView bottomNavigationView;
    int page = 1;



    public static List<Tooltips_Model> allToolTips;

    boolean doubleBackToExitPressedOnce = false;

    public static final String MYTAG = "fandydebugger";

    public static final String URL_AUDIENS_ONLINE = Server.URL_REAL + "User_online";
    //    iklan

    //start untuk notif update
    private AppUpdateManager mAppUpdateManager;
    private static final int RC_APP_UPDATE = 100;
    //end untuk notif update


    //    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/5224354917";
    private static String AD_UNIT_ID = "";
    private static final long COUNTER_TIME = 10;

    private int coinCount;

    private CountDownTimer countDownTimer;
    private boolean gameOver;
    private boolean gamePaused;

    private RewardedAd rewardedAd;

    private long timeRemaining;
    boolean isLoading;
    Kategori_Model item_dua;
    String id;
    public static String idUserLogin = "";
//    iklan end

    private AlertDialog.Builder dialogBuilderNotifikasiUpdate;
    private AlertDialog dialogNotifikasiUpdate;

    // private static final String URL_GET_TOOLTIPS = Server.URL_GET_TOOLTIPS;
    private static final String URL_GET_CUSTOM_UPDATES = Server.URL_REAL + "Get_custom_popup_update";

    private static final String URL_GET_PARAM_FOR_ACTIVITY_THAT_OPENED_FROM_POPUP = Server.URL_REAL+"Get_param_activity_from_popup";

    private static final String URL_GET_POPUP_HALAMAN_UTAMA = Server.URL_REAL+"Get_popup_halaman_utama";


    private Handler handler = new Handler();
    Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forhat);

        //start test SP
//    SharedPrefManagerZona.getInstance(mActivity).saveToolTips("tgl",19);
        // SharedPrefManagerZona.getInstance(ForhatActivity.this).saveToolTips("tgl",19);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("zonamhs", 0); // 0 - for private mode

        //end test SP

        requestDataForCustomPopUpNotifyUpdate();
        //cekAppsVersiTerbaru();
        initIklan();
        //Log.d("Fandy","Main Activity terpanggil");

        init();
        declare();

        //4 des sementara comment
//    loadPage(new Home());
        idUserLogin = SharedPrefManagerZona.getInstance(this).getid_user();
        //getToolTips();
        Log.d(MYTAG, "idUserLogin: " + idUserLogin);
        startVideo();
        startAudiensOnline();
        sendOnlineAudiens();
        showPopUpHalamanUtama();
    }


    private void initIklan() {
        //load iklan di awal (mainActivity), agar ketika digunakan nantinya cepat tidak perlu load lagi
        IklanInterstitial.initIklanVideo(getApplicationContext());
        IklanInterstitial.initIklanGambar(getApplicationContext());
    }



    public void showPopUpHalamanUtama(){

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_POPUP_HALAMAN_UTAMA, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {



                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data Global");


                    if (jObj.getString("status").equals("true")) {

                        JSONArray dataArray = jObj.getJSONArray("popup_aktif");


                        List<PopUp_Halaman_Utama_Model> itemPopUps = new Gson().fromJson(dataArray.toString(), new TypeToken<List<PopUp_Halaman_Utama_Model>>() {
                        }.getType());

                        PopUp_Halaman_Utama_Model itemPopUp = itemPopUps.get(0);

                        //jika ada data tampil, maka ada popup aktif, maka tampilkan popup
                        if(itemPopUp != null)
                        {
                            show_dialog_halaman_utama(itemPopUp.getUrl_gambar(),itemPopUp.getParam_tambahan(),itemPopUp.getText_tombol_aksi(),itemPopUp.getJenis_popup());
                        }



                    } else if (jObj.getString("status").equals("false")) {


                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error


                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("showPopupHalamanUtama", "Register Error: " + error.getMessage());
//                dismissdialog();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {


                Log.e("showPopupHalamanUtama", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void init() {
        bottomNavigationView = findViewById(R.id.nav_forhat);
    }

    void declare() {
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.home_forhat);
    }

    private void startVideo() {
        if (getIntent().getBooleanExtra("video", false) == true)
            bottomNavigationView.setSelectedItemId(R.id.video);
    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home_forhat:
                    loadPage(new ForhatHome());
                    page = 1;
                    return true;

                case R.id.terpopuler_forhat:
                    page = 2;
                    loadPage(new ForhatTerpopuler());
                    return true;

                case R.id.kategori_forhat:
                    page = 3;
                    loadPage(new ForhatKategori());
                    return true;

                case R.id.filter_forhat:
                    page = 4;
                    loadPage(new ForhatFilter());
                    return true;

                case R.id.profile_forhat:
                    page = 5;
                    loadPage(new ForhatProfile());
                    return true;
            }
            return false;
        }

    };

    public boolean loadPage(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (page != 1) {
            loadPage(new ForhatHome());
            bottomNavigationView.setSelectedItemId(R.id.home_forhat);

        } else {

            if (doubleBackToExitPressedOnce) {
                Intent i = new Intent(ForhatActivity.this,MainActivity.class); // Your list's Intent
                i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
                startActivity(i);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            CustomToast.makeBlackCustomToast(this,"Tekan sekali lagi untuk kembali ke Zona Mahasiswa",Toast.LENGTH_SHORT);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }
    }

    // iklan
    private void loadRewardedAd() {
        if (rewardedAd == null || !rewardedAd.isLoaded()) {
            rewardedAd = new RewardedAd(ForhatActivity.this, AD_UNIT_ID);
            isLoading = true;
            rewardedAd.loadAd(
                    new AdRequest.Builder().build(),
                    new RewardedAdLoadCallback() {
                        @Override
                        public void onRewardedAdLoaded() {
                            // Ad successfully loaded.
                            isLoading = false;
                            //Toast.makeText(Point.this, "onRewardedAdLoaded", Toast.LENGTH_SHORT).show();
                            startGame();
                        }

                        @Override
                        public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
                            // Ad failed to load.
                            isLoading = false;
                            //Toast.makeText(Point.this, "on Rewarded Ad FailedTo Load", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(Kirim_file.this, "Failed To Load", Toast.LENGTH_SHORT).show();
                            System.out.println("error load " + loadAdError);
                        }
                    });
        }
    }

    private void startGame() {
// Run Coll down
        if (!rewardedAd.isLoaded() && !isLoading) {
            loadRewardedAd();

        }
        createTimer(COUNTER_TIME);
        gamePaused = false;
        gameOver = false;

    }

    private void createTimer(long time) {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer =
                new CountDownTimer(time * 100, 1) {
                    @Override
                    public void onTick(long millisUnitFinished) {
                        timeRemaining = ((millisUnitFinished / 1000) + 1);
                        //Toast.makeText(Point.this, "Remaining " + timeRemaining, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFinish() {
                        if (rewardedAd.isLoaded()) {

                            showRewardedVideo();
                        }

                        gameOver = true;
                    }
                };
        countDownTimer.start();
    }

    private void showRewardedVideo() {
        //    showVideoButton.setVisibility(View.INVISIBLE);
        if (rewardedAd.isLoaded()) {
            RewardedAdCallback adCallback =
                    new RewardedAdCallback() {
                        @Override
                        public void onRewardedAdOpened() {
                            // Ad opened.

                        }

                        @Override
                        public void onRewardedAdClosed() {
                            // Ad closed.
                            loadPage(new Poling());
                        }

                        @Override
                        public void onUserEarnedReward(RewardItem rewardItem) {
                            // User earned reward.

                            // download disini ya
                            // Call API
                            //start setup progress dialog

                            //end setup progress dialog
                            // uploadPDF(displayName, uri);

                        }

                        @Override
                        public void onRewardedAdFailedToShow(AdError adError) {
                            // Ad failed to display
                            //  Log.e("Kirim_file","onRewardedAdFailedToShow "+adError.getMessage());
                        }
                    };
            rewardedAd.show(ForhatActivity.this, adCallback);
        }
    }

    // END IKLAN

    private InstallStateUpdatedListener installStateUpdatedListener = new InstallStateUpdatedListener() {
        @Override
        public void onStateUpdate(InstallState state) {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                showCompletedUpdate();
            }
        }
    };

    private void showCompletedUpdate() {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "New app is ready!", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Install", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAppUpdateManager.completeUpdate();
            }
        });
        snackbar.show();
    }

    View click_custom, tooltip;

    @Override
    protected void onStart() {
        super.onStart();
//    click_custom = findViewById(R.id.anchor_tooltip_awal);
//    tooltip(click_custom);


    }


    @Override
    protected void onStop() {
        if (mAppUpdateManager != null)
            mAppUpdateManager.unregisterListener(installStateUpdatedListener);
        super.onStop();
    }

    private void tooltip(View v) {
        final SimpleTooltip tooltip = new SimpleTooltip.Builder(this)
                .anchorView(v)
                .text("Selamat datang di aplikasi zona")
                .gravity(Gravity.CENTER)
                .showArrow(false)
                .dismissOnOutsideTouch(false)
                .dismissOnInsideTouch(false)
                .modal(true)
                .animated(true)
                .transparentOverlay(false)
                .overlayMatchParent(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .overlayOffset(0)
                .animationDuration(2000)
                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
                .contentView(R.layout.tooltip_welcome, R.id.tv_text)
                .focusable(true)
                .build();

        tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tooltip.isShowing())
                    tooltip.dismiss();
            }
        });

        tooltip.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void cekAppsVersiTerbaru() {
        InterfaceApi api = ApiClient.getClient().create(InterfaceApi.class);
        Call<NotifUpdateModel> call = api.getStatusUpdate();
        call.enqueue(new Callback<NotifUpdateModel>() {
            @Override
            public void onResponse(Call<NotifUpdateModel> call, Response<NotifUpdateModel> response) {
                if (response.body().getStatusUpdate() == 1) //jika respon 1, aktifkan pop up update
                {
                    //start code notif update
                    mAppUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
                    mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
                        @Override
                        public void onSuccess(AppUpdateInfo result) {
                            if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                                    && result.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                                try {
                                    mAppUpdateManager.startUpdateFlowForResult(result, AppUpdateType.FLEXIBLE, ForhatActivity.this, RC_APP_UPDATE);
                                } catch (IntentSender.SendIntentException e) {

                                }
                            }
                        }
                    });
                    mAppUpdateManager.registerListener(installStateUpdatedListener);

                    //end code notif update
                }

            }

            @Override
            public void onFailure(Call<NotifUpdateModel> call, Throwable t) {

            }
        });
    }


//  private void getToolTips()
//  {
//    StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_TOOLTIPS, new com.android.volley.Response.Listener<String>() {
//
//      @Override
//      public void onResponse(String response) {
//
//
//
//        try {
//
//          JSONObject jObj = new JSONObject(response);
//          // System.out.println(jObj.getString("status"));
//
//
//          if (jObj.getString("status").equals("true")) {
//
//            JSONArray dataArray = jObj.getJSONArray("data");
//
//
//
//            List<Tooltips_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Tooltips_Model>>() {
//            }.getType());
//            allToolTips = items;
//
//          }
//
//
//          // Check for error node in json
//
//        } catch (JSONException e) {
//          // JSON error
//          Log.d(MainActivity.MYTAG,"getToolTips catch "+e.getMessage());
//          e.printStackTrace();
//        }
//
//      }
//    }, new com.android.volley.Response.ErrorListener() {
//
//      @Override
//      public void onErrorResponse(VolleyError error) {
//        //  Log.e(TAG, "Register Error: " + error.getMessage());
//
//        Log.d(MainActivity.MYTAG,"getToolTips onErrorResponse "+error.getMessage());
//
//      }
//    }) {
//
//
////            parameter
//
//      @Override
//      protected Map<String, String> getParams() {
//        // Posting parameters to login url
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("TOKEN", "qwerty");
//        params.put("id_user", idUserLogin);
//        return params;
//      }
//    };
//
//// Heandling error
//
//    strReq.setRetryPolicy(new RetryPolicy() {
//      @Override
//      public int getCurrentTimeout() {
//        return 10000;
//      }
//
//      @Override
//      public int getCurrentRetryCount() {
//        return 10000;
//      }
//
//      @Override
//      public void retry(VolleyError error) throws VolleyError {
//
//        //  Log.e(TAG, "VolleyError Error: " + error.getMessage());
//
//        Log.d(MYTAG,"getTooltips error retry: "+ error.getMessage());
//      }
//    });
//
//
//    // Adding request to request queue
//    MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
//  }


    private void requestDataForCustomPopUpNotifyUpdate() {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_CUSTOM_UPDATES, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObjCustomPopUpUpdate = new JSONObject(response).getJSONObject("custom_popup_update");
                    String urlGambarPopUp = jObjCustomPopUpUpdate.getString("url_img_himbauan_update");
                    int versionCodeServer = Integer.valueOf(jObjCustomPopUpUpdate.getString("version_code_terbaru"));
                    int statusShow = Integer.valueOf(jObjCustomPopUpUpdate.getString("aktif"));
                    if (showOrNotShowPopUpNotifyUpdate(versionCodeServer, statusShow))
                        prepareCustomPopUpNotifyUpdate(urlGambarPopUp);
                    //debugXX(urlGambarPopUp);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(MainActivity.MYTAG, "requestDataForCustomPopUpNotifyUpdate()->catch " + e.getMessage());
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(MainActivity.MYTAG, "error MainActivity requestDataForCustomPopUpNotifyUpdate()->onErrorResponse: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.e(MainActivity.MYTAG, "error MainActivity requestDataForCustomPopUpNotifyUpdate()->retry: " + error.getMessage());
            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    private boolean showOrNotShowPopUpNotifyUpdate(int versionCodeServer, int statusShow) {
        if (statusShow == 0)
            return false;
        int currentVersionCode = getCurrentVersionCode();
        if (versionCodeServer > currentVersionCode)
            return true;
        else
            return false;
    }

    private int getCurrentVersionCode() {
        PackageInfo pInfo = null;
        try {
            pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionCode;
    }

    private void prepareCustomPopUpNotifyUpdate(String urlImagePopup) {
        final View vPopUp = getLayoutInflater().inflate(R.layout.popup_notif_update, null);
        setupPopUpNotifikasiUpdate(vPopUp);
        setImagePopUp(vPopUp, urlImagePopup);
        setButtonRedirectToPlayStore(vPopUp);
        setOnClickCLoseNotifUpdate(vPopUp);
        showPopUpNotifikasiUpdate();
    }

    private void setOnClickCLoseNotifUpdate(View vPopUp) {
        ImageView closePopUp = vPopUp.findViewById(R.id.close_popup_update);
        closePopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNotifikasiUpdate.dismiss();
            }
        });
    }

    private void setImagePopUp(View view, String urlGambar) {
        ImageView imgHimbauanPopUp = view.findViewById(R.id.gambar_notify_update);
        imgHimbauanPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.zonamediagroup.zonamahasiswa")));
            }
        });
        Glide.with(this)
                .load(urlGambar)
                .apply(RequestOptions.fitCenterTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imgHimbauanPopUp);
    }

    private void showPopUpNotifikasiUpdate() {
        dialogNotifikasiUpdate.show();
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.85);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.75);
        dialogNotifikasiUpdate.getWindow().setLayout(width, height);
        dialogNotifikasiUpdate.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void setButtonRedirectToPlayStore(View view) {
        Button btnRedirectPlayStore = view.findViewById(R.id.btn_update_aplikasi);
        btnRedirectPlayStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.zonamediagroup.zonamahasiswa")));
            }
        });
    }

    private void setupPopUpNotifikasiUpdate(View vPopUp) {
        dialogBuilderNotifikasiUpdate = new AlertDialog.Builder(ForhatActivity.this);
        dialogBuilderNotifikasiUpdate.setView(vPopUp);
        dialogNotifikasiUpdate = dialogBuilderNotifikasiUpdate.create();

        dialogNotifikasiUpdate.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
        vPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void showPopUpNotifikasi() {
        dialogNotifikasiUpdate.show();
    }

    AlertDialog.Builder dialogBuilder;
    AlertDialog dialog;

    public void debugXX(String urlGambar) {

        dialogBuilder = new AlertDialog.Builder(ForhatActivity.this);
        View vPopUp = getLayoutInflater().inflate(R.layout.popup_notif_update, null);
        dialogBuilder.setView(vPopUp);
        dialog = dialogBuilder.create();
        dialog.show();
        ImageView imgNya = vPopUp.findViewById(R.id.gambar_notify_update);
        Glide.with(dialog.getContext())
                .load(urlGambar)
                .apply(RequestOptions.fitCenterTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        dialog.show();
                        return false;
                    }
                })
                .into(imgNya);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
            }
        });
        vPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        //  int width = (int)(getResources().getDisplayMetrics().widthPixels*0.93);
        // int height = (int)(getResources().getDisplayMetrics().heightPixels*0.77);
        // dialog.getWindow().setLayout(width, height);
    }

    public  void startAudiensOnline(){
        runnable = new Runnable() {
            @Override
            public void run() {
                sendOnlineAudiens();
                Log.d("debagpesan", "okoko");
                handler.postDelayed(this, 60000);
            }
        };
        handler.postDelayed(runnable, 60000);
    }
    public void sendOnlineAudiens() {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_AUDIENS_ONLINE, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                } catch (JSONException e) {
                    // JSON error
//                    Log.d(MainActivity.MYTAG,"catch response getALl");
//                    eror_show();
//                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG, "onErrorResponse getALl " + error.getMessage());
            }
        }) {


            //            parameter
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("minute", "1");
                return params;
            }
        };
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }



    public void show_dialog_halaman_utama(String urlGambar,String param,String textButton,String jenisPopup){
        Log.d("fandy_debuger_25april","urlGambar: "+urlGambar);
        Log.d("fandy_debuger_25april","param: "+param);
        Log.d("fandy_debuger_25april","textButton: "+textButton);
        final View vPopUp = getLayoutInflater().inflate(R.layout.popup_zona, null);

        android.app.AlertDialog dialognya;
        android.app.AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
        WebView webView = new WebView(this);
        webView.loadUrl("http://www.google.com/");
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                // Toast.makeText(getApplicationContext(),"to string: "+request.getUrl().toString(),Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        Button button = vPopUp.findViewById(R.id.aksi_popup);
        ImageView closePopup = vPopUp.findViewById(R.id.close_popup);
        ImageView gambar_popup = vPopUp.findViewById(R.id.gambar_popup);


        //start set data popup dari server
        button.setText(textButton);
        Glide.with(this)
                .load(urlGambar)
                .apply(RequestOptions.fitCenterTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(gambar_popup);
        //end set data popup dari server

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPopUpDestination(jenisPopup,param);
                alert.dismiss();
            }
        });

        alert.setView(vPopUp);
//        alert.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
        closePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        //jika jenis pop up update apps, maka ada perlakuan khusus, yaitu jangan show pop up ketika version code pengguna sama atau lebih tinggi dari version code yang dikirim oleh popup
        if(jenisPopup.equals(UPDATE_APPS)){
            int versionCode = BuildConfig.VERSION_CODE;
            int versionCodePopUp = Integer.valueOf(param);
            if(versionCode < versionCodePopUp){
                alert.show();
            }

        }
        else{
            alert.show();
        }


        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.93);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.77);
        alert.getWindow().setLayout(width,height);

    }

    //start link action pop up berupa fragment
    private static final String POLING = "poling";
    private static final String PROFIL = "profil";
    private static final String KATEGORI = "kategori";
    private static final String VIDEO = "video";
    //end link action pop up berupa fragment

    //start link action pop up berupa activity
    private static final String DETAIL_POLING = "detail_poling";
    private static final String VIDEO_SHARE = "video_share";
    private static final String DETAIL = "detail";
    private static final String POST_BY_KATEGORI = "post_by_kategori";
    private static final String TEMPLATE = "template";
    //end link action pop up berupa activity

    //Start link action pop up berupa halaman url lain
    private static final String URL_WEB = "url_web";
    //End link action pop up berupa halaman url lain

    //start link action berupa update aplikasi
    private static final String UPDATE_APPS = "update_apps";
    //end link action berupa update aplikasi


    private void openPopUpDestination(String tujuanPopUp,String paramTambahan){
        //start popup redirect opener

        switch(tujuanPopUp){
            case POLING:{
                // tanpa param tambahan
                loadPage(new Poling());
                break;
            }
            case PROFIL:{
                // tanpa param tambahan
                loadPage(new Profile());
                break;
            }
            case KATEGORI:{
                // tanpa param tambahan
                loadPage(new Kategori());
                break;
            }
            case VIDEO:{
                // tanpa param tambahan
                loadPage(new Video());
                break;
            }
            case DETAIL:{
                // dengan param tambahan. Param tambahan berupa idPost dari post yang akan dibuka
                Intent intent = new Intent(ForhatActivity.this,Detail_by_id.class);
                intent.putExtra("id_post",paramTambahan);
                startActivity(intent);
                break;
            }
            case VIDEO_SHARE:{
                // dengan param tambahan. Param tambahan berupa id_video dari post yang akan dibuka
                Intent intent = new Intent(ForhatActivity.this,Video_share.class);
                intent.putExtra("id_video",paramTambahan);
                startActivity(intent);
                break;
            }
            case TEMPLATE:{
                // tanpa param tambahan
                Intent intent = new Intent(ForhatActivity.this,Download_file.class);
                startActivity(intent);
                break;
            }
            case URL_WEB:{
                // dengan param tambahan. Param tambahan berupa link url yang akan dibuka
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(paramTambahan));
                startActivity(browserIntent);
            }
            case UPDATE_APPS:{
                // dengan param tambahan. Param tambahan berupa version code terbaru, untuk nantinya dibandingkan dengan version code punya user dan user akan menampilkan pop up updatenya jika VC user lebih rendah dari VC kiriman server
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.zonamediagroup.zonamahasiswa"));
                startActivity(browserIntent);
            }
            case "":{
                break;
            }
            default:{
                //tujuan popup lebih lanjut, yaitu call ke server untuk mendapatkan paramTambahan
                dapatkanParamUntukBukaActivityDariPopUp(tujuanPopUp,paramTambahan);
            }
        }


        //end popup redirect opener
    }

    private void dapatkanParamUntukBukaActivityDariPopUp(String tujuanPopUp, String paramTambahan){

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_PARAM_FOR_ACTIVITY_THAT_OPENED_FROM_POPUP, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(Bank_Tag.POPUP_DINAMIS,"onResponse dapatkanParam");
                try {
                    Log.d(Bank_Tag.POPUP_DINAMIS,"onResponse try dapatkanParam");
                    switch(tujuanPopUp){
                        case DETAIL_POLING:{
                            JSONObject jObj = new JSONObject(response);
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Poling_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Poling_Model>>() {
                            }.getType());
                            Intent intent = new Intent(ForhatActivity.this,Detail_poling.class);
                            intent.putExtra("id_poling", items.get(0).getId_poling());
                            intent.putExtra("tiitle_poling", items.get(0).getTiitle_poling());
                            intent.putExtra("img_poling", items.get(0).getImg_poling());
                            intent.putExtra("poling_start", items.get(0).getPoling_start());
                            intent.putExtra("poling_end", items.get(0).getPoling_end());
                            intent.putExtra("waktu_sekarang", items.get(0).getWaktu_sekarang());
                            startActivity(intent);
                            break;
                        }
                        case POST_BY_KATEGORI:{
                            JSONObject jObj = new JSONObject(response);
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Kategori_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Kategori_Model>>() {
                            }.getType());
                            Intent intent = new Intent(ForhatActivity.this,Post_by_kategori.class);

                            intent.putExtra("id_kategori", items.get(0).getId_kategori());
                            intent.putExtra("id_categori_wp", items.get(0).getId_categori_wp());
                            intent.putExtra("nama_kategori", items.get(0).getNama_kategori());
                            intent.putExtra("gambar_kategori", items.get(0).getGambar_kategori());
                            intent.putExtra("warna_kategori", items.get(0).getWarna_kategori());
                            startActivity(intent);
                            break;
                        }
                    }
                } catch (Exception e) {
                    Log.d(Bank_Tag.POPUP_DINAMIS,"onResponse catch dapatkanParam e: "+e.getMessage());
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(Bank_Tag.POPUP_DINAMIS,"onErrorResponse dapatkanParam error: "+error.getMessage());
                Log.e(MainActivity.MYTAG, "error MainActivity requestDataForCustomPopUpNotifyUpdate()->onErrorResponse: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("param", paramTambahan);
                params.put("tujuanPopUp", tujuanPopUp);
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.e(MainActivity.MYTAG, "error MainActivity requestDataForCustomPopUpNotifyUpdate()->retry: " + error.getMessage());
            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }







}