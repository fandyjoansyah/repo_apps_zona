package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

public class KebijakanPrivasi extends AppCompatActivity {
    WebView webPriv;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_kebijakan_privasi);

        webPriv = findViewById(R.id.web_priv);

        String x =
                "<style>p{text-align: left; line-height:20px}</style>" +
                        "<h4>Kebijakan Privasi untuk zonamahasiswa.id</h4>\n" +
                        "    <p>zonamahasiswa.id, dapat diakses dari http://zonamahasiswa.id/, salah satu prioritas utama kami adalah privasi pengunjung kami. Dokumen Kebijakan Privasi ini berisi jenis informasi yang dikumpulkan dan dicatat oleh zonamahasiswa.id dan cara kami menggunakannya.</p>\n" +
                        "    <p>Jika Anda memiliki pertanyaan tambahan atau memerlukan informasi lebih lanjut tentang Kebijakan Privasi kami, jangan ragu untuk menghubungi kami.</p>\n" +
                        "    <h4>File Log</h4>\n" +
                        "    <p>zonamahasiswa.id mengikuti prosedur standar menggunakan file log. File-file ini mencatat pengunjung ketika mereka mengunjungi situs web. Semua perusahaan hosting melakukan ini dan merupakan bagian dari analisis layanan hosting. Informasi yang dikumpulkan oleh file log termasuk alamat protokol internet (IP), jenis browser, Penyedia Layanan Internet (ISP), cap tanggal dan waktu, halaman rujukan / keluar, dan mungkin jumlah klik. Ini tidak terkait dengan informasi apa pun yang dapat diidentifikasi secara pribadi. Tujuan informasi ini adalah untuk menganalisis tren, mengelola situs, melacak pergerakan pengguna di situs web, dan mengumpulkan informasi demografis.</p>\n" +
                        "    <h4>Cookie Google DoubleClick DART</h4>\n" +
                        "    <p>Google adalah salah satu vendor pihak ketiga di situs kami. Itu juga menggunakan cookie, yang dikenal sebagai cookie DART, untuk menayangkan iklan kepada pengunjung situs kami berdasarkan kunjungan mereka ke www.website.com dan situs lain di internet. Namun, pengunjung dapat memilih untuk menolak penggunaan cookie DART dengan mengunjungi iklan Google dan kebijakan privasi jaringan konten di URL berikut – https://policies.google.com/technologies/ads</p>\n" +
                        "    <h4></h4>\n" +
                        "    <p>Beberapa pengiklan di situs kami mungkin menggunakan cookie dan suar web. Mitra iklan kami tercantum di bawah ini. Setiap mitra periklanan kami memiliki Kebijakan Privasi mereka sendiri untuk kebijakan mereka tentang data pengguna. Untuk akses yang lebih mudah, kami membuat hyperlink ke Kebijakan Privasi mereka di bawah ini. Google https://policies.google.com/technologies/ads</p>\n" +
                        "    <h4>Kebijakan Privasi</h4>\n" +
                        "    <p>Anda dapat berkonsultasi dengan daftar ini untuk menemukan Kebijakan Privasi untuk masing-masing mitra periklanan zonamahasiswa.id. Kebijakan Privasi kami dibuat dengan bantuan Generator Kebijakan Privasi Gratis dan Generator Kebijakan Privasi Online.\n" +
                        "\n" +
                        "\n" +
                        "            Server iklan atau jaringan iklan pihak ketiga menggunakan teknologi seperti cookie, JavaScript, atau Web Beacon yang digunakan dalam iklan masing-masing dan tautan yang muncul di zonamahasiswa.id, yang dikirim langsung ke browser pengguna. Mereka secara otomatis menerima alamat IP Anda saat ini terjadi. Teknologi ini digunakan untuk mengukur keefektifan kampanye iklan mereka dan / atau untuk mempersonalisasi konten iklan yang Anda lihat di situs web yang Anda kunjungi.\n" +
                        "            \n" +
                        "            \n" +
                        "            Perhatikan bahwa zonamahasiswa.id tidak memiliki akses atau kontrol atas cookie ini yang digunakan oleh pengiklan pihak ketiga.</p>\n" +
                        "    <h4>Kebijakan Privasi Pihak Ketiga</h4>\n" +
                        "    <p>Kebijakan Privasi zonamahasiswa.id tidak berlaku untuk pengiklan atau situs web lain. Karenanya, kami menyarankan Anda untuk berkonsultasi dengan masing-masing Kebijakan Privasi dari server iklan pihak ketiga ini untuk informasi yang lebih rinci. Ini mungkin termasuk praktik dan instruksi mereka tentang cara menyisih dari opsi tertentu.\n" +
                        "\n" +
                        "\n" +
                        "            Anda dapat memilih untuk menonaktifkan cookie melalui opsi browser individual Anda. Untuk mengetahui informasi lebih rinci tentang manajemen cookie dengan browser web tertentu, dapat ditemukan di situs web masing-masing browser. Apakah Cookies Itu?</p>\n" +
                        "    <h4>Informasi Anak</h4>\n" +
                        "    <p>Bagian lain dari prioritas kami adalah menambahkan perlindungan untuk anak-anak saat menggunakan internet. Kami mendorong orang tua dan wali untuk mengamati, berpartisipasi, dan / atau memantau dan membimbing aktivitas online mereka.\n" +
                        "\n" +
                        "\n" +
                        "            zonamahasiswa.id tidak dengan sengaja mengumpulkan Informasi Identitas Pribadi apa pun dari anak-anak di bawah usia 13 tahun. Jika menurut Anda anak Anda memberikan informasi semacam ini di situs web kami, kami sangat menganjurkan Anda untuk segera menghubungi kami dan kami akan melakukan upaya terbaik kami untuk segera menghapus informasi tersebut dari catatan kami.</p>\n" +
                        "    <h4>Hanya Kebijakan Privasi Online</h4>\n" +
                        "    <p>Kebijakan Privasi ini hanya berlaku untuk aktivitas online kami dan berlaku untuk pengunjung situs web kami sehubungan dengan informasi yang mereka bagikan dan / atau kumpulkan di zonamahasiswa.id. Kebijakan ini tidak berlaku untuk informasi apa pun yang dikumpulkan secara offline atau melalui saluran selain situs web ini.</p>\n" +
                        "    <h4>Persetujuan</h4>\n" +
                        "    <p>Dengan menggunakan situs web kami, Anda dengan ini menyetujui Kebijakan Privasi kami dan menyetujui Syarat dan Ketentuannya.</p>";
        webPriv.loadData(x, "text/html; charset=utf-8", "UTF-8");

        back = findViewById(R.id.back_kebijakan_privasi);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
    }
}