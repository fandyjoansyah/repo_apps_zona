package com.zonamediagroup.zonamahasiswa.ActivityForhat

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.zonamediagroup.zonamahasiswa.R
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona
import com.zonamediagroup.zonamahasiswa.databinding.ActivityPertanyaanBinding
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClientForhat
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KategoriForhatModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PertanyaanActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityPertanyaanBinding
    private lateinit var dataUser: ArrayList<String>
    private lateinit var dataKategori: ArrayList<String>
    private lateinit var dataIdKategori: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPertanyaanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Mengambil data akun user untuk keperluan spinner
        val anonim = SharedPrefManagerZona.getInstance(this).getnama_anonim()
        val user = SharedPrefManagerZona.getInstance(this).getperson_name()
        // Setelah itu dimasukkan ke dalam array untuk diproses
        dataUser = arrayListOf()
        val textUser = "Tulis sebagai: "
        dataUser.add(textUser + anonim)
        dataUser.add(textUser + user)

        dataKategori = arrayListOf()
        dataIdKategori = arrayListOf()
        getKategori()

        loadImage()

        binding.imgCloseTulispertanyan.setOnClickListener(this)
        binding.btnKirim.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.img_close_tulispertanyan -> {
                finish()
            }
            R.id.btn_kirim -> {
                Toast.makeText(this, "Belum ada data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun getKategori() {
        val retrofit = ApiClientForhat.getClient().create(InterfaceApi::class.java).kategoriForhat
        retrofit.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    val responseBody = JSONObject(response.body().toString())
                    if (responseBody.getInt("status") == 1) {
                        val data = responseBody.getJSONArray("data")

                        val dataToArray = arrayListOf<KategoriForhatModel>()
                        dataToArray.addAll(
                            Gson().fromJson<Collection<KategoriForhatModel>>(
                                data.toString(),
                                object : TypeToken<List<KategoriForhatModel>>() {}.type
                            )
                        )

                        for (i in dataToArray.indices) {
                            dataKategori.add("Pilih Kategori: ${dataToArray.get(i).nama_kategori}")
                            dataIdKategori.add(dataToArray.get(i).id)
                        }

                        dataInSpinner()
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun dataInSpinner() {

        ArrayAdapter(this, R.layout.spinner_item, dataUser).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spinnerUser.adapter = adapter
        }

        ArrayAdapter(this, R.layout.spinner_item, dataKategori).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spinnerKategori.adapter = adapter
        }
    }

    fun loadImage() {
        val dataImage = SharedPrefManagerZona.getInstance(this).geturl_voto()

        Glide.with(this)
            .load(dataImage)
            .apply(RequestOptions.fitCenterTransform())
            .placeholder(R.drawable.ic_no_image_available)
            .into(binding.imgProfileTulispertanyaan)
    }
}
