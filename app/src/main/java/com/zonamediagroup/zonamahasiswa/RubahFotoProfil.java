package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import de.hdodenhof.circleimageview.CircleImageView;

public class RubahFotoProfil extends AppCompatActivity {


    public static final int GALLERY_REQUEST_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 103;
    TextView error_for_edt_rubah_data,txt_simpan;
    RelativeLayout simpan_rubah_data;
    CircleImageView img_foto_profil;
    BottomSheetBehavior sheetBehavior;
    BottomSheetDialog sheetDialog;
    FrameLayout frame_layout_foto_profil;
    View bottom_sheet;
    String fotoProfileSharedPref;
    SpinKitView loading_rubah_profil;
    private RequestQueue rQueue;
    LinearLayout layout_utama;
    Animation fade_in;
    ImageView btn_back;
    Animation fade_in_permanent, fade_out_permanent;
    Resources rs;
    private static final String URL_CHANGE_PROFILE_PICTURE = "https://zonamahasiswa.id/editprofilepicture";
    String namaFotoProfileBaru;
    String idUser;
    Uri imageUriHasilCrop;
    String currentPhotoPath;
    ProgressBar loading_simpan_rubah_data;
    TextView txt_ubah_foto;

    private static final String URL_REQUEST_DEFAULT_FOTO_PROFIL = Server.URL_REAL + "get_default_foto_profil";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_rubah_foto_profil);
        initAnimation();
        findViewByIdAllComponent();
        setListenerAllComponent();
        getAndroidResources();
        stateBtnInactiveWhenThereIsNoChange();
        getCurrentProfilePictureFromSharedPreferences();
        mekanismeSetFotoProfil();
        idUser = getIdUser();
    }

    private void mekanismeSetFotoProfil(){
        if(fotoProfileSharedPref == null || fotoProfileSharedPref.equals("") || fotoProfileSharedPref.equals("null")){
            getDefaultFotoProfilFromServer();
        }
        else{
            setFotoProfil(fotoProfileSharedPref);
        }

    }

    private void getDefaultFotoProfilFromServer(){
        AndroidNetworking.get(URL_REQUEST_DEFAULT_FOTO_PROFIL)
                .addQueryParameter("TOKEN", "qwerty")
                .setTag("get default foto profil")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String url =  response.getString("url_foto_profil");
                            setFotoProfil(url);
                        } catch (JSONException e) {

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }


    private String getIdUser(){
        return SharedPrefManagerZona.getInstance(this).getid_user();
    }

    private void getAndroidResources(){
        rs = getResources();
    }

    private void getCurrentProfilePictureFromSharedPreferences(){
        fotoProfileSharedPref = SharedPrefManagerZona.getInstance(RubahFotoProfil.this).geturl_voto();
    }
    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(RubahFotoProfil.this,R.anim.fade_in_fast);
        fade_in_permanent = AnimationUtils.loadAnimation(RubahFotoProfil.this,R.anim.fade_in_permanent);
        fade_out_permanent = AnimationUtils.loadAnimation(RubahFotoProfil.this,R.anim.fade_out_permanent_duration_100);
    }

    private void findViewByIdAllComponent() {
        img_foto_profil = findViewById(R.id.img_foto_profil);
        simpan_rubah_data = findViewById(R.id.simpan_rubah_data);
        txt_simpan = findViewById(R.id.txt_simpan);
        frame_layout_foto_profil = findViewById(R.id.frame_layout_foto_profil);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        layout_utama = findViewById(R.id.layout_utama);
        loading_rubah_profil = findViewById(R.id.loading_rubah_profil);
        btn_back = findViewById(R.id.btn_back);
        loading_simpan_rubah_data = findViewById(R.id.loading_simpan_rubah_data);
        txt_ubah_foto = findViewById(R.id.txt_ubah_foto);
    }

    private void setListenerAllComponent(){
        frame_layout_foto_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheetDialog();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        simpan_rubah_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadNewProfilePictureToServer(namaFotoProfileBaru,imageUriHasilCrop);
            }
        });
    }

    private void stateBtnInactiveWhenThereIsNoChange(){
        disableBtnSimpan();
        changeBtnColorToColorInactive();
        changeTxtBtnColorToColorInactive();
    }

    private void stateBtnActiveWhenThereIsChange(){
        enableBtnSimpan();
        changeBtnColorToColorActive();
        changeTxtBtnColorToColorActive();
    }

    private void disableBtnSimpan(){
        simpan_rubah_data.setEnabled(false);
    }

    private void enableBtnSimpan(){
        simpan_rubah_data.setEnabled(true);
    }

    private void changeBtnColorToColorInactive(){
        simpan_rubah_data.setBackgroundTintList(ContextCompat.getColorStateList(RubahFotoProfil.this,R.color.color_btn_inactive));
    }
    private void changeBtnColorToColorActive(){
        simpan_rubah_data.setBackgroundTintList(ContextCompat.getColorStateList(RubahFotoProfil.this,R.color.color_btn_ganti_foto_profil));
    }

    private void changeTxtBtnColorToColorInactive(){
        txt_simpan.setTextColor(ContextCompat.getColorStateList(RubahFotoProfil.this,R.color.color_text_btn_inactive));
    }

    private void changeTxtBtnColorToColorActive(){
        txt_simpan.setTextColor(ContextCompat.getColorStateList(RubahFotoProfil.this,R.color.white));
    }

    private void stateLoadingUploadPictureActive(){
        btn_back.setEnabled(false);
        frame_layout_foto_profil.setEnabled(false);
        simpan_rubah_data.setEnabled(false);
        btn_back.startAnimation(fade_out_permanent);
        frame_layout_foto_profil.startAnimation(fade_out_permanent);
        txt_ubah_foto.startAnimation(fade_out_permanent);
        showLoadingSimpan();
        hideTxtBtnSimpan();
    }

    private void stateLoadingUploadPictureInactive(){
        btn_back.setEnabled(true);
        frame_layout_foto_profil.setEnabled(true);
        simpan_rubah_data.setEnabled(true);
        btn_back.startAnimation(fade_in_permanent);
        frame_layout_foto_profil.startAnimation(fade_in_permanent);
        txt_ubah_foto.startAnimation(fade_in_permanent);
        hideLoadingSimpan();
        showTxtBtnSimpan();
    }

    /*bottom sheet rubah foto profil*/
    private void showBottomSheetDialog() {
        View view = getLayoutInflater().inflate(R.layout.bottomsheet_rubah_foto_profil, null);

        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }


        sheetDialog = new BottomSheetDialog(this);
        sheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            sheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }


        /*findViewById komponen  bottomsheet*/
        TextView btn_batal =  view.findViewById(R.id.btn_batal);
        TextView btn_lihat_foto = view.findViewById(R.id.btn_lihat_foto);
        TextView btn_pilih_dari_galeri = view.findViewById(R.id.btn_pilih_dari_galeri);
        TextView btn_ambil_foto = view.findViewById(R.id.btn_ambil_foto);




        /*setListener komponen bottomsheet*/
        btn_batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetDialog.dismiss();
            }
        });
        btn_lihat_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* Buka activity lihatFotoProfil */
                startActivity(new Intent(RubahFotoProfil.this,LihatFotoProfil.class));
                sheetDialog.dismiss();
            }
        });
        btn_pilih_dari_galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismePilihGambarDariGaleriUntukFotoProfil();
                sheetDialog.dismiss();
            }
        });
        btn_ambil_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismeFotoKameraUntukFotoProfil();
                sheetDialog.dismiss();
            }
        });



        sheetDialog.show();
        sheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                sheetDialog = null;
            }
        });
    }

    private void launchIntentPickImage(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,GALLERY_REQUEST_CODE);
    }

    private void launchIntentTakePictureFromCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.zonamediagroup.zonamahasiswa.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        /*start jika ingin menyimpan file secara privat dan tidak tampil di gallery*/
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        /*end jika ingin menyimpan file secara privat dan tidak tampil di gallery*/

        /*start jika ingin menyimpan file secara public dan tampil di gallery*/
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        /*end jika ingin menyimpan file secara public dan tampil di gallery*/

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    //check permission
    public boolean checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(RubahFotoProfil.this, permission) == PackageManager.PERMISSION_DENIED)
            return false;
        else
            return true;
    }

    private void mekanismePilihGambarDariGaleriUntukFotoProfil(){
        boolean isPermissionGalleryGranted = checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE,GALLERY_REQUEST_CODE);
        if(isPermissionGalleryGranted)
        {
            launchIntentPickImage();
        }

        else
        {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,GALLERY_REQUEST_CODE);
        }
    }

    private void askCameraPermission() {
        if(ContextCompat.checkSelfPermission(RubahFotoProfil.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(RubahFotoProfil.this,new String[]{Manifest.permission.CAMERA},CAMERA_REQUEST_CODE);
        }
        else{
            launchIntentTakePictureFromCamera();
            //okee siap
        }
    }

    private void askWriteStoragePermission(){
        if(ContextCompat.checkSelfPermission(RubahFotoProfil.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(RubahFotoProfil.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }
        else{


            askCameraPermission();
            //okee siap
        }
    }

    private void mekanismeFotoKameraUntukFotoProfil(){
        askCameraPermission();
    }

    public void requestPermission(String permission, int requestPermissionCode){
//        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        requestPermissions(new String[]{
                permission}, requestPermissionCode);
    }





    private void setFotoProfil(String url)
    {
        showLoadingHalaman();
        hideLayoutUtama();
        Glide.with(RubahFotoProfil.this)
                .load(url)
                .apply(RequestOptions.fitCenterTransform())
                .placeholder(R.drawable.ic_no_image_available)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        hideLoadingHalaman();
                        showLayoutUtama();
                        CustomToast.makeBlackCustomToast(RubahFotoProfil.this,getResources().getString(R.string.terjadi_kesalahan_saat_mencoba_mendapatkan_foto_profil), Toast.LENGTH_SHORT);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        hideLoadingHalaman();
                        showLayoutUtama();
                        return false;
                    }
                })
                .into(img_foto_profil);
    }

    private void hideLoadingHalaman(){
        loading_rubah_profil.setVisibility(View.GONE);
    }

    private void showLoadingHalaman(){
        loading_rubah_profil.setVisibility(View.VISIBLE);
    }

    private void showLayoutUtama(){
        layout_utama.startAnimation(fade_in);
        layout_utama.setVisibility(View.VISIBLE);
    }

    private void hideLayoutUtama(){
        layout_utama.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                launchIntentPickImage();
            } else {
                CustomToast.makeBlackCustomToast(RubahFotoProfil.this, rs.getString(R.string.aktifkan_izin_penyimpananmu_terlebih_dahulu), Toast.LENGTH_LONG);
            }
        }
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                launchIntentTakePictureFromCamera();
            } else {
                CustomToast.makeBlackCustomToast(RubahFotoProfil.this, rs.getString(R.string.aktifkan_izin_kameramu_terlebih_dahulu), Toast.LENGTH_LONG);
            }
        }
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                askCameraPermission();
            } else {
                CustomToast.makeBlackCustomToast(RubahFotoProfil.this, rs.getString(R.string.aktifkan_izin_penyimpananmu_terlebih_dahulu), Toast.LENGTH_LONG);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contentUri = data.getData();
                doCrop(contentUri);
            }
        }
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(currentPhotoPath);
                doCrop(Uri.fromFile(f));
            }
        }
        if(resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP)
        {
            final Uri resultUri = UCrop.getOutput(data);
            img_foto_profil.setImageURI(resultUri);
            stateBtnActiveWhenThereIsChange();
            imageUriHasilCrop = resultUri;
        }
        else if(resultCode == UCrop.RESULT_ERROR){
            final Throwable cropError = UCrop.getError(data);
        }
    }

    private void sendNewProfilePictureImageToServer(Uri uri){
        File fileYangDikirim = new File(uri.getPath());
        //start code android networking
        AndroidNetworking.upload(URL_CHANGE_PROFILE_PICTURE)
                .addMultipartFile("image",fileYangDikirim)
                .addMultipartParameter("id","321")
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }
                    @Override
                    public void onError(ANError error) {
                    }
                });
        //end code android networking
    }

    private void doCrop(Uri sourceUri){
        UCrop.Options opt = new UCrop.Options();
        opt.setCircleDimmedLayer(true);

        try {
            String timeStamp = new SimpleDateFormat("yyyyMMMM_HHmmss").format(new Date());
            String imageFileName = "mobile_profile_picture_";
            namaFotoProfileBaru = imageFileName+".jpg";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File destinationFile = File.createTempFile(imageFileName, ".jpg", storageDir);
            Uri destinationUri = Uri.fromFile(destinationFile);
            UCrop.of(sourceUri,destinationUri)
                    .withAspectRatio(1,1)
                    .withOptions(opt)
                    .withMaxResultSize(1000,1000)
                    .start(RubahFotoProfil.this);
        }
        catch (Exception e)
        {
        }


    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private void uploadNewProfilePictureToServer(final String pdfname, Uri pdffile) {
        stateLoadingUploadPictureActive();
        InputStream iStream = null;
        try {
            iStream = getContentResolver().openInputStream(pdffile);
            final byte[] inputData = getBytes(iStream);

            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL_CHANGE_PROFILE_PICTURE,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            stateLoadingUploadPictureInactive();
                            rQueue.getCache().clear();
                            try{
                                JSONObject jsonObject = new JSONObject(new String(response.data));
                                if(jsonObject.getBoolean("status") == true)
                                {
                                    CustomToast.makeBlackCustomToast(RubahFotoProfil.this,rs.getString(R.string.berhasil_memperbarui_foto_profil),Toast.LENGTH_LONG);
                                    rubahValueSharedPreferences(jsonObject.getString("newProfilePictureUrl"));
                                    changeBtnColorToColorBtnBerhasil();
                                    disableBtnSimpan();
                                    setTextLabelBtnSimpan(rs.getString(R.string.disimpan));
                                    new Delay().delayTask(300, new Runnable() {
                                        @Override
                                        public void run() {
                                            onBackPressed();
                                        }
                                    });
                                }
                                else
                                {
                                    CustomToast.makeBlackCustomToast(RubahFotoProfil.this,rs.getString(R.string.gagal_memperbarui_foto_profil),Toast.LENGTH_LONG);
                                }
                            }
                            catch (JSONException e){
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            stateLoadingUploadPictureInactive();
                            CustomToast.makeBlackCustomToast(RubahFotoProfil.this,rs.getString(R.string.permintaan_gagal_dieksekusi),Toast.LENGTH_LONG);
                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 * */
//                Parameter
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String, String> params = new HashMap<>();
                    params.put("id", idUser);
                    // params.put("tags", "ccccc");  add string parameters
                    return params;
                }

                /*
                 *pass files using below method
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("new_profile_picture", new DataPart(pdfname, inputData));

                    return params;
                }
            };


            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rQueue = Volley.newRequestQueue(RubahFotoProfil.this);
            rQueue.add(volleyMultipartRequest);


        } catch (FileNotFoundException e) {
            CustomToast.makeBlackCustomToast(RubahFotoProfil.this,rs.getString(R.string.general_failure),Toast.LENGTH_LONG);
            stateLoadingUploadPictureInactive();
            e.printStackTrace();
        } catch (IOException e) {
            CustomToast.makeBlackCustomToast(RubahFotoProfil.this,rs.getString(R.string.general_failure),Toast.LENGTH_LONG);
            stateLoadingUploadPictureInactive();
            e.printStackTrace();
        }
    }

    private void rubahValueSharedPreferences(String newValue){
        SharedPrefManagerZona.getInstance(RubahFotoProfil.this).updateSingleSharedPreferencesColumn(SharedPrefManagerZona.TAG_URL_VOTO,newValue);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_transition_slide_from_left,R.anim.activity_transition_slide_to_right);
    }

    private void changeBtnColorToColorBtnBerhasil(){
        simpan_rubah_data.startAnimation(fade_in);
        simpan_rubah_data.setBackgroundTintList(ContextCompat.getColorStateList(RubahFotoProfil.this,R.color.color_btn_berhasil));
    }
    private void setTextLabelBtnSimpan(String text){
        txt_simpan.setText(text);
    }

    private void hideTxtBtnSimpan(){
        txt_simpan.setVisibility(View.GONE);
    }

    private void showTxtBtnSimpan(){
        txt_simpan.setVisibility(View.VISIBLE);
    }

    private void showLoadingSimpan(){
        loading_simpan_rubah_data.startAnimation(fade_in);
        loading_simpan_rubah_data.setVisibility(View.VISIBLE);
    }

    private void hideLoadingSimpan(){
        loading_simpan_rubah_data.setVisibility(View.GONE);
    }
}