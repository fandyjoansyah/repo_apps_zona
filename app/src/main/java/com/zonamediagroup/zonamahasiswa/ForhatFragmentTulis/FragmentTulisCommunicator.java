package com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis;

import android.net.Uri;

import java.util.ArrayList;

public interface FragmentTulisCommunicator {
    public void communicatorGoToNextStep(int stepTujuan);
    public void communicatorGoBackToPreviousStep(int stepTujuan);
    public void communicatorSetLastStepDone(int lastStepDone);
    public void finishAllStep();
    public void saveStep1Data(String judul, String deskripsi, int idRadioCoverChecked, Uri uploadCoverUri, String namaFilePilihanUser);
    public void saveStep2Data(ArrayList<Uri> dataUri);
}

