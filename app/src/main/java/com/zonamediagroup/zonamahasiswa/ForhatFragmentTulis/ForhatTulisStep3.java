package com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.ForhatTulisActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.adapters.KategoriForhatAdapter;
import com.zonamediagroup.zonamahasiswa.adapters.PilihKategoriForhatInTulisAdapter;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClientForhat;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KategoriForhatModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForhatTulisStep3 extends Fragment implements PilihKategoriForhatInTulisAdapter.PilihKategoriForhatInTulisAdapterListener {
    Animation fade_in;
    ArrayList<KategoriForhatModel> listKategoriForhat;
    FragmentTulisCommunicator communicator;
    PilihKategoriForhatInTulisAdapter kategoriForhatAdapter;
    NestedScrollView scrollview_tulis_step3;
    RecyclerView recycler_pilih_kategori_tulisan;
    Resources rs;
    TextView btn_lanjutkan_step3,btn_kembali_step3,error_for_recycler_pilih_kategori_tulisan;
    KategoriForhatModel kategoriForhatSelected;

    ArrayList<KategoriForhatModel> listKategoriForhatSelected;

    private static final int MAXIMUM_CATEGORY_ALLOWED = 5;


    public ForhatTulisStep3() {

    }

    private void initResources(){
        rs = getResources();
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.include_forhat_tulis_step3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAnimation();
        initResources();
        findViewByIdAllComponents(view);
        setListenerAllComponents();
        initRecyclerKategoriForhat();
        getKategoriForhat();
    }

    private void findViewByIdAllComponents(View v){
        btn_kembali_step3 = v.findViewById(R.id.btn_kembali_step3);
        btn_lanjutkan_step3 = v.findViewById(R.id.btn_lanjutkan_step3);
        recycler_pilih_kategori_tulisan = v.findViewById(R.id.recycler_pilih_kategori_tulisan);
        error_for_recycler_pilih_kategori_tulisan = v.findViewById(R.id.error_for_recycler_pilih_kategori_tulisan);
        scrollview_tulis_step3 = v.findViewById(R.id.scrollview_tulis_step3);
    }

    private void setListenerAllComponents(){
        btn_kembali_step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                communicator.communicatorGoBackToPreviousStep(2);
            }
        });
        btn_lanjutkan_step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeLanjutkanStep(4);
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        communicator = (FragmentTulisCommunicator)(ForhatTulisActivity)context;
    }
    private void initRecyclerKategoriForhat(){
        listKategoriForhat = new ArrayList<>();
        kategoriForhatAdapter = new PilihKategoriForhatInTulisAdapter(getContext(), listKategoriForhat,this);
        RecyclerView.LayoutManager mLayoutManagerkategoriForhat = new GridLayoutManager(getActivity(), 2);
        recycler_pilih_kategori_tulisan.setLayoutManager(mLayoutManagerkategoriForhat);
        recycler_pilih_kategori_tulisan.setItemAnimator(new DefaultItemAnimator());
        recycler_pilih_kategori_tulisan.setAdapter(kategoriForhatAdapter);


        /*start optimization*/
        recycler_pilih_kategori_tulisan.setHasFixedSize(true);
        recycler_pilih_kategori_tulisan.setItemViewCacheSize(20);
        recycler_pilih_kategori_tulisan.setDrawingCacheEnabled(true);
        recycler_pilih_kategori_tulisan.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        /*end optimization*/
    }

    private void mekanismeLanjutkanStep(int stepTujuan)
    {
        hideAllTvError();
        if(mekanismeValidasiInput()) {
            communicator.communicatorGoToNextStep(stepTujuan);
        }
        else
            scrollToFirstVisibleTvError();
    }

    private boolean mekanismeValidasiInput(){
        /*hasilValidasi true = boleh lanjut. false = tidak boleh lanjut (ada validasi gagal)*/
        boolean hasilValidasi = true;
        if(!checkValidasiAdaKategoriTerpilih())
        {
            showErrorTextView(error_for_recycler_pilih_kategori_tulisan,rs.getString(R.string.kategori_harus_diisi));
            hasilValidasi = false;
        }

        return hasilValidasi;
    }

    private void hideAllTvError(){
        error_for_recycler_pilih_kategori_tulisan.setVisibility(View.INVISIBLE);
    }

    private boolean checkValidasiAdaKategoriTerpilih(){
        if(kategoriForhatSelected == null)
            return false;
        else
            return true;
    }

    private void showErrorTextView(TextView tv,String pesanError){
        tv.startAnimation(fade_in);
        tv.setVisibility(View.VISIBLE);
        tv.setText(pesanError);
    }

    private boolean checkTotalCategorySelected(){
        int totalActiveCategory = listKategoriForhatSelected.size();
        if(totalActiveCategory>0 && totalActiveCategory<=MAXIMUM_CATEGORY_ALLOWED)
            return true;
            else
                return false;
    }

    private void scrollToFirstVisibleTvError(){
        if(error_for_recycler_pilih_kategori_tulisan.getVisibility() == View.VISIBLE)
        {
            scrollview_tulis_step3.smoothScrollTo(0,recycler_pilih_kategori_tulisan.getTop());
        }

    }

    private void getKategoriForhat(){
        Log.d("1_agustus_2022","getKategoriForhat()");
        InterfaceApi api = ApiClientForhat.getClient().create(InterfaceApi.class);
        Call<JsonObject> call = api.getKategoriForhat();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> responseRetrofit) {
                Log.d("1_agustus_2022","getKategoriForhat() onResponse");
                try{
                    JSONObject response =new JSONObject(responseRetrofit.body().toString());


                    if (response.getInt("status") == 1)
                    {
                        //cek apakah data adalah jsonArray
                        if(response.get("data") instanceof JSONArray) {
                            Log.d("22_juli_2022","getKategori() onResponse try if data instance of Array = true");
                            JSONArray arrayData = response.getJSONArray("data");
                            Log.d("1_agustus_2022","getKategoriForhat() onResponse try if if");
                            setDataKonten(arrayData);
                        }

                    }
                    //dapatkan data konten(array)
                } catch (JSONException e) {
                    Log.d("1_agustus_2022","getKategoriForhat() onResponse catch");
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
               if(t.getMessage().equals("SSL handshake timed out"))
               {
                   /*jika error karena ssl handshake, maka paksa untuk getKategoriForhat lagi */
                   getKategoriForhat();
               }
            }
        });
    }

    private void setDataKonten(JSONArray arrayData){
        listKategoriForhat.addAll(new Gson().fromJson(arrayData.toString(), new TypeToken<List<KategoriForhatModel>>() {
        }.getType()));
        kategoriForhatAdapter.notifyDataSetChanged();
        Log.d("1_agustus_2022","setDataKonten()");
        Log.d("1_agustus_2022","setDataKonten() listKategoriForhat nama kategori pertama: "+listKategoriForhat.get(0).getNama_kategori());
    }


    @Override
    public void toggleKategoriForhatSelected(KategoriForhatModel kategoriForhatSelected) {
        this.kategoriForhatSelected = kategoriForhatSelected;
    }
}