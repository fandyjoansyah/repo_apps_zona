package com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.ForhatTulisActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.adapters.PilihKategoriForhatInTulisAdapter;
import com.zonamediagroup.zonamahasiswa.adapters.PreviewBuktiCurhatanAdapter;

import java.io.File;
import java.util.ArrayList;


public class ForhatTulisStep2 extends Fragment implements PreviewBuktiCurhatanAdapter.PreviewBuktiCurhatanListener {

    FragmentTulisCommunicator communicator;
    PreviewBuktiCurhatanAdapter previewBuktiCurhatanAdapter;
    RecyclerView recycler_preview_bukti_curhatan;
    RelativeLayout area_upload_bukti;
    Resources rs;
    TextView btn_lanjutkan_step2,btn_kembali_step2;
    ArrayList<Uri> dataUri;

    final int GALLERY_REQUEST_CODE = 100;
    final int DETIK_DURASI_MAKSIMAL_VIDEO = 60;
    final int MB_UKURAN_MAKSIMAL_VIDEO = 70;

    final String[] arrEkstensiDiperbolehkan = {"mp4","jpg","jpeg","png"};


    public ForhatTulisStep2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("10X2_agustus_2022","step 2 onCreateView()");
        return inflater.inflate(R.layout.include_forhat_tulis_step2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewByIdAllComponents(view);
        setListenerAllComponents();
        initRecyclerPreviewBuktiCurhatan();
        initRs();
        Log.d("10X2_agustus_2022","step 2 onViewCreated()");
    }

    private void initRs(){
        rs = getResources();
    }


    private void findViewByIdAllComponents(View v){
        btn_kembali_step2 = v.findViewById(R.id.btn_kembali_step2);
        btn_lanjutkan_step2 = v.findViewById(R.id.btn_lanjutkan_step2);
        area_upload_bukti = v.findViewById(R.id.area_upload_bukti);
        recycler_preview_bukti_curhatan = v.findViewById(R.id.recycler_preview_bukti_curhatan);
    }

    private void initRecyclerPreviewBuktiCurhatan(){
        dataUri = new ArrayList<>();
        previewBuktiCurhatanAdapter = new PreviewBuktiCurhatanAdapter(dataUri, getContext(),this);
        RecyclerView.LayoutManager mLayoutManagerkategoriForhat = new GridLayoutManager(getActivity(), 2);
        recycler_preview_bukti_curhatan.setLayoutManager(mLayoutManagerkategoriForhat);
        recycler_preview_bukti_curhatan.setItemAnimator(new DefaultItemAnimator());
        recycler_preview_bukti_curhatan.setAdapter(previewBuktiCurhatanAdapter);

        /*start optimization*/
        recycler_preview_bukti_curhatan.setHasFixedSize(true);
        recycler_preview_bukti_curhatan.setItemViewCacheSize(20);
        recycler_preview_bukti_curhatan.setDrawingCacheEnabled(true);
        recycler_preview_bukti_curhatan.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        /*end optimization*/
    }



    private void setListenerAllComponents(){
        btn_kembali_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                communicator.communicatorGoBackToPreviousStep(1);
            }
        });
        btn_lanjutkan_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                communicator.communicatorGoToNextStep(3);
            }
        });
        area_upload_bukti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismeUploadBukti();
            }
        });
    }

    private void mekanismeUploadBukti(){
        Log.d("8_agustus_2022","mekanismeUploadBukti()");
        boolean isPermissionGalleryGranted = checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE,GALLERY_REQUEST_CODE);
        if(isPermissionGalleryGranted)
        {
            launchIntentPickImageOrVideo();
        }

        else
        {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,GALLERY_REQUEST_CODE);
        }
    }

    //check permission
    public boolean checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_DENIED)
            return false;
        else
            return true;
    }

    public void requestPermission(String permission, int requestPermissionCode){
//        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        requestPermissions(new String[]{
                permission}, requestPermissionCode);
    }

    private void launchIntentPickImageOrVideo(){
        Log.d("8_agustus_2022","launchIntentPickImageOrVideo()");
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/* video/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*","video/*"});
        startActivityForResult(intent,GALLERY_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchIntentPickImageOrVideo();
            } else {
                CustomToast.makeBlackCustomToast(getContext(),rs.getString(R.string.aktifkan_izin_penyimpananmu_terlebih_dahulu), Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("8_agustus_2022","onActivityResult()");
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("8_agustus_2022","onActivityResult() if if");
                Uri uriUploadedBukti = data.getData();
                mekanismeValidasiBukti(uriUploadedBukti);
            }
        }
    }

    private void mekanismeValidasiBukti(Uri uriUploadedBukti){
        Log.d("8_agustus_2022","mekanismeValidasiBukti()");
        String ekstensiBukti = getEkstensi(uriUploadedBukti);
        if(!validasiEkstensiFile(ekstensiBukti,arrEkstensiDiperbolehkan))
        {
            Log.d("8_agustus_2022","mekanismeValidasiBukti() if");
            showErrorBukti(rs.getString(R.string.file_yang_diupload_harus_berupa_gambar_atau_video));
        }

        else
        {
            /*
        jika merupakan video, maka ada validasi tambahan yaitu validasi ukuran dan durasi
        */
            Log.d("8_agustus_2022","mekanismeValidasiBukti() else");
            if(ekstensiBukti.equals("mp4")) {
                if (!validasiDurasiVideo(uriUploadedBukti, DETIK_DURASI_MAKSIMAL_VIDEO)) {
                    Log.d("8_agustus_2022", "mekanismeValidasiBukti() else. if if");
                    showErrorBukti(rs.getString(R.string.durasi_videomu_melebihi_batas));
                } else if (!validasiUkuranVideo(uriUploadedBukti, MB_UKURAN_MAKSIMAL_VIDEO)) {
                    Log.d("8_agustus_2022", "mekanismeValidasiBukti() else. if else if");
                    showErrorBukti(rs.getString(R.string.ukuran_videomu_melebihi_batas));
                }
                else
                {
                    Log.d("8_agustus_2022", "mekanismeValidasiBukti() else. if else");
                    mekanismeTambahUriBuktiYangDiterima(uriUploadedBukti);
                }
            }
            else
            {
                Log.d("8_agustus_2022","mekanismeValidasiBukti() else. else");
                mekanismeTambahUriBuktiYangDiterima(uriUploadedBukti);
            }
        }

    }

    private void mekanismeTambahUriBuktiYangDiterima(Uri newUri){
        Log.d("8_agustus_2022","mekanismeTambahUriBuktiYangDiterima()");
        dataUri.add(newUri);
        previewBuktiCurhatanAdapter.notifyDataSetChanged();
    }


    private boolean validasiDurasiVideo(Uri uriVideo, int secondDurasiMaksimal){
        boolean hasilValidasi = false;

        MediaPlayer mp = MediaPlayer.create(getContext(), uriVideo);
        int durationVideo = mp.getDuration();
        mp.release();

        if((durationVideo/1000) <= secondDurasiMaksimal+1) //dikasih +1 untuk toleransi waktu agar kalo semisal 5,2 detik itu masih diterima (dianggap 5 detik)
        {
            return true;
        }

        return hasilValidasi;
    }

    private boolean validasiUkuranVideo(Uri uriVideo, int mbUkuranMaksimal){
        String[] mediaColumns = {MediaStore.Video.Media.SIZE};
        Cursor cursor = getContext().getContentResolver().query(uriVideo, mediaColumns, null, null, null);
        cursor.moveToFirst();
        int sizeColInd = cursor.getColumnIndex(mediaColumns[0]);
        long fileSizeInByte = cursor.getLong(sizeColInd);
        long fileSizeInMB = fileSizeInByte/1000000;
        cursor.close();
        if(fileSizeInMB <= mbUkuranMaksimal)
        {
            return true;
        }
        else
        {
            return false;
        }

    }


    private void showErrorBukti(String pesanError)
    {

    }

    private String getEkstensi(Uri uri){
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(getContext().getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }
        return extension;
    }

    private boolean validasiEkstensiFile(String ekstensiFileYangDicek, String[] arrayEkstensiFileYangDiperbolehkan){
        boolean hasilValidasi = false;
        for(int i=0;i<arrayEkstensiFileYangDiperbolehkan.length;i++)
        {
            if(ekstensiFileYangDicek.equals(arrayEkstensiFileYangDiperbolehkan[i]))
            {
                hasilValidasi = true;
            }
        }
        return hasilValidasi;
    }




    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        communicator = (FragmentTulisCommunicator)(ForhatTulisActivity)context;
    }

    @Override
    public String getEkstensiFromActivity(Uri uri) {
        return getEkstensi(uri);
    }

    private void mekanismeRetreiveDataFromActivity(){
        ForhatTulisActivity currentActivity = (ForhatTulisActivity) getActivity();
        dataUri = currentActivity.retreiveDataUri();
        previewBuktiCurhatanAdapter.notifyDataSetChanged();
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d("10X2_agustus_2022","step 2 onPause()");
        communicator.saveStep2Data(dataUri);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("10X2_agustus_2022","step 2 onResume()");
        mekanismeRetreiveDataFromActivity();
    }
}