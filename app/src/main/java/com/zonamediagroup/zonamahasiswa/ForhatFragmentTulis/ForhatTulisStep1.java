package com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.ForhatTulisActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.RubahFotoProfil;
import com.zonamediagroup.zonamahasiswa.models.forhat_tulis_models.ForhatTulisStep1Model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.wasabeef.richeditor.RichEditor;


public class ForhatTulisStep1 extends Fragment {

    Animation fade_in;
    EditText edt_judul;
    FragmentTulisCommunicator communicator;
    ImageView img_cover_preview,delete_cover;
    LinearLayout ly_pilih_file_cover_forhat;
    NestedScrollView scrollview_tulis_step1;
    RadioButton radio_cover_pilihan_admin,radio_unggah_cover;
    RadioGroup radio_group_cover;
    RelativeLayout icon_bold,icon_italic,icon_underline,icon_numbering,icon_unordered,icon_outdent,icon_indent,ly_preview_uploaded_cover;
    Resources res;
    RichEditor rich_editor;
    String namaFotoCoverPilihanUser;
    String richTextEditorValue = "";
    TextView btn_lanjutkan_step1, tv_keterangan_file,error_for_edt_judul,error_for_rich_editor,error_for_radio_group_cover;


    Uri imageUriCoverPilihanUser;

    public static final int GALLERY_REQUEST_CODE = 101;



    public ForhatTulisStep1() {
        // Required empty public constructor
    }





    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
    }
    
    private void initResources(){
        res = getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.include_forhat_tulis_step1, container, false);
    }

    private void removeRichTextAreaFocus(){
        rich_editor.clearFocus();
    }


    //param pertama view dari method onViewCreated, adalah hasil return dari method onCreateView
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("9_agustus_2022","onViewCreatedStep1()");
        initAnimation();
        findViewByIdAllComponents(view);
        initResources();
        setListenerAllComponents();
        setPlaceHolderRichText("Tulis sesuatu...");

    }

    private void setListenerAllComponents() {
        icon_bold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               rich_editor.setBold();
            }
        });
        icon_italic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rich_editor.setItalic();
            }
        });
        icon_underline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rich_editor.setUnderline();
            }
        });
        icon_numbering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rich_editor.setNumbers();
            }
        });
        icon_unordered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rich_editor.setBullets();
            }
        });
        icon_outdent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rich_editor.setOutdent();
            }
        });
        icon_indent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rich_editor.setIndent();
            }
        });
        btn_lanjutkan_step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeLanjutkanStep(2);
            }
        });
        radio_group_cover.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mekanismeSelectingRadioButtonCover();
            }
        });
        ly_pilih_file_cover_forhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismeUploadCoverPiihanUser();
            }
        });
        delete_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismeHapusFotoCoverPengguna();
            }
        });
    }



    private void mekanismeLanjutkanStep(int stepTujuan)
    {
        hideAllTvError();
        if(mekanismeValidasiInput()) {
            communicator.communicatorGoToNextStep(stepTujuan);
        }
        else
            scrollToFirstVisibleTvError();
    }


    private void mekanismeSelectingRadioButtonCover(){
        removeRichTextAreaFocus();
        int idRadioButtonSelected = GetIdOfCheckedRadioButton();
        switch (idRadioButtonSelected)
        {
            case R.id.radio_cover_pilihan_admin:
                mekanismeHapusFotoCoverPengguna();
                hide_ly_pilih_file_cover_forhat();
                break;
            case  R.id.radio_unggah_cover:
                    show_ly_pilih_cover_forhat();
                    break;
        }
    }

    public int GetIdOfCheckedRadioButton(){
        int idRadioButtonSelected = radio_group_cover.getCheckedRadioButtonId();
        if(idRadioButtonSelected == radio_cover_pilihan_admin.getId())
        {
            return radio_cover_pilihan_admin.getId();
        }
        else
        {
            return radio_unggah_cover.getId();
        }
    }

    private void show_ly_pilih_cover_forhat(){
        ly_pilih_file_cover_forhat.setVisibility(View.VISIBLE);
    }

    private void hide_ly_pilih_cover_forhat(){
        ly_pilih_file_cover_forhat.setVisibility(View.GONE);
    }

    //check permission
    private boolean checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_DENIED)
            return false;
        else
            return true;
    }

    private void show_ly_preview_uploaded_cover(){
        ly_preview_uploaded_cover.setVisibility(View.VISIBLE);
    }

    private void hide_ly_preview_uploaded_cover(){
        ly_preview_uploaded_cover.setVisibility(View.GONE);
    }

    private void mekanismeHapusFotoCoverPengguna(){
        nullifiedImageUriCoverPilihanUser();
        hide_ly_preview_uploaded_cover();
        set_text_tv_keterangan_file("");
    }

    private void nullifiedImageUriCoverPilihanUser() {
        imageUriCoverPilihanUser = null;
        img_cover_preview.setImageURI(null);
    }

    //start method managemen uri baru
    private void mekanismeSetImageUriCoverPilihanUser(Uri uri){
        imageUriCoverPilihanUser = uri;
        img_cover_preview.setImageURI(imageUriCoverPilihanUser);
    }
    //end method managemen uri baru


    private void mekanismeUploadCoverPiihanUser(){
        boolean isPermissionGalleryGranted = checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE,GALLERY_REQUEST_CODE);
        if(isPermissionGalleryGranted)
        {
            launchIntentPickImage();
        }

        else
        {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,GALLERY_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchIntentPickImage();
            } else {
                CustomToast.makeBlackCustomToast(getContext(), res.getString(R.string.aktifkan_izin_penyimpananmu_terlebih_dahulu), Toast.LENGTH_LONG);
            }
        }
    }

    private void requestPermission(String permission, int requestPermissionCode){
//        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        requestPermissions(new String[]{
                permission}, requestPermissionCode);
    }



    private void launchIntentPickImage(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,GALLERY_REQUEST_CODE);
    }

    private String getFileName(Uri uri)
    {
       return DocumentFile.fromSingleUri(getContext(), uri).getName();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contentUri = data.getData();
                set_text_tv_keterangan_file(protoGetName(contentUri));
                communicator.saveStep1Data(edt_judul.getText().toString(),rich_editor.getHtml(),GetIdOfCheckedRadioButton(),imageUriCoverPilihanUser, tv_keterangan_file.getText().toString());
                doCrop(contentUri);
            }
        }

        if(resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP)
        {
            hideAllTvError();
            mekanismeSetImageUriCoverPilihanUser(UCrop.getOutput(data));
            show_ly_preview_uploaded_cover();
            communicator.saveStep1Data(edt_judul.getText().toString(),rich_editor.getHtml(),GetIdOfCheckedRadioButton(),imageUriCoverPilihanUser, tv_keterangan_file.getText().toString());
        }


    }

    private String protoGetName(Uri uri)
    {

        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    private void doCrop(Uri sourceUri){

        try {
            String timeStamp = new SimpleDateFormat("yyyyMMMM_HHmmss").format(new Date());
            String imageFileName = "mobile_cover_forhat_";
            namaFotoCoverPilihanUser = imageFileName+"_"+timeStamp+".jpg";
            File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File destinationFile = File.createTempFile(imageFileName, ".jpg", storageDir);
            Uri destinationUri = Uri.fromFile(destinationFile);
            UCrop.of(sourceUri,destinationUri)
                    .withAspectRatio(1,1)
                    .withMaxResultSize(1000,1000)
                    .start(getContext(),this,UCrop.REQUEST_CROP);
        }
        catch (Exception e)
        {

        }

    }

    private void hideAllTvError(){
        error_for_edt_judul.setVisibility(View.INVISIBLE);
        error_for_rich_editor.setVisibility(View.INVISIBLE);
        error_for_radio_group_cover.setVisibility(View.INVISIBLE);
    }

    private boolean mekanismeValidasiInput(){
        /*hasilValidasi true = boleh lanjut. false = tidak boleh lanjut (ada validasi gagal)*/
        boolean hasilValidasi = true;
        if(!checkValidasiEdtJudulKosong())
        {
            showErrorTextView(error_for_edt_judul,res.getString(R.string.judul_tidak_boleh_kosong));
            hasilValidasi = false;
        }
        if(!checkValidasiRichTextEditorKosong())
        {
            showErrorTextView(error_for_rich_editor,res.getString(R.string.deskripsi_tidak_boleh_kosong));
            hasilValidasi = false;
        }
        if(!checkValidasiRichTextEditorKosong())
        {
            showErrorTextView(error_for_rich_editor,res.getString(R.string.deskripsi_tidak_boleh_kosong));
            hasilValidasi = false;
        }
        /*jika pengguna memilih radio unggah foto cover, maka check apakah sudah mengupload foto cover atau belum*/
        if(radio_unggah_cover.isChecked())
        {
            if(!checkValidasiSudahMenguploadFotoCover())
            {
                showErrorTextView(error_for_radio_group_cover,res.getString(R.string.kamu_belum_mengupload_cover));
                hasilValidasi = false;
            }
        }
        return hasilValidasi;
    }

    private boolean checkValidasiEdtJudulKosong(){
        if(edt_judul.getText().toString().trim().length() == 0)
        {
            return false;
        }
        return true;
    }

    private void scrollToFirstVisibleTvError(){
        if(error_for_edt_judul.getVisibility() == View.VISIBLE)
        {
            scrollview_tulis_step1.smoothScrollTo(0,edt_judul.getTop());
        }
        else if(error_for_rich_editor.getVisibility() == View.VISIBLE)
        {
            scrollview_tulis_step1.smoothScrollTo(0,rich_editor.getTop());
        }
        else if(error_for_radio_group_cover.getVisibility() == View.VISIBLE)
        {
            scrollview_tulis_step1.smoothScrollTo(0,radio_group_cover.getTop());
        }
    }
    private boolean checkValidasiRichTextEditorKosong(){
        if(rich_editor.getHtml() == null){
            return false;
        }
        else if(rich_editor.getHtml() != null) {
            if (rich_editor.getHtml().trim().length() == 0) {
                return false;
            }
        }
        return true;
    }
    private boolean checkValidasiSudahMenguploadFotoCover(){
        if(imageUriCoverPilihanUser == null)
        {
            Log.d("10_agustus_2022","iUCP null");
            return false;
        }
        return true;
    }

    private void showErrorTextView(TextView tv,String pesanError){
        tv.startAnimation(fade_in);
        tv.setVisibility(View.VISIBLE);
        tv.setText(pesanError);
    }



    private void hide_ly_pilih_file_cover_forhat(){
        ly_pilih_file_cover_forhat.setVisibility(View.GONE);
    }

    private void setPlaceHolderRichText(String text)
    {
        rich_editor.setPlaceholder(text);
    }

    private void findViewByIdAllComponents(View v) {
        rich_editor = v.findViewById(R.id.rich_editor);
        icon_bold = v.findViewById(R.id.icon_bold);
        icon_italic = v.findViewById(R.id.icon_italic);
        icon_underline = v.findViewById(R.id.icon_underline);
        icon_numbering = v.findViewById(R.id.icon_numbering);
        icon_unordered = v.findViewById(R.id.icon_unordered);
        icon_outdent = v.findViewById(R.id.icon_outdent);
        icon_indent = v.findViewById(R.id.icon_indent);
        btn_lanjutkan_step1 = v.findViewById(R.id.btn_lanjutkan_step1);
        img_cover_preview = v.findViewById(R.id.img_cover_preview);
        delete_cover = v.findViewById(R.id.delete_cover);
        ly_pilih_file_cover_forhat = v.findViewById(R.id.ly_pilih_file_cover_forhat);
        tv_keterangan_file = v.findViewById(R.id.tv_keterangan_file);
        radio_cover_pilihan_admin = v.findViewById(R.id.radio_cover_pilihan_admin);
        radio_unggah_cover = v.findViewById(R.id.radio_unggah_cover);
        radio_group_cover = v.findViewById(R.id.radio_group_cover);
        ly_preview_uploaded_cover = v.findViewById(R.id.ly_preview_uploaded_cover);
        edt_judul = v.findViewById(R.id.edt_judul);
        error_for_edt_judul = v.findViewById(R.id.error_for_edt_judul);
        error_for_rich_editor = v.findViewById(R.id.error_for_rich_editor);
        error_for_radio_group_cover = v.findViewById(R.id.error_for_radio_group_cover);
        scrollview_tulis_step1 = v.findViewById(R.id.scrollview_tulis_step1);
    }

    private void set_text_tv_keterangan_file(String text){
        tv_keterangan_file.setText(text);
    }



    private void mekanismeRetreiveDataFromActivity(){

        ForhatTulisActivity currentActivity = (ForhatTulisActivity) getActivity();
        edt_judul.setText(currentActivity.retreiveJudul());
        rich_editor.setHtml(currentActivity.retreiveDeskripsi());
        Uri uriFromServer = currentActivity.retreiveUriCoverPilihanUser();
        switch(currentActivity.retreiveIdRadioCoverChecked())
        {
            case R.id.radio_unggah_cover:
                radio_unggah_cover.setChecked(true);
                radio_cover_pilihan_admin.setChecked(false);
                show_ly_pilih_cover_forhat();
                mekanismeSetImageUriCoverPilihanUser(uriFromServer);
                set_text_tv_keterangan_file(currentActivity.retreiveNamaFilePilihanUser());
                show_ly_preview_uploaded_cover();
                uriFromServer = null;
                break;
            case R.id.radio_cover_pilihan_admin:
                radio_unggah_cover.setChecked(false);
                hide_ly_pilih_cover_forhat();
                hide_ly_preview_uploaded_cover();
                radio_cover_pilihan_admin.setChecked(true);

                break;
        }

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        communicator = (FragmentTulisCommunicator)(ForhatTulisActivity)context;
    }


    @Override
    public void onResume() {
        super.onResume();
        mekanismeRetreiveDataFromActivity();

        Log.d("9_agustus_2022","onResumeStep1()");
    }

    @Override
    public void onPause() {
        super.onPause();

        communicator.saveStep1Data(edt_judul.getText().toString(),rich_editor.getHtml(),GetIdOfCheckedRadioButton(),imageUriCoverPilihanUser, tv_keterangan_file.getText().toString());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("9_agustus_2022","onDestroyStep1()");
    }
}