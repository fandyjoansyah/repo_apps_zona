package com.zonamediagroup.zonamahasiswa.ForhatFragmentTulis;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.ForhatTulisActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class ForhatTulisStep4 extends Fragment {

    Animation fade_in;
    boolean flagFotoAsliTerpilih,flagFotoAnonimTerpilih = false;
    CircleImageView img_foto_profil_asli,img_foto_profil_anonimus;
    FragmentTulisCommunicator communicator;
    LinearLayout container_tooltips_foto_asli,container_tooltips_foto_anonimus;
    NestedScrollView scroll_view_step4;
    RelativeLayout ly_img_foto_profil_asli, ly_img_foto_profil_anonimus, area_foto_profil;
    TextView txt_nama_pengguna,txt_nama_anonimus, btn_kembali_step4,btn_lanjutkan_step4, error_for_area_foto_profil;
    Resources rs;
    String urlFotoAnonimus, urlFotoAsli,namaAnonimus,namaAsli;

    private static final String URL_REQUEST_DEFAULT_FOTO_PROFIL = Server.URL_REAL + "get_default_foto_profil";

    public ForhatTulisStep4() {
        // Required empty public constructor
    }

    private void initRs(){
        rs = getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.include_forhat_tulis_step4, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAnimation();
        initRs();
        findViewByIdAllComponents(view);
        setListenerAllComponents();
        mekanismeGetDataFromSharedPreferences();
        getUrlFotoAnonimus();
        mekanismeSetTextNamaPengguna();
    }

    private void mekanismeGetDataFromSharedPreferences() {
        urlFotoAsli = SharedPrefManagerZona.getInstance(getActivity()).geturl_voto();
        namaAnonimus = SharedPrefManagerZona.getInstance(getActivity()).getnama_anonim();
        namaAsli = SharedPrefManagerZona.getInstance(getActivity()).get_username();
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
    }

    private void mekanismeSetTextNamaPengguna(){
        txt_nama_pengguna.setText(namaAsli);
        txt_nama_anonimus.setText(namaAnonimus);
    }


    private void findViewByIdAllComponents(View v){
        btn_kembali_step4 = v.findViewById(R.id.btn_kembali_step4);
        txt_nama_pengguna = v.findViewById(R.id.txt_nama_pengguna);
        txt_nama_anonimus = v.findViewById(R.id.txt_nama_anonimus);
        ly_img_foto_profil_anonimus = v.findViewById(R.id.ly_img_foto_profil_anonimus);
        ly_img_foto_profil_asli = v.findViewById(R.id.ly_img_foto_profil_asli);
        error_for_area_foto_profil = v.findViewById(R.id.error_for_area_foto_profil);
        scroll_view_step4 = v.findViewById(R.id.scroll_view_step4);
        area_foto_profil = v.findViewById(R.id.area_foto_profil);
        img_foto_profil_asli = v.findViewById(R.id.img_foto_profil_asli);
        img_foto_profil_anonimus = v.findViewById(R.id.img_foto_profil_anonimus);
        btn_lanjutkan_step4 = v.findViewById(R.id.btn_lanjutkan_step4);
    }

    private void selectAkunAsli(){


        ly_img_foto_profil_asli.animate()
                .alpha(0.9f)
                .setDuration(50)
                .scaleX(1.17f)
                .scaleY(1.17f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        ly_img_foto_profil_asli.animate().alpha(1).setDuration(150).scaleX(1.12f).scaleY(1.12f);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
        ly_img_foto_profil_asli.setBackgroundResource(R.drawable.bg_foto_profil_tulis_selected);
        ly_img_foto_profil_anonimus.setBackgroundResource(R.drawable.bg_foto_profil_tulis_notselected);
        flagFotoAsliTerpilih = true;
        flagFotoAnonimTerpilih = false;
    }

    private void selectAkunAnonim(){
        ly_img_foto_profil_asli.setBackgroundResource(R.drawable.bg_foto_profil_tulis_notselected);
        ly_img_foto_profil_anonimus.animate()
                .alpha(0.9f)
                .setDuration(50)
                .scaleX(1.17f)
                .scaleY(1.17f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        ly_img_foto_profil_anonimus.animate().alpha(1).setDuration(150).scaleX(1.12f).scaleY(1.12f);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
        ly_img_foto_profil_anonimus.setBackgroundResource(R.drawable.bg_foto_profil_tulis_selected);
        flagFotoAsliTerpilih = true;
        flagFotoAnonimTerpilih = false;
    }

    private void setListenerAllComponents(){
        btn_kembali_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                communicator.communicatorGoBackToPreviousStep(3);
            }
        });
        ly_img_foto_profil_anonimus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAkunAnonim();
            }
        });
        ly_img_foto_profil_asli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAkunAsli();
            }
        });
        btn_lanjutkan_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanismeSelesaikanStep();
            }
        });
    }

    private void mekanismeSelesaikanStep()
    {
        hideAllTvError();
        if(mekanismeValidasiInput()) {
            communicator.finishAllStep();
        }
        else
            scrollToFirstVisibleTvError();
    }

    private void scrollToFirstVisibleTvError(){
        if(error_for_area_foto_profil.getVisibility() == View.VISIBLE)
        {
            scroll_view_step4.smoothScrollTo(0,area_foto_profil.getTop());
        }

    }

    private void hideAllTvError(){
        error_for_area_foto_profil.setVisibility(View.INVISIBLE);
    }

    private boolean mekanismeValidasiInput(){
        /*hasilValidasi true = boleh lanjut. false = tidak boleh lanjut (ada validasi gagal)*/
        boolean hasilValidasi = true;
        if(!checkValidasiAdaAkunTerpilih())
        {
            showErrorTextView(error_for_area_foto_profil,rs.getString(R.string.silahkan_pilih_salah_satu_pilihan_diatas));
            hasilValidasi = false;
        }

        return hasilValidasi;
    }

    private boolean checkValidasiAdaAkunTerpilih(){
        if(flagFotoAnonimTerpilih == false && flagFotoAsliTerpilih == false)
        {
            return false;
        }
        return true;
    }
    private void showErrorTextView(TextView tv,String pesanError){
        tv.startAnimation(fade_in);
        tv.setVisibility(View.VISIBLE);
        tv.setText(pesanError);
    }






    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        communicator = (FragmentTulisCommunicator)(ForhatTulisActivity)context;
    }

    private void getUrlFotoAnonimus(){
        AndroidNetworking.get(URL_REQUEST_DEFAULT_FOTO_PROFIL)
                .addQueryParameter("TOKEN", "qwerty")
                .setTag("get default foto profil")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            urlFotoAnonimus = response.getString("url_foto_profil");
                            mekanismeSetFotoProfil();
                        } catch (JSONException e) {

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void mekanismeSetFotoProfil(){
        mekanismeSetFotoProfilAsli();
        setFotoProfil(img_foto_profil_anonimus,urlFotoAnonimus);
    }

    private void mekanismeSetFotoProfilAsli(){
        if(urlFotoAsli.equals("") || urlFotoAsli == null || urlFotoAsli.equals("null"))
        {
            Log.d("2_agustus_2022","urlFotoAsli empty");
            setFotoProfil(img_foto_profil_asli,urlFotoAnonimus);
        }
        else
        {
            Log.d("2_agustus_2022","urlFotoAsli is not empty: "+urlFotoAsli);
            setFotoProfil(img_foto_profil_asli,urlFotoAsli);
        }
    }




    private void setFotoProfil(ImageView imgViewTujuan, String url)
    {
        Glide.with(getActivity())
                .load(url)
                .apply(RequestOptions.fitCenterTransform())
                .placeholder(R.drawable.ic_no_image_available)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        CustomToast.makeBlackCustomToast(getContext(),getResources().getString(R.string.terjadi_kesalahan_saat_mencoba_mendapatkan_foto_profil), Toast.LENGTH_SHORT);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imgViewTujuan);
    }
}