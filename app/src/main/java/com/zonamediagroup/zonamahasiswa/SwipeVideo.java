package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.SwipeVideo_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwipeVideo extends AppCompatActivity implements SwipeVideo_Adapter.SwipeListener, Komentar_Video_Level1_Adapter.KomentarLevel1 {

    private static final String URL_HALAMAN_UTAMA_VIDEO = Server.URL_HALAMAN_UTAMA_VIDEO;
    LinearLayout loding_layar;
    LinearLayout eror_layar;
    private static final String URL_LIKE_VIDEO = Server.URL_REAL_VIDEO+"Video_like/index_post";


    TextView coba_lagi;

    ViewPager2 videosViewPager;

    ArrayList<Video_Model> dataFromFirstPage;

    //start component for comment
    Komentar_Video_Level1_Adapter commentAdapterLevel1;

    RecyclerView recyclerCommentInBottomSheet;

    BottomSheetDialog bottomSheetDialog;
    ProgressBar pbBottomSheet;

    EditText txt_komentar_video;

    ImageView send_button, img_swipe_down;

    ArrayList<Video_Model> dataVideos;

    public static Integer notifSwipe = 0;

   public static RelativeLayout relative_swipe_down;

    ArrayList<Komentar_Video_Model> dataRecyclerKomentarBottomSheet;

    private static final String URL_GET_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment/index_post";

    private static final String URL_TAMBAH_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_create/index_post";

    private static final String URL_TAMBAH_PLAY = Server.URL_REAL_VIDEO+"Video_play/index_post";

    //start timer
    public static Handler handler = new Handler();
    public static int delay = 5000; // 1000 milliseconds == 1 second
    public static Runnable runnable;


    //end timer


    //end component for comment

    int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_swipe_video);
        initCommentBottomSheet();
        videosViewPager = findViewById(R.id.swipe_viewpager);
        getData();
        relative_swipe_down = findViewById(R.id.relative_swipe_down);
        img_swipe_down = findViewById(R.id.img_swipe_down);
        notifSwipe = 0;
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        videosViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);

            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
                if(notifSwipe == 0){
                    notifSwipe = 1;
                    notifShow();
                }else{
                    notifShow();
                }
            }
        });
//membuat gambar bergerak
        TranslateAnimation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.05f);
        mAnimation.setDuration(700);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        img_swipe_down.setAnimation(mAnimation);

//        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
//        String toastMsg;
//        switch(screenSize) {
//            case Configuration.SCREENLAYOUT_SIZE_LARGE:
//                toastMsg = "Large screen";
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
//                toastMsg = "Normal screen";
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_SMALL:
//                toastMsg = "Small screen";
//                break;
//            default:
//                toastMsg = "Screen size is neither large, normal or small";
//        }
//        CustomToast.s(getApplicationContext(),toastMsg);


        //cek resolusi
//        Map<String, Integer> map = deriveMetrics(this);
//
//        String textnya = map.get("screenWidth") +" "+ map.get("screenHeight") +" "+ map.get("screenDensity");

//        CustomToast.s(getApplicationContext(),""+textnya);
    }
    //start timer

    public static void tambahPlay(String idVideo, String idUser, int currentTime, int interval)
    {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_TAMBAH_PLAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(MainActivity.MYTAG,"on Response swipe");
                try {

                    // Check for error node in json

                } catch (Exception e) {
                    Log.d(MainActivity.MYTAG,"catch add play");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse add play "+error.getMessage());
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user",idUser);
                params.put("id_video",idVideo);
                params.put("position",String.valueOf(currentTime));
                params.put("miliseconds",String.valueOf(interval));
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e("swipevideo", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");

    }

    public static void showLogX(String idVideo, String idUser, int currentTime)
    {
        Log.d(MainActivity.MYTAG, "oyii idVideo: "+idVideo+" idUser: "+idUser+" currentTime: "+currentTime); // Do your work here
    }
    //end timer
    //start timer
    public static void endTimer(){
        handler.removeCallbacks(runnable);
    }


    //end timer

    //get angka tooltips swipe
    public static void notifShow()
    {
        if(notifSwipe == 0){
            notifSwipe = 1;
            relative_swipe_down.setVisibility(View.VISIBLE);
        }else{
            relative_swipe_down.setVisibility(View.GONE);
        }
    }

    public Map<String, Integer> deriveMetrics(Activity activity) {
        try {
            DisplayMetrics metrics = new DisplayMetrics();

            if (activity != null) {
                activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            }

            Map<String, Integer> map = new HashMap<String, Integer>();
            map.put("screenWidth", Integer.valueOf(metrics.widthPixels));
            map.put("screenHeight", Integer.valueOf(metrics.heightPixels));
            map.put("screenDensity", Integer.valueOf(metrics.densityDpi));

            return map;
        } catch (Exception err) {
            ; // just use zero values
            return null;
        }
    }

    public void getData(){
        dataFromFirstPage = new ArrayList<>();
       Bundle bundle = getIntent().getBundleExtra("bundlenya");
       String dataVideo = bundle.getString("data");
       position = bundle.getInt("position");
        dataFromFirstPage = new Gson().fromJson(dataVideo, new TypeToken<List<Video_Model>>(){}.getType());
        dataVideos = dataFromFirstPage;
        if(dataFromFirstPage.size() > 0)
        {
            videosViewPager.setAdapter(new SwipeVideo_Adapter(dataFromFirstPage,SwipeVideo.this,this));
            videosViewPager.setCurrentItem(position);
        }
        else
            {
            getSwapVideoByKategori();
        }
    }

    public void getSwapVideoByKategori(){
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_HALAMAN_UTAMA_VIDEO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(MainActivity.MYTAG,"on Response swipe");
                try {
                    Log.d(MainActivity.MYTAG,"try swipe");
                    JSONObject jObj = new JSONObject(response);
                    Log.d(MainActivity.MYTAG,"try response getALl");


                    if (jObj.getString("status").equals("true")) {
                        //set nextVideo yang akan diload awal di next page
                        JSONArray dataArray = jObj.getJSONArray("data");

                        //Log.d("Response VideoX","vbgvbg: "+dataArray.getJSONObject(0).getJSONArray("comment").getJSONObject(0).get("video_comment"));
                        ArrayList<Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Video_Model>>() {
                        }.getType());
                        dataVideos = items;
                        videosViewPager.setAdapter(new SwipeVideo_Adapter(dataVideos,SwipeVideo.this,SwipeVideo.this));

                    } else if (jObj.getString("status").equals("false")) {

                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    Log.d(MainActivity.MYTAG,"catch swipe");
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
//                    eror_show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse swipe "+error.getMessage());
                Log.e("SwipeVideo", "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
//                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user",MainActivity.idUserLogin);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e("swipevideo", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }


    @Override
    public void storeLikeSwipe(String idVideo, String like) {
        int likeAngka = Integer.valueOf(like);
        int videoIdAngka = Integer.valueOf(idVideo);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                    } else if (jObj.getString("status").equals("false")) {

                    }

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                params.put("like",like);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void initCommentBottomSheet()
    {
        bottomSheetDialog = new BottomSheetDialog(SwipeVideo.this);
        View view= LayoutInflater.from(SwipeVideo.this).inflate(R.layout.sheet_dialog_komentar,null);

        recyclerCommentInBottomSheet = view.findViewById(R.id.recycler_bottomsheet_komen_video);
        LinearLayoutManager layoutLevel1BottomSheet = new LinearLayoutManager(SwipeVideo.this, RecyclerView.VERTICAL,false);
        recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1BottomSheet);

        pbBottomSheet = view.findViewById(R.id.progress_bottom_video);

        txt_komentar_video = view.findViewById(R.id.txt_komentar_video);

        send_button = view.findViewById(R.id.send_button);

        dataRecyclerKomentarBottomSheet = new ArrayList<>();



        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
    }
    
    @Override
    public void showAllCommentOnSwipe(String idVideo) {
        Log.d(MainActivity.MYTAG,"CALLER FROM Video. idVideo: "+idVideo);
        bottomSheetDialog.show();
        //start code getCommentLevel1
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONArray dataArray = jObj.getJSONArray("data");
                        ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());
                        dataRecyclerKomentarBottomSheet.clear();
                        dataRecyclerKomentarBottomSheet.addAll(items);
                        commentAdapterLevel1 = new Komentar_Video_Level1_Adapter(SwipeVideo.this,dataRecyclerKomentarBottomSheet,SwipeVideo.this);
                        recyclerCommentInBottomSheet.setAdapter(commentAdapterLevel1);
                        commentAdapterLevel1.notifyDataSetChanged();
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"komen pertama: "+items.get(0).getVideo_comment());
                        pbBottomSheet.setVisibility(View.GONE);
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");

                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catch");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end code getCommentLevel1
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahKomentar(MainActivity.idUserLogin, idVideo, txt_komentar_video.getText().toString());
            }
        });
    }

    @Override
    public void shareVideoSwipe(String url) {

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,  " https://play.google.com/store/apps/details?id=com.zonamediagroup.zonamahasiswa.app");
            String app_url = url;
            shareIntent.putExtra(Intent.EXTRA_TEXT, app_url);
            startActivity(Intent.createChooser(shareIntent, "Share via"));
    }

    @Override
    public void backVideo() {
        onBackPressed();
    }

    @Override
    public void playNextVideo(int currentPosition) {
        videosViewPager.setCurrentItem(currentPosition+1);
    }



    private void tambahKomentar(String idUser, String idVideo, String komentar){
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_TAMBAH_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONObject dataObject = jObj.getJSONObject("data");
                        Komentar_Video_Model items = new Gson().fromJson(dataObject.toString(), new TypeToken<Komentar_Video_Model>() {
                        }.getType());
                        dataRecyclerKomentarBottomSheet.add(0,items);

                        //commentAdapterLevel1.notifyItemInserted(0);
                        commentAdapterLevel1.notifyDataSetChanged();
                        //lanjut disini. refresh recycler sehabis menambahkan komentar
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");

                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catchX");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUser);
                params.put("id_video",idVideo);
                params.put("comment",komentar);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }


    @Override
    public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {

    }

    @Override
    public void balasKomentarLevel1(String idKomentarYangDibalas) {

    }


    @Override
    protected void onStop() {
        super.onStop();
        endTimer();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CurrentActiveVideoData.initDataVideos();
        CurrentActiveVideoData.setDataVideos(dataVideos);
    }
}