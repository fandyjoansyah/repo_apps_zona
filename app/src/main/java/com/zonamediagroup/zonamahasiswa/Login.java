package com.zonamediagroup.zonamahasiswa;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    /*start comment 30 mei 2022

    private String URL = Server.URL_REAL + "Login/";
    String tag_json_obj = "json_obj_req";


    private static final String TAG = Login.class.getSimpleName();


    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;
    CardView signInButton;
    String person_family_nama = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseApp.initializeApp(com.zonamediagroup.zonamahasiswa.Login.this);

        setContentView(R.layout.page_login);




        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(com.zonamediagroup.zonamahasiswa.Login.this.getResources().getString(R.string.default_web_client_id_login))
                .requestEmail()
                .build());

        signInButton = findViewById(R.id.logingoogle);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signIn();

            }
        });

    }


    @Override
    public void onStart() {
        super.onStart();

        // [START on_start_sign_in]
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.

        updateUI(GoogleSignIn.getLastSignedInAccount(this));

        //  Log.w(TAG, "sudah masuk");
        // [END on_start_sign_in]
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            if(data != null)
            {
                Log.d(Bank_Tag.LOGIN,"data not null");
            }
            else{
                Log.d(Bank_Tag.LOGIN,"data null");
            }
            Log.d(Bank_Tag.LOGIN,"if onActivityResult");
        }
        else
        {
            Log.d(Bank_Tag.LOGIN,"else onActivityResult");
        }
    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            Log.d(Bank_Tag.LOGIN,"try handleSignInREsult");
            // Signed in successfully, show authenticated UI.
            updateUI(completedTask.getResult(ApiException.class));
        } catch (ApiException e) {
            Log.d(Bank_Tag.LOGIN,"catch handleSignInResult ex: "+e.getStatus());
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            //    Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //  System.out.println("rusak dah " + e.getStatusCode());

            updateUI(null);
        }
    }
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        try {
          //  CustomToast.s(getApplicationContext(),"try sign in");
            startActivityForResult(mGoogleSignInClient.getSignInIntent(), RC_SIGN_IN);
        }
        catch(Exception e){
          //  CustomToast.s(getApplicationContext(),"catch sign in "+e.getMessage());
        }
    }
    // [END signIn]

    // [START signOut]
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {

//            ngambil id

            final String personName = account.getDisplayName();
            final String personGivenName = account.getGivenName();
            final String personFamilyName = account.getFamilyName();
            final String personEmail = account.getEmail();
            final String personId = String.valueOf(account.getId());
            Uri personPhoto = account.getPhotoUrl();
            final String urlvoto = String.valueOf(account.getPhotoUrl());


            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            //  Log.d(TAG, "Refreshed token cok : " + refreshedToken);


// Call API
            regis(personId, personName, personGivenName, personFamilyName, personEmail, urlvoto, refreshedToken);

        }
    }

    private void regis(final String person_idx, final String person_namex, final String person_given_namex, final String person_family_namex, final String person_emailx, final String person_photox, final String person_tokenx) {

        //cek jika person_family_namex = null, maka ganti value null ke ""
        if(person_family_namex != null){
            person_family_nama = person_family_namex;
        }
        else
        {
            person_family_nama = "";
        }
        StringRequest strReq = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //   Log.e(TAG, "Register Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);

                    JSONArray dataarray = jObj.getJSONArray("data");


                    if (jObj.getString("status").equals("true")) {

                        JSONObject jsonObject1 = dataarray.getJSONObject(0);
                        String json_id_user = jsonObject1.optString("id_user");
                        String json_person_id = jsonObject1.optString("person_id");
                        String json_person_name = jsonObject1.optString("person_name");
                        String json_person_given_name = jsonObject1.optString("person_given_name");
                        String json_person_family_name = jsonObject1.optString("person_family_name");
                        String json_person_email = jsonObject1.optString("person_email");
                        String json_person_photo = jsonObject1.optString("person_photo");
                        String json_person_gender = jsonObject1.optString("jenis_kelamin");


                        // sesion save

                        Boolean a = SharedPrefManagerZona.getInstance(getApplicationContext()).saveData(json_id_user, json_person_id, json_person_name, json_person_given_name,
                                json_person_family_name, json_person_email, json_person_photo, json_person_gender);


                        if (a) {

//                        Subcribe ke topik. Toggle salah satu
//                            String topic = "notif-zona-develop";
                            String topic = "notif-zona-production";
                            FirebaseMessaging.getInstance().subscribeToTopic(topic);


//                        End Topic


                            finish();
                            startActivity(new Intent(Login.this, MainActivity.class));

                        }
//


                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //    Log.e(TAG, "Login Error: " + error.getMessage());


            }
        }) {



//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("person_id", person_idx);
                params.put("person_name", person_namex);
                params.put("person_given_name", person_given_namex);
                params.put("person_family_name", person_family_nama);
                params.put("person_email", person_emailx);
                params.put("person_photo", person_photox);
                params.put("person_token", person_tokenx);

                return params;
            }
        };


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }
     end comment 30 mei 2022*/
}