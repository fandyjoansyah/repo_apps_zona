package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Iklan extends AppCompatActivity {

    ImageView back;
    WebView p4;
    CardView lihat_harga;

    static String tag_json_obj = "json_obj_req";

    String URL_NOMOR_WA = Server.URL_REAL + "get_nomor_wa?TOKEN=qwerty";

    String nomorWa;

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_iklan);

        p4 = findViewById(R.id.p4);

        String x =
                "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
                        "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
                        "<link href=\"https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap\" rel=\"stylesheet\">\n" +
                        "\n" +
                        "\n" +
                        "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
                        "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
                        "<link href=\"https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap\" rel=\"stylesheet\"> \n" +
                        "\n" +
                        "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
                        "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
                        "<link href=\"https://fonts.googleapis.com/css2?family=Noto+Sans&family=Noto+Sans+KR:wght@900&display=swap\" rel=\"stylesheet\"> \n<p style=\"text-align: left; line-height: 22px; font-family: 'Noto Sans', sans-serif;\">Halo Sobat Zona.<br />Selamat datang di Zona Mahasiswa.<br /> Bagi Sobat Zona yang memiliki produk, bisnis, event dan ingin mengiklankan di Zona Mahasiswa untuk meningkatkan penjualan? Yuk klik tombol di bawah ini.</p>";
        p4.loadData(x, "text/html; charset=utf-8", "UTF-8");


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //start adview
        AdView mAdView = (AdView) findViewById(R.id.adViewIklan);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        //end adview

        lihat_harga=findViewById(R.id.lihat_harga);
        lihat_harga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //start iklan lama
                //Uri uri = Uri.parse("https://zonamahasiswa.id/iklan/"); // missing 'http://' will cause crashed
                //Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                //startActivity(intent);
                //end iklan lama

                progressDialog = new ProgressDialog(Iklan.this);
                progressDialog.setMessage("Menghubungkan...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();
                //start volley get nomor wa
                StringRequest strReq = new StringRequest(Request.Method.GET, URL_NOMOR_WA, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jObj = new JSONObject(response);

                            if (jObj.getString("status").equals("true")) {
                                nomorWa =  jObj.getString("nomor_wa");
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://wa.me/"+nomorWa));
                                startActivity(intent);
                            }


                        } catch (JSONException e) {
                            // JSON error
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("mimin zona", "Register Error: " + error.getMessage());
                        progressDialog.dismiss();

                    }
                }) {


//            parameter

                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to login url
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("TOKEN", "qwerty");

                        return params;
                    }
                };


//        ini heandling requestimeout
                strReq.setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 10000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 10000;
                    }

                    @Override
                    public void retry(VolleyError error) throws VolleyError {

                        Log.e("mimin zona", "VolleyError Error: " + error.getMessage());
//                eror_show();
                    }
                });

                // Adding request to request queue
                MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
                //end volley get nomor wa
            }
        });

    }
}