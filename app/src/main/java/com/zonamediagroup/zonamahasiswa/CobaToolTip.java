package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import com.zonamediagroup.zonamahasiswa.tooltip.OverlayView;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltip;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltipUtils;

public class CobaToolTip extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coba_tool_tip);
        View yourView = findViewById(R.id.coba_tooltip_tv_pertama);


    }

    View click_custom, tooltip;
    @Override
    protected void onStart() {
        super.onStart();
        click_custom = findViewById(R.id.coba_tooltip_tv_pertama);
        tooltip(click_custom);

    }

    private void tooltip(View v){
         SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                .anchorView(v)
                .text("Selamat datang di aplikasi zona")
                .gravity(Gravity.BOTTOM)
                .showArrow(false)
                .dismissOnOutsideTouch(false)
                .dismissOnInsideTouch(false)
                .modal(true)
                .animated(true)
                .transparentOverlay(false)
                .overlayMatchParent(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .overlayOffset(0)
                .animationDuration(2000)
                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
               .contentView(R.layout.tooltip_welcome,R.id.tv_text)
                .focusable(true)
                .build();


        tooltip.show();
    }
}