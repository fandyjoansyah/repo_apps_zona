package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Blokir_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Blokir_Model;
import com.zonamediagroup.zonamahasiswa.models.Kategori_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaftarBlokir extends AppCompatActivity {
    RecyclerView receler_daftar_blokir;
    List<Blokir_Model> blokirModel;
    Blokir_Adapter blokir_adapter;
    LinearLayout loding_layar;
    LinearLayout eror_layar;
    TextView coba_lagi;
    RelativeLayout ly_notif_blokir_kosong;
    String id_user_sv;
    ImageView image_animasi_loading;
    static String tag_json_obj = "json_obj_req";
    private String URL_get_blokir_pengguna = Server.URL_REAL_NATIVE + "Get_blokir_pengguna";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_daftar_blokir);
        receler_daftar_blokir = findViewById(R.id.receler_daftar_blokir);
        //        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);
        ly_notif_blokir_kosong = findViewById(R.id.ly_notif_blokir_kosong);

//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);
        blokirModel  = new ArrayList<>();
        blokir_adapter = new Blokir_Adapter(DaftarBlokir.this, blokirModel);
        RecyclerView.LayoutManager mLayoutManagerkategorix = new LinearLayoutManager(DaftarBlokir.this,RecyclerView.VERTICAL,false);
        receler_daftar_blokir.setLayoutManager(mLayoutManagerkategorix);
        receler_daftar_blokir.setItemAnimator(new DefaultItemAnimator());
        receler_daftar_blokir.setAdapter(blokir_adapter);
        id_user_sv = SharedPrefManagerZona.getInstance(DaftarBlokir.this).getid_user();
        dapatkanDataBlokir();
    }

    public void adaDataBlokir(){
        ly_notif_blokir_kosong.setVisibility(View.GONE);
        receler_daftar_blokir.setVisibility(View.VISIBLE);
    }

    public void tidakAdaDataBlokir(){
        ly_notif_blokir_kosong.setVisibility(View.VISIBLE);
        receler_daftar_blokir.setVisibility(View.GONE);
    }
    public void dapatkanDataBlokir(){


        StringRequest strReq = new StringRequest(Request.Method.POST, URL_get_blokir_pengguna, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {


                        JSONArray dataArray2 = jObj.getJSONArray("data_blokir");

                        List<Blokir_Model> itemsdua = new Gson().fromJson(dataArray2.toString(), new TypeToken<List<Blokir_Model>>() {
                        }.getType());


                        blokirModel.clear();
                        blokirModel.addAll(itemsdua);

                        if(blokirModel.size() > 0)
                        {
                            adaDataBlokir();
                        }
                        else{
                            tidakAdaDataBlokir();
                        }
                        blokir_adapter.notifyDataSetChanged();

                        loading_dismiss();
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();

                    }
//                    doToolTipsKategori();
//                    initToolTipsKategori();
//                    runToolTipsKategori();

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dapatkan_blokir_error", "Register Error: " + error.getMessage());
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("person_id", id_user_sv);

                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("dapatkan_blokir_error", "VolleyError Error: " + error.getMessage());
//                eror_show();
            }
        });

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }
}