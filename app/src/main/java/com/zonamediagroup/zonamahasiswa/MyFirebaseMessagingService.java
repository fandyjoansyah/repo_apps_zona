package com.zonamediagroup.zonamahasiswa;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by Belal on 03/11/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    Bitmap bitmap;

    public static final int ID_BIG_NOTIFICATION = 234;
    public static final int ID_SMALL_NOTIFICATION = 235;

    // private Context mCtx;
    String imageUri, message, title, click_action="";

    String id_poling, tiitle_poling, img_poling, poling_start, poling_end, waktu_sekarang;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Log.d(TAG, "Data Notif OYI  " + remoteMessage);


        Log.d(TAG, "From: FB " + remoteMessage.getFrom());
        Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        Log.d(TAG, "Message data payload: " + remoteMessage.getNotification());


        message = remoteMessage.getData().get("body");
        title = remoteMessage.getData().get("title");

        //start fandy get payload for polling inside data
        id_poling = remoteMessage.getData().get("id_poling");
        tiitle_poling = remoteMessage.getData().get("tiitle_poling");
        img_poling = remoteMessage.getData().get("img_poling");
        poling_start = remoteMessage.getData().get("poling_start");
        poling_end = remoteMessage.getData().get("poling_end");
        waktu_sekarang = remoteMessage.getData().get("waktu_sekarang");





        //end fandy get payload for polling inside data

        //imageUri will contain URL of the image to be displayed with Notification
        imageUri = remoteMessage.getData().get("image");

        click_action = remoteMessage.getNotification().getClickAction();

        show_notifwithimage();

    }




    @SuppressLint("StaticFieldLeak")
    private void show_notifwithimage() {
        new AsyncTask<String, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... strings) {
                InputStream inputStream;
                try {
                    Log.d("gambar_notif","try gambar notif");
                    URL url = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                    return bitmap;

                } catch (Exception ignored) {
                    Log.d("gambar_notif","catch gambar notif");

                }
                return null;

            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                show_notif(bitmap);
            }
        }.execute(imageUri);
    }


        private void show_notif(Bitmap bitmap) {

    
            int notificationID = new Random().nextInt(100);
            String chanelId = "notification_c_10";
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



            Intent intent;
            //start cek click_action
            if(click_action != null) {
                if (click_action.equals("Detail_poling")) {
                    intent = new Intent(getApplicationContext(), Detail_poling.class);
                    intent.putExtra("id_poling", id_poling);
                    intent.putExtra("tiitle_poling", tiitle_poling);
                    intent.putExtra("img_poling", img_poling);
                    intent.putExtra("poling_start", poling_start);
                    intent.putExtra("poling_end", poling_end);
                    intent.putExtra("waktu_sekarang", waktu_sekarang);
                } else {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }
            }
            else {
                intent = new Intent(getApplicationContext(), MainActivity.class);
            }
            //end cek click_action
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivities(
                    getApplicationContext(),
                    0, new Intent[]{intent}, PendingIntent.FLAG_UPDATE_CURRENT);
    
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), chanelId);
            builder.setSmallIcon(R.drawable.ic_logo);
            builder.setDefaults(NotificationCompat.DEFAULT_ALL);
            builder.setContentTitle(title);
            builder.setContentText(message);
    
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
    
            builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
    
    
            builder.setContentIntent(pendingIntent);
            builder.setAutoCancel(true);
            builder.setPriority(NotificationCompat.PRIORITY_MAX);
    
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (notificationManager != null && notificationManager.getNotificationChannel(chanelId) == null) {
                    NotificationChannel notificationChannel = new NotificationChannel(
                            chanelId,
                            "Notification Vhennel 01",
                            NotificationManager.IMPORTANCE_HIGH
                    );
                    notificationChannel.setDescription("is used to notification");
                    notificationChannel.enableVibration(true);
                    notificationChannel.enableLights(true);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
            }
    
            Notification notification = builder.build();
            if (notificationManager != null) {
                notificationManager.notify(notificationID, notification);
            }
        }
}