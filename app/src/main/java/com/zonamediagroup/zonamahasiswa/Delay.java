package com.zonamediagroup.zonamahasiswa;

import android.os.Handler;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


//Custom timer adalah kelas untuk static function timer serba guna yang dapat digunakan oleh activity/kelas manapun

public class Delay {
    public  ScheduledExecutorService worker;

    public Delay(){
     worker = Executors.newSingleThreadScheduledExecutor();


    }

    public void delayTask(int time, Runnable job){ //time in millisecond
        new Handler().postDelayed(job, time);
    }
}
