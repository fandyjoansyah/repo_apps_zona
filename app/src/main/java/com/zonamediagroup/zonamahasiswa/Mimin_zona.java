package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Mimin_zona extends AppCompatActivity {

    ImageView back;
    WebView p4;
    LinearLayout miminZona;

    static String tag_json_obj = "json_obj_req";

    String URL_NOMOR_WA = Server.URL_REAL + "get_nomor_wa?TOKEN=qwerty";

    String nomorWa;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_mimin_zona);

        p4 = findViewById(R.id.p4);
        miminZona = findViewById(R.id.btn_hubungi_mimin_zona);

        //start adview
        AdView mAdView = (AdView) findViewById(R.id.adViewMimin);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        //end adview

        String x =
                "<p style=\"text-align: left;\">Halo Sobat Zona.<br />Selamat datang di Zona Mahasiswa. <br> Jika ada yang ingin ditanyakan atau masukan kepada kami, silahkan hubungi Mimin di sini.</p>";
        p4.loadData(x, "text/html; charset=utf-8", "UTF-8");

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        miminZona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(Mimin_zona.this);
                progressDialog.setMessage("Menghubungkan...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();
                //start volley get nomor wa
                StringRequest strReq = new StringRequest(Request.Method.GET, URL_NOMOR_WA, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jObj = new JSONObject(response);

                            if (jObj.getString("status").equals("true")) {
                                nomorWa =  jObj.getString("nomor_wa");
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://wa.me/"+nomorWa));
                                startActivity(intent);
                            }


                        } catch (JSONException e) {
                            // JSON error
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("mimin zona", "Register Error: " + error.getMessage());
                        progressDialog.dismiss();

                    }
                }) {


//            parameter

                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to login url
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("TOKEN", "qwerty");

                        return params;
                    }
                };


//        ini heandling requestimeout
                strReq.setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 10000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 10000;
                    }

                    @Override
                    public void retry(VolleyError error) throws VolleyError {

                        Log.e("mimin zona", "VolleyError Error: " + error.getMessage());
//                eror_show();
                    }
                });

                // Adding request to request queue
                MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
                //end volley get nomor wa
            }
        });

    }
}