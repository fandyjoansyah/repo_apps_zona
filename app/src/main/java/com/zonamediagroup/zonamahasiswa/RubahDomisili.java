package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.ybq.android.spinkit.SpinKitView;

import org.json.JSONObject;

public class RubahDomisili extends AppCompatActivity {
    private static final String URL_GET_CURRENT_USER_DATA = Server.URL_PROFIL_PENGGUNA+"Get_single_field_value_in_user";
    private static final String URL_CHANGE_USER_DATA = Server.URL_PROFIL_PENGGUNA+"Edit_single_field_value_in_user";
    private static final String FIELD = "alamat";
    ImageView btn_back, btn_reload;
    EditText edt_rubah_data;
    TextView error_for_edt_rubah_data,txt_simpan;
    RelativeLayout simpan_rubah_data;
    ProgressBar loading_simpan_rubah_data;
    LinearLayout layout_utama,layout_error;
    SpinKitView loading_rubah_profil;
    String idUser;
    Animation fade_in;
    String originalDataUser;
    Resources rs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_rubah_domisili);
        findViewByIdAllComponent();
        setListenerAllComponent();
        initAnimation();
        getIdUser();
        getCurrentUserData(idUser,FIELD);
        getAndroidResources();
    }


    private void getIdUser(){
        idUser = SharedPrefManagerZona.getInstance(RubahDomisili.this).getid_user();
    }

    private void getAndroidResources(){
        rs = getResources();
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(RubahDomisili.this,R.anim.fade_in_fast);
    }
    private void findViewByIdAllComponent(){
        btn_back = findViewById(R.id.btn_back);
        edt_rubah_data = findViewById(R.id.edt_rubah_data);
        simpan_rubah_data = findViewById(R.id.simpan_rubah_data);
        layout_utama = findViewById(R.id.layout_utama);
        loading_rubah_profil = findViewById(R.id.loading_rubah_profil);
        btn_reload = findViewById(R.id.btn_reload);
        layout_error = findViewById(R.id.layout_error);
        error_for_edt_rubah_data = findViewById(R.id.error_for_edt_rubah_data);
        loading_simpan_rubah_data = findViewById(R.id.loading_simpan_rubah_data);
        txt_simpan = findViewById(R.id.txt_simpan);
    }

    private void setListenerAllComponent(){
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        simpan_rubah_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prosesPerubahanData();
            }
        });
        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentUserData(idUser,FIELD);
            }
        });
        edt_rubah_data.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkEditTextValueChange();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkEditTextValueChange() {
        if(edt_rubah_data.getText().toString().trim().equals(originalDataUser))
        {
            stateBtnInactiveWhenThereIsNoChange();
        }
        else{
            stateBtnActive();
        }
    }

    private void prosesPerubahanData() {
        hideLayoutError();
        boolean hasilValidasi = validasiPerubahanData();
        if(hasilValidasi)
        {
            sendRequestChangeData(idUser,FIELD,edt_rubah_data.getText().toString().trim());
        }
    }

    private void setFocusToInput(){
        edt_rubah_data.requestFocus();
        if(edt_rubah_data.requestFocus()) {
            /* berikan aksi seolah2 ada pencetan ke edit text agar keyboard tampil*/
            edt_rubah_data.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0f, 0f, 0));
            edt_rubah_data.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0f, 0f, 0));

            /* mengarahkan kursor agar ke karakter terakhir dari edit text*/
            int pos = edt_rubah_data.getText().length();
            edt_rubah_data.setSelection(pos);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_transition_slide_from_left,R.anim.activity_transition_slide_to_right);
    }

    private void getCurrentUserData(String id,String field){
        hideReload();
        showLoadingHalaman();
        AndroidNetworking.post(URL_GET_CURRENT_USER_DATA)
                .addBodyParameter("TOKEN","qwerty")
                .addBodyParameter("id",id)
                .addBodyParameter("field",field)
                .setTag("Get Single Value")
                .setPriority(Priority.LOW)
                .build()

                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoadingHalaman();
                        showLayoutUtama();
                        stateBtnInactiveWhenThereIsNoChange();
                        //Log.d("3_juni_2022","RubahNama getCurrentUserData onResponse");
                        try{
                            String hasilDariServer = response.getJSONArray("responseProfilPengguna").getJSONObject(0).getString(FIELD);
                            //Log.d("3_juni_2022","RubahNama getCurrentUserData onResponse try hasilDariServer: "+hasilDariServer);
                            originalDataUser = hasilDariServer;
                            setTextToInput(hasilDariServer);

                            tutupSoftKeyboard();
                            //start delay
                            Runnable r = new Runnable() {
                                @Override
                                public void run() {
                                    setFocusToInput();
                                }
                            };
                            new Delay().delayTask(150,r);
                            //end delay

                        }
                        catch (Exception e)
                        {
                            // Log.d("3_juni_2022","RubahNama getCurrentUserData onResponse catch: "+e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {


                        Log.d("3_juni_2022","RubahNama getCurrentUserData onError: "+anError.getMessage());
                        hideLoadingHalaman();
                        CustomToast.makeBlackCustomToast(getApplicationContext(),rs.getString(R.string.permintaan_gagal_dieksekusi), Toast.LENGTH_LONG);
                        Log.d("3_juni_2022","error 165 " +anError.getErrorBody());
                        showReload();
                    }
                });
    }

    private void setTextToInput(String text){
        edt_rubah_data.setText(text);
    }

    private void sendRequestChangeData(String id, String field, String newValue){
        hideLabelBtnSimpan();
        showLoadingSimpan();
        AndroidNetworking.post(URL_CHANGE_USER_DATA)
                .addBodyParameter("TOKEN","qwerty")
                .addBodyParameter("id",id)
                .addBodyParameter("field",field)
                .addBodyParameter("newValue",newValue)
                .setTag("Update Single Value")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(JSONObject response) {
                        showLabelBtnSimpan();
                        hideLoadingSimpan();

                        try {
                            boolean berhasilMerubahData = response.getBoolean("status");
                            if(berhasilMerubahData)
                            {
                                changeBtnColorToColorBtnBerhasil();
                                disableBtnSimpan();
                                setTextLabelBtnSimpan(rs.getString(R.string.disimpan));
                                CustomToast.makeBlackCustomToast(getApplicationContext(),rs.getString(R.string.disimpan),Toast.LENGTH_SHORT);

                                new Delay().delayTask(300, new Runnable() {
                                    @Override
                                    public void run() {
                                        onBackPressed();
                                    }
                                });
                            }
                            else
                            {
                                CustomToast.makeBlackCustomToast(getApplicationContext(),rs.getString(R.string.data_tidak_ditemukan),Toast.LENGTH_SHORT);
                            }
                        }
                        catch (Exception e)
                        {
                            CustomToast.makeBlackCustomToast(getApplicationContext(),rs.getString(R.string.terjadi_error),Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        showLabelBtnSimpan();
                        hideLoadingSimpan();
                        CustomToast.makeBlackCustomToast(getApplicationContext(),rs.getString(R.string.permintaan_gagal_dieksekusi),Toast.LENGTH_SHORT);
                    }
                });
    }

    private boolean validasiPerubahanData(){
        //domisili tidak memiliki validasi
        return true;
    }



    private void setTextErrorLabel(String text){
        error_for_edt_rubah_data.setText(text);
    }



    private void hideLoadingHalaman(){
        loading_rubah_profil.setVisibility(View.GONE);
    }

    private void showLoadingHalaman(){
        loading_rubah_profil.setVisibility(View.VISIBLE);
    }

    private void showLayoutUtama(){
        layout_utama.startAnimation(fade_in);
        layout_utama.setVisibility(View.VISIBLE);
    }

    private void hideLayoutUtama(){
        layout_utama.setVisibility(View.GONE);
    }

    private void showReload(){
        btn_reload.setVisibility(View.VISIBLE);
    }

    private void hideReload(){
        btn_reload.setVisibility(View.GONE);
    }

    private void showLayoutError(){
        layout_error.startAnimation(fade_in);
        layout_error.setVisibility(View.VISIBLE);
    }

    private void hideLayoutError(){
        layout_error.setVisibility(View.GONE);
    }

    private void showLoadingSimpan(){
        loading_simpan_rubah_data.startAnimation(fade_in);
        loading_simpan_rubah_data.setVisibility(View.VISIBLE);
    }

    private void hideLoadingSimpan(){
        loading_simpan_rubah_data.setVisibility(View.GONE);
    }

    private void showLabelBtnSimpan(){
        txt_simpan.startAnimation(fade_in);
        txt_simpan.setVisibility(View.VISIBLE);
    }

    private void hideLabelBtnSimpan(){
        txt_simpan.setVisibility(View.GONE);
    }

    private void setTextLabelBtnSimpan(String text){
        txt_simpan.setText(text);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void changeBtnColorToColorBtnBerhasil(){
        simpan_rubah_data.startAnimation(fade_in);
        simpan_rubah_data.setBackgroundTintList(ContextCompat.getColorStateList(RubahDomisili.this,R.color.color_btn_berhasil));
    }

    private void tutupSoftKeyboard(){
        edt_rubah_data.onEditorAction(EditorInfo.IME_ACTION_DONE);
    }

    private void disableBtnSimpan(){
        simpan_rubah_data.setEnabled(false);
    }

    private void enableBtnSimpan(){
        simpan_rubah_data.setEnabled(true);
    }

    private void changeBtnColorToColorInactive(){

        simpan_rubah_data.setBackgroundTintList(ContextCompat.getColorStateList(RubahDomisili.this,R.color.color_btn_inactive));
    }

    private void changeBtnColorToColorActive(){

        simpan_rubah_data.setBackgroundTintList(ContextCompat.getColorStateList(RubahDomisili.this,R.color.color_btn_ganti_foto_profil));
    }

    private void changeTxtBtnColorToColorInactive(){

        txt_simpan.setTextColor(ContextCompat.getColorStateList(RubahDomisili.this,R.color.color_text_btn_inactive));
    }

    private void changeTxtBtnColorToColorActive(){

        txt_simpan.setTextColor(ContextCompat.getColorStateList(RubahDomisili.this,R.color.white));
    }

    private void stateBtnInactiveWhenThereIsNoChange(){
        disableBtnSimpan();
        changeBtnColorToColorInactive();
        changeTxtBtnColorToColorInactive();
    }

    private void stateBtnActive(){
        enableBtnSimpan();
        changeBtnColorToColorActive();
        changeTxtBtnColorToColorActive();
    }
}