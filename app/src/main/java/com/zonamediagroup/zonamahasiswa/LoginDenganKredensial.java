package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginDenganKredensial extends AppCompatActivity {
    ImageView img_eye;
    EditText edt_password, edt_username;
    TextView error_for_edt_username, error_for_ly_password,txt_daftar;
    RelativeLayout ly_keterangan_error_dari_server;
    TextView keterangan_error_dari_server, txt_lupa_kata_sandi;
    private static final int RC_SIGN_IN = 9001;
    Button btn_masuk;
    private GoogleSignInClient mGoogleSignInClient;
    RelativeLayout ly_btn_login_google;
    private static final String URL_MANUAL_LOGIN = Server.URL_DAFTAR_MASUK+"Manual_login";
    private static final String URL_CHECK_GMAIL_ON_SERVER = Server.URL_DAFTAR_MASUK+"Check_gmail_on_server";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_login_dengan_kredensial);
        img_eye = findViewById(R.id.img_eye);
        edt_username = findViewById(R.id.edt_username);
        //filter kustom agar tidak dapat diinputkan emoji
        edt_username.setFilters(EmojiFilter.getFilter());
        edt_password = findViewById(R.id.edt_password);
        error_for_edt_username = findViewById(R.id.error_for_edt_username);
        error_for_ly_password = findViewById(R.id.error_for_ly_password);
        txt_lupa_kata_sandi = findViewById(R.id.txt_lupa_kata_sandi);
        btn_masuk = findViewById(R.id.btn_masuk);
        ly_keterangan_error_dari_server = findViewById(R.id.ly_keterangan_error_dari_server);
        keterangan_error_dari_server = findViewById(R.id.keterangan_error_dari_server);
        ly_btn_login_google = findViewById(R.id.ly_btn_login_google);
        txt_daftar = findViewById(R.id.txt_daftar);
        txt_lupa_kata_sandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginDenganKredensial.this,LupaPassword.class));
                overridePendingTransition(R.anim.activity_transition_slide_from_right,R.anim.activity_transition_slide_to_left);
            }
        });
        txt_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginDenganKredensial.this,PendaftaranAkun.class));
            }
        });
        btn_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeteranganErrorDariServer();
                hideError();
               boolean validateInternal = validateLogin();
               if(validateInternal)
               {
                   loginManualKeServer(edt_username.getText().toString(),edt_password.getText().toString());
               }

            }
        });
        initGoogleSignInClient();
        ly_btn_login_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                daftarAtauMasukMenggunakanGoogle();
            }
        });
        img_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleEyeIcon();
                togglePassword();
            }
        });
        setEditTextOnFocusChangelistener();
    }

    private void setEditTextOnFocusChangelistener(){
        edt_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    validateEdtUsername();
                }
            }
        });

        edt_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    validateEdtPassword();
                }
            }
        });
    }

    private void togglePassword(){

        if(edt_password.getTransformationMethod() == null)
        {
            edt_password.setTransformationMethod(new PasswordTransformationMethod());
        }
        else
        {
            edt_password.setTransformationMethod(null);
        }
    }

    private void showKeteranganErrorDariServer(String keteranganError){
        ly_keterangan_error_dari_server.setVisibility(View.VISIBLE);
        keterangan_error_dari_server.setText(keteranganError);
    }

    private void hideKeteranganErrorDariServer(){
        ly_keterangan_error_dari_server.setVisibility(View.GONE);
    }

    private boolean saveSession(String id_user,String person_id,String person_name,String person_given_name,String person_family_name,String person_email,String person_photo, String person_gender,String username, String nama_anonim){

        // sesion save
        Boolean a = SharedPrefManagerZona.getInstance(getApplicationContext()).saveData(id_user, person_id, person_name,person_given_name,
                person_family_name, person_email, person_photo, person_gender,username,nama_anonim);
        if(a)
        {
            return true;
        }
        else{
            return false;
        }

    }




    private void loginManualKeServer(String email_or_username, String password){
        //start progressdialog
        ProgressDialog progressDialog = new ProgressDialog(LoginDenganKredensial.this);
        progressDialog.setMessage("Login...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        //end progressdialog

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_MANUAL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean status = jObj.getBoolean("status");
                    if (status) {
                        JSONObject data_login = jObj.getJSONArray("data_login").getJSONObject(0);
                        //start data
                        String id_user = data_login.getString("id");

                        /*person_id = id_ref_login_sosmed. Namun jika id_ref_login_sosmed tidak ada, maka person_id = id*/
                        String person_id;
                        if (data_login.getString("id_ref_login_sosmed").equals("")) {
                            person_id = data_login.getString("id");
                        } else {
                            person_id = data_login.getString("id_ref_login_sosmed");
                        }

                        /* di login DB native, person_name, person_given_name, person_family name diisi sama yaitu diisi nama, dikarenakan di DB native tidak memiliki person_given_name(nama depan) dan person_family_name(nama belakang)*/
                        String person_name = data_login.getString("nama");
                        String person_given_name = data_login.getString("nama");
                        String person_family_name = data_login.getString("nama");
                        String person_email = data_login.getString("email");
                        String person_photo = data_login.getString("image_user");
                        String person_gender = data_login.getString("jenis_kelamin");
                        String username = data_login.getString("username");
                        String nama_anonim = data_login.getString("nama_anonim");


                        //end data

                        //lanjut proses login
                        prosesMasukAplikasiZona(id_user,person_id,person_name,person_given_name,person_family_name,person_email,person_photo,person_gender,username,nama_anonim);

                    } else {
                        //gagal login
                        showKeteranganErrorDariServer(jObj.getString("error_message_for_user"));
                    }
                } catch (JSONException e) {
                    // JSON error

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//                dismissdialog();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("email_or_username", email_or_username);
                params.put("password", password);
                return params;
            }

        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("LoginDenganKredensial", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, RuleValueAplikasi.tag_json_obj);

    }

    private boolean prosesMasukAplikasiZona(String id_user,String person_id,String person_name,String person_given_name,String person_family_name,String person_email,String person_photo,String person_gender,String username,String nama_anonim){
        Log.d("5_agustus_2022","prosesMasukAplikasiZona()");
        boolean apakahHasilSessionBerhasil = saveSession(id_user,person_id,person_name,person_given_name,person_family_name,person_email,person_photo,person_gender,username,nama_anonim);
        if(apakahHasilSessionBerhasil)
        {
            Log.d("5_agustus_2022","prosesMasukAplikasiZona() if");
            subscribeToFirebaseTopics();
            startMainActivity();
            return true;
        }
        else{
            Log.d("5_agustus_2022","prosesMasukAplikasiZona() else");
            return false;
        }
    }

    private void subscribeToFirebaseTopics(){
        /*Subcribe ke topik. Toggle salah satu */
        String topic = "notif-zona-develop";
        //String topic = "notif-zona-production";

        FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }

    private void startMainActivity(){
        finish();
        startActivity(new Intent(LoginDenganKredensial.this, MainActivity.class));
    }

    private void toggleEyeIcon(){
        if(img_eye.getDrawable().getConstantState().equals(img_eye.getContext().getDrawable(R.drawable.eye).getConstantState()))
        {
            funcTransisiGambar(img_eye,img_eye.getContext().getDrawable(R.drawable.eye_slash),150,150);
        }
        else if(img_eye.getDrawable().getConstantState().equals(img_eye.getContext().getDrawable(R.drawable.eye_slash).getConstantState()))
        {

            funcTransisiGambar(img_eye,img_eye.getContext().getDrawable(R.drawable.eye),150,150);
        }
        else
        {
            funcTransisiGambar(img_eye,getApplicationContext().getDrawable(R.drawable.eye_slash),150,150);
        }
    }

    private boolean validateLogin(){
        boolean validate = true;
        if(validateEdtUsername() != true)
        {
            validate = false;
        }
        if(validateEdtPassword() != true)
        {
            validate = false;
        }

        return validate;
    }

    private boolean validateEdtUsername(){
        boolean validate = true;
        if(edt_username.getText().toString().trim().length() == 0){
            showErrorInformationAndTextViewError(error_for_edt_username,"Email/username tidak boleh kosong");
            validate = false;
        }
        /*cek jika pengguna menggunakan @, maka itu artinya pengguna mencoba login lewat email, maka lakukan validasi domain*/
        else if(edt_username.getText().toString().contains("@"))
        {
            if(!EmailValidator.checkValidasiDomainAllowed(edt_username.getText().toString()))
            {
                showErrorInformationAndTextViewError(error_for_edt_username,getResources().getString(R.string.format_penulisan_email_salah));
                validate = false;
            }
        }
        return validate;
    }

    private boolean validateEdtPassword(){
        boolean validate = true;
        if(edt_password.getText().toString().trim().length() == 0){
            showErrorInformationAndTextViewError(error_for_ly_password,"Password tidak boleh kosong");
            validate = false;
        }
        return validate;
    }

    private void hideError(){
        error_for_edt_username.setVisibility(View.INVISIBLE);
        error_for_ly_password.setVisibility(View.INVISIBLE);
    }

    private void showErrorInformationAndTextViewError(TextView tvError, String pesanError){
        tvError.setText(pesanError);
        tvError.setVisibility(View.VISIBLE);
    }


    private void daftarAtauMasukMenggunakanGoogle(){
        Log.d("5_agustus_2022","daftarAtauMasukMenggunakanGoogle()");
        trySignInWithGoogle();
    }

    private void initGoogleSignInClient(){
        mGoogleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(com.zonamediagroup.zonamahasiswa.LoginDenganKredensial.this.getResources().getString(R.string.default_web_client_id_login))
                .requestEmail()
                .build());
    }

    private void trySignInWithGoogle() {
        Log.d("5_agustus_2022","trySignInWithGoogle()");
        try {
            Log.d("5_agustus_2022","trySignInWithGoogle() try");
            //  CustomToast.s(getApplicationContext(),"try sign in");
            startActivityForResult(mGoogleSignInClient.getSignInIntent(), RC_SIGN_IN);
        }
        catch(Exception e){
            Log.d("5_agustus_2022","trySignInWithGoogle() catch");
            //  CustomToast.s(getApplicationContext(),"catch sign in "+e.getMessage());
        }
    }


    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("5_agustus_2022","onActivityResult()");

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Log.d("5_agustus_2022","onActivityResult() if");
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            getDataPersonFromGmail(task);
            if(data != null)
            {
                Log.d("5_agustus_2022","onActivityResult() if if");
                Log.d(Bank_Tag.LOGIN,"data not null");
            }
            else{
                Log.d("5_agustus_2022","onActivityResult() if else");
                Log.d(Bank_Tag.LOGIN,"data null");
            }
            Log.d(Bank_Tag.LOGIN,"if onActivityResult");
        }
        else
        {
            Log.d(Bank_Tag.LOGIN,"else onActivityResult");
        }
    }
    // [END onActivityResult]

    private void getDataPersonFromGmail(Task<GoogleSignInAccount> completedTask){
        try {
            Log.d("5_agustus_2022","getDataPersonFromGmail() try");
            GoogleSignInAccount mGoogleSignInAccount = completedTask.getResult(ApiException.class);

            /*start ambil data akun google*/
            final String personName = mGoogleSignInAccount.getDisplayName();
            final String personGivenName = mGoogleSignInAccount.getGivenName();
            final String personFamilyName = mGoogleSignInAccount.getFamilyName();
            final String personEmail = mGoogleSignInAccount.getEmail();
            final String personIdGoogle = String.valueOf(mGoogleSignInAccount.getId());
            Uri personPhoto = mGoogleSignInAccount.getPhotoUrl();
            final String urlvoto = String.valueOf(mGoogleSignInAccount.getPhotoUrl());
            /*end ambil data akun google */
            checkGmailOnServer(personEmail,personName,urlvoto,personIdGoogle);
//            CustomToast.l(getApplicationContext(),"email user: "+personEmail);


        } catch (ApiException e) {
            Log.d("5_agustus_2022","getDataPersonFromGmail() catch "+e.getMessage());
            e.printStackTrace();
        }
    }

    private void checkGmailOnServer(String gmail,String personName,String personPhoto, String personIdGoogle){
        Log.d("5_agustus_2022","checkGmailOnServer()");
        mGoogleSignInClient.signOut();
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_CHECK_GMAIL_ON_SERVER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    Log.d("5_agustus_2022","checkGmailOnServer() onResponse try");

                    JSONObject jObj = new JSONObject(response);
                    if(jObj.getBoolean("status")){
                        Log.d("5_agustus_2022","checkGmailOnServer() onResponse try if");
                        //  email google sudah terdaftar, lanjut login
                        //lanjut proses login
                        JSONArray jsonArrDataEmailDariServer = jObj.getJSONArray("data_login");
                        JSONObject jx = jsonArrDataEmailDariServer.getJSONObject(0);
                        prosesMasukAplikasiZona(jx.getString("id"),jx.getString("id"),jx.getString("nama"),jx.getString("nama"),jx.getString("nama"),jx.getString("email"),jx.getString("image_user"),jx.getString("jenis_kelamin"),jx.getString("username"),jx.getString("nama_anonim"));
                    }
                    else{
                        Log.d("5_agustus_2022","checkGmailOnServer() onResponse try else");
                        //email google belum terdaftar, lanjut ke halaman daftar dengan membawa beberapa data dari gmail untuk form pendaftaran
                        CustomToast.makeCustomToast(LoginDenganKredensial.this,"Akunmu belum terdaftar, yuk daftar terlebih dahulu",CustomToast.JENIS_TOAST_INFORMATION,Toast.LENGTH_LONG,true);

                        Intent i = new Intent(LoginDenganKredensial.this,PendaftaranAkun.class);
                        i.putExtra("gmail",gmail);
                        i.putExtra("personName",personName);
                        i.putExtra("personPhoto",personPhoto);
                        i.putExtra("personIdGoogle",personIdGoogle);
                        startActivity(i);
                    }
                }
                catch (Exception e) {
                    Log.d("5_agustus_2022","checkGmailOnServer() onResponse catch "+e.getMessage());
                    // JSON error
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("5_agustus_2022","checkGmailOnServer() onErrorResponse"+error.getMessage());
//                dismissdialog();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("gmail", gmail);
                return params;
            }

        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("LoginDenganKredensial", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, RuleValueAplikasi.tag_json_obj);
    }



    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }
}