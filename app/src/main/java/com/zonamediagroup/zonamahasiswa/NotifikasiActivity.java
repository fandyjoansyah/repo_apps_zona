package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Notif_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Notif_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotifikasiActivity extends AppCompatActivity implements Notif_Adapter.ContactsAdapterListener {

    RecyclerView receler_notif;
    private List<Notif_Model> receler_notifModels;
    private Notif_Adapter receler_notifAdapter;

    private static final String TAG = Home.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";
    private String URL_home = Server.URL_REAL + "Get_notifikasi/?TOKEN=qwerty";


    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_notifikasi_activity);
//        /*start sementara comment
        //start init dari fragment notifikasi

        receler_notif = findViewById(R.id.receler_satu_notifikasi);

        receler_notifModels = new ArrayList<>();
        receler_notifAdapter = new Notif_Adapter(getApplicationContext(), receler_notifModels, this);
        LinearLayoutManager mLayoutManagerkategori;
        mLayoutManagerkategori = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        receler_notif.setLayoutManager(mLayoutManagerkategori);
        receler_notif.setItemAnimator(new DefaultItemAnimator());
        receler_notif.setAdapter(receler_notifAdapter);


//        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);

//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();
// Call API
                get_notif();

            }
        });


        search = findViewById(R.id.search);
        saveok = findViewById(R.id.saveok);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Search.class);
                startActivity(intent);
            }
        });


        eror_dismiss();
        loading_show();

//        Call API
        get_notif();
        //end init dari fragment notifikasi
//        end sementara comment*/
    }


    private void get_notif() {


        StringRequest strReq = new StringRequest(Request.Method.GET, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data Global");


                    if (jObj.getString("status").equals("true")) {


                        JSONArray dataArray = jObj.getJSONArray("data");


                        List<Notif_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Notif_Model>>() {
                        }.getType());

                        receler_notifModels.clear();
                        receler_notifModels.addAll(items);
                        receler_notifAdapter.notifyDataSetChanged();


                        loading_dismiss();

                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error

                    eror_show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());

                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    @Override
    public void onContactSelected(Notif_Model notif_data) {
        String jenis_notif=notif_data.getJenis_notifikasi();

        if(jenis_notif.equals("Download")){
            // to dowload dfile
            Intent intent = new Intent(getApplicationContext(), Download_file.class);
            startActivity(intent);

        }else if(jenis_notif.equals("Poling")){
            // to page poling
            Intent intent = new Intent(getApplicationContext(), Detail_poling_by_id.class);
            intent.putExtra("id_poling", notif_data.getId_reverensi());
            startActivity(intent);

        }else{
            // to post by id
            Intent intent = new Intent(getApplicationContext(), Detail_by_id.class);
            intent.putExtra("id_post", notif_data.getId_reverensi());
            startActivity(intent);
        }


    }
}