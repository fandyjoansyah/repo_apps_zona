package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Detail_poling_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.Poling_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Detail_poling_Model;
import com.zonamediagroup.zonamahasiswa.models.Poling_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TampilGrafikPollingActivity extends AppCompatActivity implements Poling_Adapter.InterHasilPollingListener {
    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;
    FrameLayout frameLayoutTampilGrafik;
    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    TextView keterangan_berlangsungnya_poling, BelumAdaVote;

    private static final String TAG = "TampilGrafikPollingActivity";

    static String tag_json_obj = "json_obj_req";
    int eror_param = 0;
    private String URL_home = Server.URL_REAL + "Get_jumlah_polling";
    private String URL_get_gambar_pemenang = Server.URL_REAL + "Get_gambar_pemenang";
    String idPolling = "";
    String titlePolling = "";
    String urlGambarPemenang = "";
    boolean polling_selesai = false;
    ImageView back;
    ImageView search;
    ImageView saveok;

    String pemenangPertama;


    //start komponen popup selebrasi
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog dialog;
    //end komponen popup selebrasi
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.page_tampil_grafik_polling);
        //dapatkan idPolling untuk diteruskan ke volley untuk dicari jummlah vote dari setiap pilihan polling dari idPolling tersebut
        back = findViewById(R.id.back_tampil_grafik);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });
        idPolling = getIntent().getStringExtra("idPolling");
        titlePolling = getIntent().getStringExtra("titlePolling");
        polling_selesai = getIntent().getBooleanExtra("status_polling",false);
        //        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);

        BelumAdaVote = findViewById(R.id.txt_belum_ada_vote);

        frameLayoutTampilGrafik = findViewById(R.id.diskoncontainer_tampil_grafik);

        keterangan_berlangsungnya_poling = findViewById(R.id.keterangan_berlangsungnya_poling);
        if(polling_selesai == true)
        {
            keterangan_berlangsungnya_poling.setText("Hasil Akhir Perolehan Polling");
            //code tampil pop up namun belum berhasil
            //Intent i = new Intent(TampilGrafikPollingActivity.this, Pop.class);
            //startActivity(i);
        }
        //        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        //start adview
        AdView mAdView = (AdView) findViewById(R.id.adViewHasilPolling);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        //end adview

//        coba_lagi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                eror_dismiss();
//                loading_show();
//                // Call API
//                funcGetTotalPolling(idPolling);
//            }
//        });



        //lakukan volley
        funcGetTotalPolling(idPolling,titlePolling);

        search = findViewById(R.id.search);
        saveok = findViewById(R.id.saveok);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TampilGrafikPollingActivity.this, Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TampilGrafikPollingActivity.this, Search.class);
                startActivity(intent);
            }
        });
    }

    private void getUrlGambarPemenang(String pilihanMenang)
    {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_get_gambar_pemenang, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true"))
                    urlGambarPemenang = jObj.getJSONArray("data").getJSONObject(0).getString("img_tokoh");
                    Log.d("urlnya",urlGambarPemenang);

                } catch (JSONException e) {
                    eror_show();
                    e.printStackTrace();
                }
                tampilPopUpSelebrasi(pilihanMenang,titlePolling,urlGambarPemenang);
                Log.d("line192",urlGambarPemenang);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                eror_show();
            }
        }) {

//            parameter
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("pilihan_menang", pilihanMenang);
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("DebugZona", "VolleyError Error: " + error.getMessage());

            }
        });

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private ShapeAppearanceModel getCornerGambarRounded()
    {
        //menjadikan gambar menjadi rounded
        return  new ShapeAppearanceModel()
                .toBuilder()
                .setAllCorners(CornerFamily.ROUNDED,30)
                .build();

    }

    private void tampilPopUpSelebrasi(String pilihanMenang, String namaKategori, String urlGambar) {
        TextView tvPemenang, tvKategori;
        ShapeableImageView imgWinner;
        ImageView lihatHasil;
        dialogBuilder = new AlertDialog.Builder(TampilGrafikPollingActivity.this);
        final View vPopUp = getLayoutInflater().inflate(R.layout.popup_selebrasi,null);

        tvPemenang = vPopUp.findViewById(R.id.tv_pemenang);
        tvKategori = vPopUp.findViewById(R.id.tv_kategori_pemenang);
        imgWinner = vPopUp.findViewById(R.id.img_popup_gambar_pemenang);
        lihatHasil = vPopUp.findViewById(R.id.img_lihat_hasil_popup);
        imgWinner.setShapeAppearanceModel(getCornerGambarRounded());
        Glide.with(TampilGrafikPollingActivity.this)
                .load(urlGambar)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        loading_dismiss();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        loading_dismiss();
                        return false;
                    }
                })
                .into(imgWinner)
        ;
        lihatHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvPemenang.setText(pilihanMenang);
        tvKategori.setText(namaKategori);

        dialogBuilder.setView(vPopUp);
        dialog = dialogBuilder.create();
        dialog.show();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
        //lebar dialog n%
        //tinggi dialog n%
        int width = (int)(getResources().getDisplayMetrics().widthPixels*1.00);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.77);
        dialog.getWindow().setLayout(width, height);

        KonfettiView konfettiView = vPopUp.findViewById(R.id.viewKonfetti);

        konfettiView.build()
                .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(2000L)
                .addShapes(Shape.Square.INSTANCE, Shape.Circle.INSTANCE)
                .addSizes(new Size(12, 5f))
                .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                .streamFor(300, 5000L);
    }

    //Volley hubungi API untuk mendapatkan data jumlah polling untuk ditampilkan ke chart
    private void funcGetTotalPolling(String id_poling, String titlePolling)
    {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));
                    Log.d("DebugZona","onResponse called "+jObj.getString("status"));
                    System.out.println("Data POLING " + response.toString());



                    if (jObj.getString("status").equals("true")) {


                        JSONArray dataArray = jObj.getJSONArray("data");
                        List<Detail_poling_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Detail_poling_Model>>() {
                        }.getType());

                        //dapatkan siapa pemenang juara 1 polling
                       pemenangPertama = getPemenangPolling(items);

                       //panggil popup ketika tanggal telah berakhir
                        if(polling_selesai == true)
                            getUrlGambarPemenang(pemenangPertama);
                            //tampilPopUpSelebrasi(pemenangPertama,titlePolling,urlGambarPemenang);
                        else
                        loading_dismiss();

                        //set data hasil volley ke chart
                        setChart(items,titlePolling);

                    } else if (jObj.getString("status").equals("false")) {

                        loading_dismiss();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_param = 1;
                    eror_show();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("DebugZona","onErrorResponse called");
                Log.e("DebugZona", "Register Error: " + error.getMessage());
                eror_param = 1;
                eror_show();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_poling", id_poling);
                return params;
            }
        };


        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e("DebugZona", "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private String getPemenangPolling(List<Detail_poling_Model> items) {
        int totalVote = 0;
        String juara1 = "";
        int sizeLoop = items.size();
        for(int i=0;i<sizeLoop;i++)
        {
           int currentItemsTotalVotes = Integer.valueOf(items.get(i).getTotal_vote());
           if(currentItemsTotalVotes > totalVote)
            {
                totalVote = currentItemsTotalVotes;
                juara1 = items.get(i).getNama_tokoh();
            }
        }
        Log.d(TAG, "getPemenangPolling: totalVote: "+totalVote);
        Log.d(TAG, "getPemenangPolling: juara 1: "+juara1);
        Log.d(TAG, "getPemenangPolling: itemSize "+sizeLoop);
        return juara1;
    }

    //fungsi untuk memasang data vote hasil volley ke chart.
    public void setChart(List<Detail_poling_Model> items, String titlePolling)
    {
        //warna pie. diambil sama seperti warna pada google chart.
        final int[] MY_COLORS = {Color.parseColor("#a273d9"),Color.parseColor("#9c450e"),Color.parseColor("#14034a"),Color.parseColor("#a24fa4"),Color.parseColor("#bbc929"),Color.parseColor("#49725a"),Color.parseColor("#7b44fc"),Color.parseColor("#d2a67d"),Color.parseColor("#1c0ef9"),Color.parseColor("#3627b6"),Color.parseColor("#be77a0"),Color.parseColor("#e917ca"),Color.parseColor("#736a10"),Color.parseColor("#af13f5"),Color.parseColor("#055863"),Color.parseColor("#a5cc31"),Color.parseColor("#e8a32b"),Color.parseColor("#a043da"),Color.parseColor("#8e50f2"),Color.parseColor("#71d966"),};
        ArrayList<Integer> colorsX = new ArrayList<Integer>();

        for(int c: MY_COLORS) colorsX.add(c);


        PieChart chartGrafik = findViewById(R.id.grafik_chart);

        ArrayList<PieEntry> arrayListDataVoting = new ArrayList<>();


        //perulangan di dalam List items, lalu hasilnya add kan ke arrayList nya param chart
        for(int i=0; i<items.size();i++)
        {
            if(Integer.valueOf(items.get(i).getTotal_vote()) > 0)
            {
                arrayListDataVoting.add(new PieEntry(Float.parseFloat(items.get(i).getTotal_vote()), items.get(i).getNama_tokoh()));
            }
        }
        //start vv
        if(arrayListDataVoting.isEmpty() == true)
        {
            chartGrafik.setVisibility(View.GONE);
            BelumAdaVote.setVisibility(View.VISIBLE);
        }
        else
        {
            chartGrafik.setVisibility(View.VISIBLE);
            BelumAdaVote.setVisibility(View.GONE);
        }
        //end vv
        PieDataSet pieDataSet = new PieDataSet(arrayListDataVoting, "");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieDataSet.setValueTextColor(Color.parseColor("#f7f7f7"));
        pieDataSet.setValueTextSize(16f);
        pieDataSet.setValueFormatter(new PercentFormatter());
        pieDataSet.setColors(colorsX);

        PieData pieData = new PieData(pieDataSet);



        //start new
        chartGrafik.setEntryLabelTextSize(7f);
        chartGrafik.setEntryLabelColor(Color.parseColor("#f7f7f7"));
        //end new
        chartGrafik.setNoDataText("");
        chartGrafik.invalidate();
        chartGrafik.setData(pieData);
        chartGrafik.getDescription().setEnabled(false);
        chartGrafik.setCenterText(titlePolling);
        chartGrafik.animate();

        chartGrafik.getLegend().setEnabled(false);

    }



    @Override
    public void onHasilPollingClick(Poling_Model detailPolling) {
        Log.d("DebugZona","TGPA "+detailPolling.getId_poling());
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
        frameLayoutTampilGrafik.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {
        eror_layar.setVisibility(View.GONE);
        frameLayoutTampilGrafik.setVisibility(View.GONE);
    }

    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }
    public void loading_dismiss() {
        loding_layar.setVisibility(View.GONE);
    }



    //class untuk memformat hasil piechart agar tidak menampilkan koma setelah digit nilai voting
    public class PercentFormatter extends ValueFormatter {
        public DecimalFormat mFormat;
        private PieChart pieChart;

        public PercentFormatter() {
            mFormat = new DecimalFormat("###,###,##");
        }
        public PercentFormatter(PieChart pieChart) {
            this();
            this.pieChart = pieChart;
        }

        @Override
        public String getFormattedValue(float value) {
            return mFormat.format(value) + " %";
        }

        @Override
        public String getPieLabel(float value, PieEntry pieEntry) {
            if (pieChart != null && pieChart.isUsePercentValuesEnabled()) {
                // Converted to percent
                return getFormattedValue(value);
            } else {
                // raw value, skip percent sign
                return mFormat.format(value);
            }
        }
    }

}