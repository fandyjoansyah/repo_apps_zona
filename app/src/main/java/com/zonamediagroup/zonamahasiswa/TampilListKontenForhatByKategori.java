package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.KontenForhatAdapter;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClientForhat;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi;
import com.zonamediagroup.zonamahasiswa.models.KeteranganKontenHabisModel;
import com.zonamediagroup.zonamahasiswa.models.PengenalanForhatModel;
import com.zonamediagroup.zonamahasiswa.models.TulisCurhatanModel;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KontenForhatRecyclerView_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class TampilListKontenForhatByKategori extends AppCompatActivity implements KontenForhatAdapter.ListKontenForhatListener {

    public static final int ITEMS_PER_AD = 5;

    RelativeLayout btn_search_forhat;

    Resources rs;
    RecyclerView.LayoutManager layoutManagerRecyclerViewKontenForhat;

    boolean isLoadingShimmerActive = false;

    SwipeRefreshLayout swipe_refresh;

    boolean semuaKontenSudahDitampilkan = false;

    String idUser, kategori;

    ShimmerFrameLayout shimmer_framelayout;


    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    String pageNum;
    ProgressBar loadingLoadMore;
    FrameLayout layout_include_tampil_list_konten_forhat_by_kategori;
    Animation fade_in;
    private static final String FILTER = "Kategori";

    // start section init recycler view
    RecyclerView recycler_tampil_list_konten_forhat_by_kategori;
    ArrayList<Object> kontenForhat_RecyclerView_model;
    KontenForhatAdapter kontenForhatadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_tampil_list_konten_forhat_by_kategori);
        getKategoriExtra();
        findViewByIdAllComponent();
        initAnimation();
        initAdMobAdsSDK();
        initRecyclerKontenForhat();
        setListenerAllComponent();
        setIdUser(getIdUserFromSharedPreferences());
        setRs(getResources());
        startLoadingShimmer();
        getKontenBySelectedKategori(FILTER,pageNum,kategori);
    }

    private void getKategoriExtra(){
        kategori = getIntent().getStringExtra("kategori");
    }

    private void findViewByIdAllComponent() {
        btn_search_forhat = findViewById(R.id.btn_search_forhat);
        recycler_tampil_list_konten_forhat_by_kategori = findViewById(R.id.recycler_tampil_list_konten_forhat_by_kategori);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        shimmer_framelayout = findViewById(R.id.shimmer_framelayout);
        layout_include_tampil_list_konten_forhat_by_kategori = findViewById(R.id.layout_include_tampil_list_konten_forhat_by_kategori);
        loadingLoadMore = (ProgressBar) findViewById(R.id.loadingLoadMore);
    }

//    start copas

    public void setIdUser(String idUser){
        this.idUser = idUser;
    }

    public String getIdUserFromSharedPreferences(){
        return SharedPrefManagerZona.getInstance(TampilListKontenForhatByKategori.this).getid_user();
    }

    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(TampilListKontenForhatByKategori.this,R.anim.fade_in);
    }


    private void setRs(Resources rs){
        this.rs = rs;
    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setListenerAllComponent(){
        btn_search_forhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TampilListKontenForhatByKategori.this, ForhatSearchActivity.class));
            }
        });
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getKontenBySelectedKategori(FILTER,pageNum,kategori);
                new Delay().delayTask(500, new Runnable() {
                    @Override
                    public void run() {
                        swipe_refresh.setRefreshing(false);
                    }
                });

            }
        });
        recycler_tampil_list_konten_forhat_by_kategori.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d("9_juli_2022","RecyclerView has reached bottom");
                    mekanismeLoadMore();
                }
            }
        });
    }

    private void mekanismeLoadMore(){
        if(semuaKontenSudahDitampilkan == false)
        {
            Log.d("7_juli_2022","mekanismeLoadMore()");
            showLoadingLoadMore();
            getKontenBySelectedKategori(FILTER,pageNum,kategori);
        }
    }


    //start section init recycler view
    private void initRecyclerKontenForhat(){
        kontenForhat_RecyclerView_model = new ArrayList<>();
        //start temporary set data dummy
        //kontenForhat_model = generateDataDummy();
        //end temporary set data dummy
        kontenForhatadapter = new KontenForhatAdapter(TampilListKontenForhatByKategori.this, kontenForhat_RecyclerView_model,this);
        layoutManagerRecyclerViewKontenForhat = new LinearLayoutManager(TampilListKontenForhatByKategori.this,RecyclerView.VERTICAL,false);
        recycler_tampil_list_konten_forhat_by_kategori.setLayoutManager(layoutManagerRecyclerViewKontenForhat);
        recycler_tampil_list_konten_forhat_by_kategori.setItemAnimator(new DefaultItemAnimator());
        recycler_tampil_list_konten_forhat_by_kategori.setAdapter(kontenForhatadapter);

        /*start optimization*/
        recycler_tampil_list_konten_forhat_by_kategori.setHasFixedSize(true);
        recycler_tampil_list_konten_forhat_by_kategori.setItemViewCacheSize(20);
        recycler_tampil_list_konten_forhat_by_kategori.setDrawingCacheEnabled(true);
        recycler_tampil_list_konten_forhat_by_kategori.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        /*end optimization*/
    }
    //end section init recycler view


    public void startLoadingShimmer(){
        isLoadingShimmerActive = true;
        shimmer_framelayout.startShimmerAnimation();
        shimmer_framelayout.setVisibility(View.VISIBLE);
        layout_include_tampil_list_konten_forhat_by_kategori.setVisibility(View.GONE);
        isLoadingShimmerActive = true;
    }

    public void endLoadingShimmer(){
        isLoadingShimmerActive = false;
        shimmer_framelayout.stopShimmerAnimation();
        shimmer_framelayout.setVisibility(View.GONE);
        layout_include_tampil_list_konten_forhat_by_kategori.startAnimation(fade_in);
        layout_include_tampil_list_konten_forhat_by_kategori.setVisibility(View.VISIBLE);
    }


    private void notifyDataSetChangeHomeAdapter(){
        kontenForhatadapter.notifyDataSetChanged();
    }

    private void showLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.VISIBLE);
    }

    private boolean isLoadingLoadMoreActive(){
        if(loadingLoadMore.getVisibility() == View.VISIBLE)
            return true;
        else
            return false;
    }

    private void hideLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.GONE);
    }


    private void setDataKonten(JSONArray arrayData){
        Log.d("6_juli_2022","setDataKonten()");
        List<KontenForhatRecyclerView_Model> dataForhat = new Gson().fromJson(arrayData.toString(), new TypeToken<List<KontenForhatRecyclerView_Model>>() {
        }.getType());
        kontenForhat_RecyclerView_model.addAll(dataForhat);
        addAdMobBannerAds();
        notifyDataSetChangeHomeAdapter();
    }

    private void showToastSemuaKontenSudahDitampilkan(){
        CustomToast.makeBlackCustomToast(TampilListKontenForhatByKategori.this,rs.getString(R.string.kamu_sudah_menampilkan_seluruh_konten),Toast.LENGTH_SHORT);
    }



    private void getKontenBySelectedKategori(String filter,String pageNum, String kategori)
    {
        Log.d("26_juli_2022","getKontenBySelectedKategori(). filter: "+filter);
        Log.d("26_juli_2022","getKontenBySelectedKategori(). pageNum: "+pageNum);
        Log.d("26_juli_2022","getKontenBySelectedKategori(). kategori: "+kategori);
        InterfaceApi api = ApiClientForhat.getClient().create(InterfaceApi.class);
        Call<JsonObject> call = api.getKontenBySelectedKategoriRetrofit(filter,pageNum,kategori);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> responseRetrofit) {
                Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onResponse");
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
                if(isLoadingShimmerActive==true)
                    endLoadingShimmer();
                try{
                    Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onResponse try");
                    Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onResponse try");
                    if(responseRetrofit.isSuccessful())
                    {
                        Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onResponse isSuccesful");
                    }
                    else
                    {
                        Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onResponse isNotSuccesful (empty)");
                    }
                    JSONObject response =new JSONObject(responseRetrofit.body().toString());

                    if(response.getInt("status") == 0)
                    {
                        //jika status = 0 maka konten sudah habis
                        if(semuaKontenSudahDitampilkan == false) {
                            semuaKontenSudahDitampilkan = true;
                            addKeteranganKontenHabis();
                            notifyDataSetChangeHomeAdapter();
                        }
                        showToastSemuaKontenSudahDitampilkan();
                    }
                    else if (response.getInt("status") == 1)
                    {
                        //jika status = 1 maka ada konten diterima
                        Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit() onResponse try. response message="+response.getString("message"));
                        //cek apakah data adalah jsonArray
                        if(response.get("data") instanceof JSONArray) {
                            Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit() onResponse try if data instance of Array = true");
                            JSONArray arrayData = response.getJSONArray("data");
                            setDataKonten(arrayData);
                        }
                        setPageNum(response.getString("page"));
                    }
                    //dapatkan data konten(array)
                } catch (JSONException e) {
                    Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onResponse catch: "+e.getMessage());
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("9_juli_2022","getKontenBySelectedKategoriRetrofit onFailure "+t.getMessage());
                if(isLoadingLoadMoreActive() == true)
                    hideLoadingLoadMore();
            }
        });
    }




    @Override
    public void like(String idUser, String idKonten) {
        Log.d("29_juni_2022","method like idUser: "+idUser);
        Log.d("29_juni_2022","method like idKonten: "+idKonten);
        AndroidNetworking.post(ServerForhat.URL_POST_LIKE)
                .addBodyParameter("id_curhatan", idKonten)
                .addHeaders("id",idUser)
                .setTag("toggle_like")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    @Override
    public void openBacaKonten(String slug) {
        Intent i = new Intent(TampilListKontenForhatByKategori.this, ForhatBaca.class);
        i.putExtra("slug",slug);
        startActivity(i);
    }

    private void initAdMobAdsSDK()
    {
        MobileAds.initialize(TampilListKontenForhatByKategori.this, new OnInitializationCompleteListener()
        {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus)
            {
            }
        });
    }

    /*menambahkan object PengenalanForhatModel ke recycler view*/
    private void addPengenalanForhat(int posisi){
        PengenalanForhatModel pFM = new PengenalanForhatModel();
        kontenForhat_RecyclerView_model.add(posisi,pFM);
    }

    private void addKeteranganKontenHabis()
    {
        KeteranganKontenHabisModel kKHM = new KeteranganKontenHabisModel();
        kontenForhat_RecyclerView_model.add(kKHM);
    }

    /*menambahkan object tulis curhatan ke recycler view*/
    private void addTulisCurhatan(int posisi){
        TulisCurhatanModel tCM = new TulisCurhatanModel();
        kontenForhat_RecyclerView_model.add(posisi,tCM);
    }

    private void addAdMobBannerAds()
    {
        Log.d("6_juli_2022","size awal: "+kontenForhat_RecyclerView_model.size());
        for (int i = ITEMS_PER_AD; i < kontenForhat_RecyclerView_model.size(); i += ITEMS_PER_AD)
        {
            //skip jika item berupa iklan
            if(kontenForhat_RecyclerView_model.get(i) instanceof AdView){
                continue;
            }
            else{
                final AdView adView = new AdView(TampilListKontenForhatByKategori.this);
                adView.setAdSize(AdSize.LEADERBOARD);
                adView.setAdUnitId(getResources().getString(R.string.ad_unit_id_banner));
                kontenForhat_RecyclerView_model.add(i, adView);
                Log.d("6_juli_2022","item ads added at: "+i);
                Log.d("6_juli_2022","Structure Data After insert ads: ");
                for(int j=0;j<kontenForhat_RecyclerView_model.size();j++)
                {
                    if(kontenForhat_RecyclerView_model.get(j) instanceof KontenForhatRecyclerView_Model)
                    {
                        Log.d("6_juli_2022","<<data>>");
                    }
                    else if(kontenForhat_RecyclerView_model.get(j) instanceof AdView)
                    {
                        Log.d("6_juli_2022","<<adView>>");
                    }
                }

            }
        }

        loadBannerAds();
    }

    private void loadBannerAds()
    {
        //Load the first banner ad in the items list (subsequent ads will be loaded automatically in sequence).
        loadBannerAd(ITEMS_PER_AD);
    }

    private void loadBannerAd(final int index)
    {
        if (index >= kontenForhat_RecyclerView_model.size())
        {
            return;
        }

        Object item = kontenForhat_RecyclerView_model.get(index);
        if (!(item instanceof AdView))
        {
            throw new ClassCastException("Expected item at index " + index + " to be a banner ad" + " ad.");
        }

        final AdView adView = (AdView) item;

        // Set an AdListener on the AdView to wait for the previous banner ad
        // to finish loading before loading the next ad in the items list.
        adView.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                super.onAdLoaded();
                // The previous banner ad loaded successfully, call this method again to
                // load the next ad in the items list.
                loadBannerAd(index + ITEMS_PER_AD);
            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                // The previous banner ad failed to load. Call this method again to load
                // the next ad in the items list.
                Log.e("MainActivity", "The previous banner ad failed to load. Attempting to"
                        + " load the next banner ad in the items list.");
                loadBannerAd(index + ITEMS_PER_AD);
            }
        });

        // Load the banner ad.
        adView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onResume() {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.resume();
            }
        }
        super.onResume();
    }

    @Override
    public void onPause()
    {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.pause();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        for (Object item : kontenForhat_RecyclerView_model)
        {
            if (item instanceof AdView)
            {
                AdView adView = (AdView) item;
                adView.destroy();
            }
        }
        super.onDestroy();
    }
//    end copas
}