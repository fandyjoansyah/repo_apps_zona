package com.zonamediagroup.zonamahasiswa.interfaces;

public interface PopUpTambahBalasKomentarArtikel {
     void showReplyCommentDialog();
     void setDataToReplyCommentDialog(String namaPenggunaYangDibalas, String idKomentarYangDibalas, String idUserYangMembalas);
     void kirimBalasKomentar(String komentar, String idKomentarYangDibalas, String idUserPembalas);
}
