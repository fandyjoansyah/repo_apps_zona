package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DummyActivityForTryingBottomsheet extends AppCompatActivity {
    Button open_bottomsheet;
    TextView yang_di_klik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy_for_trying_bottomsheet);
        open_bottomsheet = findViewById(R.id.open_bottomsheet);
        yang_di_klik = findViewById(R.id.yang_di_klik);
        open_bottomsheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DummyBottomSheetDialog bottomSheetDialogKu = new DummyBottomSheetDialog();
                bottomSheetDialogKu.show(getSupportFragmentManager(),"exampleBottomSheet");
            }
        });
    }
}