package com.zonamediagroup.zonamahasiswa;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class IklanInterstitial {
    public static InterstitialAd iklanVideo, iklanGambar;
    static Context context;



    public static void initIklanVideo(Context context)
    {
        // iklan video
        MobileAds.initialize(context, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}});
        iklanVideo = new InterstitialAd(context);
        iklanVideo.setAdUnitId(context.getString(R.string.ad_unit_id_interstitial));
        iklanVideo.loadAd(new AdRequest.Builder().build());
        IklanInterstitial.iklanVideo.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                //load another ad
                iklanVideo.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public static long substractTwoDateTimes(String stringFirstDate, String stringSecondDate){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
        Date firstDate = null;
        Date secondDate = null;
        try {
            firstDate = sdf.parse(stringFirstDate);
            secondDate = sdf.parse(stringSecondDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
        long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return diff;
    }

    public static String getCurrentDateTime(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }



    public static void initIklanGambar(Context context)
    {
        // iklan video
        MobileAds.initialize(context, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}});
        iklanGambar = new InterstitialAd(context);
        iklanGambar.setAdUnitId(context.getString(R.string.ad_unit_id_gambar_interstitial));
        iklanGambar.loadAd(new AdRequest.Builder().build());
    }
}
