package com.zonamediagroup.zonamahasiswa.komponen_retrofit;



import com.google.gson.JsonObject;
import com.zonamediagroup.zonamahasiswa.ServerForhat;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Model;
import com.zonamediagroup.zonamahasiswa.models.NotifUpdateModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface InterfaceApi {

    @GET("Get_status_notif_update?TOKEN=qwerty")
    Call<NotifUpdateModel> getStatusUpdate();

    @FormUrlEncoded
    @POST("customer/get_all_transaction.php")
    Call<ArrayList<String>> getAllTransaction(@Field("no_telp") String no_telp);

    @FormUrlEncoded
    @POST("Get_komentar_v2")
    Call <Response<String>> getKomentar(@Field("TOKEN") String token, @Field("id_post") String id_post, @Field("id_user") String id_user);


    @FormUrlEncoded
    @POST(ServerForhat.URL_FORHAT_LIST)
    Call <JsonObject> getHomeKontenRetrofit(@Header("id")String id, @Field("kode") String kode, @Field("filter") String filter, @Field("pageNum") String pageNum);

    @GET("get_token_forhat_list")
    Call<JsonObject> getForhatToken();

    @GET("kategori")
    Call <JsonObject> getKategoriForhat();


    @FormUrlEncoded
    @POST(ServerForhat.URL_FORHAT_LIST)
    Call <JsonObject> getKontenBySelectedKategoriRetrofit(@Field("filter") String filter, @Field("pageNum") String pageNum, @Field("kategori") String kategori);


}

