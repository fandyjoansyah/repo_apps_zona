package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Notif_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Video_Notif_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Video_Notif extends AppCompatActivity {

    private static final String URL_NOTIF_VIDEO = Server.URL_REAL_VIDEO+"Video_notification/index_post";
    private static final String URL_NOTIF_OPEN_VIDEO = Server.URL_REAL_VIDEO+"Video_notification_open/index_post";

    LinearLayout loding_layar;
    ImageView icon_back_notif;
    RecyclerView recycle_notif;
    TextView ket_notif_kosong;

    Video_Notif_Adapter adapterNotif;

    List<Video_Notif_Model> dataNotifVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_video_notif);

        loding_layar = findViewById(R.id.loding_layar);
        recycle_notif = findViewById(R.id.recycle_notif);
        ket_notif_kosong = findViewById(R.id.ket_notif_kosong);
        dataNotifVideo = new ArrayList<>();

        //start init reyclerview
        LinearLayoutManager layoutRecyclerNotif = new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        adapterNotif = new Video_Notif_Adapter(getApplicationContext(),dataNotifVideo);
        recycle_notif.setLayoutManager(layoutRecyclerNotif);
        recycle_notif.setAdapter(adapterNotif);
        //end init recyclerview

        icon_back_notif = findViewById(R.id.icon_back_notif);
        icon_back_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        getNotif();
        storeNotifOpen();
    }

    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {
        loding_layar.setVisibility(View.GONE);
    }

    private void getNotif() {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_NOTIF_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {

                        JSONArray dataArray = jObj.getJSONArray("data");
                        dataNotifVideo.clear();

                        List<Video_Notif_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Video_Notif_Model>>() {
                        }.getType());

                        dataNotifVideo.addAll(items);
                       adapterNotif.notifyDataSetChanged();

                        loading_dismiss();

                        if(dataNotifVideo != null) {
                            recycle_notif.setVisibility(View.VISIBLE);
                            ket_notif_kosong.setVisibility(View.GONE);
                        }else{
                            recycle_notif.setVisibility(View.GONE);
                            ket_notif_kosong.setVisibility(View.VISIBLE);
                        }


                    } else if (jObj.getString("status").equals("false")) {
                        recycle_notif.setVisibility(View.GONE);
                        ket_notif_kosong.setVisibility(View.VISIBLE);
                        loading_dismiss();
                    }

                    // Check for error node in json



                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    e.printStackTrace();
                    Log.d(MainActivity.MYTAG,"kateg cach "+e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                Log.d(MainActivity.MYTAG,"kateg onErrorRsponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
//                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void storeNotifOpen()
    {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_NOTIF_OPEN_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

//                    if (jObj.getString("status").equals("true")) {
//                        if(jObj.getInt("data") >= 99){
//                            text_count_notif.setText("99+");
//                        }else{
//                            text_count_notif.setText(jObj.getString("data"));
//                        }
//                    } else if (jObj.getString("status").equals("false")) {
//                    }

                } catch (JSONException e) {
                    // JSON error
//                    Log.d(MainActivity.MYTAG,"catch response getALl");
//                    eror_show();
//                    e.printStackTrace();
                }
//                loading_bar_dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
//                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
//                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

}