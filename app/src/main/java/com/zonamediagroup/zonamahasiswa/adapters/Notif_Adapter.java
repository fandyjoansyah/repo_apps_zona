package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Notif_Model;

import java.util.ArrayList;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Notif_Adapter extends RecyclerView.Adapter<Notif_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Notif_Model> barangdataList;
    private List<Notif_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;


    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView title_notif, jenis_notif;
        ImageView img_thumnile_notif;
        ProgressBar pb_item_notifikasi;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            title_notif = view.findViewById(R.id.title_notif);
            jenis_notif = view.findViewById(R.id.jenis_notif);
            img_thumnile_notif = view.findViewById(R.id.img_thumnile_notif);
            pb_item_notifikasi = view.findViewById(R.id.pb_item_notifikasi);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public Notif_Adapter(Context context, List<Notif_Model> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notifikasi, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Notif_Model produk_data = barangdataListFiltered.get(position);
        // System.out.println("Potition " + (position + 1));


//        Holder
        holder.title_notif.setText(produk_data.getTitle_notifikasi());
        Log.d("fandy_ndebug_notif_26april","judul notifikasi: "+produk_data.getTitle_notifikasi());
        Log.d("fandy_ndebug_notif_26april","gambar notifikasi: "+produk_data.getImg_notifikasi());

        String jenis = produk_data.getJenis_notifikasi();
        if (jenis.equals("Poling")) {
            // Poling
            holder.jenis_notif.setText("#Polingbaru");

        } else if (jenis.equals("Download")) {
            // Poling
            holder.jenis_notif.setText("#Download");
        }
        else {
            // post
            //String jenisNotifikasiHapusWhiteSpace = produk_data.getJenis_notifikasi().replaceAll("\\s+","");
            //holder.jenis_notif.setText("#"+jenisNotifikasiHapusWhiteSpace+"Baru");
            holder.jenis_notif.setText(produk_data.getJenis_notifikasi());
        }

//        .placeholder(R.drawable.ic_img_thumnile)
        Glide.with(context)
                .load(produk_data.getImg_notifikasi())
                .apply(RequestOptions.centerCropTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.pb_item_notifikasi.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.pb_item_notifikasi.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.img_thumnile_notif);

//        Holder

    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Notif_Model> filteredList = new ArrayList<>();
                    for (Notif_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getTitle_notifikasi().toLowerCase().contains(charString.toLowerCase()) || row.getTitle_notifikasi().contains(charSequence) || row.getTitle_notifikasi().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Notif_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Notif_Model transaksihariinidataList);
    }
}
