package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MenuFilterForhatManager;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KategoriForhatModel;

import java.util.ArrayList;

public class KategoriForhatAdapter extends RecyclerView.Adapter<KategoriForhatAdapter.MyKategoriForhatViewholder> {
    ArrayList<KategoriForhatModel> listKategoriForhat;
    Context context;
    Animation fade_in;
    KategoriForhatAdapterListener kategoriForhatAdapterListener;
    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(context,R.anim.fade_in);
    }
    public KategoriForhatAdapter(Context context, ArrayList<KategoriForhatModel> listKategoriForhat,KategoriForhatAdapterListener  kategoriForhatAdapterListener){
        this.listKategoriForhat = listKategoriForhat;
        this.context = context;
        this.kategoriForhatAdapterListener = kategoriForhatAdapterListener;
        initAnimation();
    }
    @NonNull
    @Override
    public MyKategoriForhatViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutKategoriForhat = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori_forhat, parent, false);
        MyKategoriForhatViewholder myKategoriForhatViewholder = new MyKategoriForhatViewholder(layoutKategoriForhat);
        return myKategoriForhatViewholder;
    }

    @Override
    public void onBindViewHolder(MyKategoriForhatViewholder holder, int position) {
        Log.d("22_juli_2022","dari recycler: "+listKategoriForhat.get(0).getNama_kategori());
            holder.setTextRadioButton(listKategoriForhat.get(position).getNama_kategori());
    }

    @Override
    public int getItemCount() {
        return listKategoriForhat.size();
    }

    public class MyKategoriForhatViewholder extends RecyclerView.ViewHolder{
        RadioButton item_kategori_forhat;
        public MyKategoriForhatViewholder(@NonNull View itemView) {
            super(itemView);
            findViewByIdAllComponent();
            setListenerAllComponent();
        }
        private void findViewByIdAllComponent(){
            item_kategori_forhat = itemView.findViewById(R.id.item_kategori_forhat);
        }

        private void setListenerAllComponent(){
            item_kategori_forhat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mekanismeToggleRadioButton();
                }
            });
        }

        private void mekanismeToggleRadioButton() {
            if(item_kategori_forhat.isChecked()) {
                if (!listKategoriForhat.get(getAdapterPosition()).isSelected()) {
                    item_kategori_forhat.setChecked(true);
                    kategoriForhatAdapterListener.addCategorySelected(listKategoriForhat.get(getAdapterPosition()));
                    listKategoriForhat.get(getAdapterPosition()).setSelected(true);
                } else {
                    item_kategori_forhat.setChecked(false);
                    kategoriForhatAdapterListener.removeCategoryUnSelected(listKategoriForhat.get(getAdapterPosition()));
                    listKategoriForhat.get(getAdapterPosition()).setSelected(false);
                }
            }
        }



        private void setTextRadioButton(String newText)
        {
            item_kategori_forhat.setText(newText);
        }
    }

    public interface KategoriForhatAdapterListener{
        public void addCategorySelected(KategoriForhatModel kategoriForhatModel);
        public void removeCategoryUnSelected(KategoriForhatModel kategoriForhatModel);
    }
}
