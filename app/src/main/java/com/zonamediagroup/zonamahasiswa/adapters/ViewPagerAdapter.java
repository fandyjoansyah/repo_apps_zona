package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.squareup.picasso.Picasso;
import com.zonamediagroup.zonamahasiswa.Detail_poling;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Data_ViewPager_Model;
import com.zonamediagroup.zonamahasiswa.models.Poling_Model;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> imageUrls;
    ShapeableImageView imgGambarPolling;
    TextView tvJudulPolling;
    LayoutInflater inflater;
    List<Data_ViewPager_Model> dataPolling;

    public ViewPagerAdapter(Context context, List<Data_ViewPager_Model> dataPolling) {
        this.context = context;
        this.dataPolling = dataPolling;
    }

    @Override
    public int getCount() {
        return dataPolling.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //ImageView imageView = new ImageView(context);

        //ambil file xml untuk jadi patokan viewpager
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.layout_untuk_viewpager, container, false);
        tvJudulPolling = v.findViewById(R.id.textnya);
        imgGambarPolling = v.findViewById(R.id.gambarnya);

        //start on click
        imgGambarPolling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, Detail_poling.class);
                i.putExtra("id_poling", dataPolling.get(position).getId_poling());
                i.putExtra("tiitle_poling", dataPolling.get(position).getTiitle_poling());
                i.putExtra("img_poling", dataPolling.get(position).getImg_poling());
                i.putExtra("poling_start", dataPolling.get(position).getPoling_start());
                i.putExtra("poling_end", dataPolling.get(position).getPoling_end());
                i.putExtra("waktu_sekarang", dataPolling.get(position).getWaktu_sekarang());

                context.startActivity(i);
            }
        });
        //end on click

        //menjadikan gambar menjadi rounded
        ShapeAppearanceModel shapeAppearanceModel = new ShapeAppearanceModel()
                .toBuilder()
                .setAllCorners(CornerFamily.ROUNDED,30)
                .build();
        imgGambarPolling.setShapeAppearanceModel(shapeAppearanceModel);
        //set gambar
        Picasso.get()
                .load(dataPolling.get(position).getImg_poling())
                .fit()
                .into(imgGambarPolling);
        container.addView(v);
        //set judul
        tvJudulPolling.setText(dataPolling.get(position).getTiitle_poling());
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
