package com.zonamediagroup.zonamahasiswa.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Kategori_Model;
import com.zonamediagroup.zonamahasiswa.tooltip.OverlayView;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZonaController;

import java.util.ArrayList;
import java.util.List;

import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltip;
import com.zonamediagroup.zonamahasiswa.tooltip.SimpleTooltipUtils;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Kategori_Adapter extends RecyclerView.Adapter<Kategori_Adapter.MyViewHolder>
        implements Filterable {
  private Context context;
  Activity contextRnD;
  private List<Kategori_Model> barangdataList;
  private List<Kategori_Model> barangdataListFiltered;
  private ContactsAdapterListener listener;
  private ToolTipsZona[] arrToolTipsKategori;
  private ArrayList<ToolTipsZona> listToolTipsZona;

  View layoutItemTemplate;

  //    Chace

  public class MyViewHolder extends RecyclerView.ViewHolder {


    //        public CardView bg_color;
    public ImageView img_categori;
    TextView nama_kategori;
    LinearLayout layoutPerItem;



    public MyViewHolder(View view) {
      super(view);

//            definisi komponen

//            bg_color = view.findViewById(R.id.bg_color);
      img_categori = view.findViewById(R.id.img_categori_ok);
      nama_kategori = view.findViewById(R.id.nama_kategori);
      layoutPerItem = view.findViewById(R.id.bg_color);

      view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          // send selected contact in callback
          listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
        }
      });


    }
  }


  public Kategori_Adapter(Context context, List<Kategori_Model> barangdataList, ContactsAdapterListener listener, Activity contextRnD) {
    this.context = context;
    this.listener = listener;
    this.barangdataList = barangdataList;
    this.barangdataListFiltered = barangdataList;
    this.contextRnD = contextRnD;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_kategori_dua, parent, false);
    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(MyViewHolder holder, final int position) {


    final Kategori_Model produk_data = barangdataListFiltered.get(position);
    //System.out.println("Potition " + (position + 1));

    String thumnile_post = "http://zonamahasiswa.id/wp-content/uploads/2021/06/pexels-christina-morillo-1181534.jpg" ;

    Glide.with(context)
            .load(produk_data.getGambar_kategori())
            .apply(RequestOptions.fitCenterTransform())
            .listener(new RequestListener<Drawable>() {
              @Override
              public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                return false;
              }

              @Override
              public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                if(produk_data.getId_kategori().equals("yogi"))
                {
                  holder.layoutPerItem.setId(R.id.layout_template);
                  layoutItemTemplate = holder.layoutPerItem;
                  doToolTipsKategori(holder.layoutPerItem,holder.img_categori);
                }

                return false;
              }
            })
            .into(holder.img_categori);
    holder.nama_kategori.setText(produk_data.getNama_kategori());


//    if(position == getItemCount()-1){
//      listener.doToolTipsKategori(layoutItemTemplate,contextRnD);
//    }


//        holder.bg_color.setCardBackgroundColor(Color.parseColor(produk_data.getWarna_kategori()));

//        holder.img_categori.setBackgroundColor(Color.parseColor(produk_data.getWarna_kategori()));
//        Holder

  }



  @Override
  public int getItemCount() {
    return barangdataListFiltered.size();
  }

  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
          barangdataListFiltered = barangdataList;
        } else {
          List<Kategori_Model> filteredList = new ArrayList<>();
          for (Kategori_Model row : barangdataList) {

//                       Filter list receler view
            if (row.getNama_kategori().toLowerCase().contains(charString.toLowerCase()) || row.getNama_kategori().contains(charSequence) || row.getNama_kategori().contains(charSequence)) {
              filteredList.add(row);
            }
          }

          barangdataListFiltered = filteredList;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = barangdataListFiltered;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        barangdataListFiltered = (ArrayList<Kategori_Model>) filterResults.values;
        notifyDataSetChanged();
      }
    };
  }

  public interface ContactsAdapterListener {
    /**
     * @param transaksihariinidataList
     */
    void onContactSelected(Kategori_Model transaksihariinidataList);
    void doToolTipsKategori(View view,Context context);
  }

  public  void jalankanToolTips(View v, ImageView img){
    final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
            .anchorView(v)
            .text("Download template makalah, skripsi, PPT di sini")
            .gravity(Gravity.TOP)
            .dismissOnOutsideTouch(false)
            .dismissOnInsideTouch(false)
            .modal(true)
            .animated(true)
            .showArrow(true)
            .transparentOverlay(false)
            .overlayMatchParent(false)
            .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR_RADIUS)
            .overlayOffset(0)
            .animationDuration(2000)
            .animationPadding(SimpleTooltipUtils.pxFromDp(0))
            .contentView(R.layout.tooltip_small, R.id.tv_text)
            .focusable(true)
            .build();

    Button btn = tooltip.findViewById(R.id.btn_next);
    btn.setText("Selesai");
    tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        tooltip.dismiss();
      }
    });

    tooltip.show();
  }

  private void doToolTipsKategori(View v, ImageView img)
  {
    if(context != null) {
      SharedPreferences pref = context.getSharedPreferences("zonamhs", 0);
      if (pref.contains("kategoriToolTips") == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
      {
        jalankanToolTips(v,img);
        pref.edit().putInt("kategoriToolTips", 1).commit();
      }
    }
  }
}
