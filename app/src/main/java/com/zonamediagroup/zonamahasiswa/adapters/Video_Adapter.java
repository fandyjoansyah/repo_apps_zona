package com.zonamediagroup.zonamahasiswa.adapters;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.CheckNetworkAvailable;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.SwipeVideo;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Halaman_Awal_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Model;
import com.zonamediagroup.zonamahasiswa.utils.ExoPlayerRecyclerViewAUTOPREVIEW;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Video_Adapter extends RecyclerView.Adapter<Video_Adapter.ViewHolderVideo> {
        private Context context;
        private List<Video_Model> dataVideo;
        VideoListener videoListener;
        String TAG = MainActivity.MYTAG;
        int banyakKomentar=0;
        String user_img="";
        ViewGroup mParent;
    private static final String URL_GET_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment/index_post";
    private static final String URL_POST_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_create/index_post";
    private static final String URL_TAMBAH_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_create/index_post";
    private static final String URL_BLOKIR_PENGGUNA = Server.URL_REAL_NATIVE+"Send_blokir_pengguna";
    private static final String URL_REPORT_VIDEO = Server.URL_REAL_NATIVE+"Report_video";
    private static final String URL_GET_SINGLE_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_last/index_post";
    public Video_Adapter(Context context, List<Video_Model> dataVideo, VideoListener videoListener) {
        //ambil context dan data video dari activity
        this.context = context;
        this.dataVideo = dataVideo;
        this.videoListener = videoListener;
        user_img = SharedPrefManagerZona.getInstance(context).geturl_voto();
    }

    @NonNull
    @Override
    public ViewHolderVideo onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mParent = parent;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        return new ViewHolderVideo(v);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderVideo holder, @SuppressLint("RecyclerView") int position) {


        Video_Model dataV = dataVideo.get(position);
        //start code autoplay
        holder.setTag();
        holder.title.setText(dataV.getVideo_title());
        holder.userHandle.setText(dataV.getPerson_id());
        //end code autoplay
        banyakKomentar = dataV.getComment().size();
        //set icon awal load by value
        //cek like video
        cekDanInitIconAwalLoad(holder.like_video,context.getDrawable(R.drawable.love_fill),context.getDrawable(R.drawable.love_video),dataVideo.get(position).getVideo_like_id());
        cekDanInitIconAwalLoad(holder.save_video,context.getDrawable(R.drawable.save),context.getDrawable(R.drawable.ic_simpan_ajus_color),dataVideo.get(position).getVideo_save_id());
        holder.jumlah_share_video.setText(dataV.getVideo_share());
        holder.jumlah_tayangan.setText(dataV.getVideo_view());
        holder.jumlah_like_video.setText(dataV.getVideo_like());
        holder.text_nama_video.setText(dataV.getVideo_title());
//        holder.komentar_video_count.setText("Komentar "+dataV.getVideo_comment());

        String textnya = "<font color='black'>Komentar </font><font color='#919191'>"+dataV.getVideo_comment()+"</font>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.komentar_video_count.setMovementMethod(LinkMovementMethod.getInstance());
            holder.komentar_video_count.setText(Html.fromHtml(textnya, Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.komentar_video_count.setMovementMethod(LinkMovementMethod.getInstance());
            holder.komentar_video_count.setText(Html.fromHtml(textnya));
        }

        Glide.with(context)
                .load(dataV.getVideo_thumbnail())
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.gambar_thumbnail_video);

//        holder.video_thumb.setText(dataV.getVideo_url());
        Uri uri = Uri.parse(dataV.getVideo_url());

        holder.download_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckNetworkAvailable.check(context))
                videoListener.persiapanDownloadVideo(position,dataV.getVideo_url(),dataV.getVideo_title());
            }
        });

        holder.share_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckNetworkAvailable.check(context))
                videoListener.shareVideo(dataV.getVideo_share_url());
            }
        });

        holder.relative_komentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckNetworkAvailable.check(context))
                    holder.showDialogComment(dataV.getVideo_id());
               // videoListener.showAllComment(dataV.getVideo_id());
            }
        });

        holder.txt_belum_ada_komentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckNetworkAvailable.check(context))
                    holder.showDialogComment(dataV.getVideo_id());
               // videoListener.showAllComment(dataV.getVideo_id());
            }
        });

            //Toggle
         if(dataV.getVideo_comment().equals("0")) {
            holder.relative_komentar.setVisibility(View.GONE);
        //    holder.recyclerKomentarVideoLevel1.setVisibility(View.GONE);
        }else{
             Log.e(MainActivity.MYTAG,"onBind 191 visible");
            holder.txt_belum_ada_komentar.setVisibility(View.VISIBLE);
        }


     //   holder.video_thumb.start();
        holder.kategori_video.setText(dataV.getVideo_category_nama());
        holder.waktu_post_video.setText(dataV.getVideo_date_created());
        if(dataV.getPerson_name() == null) {
            //jika person name = null. maka author = zona mahasiswa
            holder.nama_pengirim.setText("Zona Mahasiswa");
            Glide.with(context)
                    .load(R.drawable.ic_logo)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.gambar_author);
        }
        else {
            holder.nama_pengirim.setText(dataV.getPerson_name());
            Glide.with(context)
                    .load(dataV.getPerson_photo())
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.gambar_author);
        }
        //start tambahkan komentar level1
      //xx  T.l(context,"komentar: "+dataVideo.get(0).getComment().get(0).getVideo_comment());

        Komentar_Video_Model komentarnya = dataV.getComment().get(0);
        Log.d(MainActivity.MYTAG,"ukurannya sam: "+dataV.getComment().size());

//        //start show komentar
//        for(int i=0;i<banyakKomentar;i++)
//        {
//            komentarnya = dataV.getComment().get(i);
//            Log.d(MainActivity.MYTAG,"di loop sebanyak "+i+" kali");
//            //JIKA ADA KOMENTAR LEVEL 1
//            if(komentarnya != null)
//            {
//                Log.d(MainActivity.MYTAG,"dan tidak null "+komentarnya.getVideo_comment());
//                Log.d(MainActivity.MYTAG,"CALLER FROM Video_Adapter "+banyakKomentar);
//                //start bind data ke recycler single komentar
//                holder.dataRecyclerSingleKomentarHalamanAwal = null;
//                holder.dataRecyclerSingleKomentarHalamanAwal = dataV.getComment();
//                holder.adapterRecyclerSingleKomentarHalamanAwal.notifyDataSetChanged();
//                //end bind data ke recycler single komentar
//                holder.txt_belum_ada_komentar.setVisibility(View.GONE);
//                holder.relative_komentar.setVisibility(View.VISIBLE);
//            }
//            //JIKA TIDAK ADA KOMENTAR LEVEL 1
//            else
//            {
//                Log.d(MainActivity.MYTAG,"dan null");
//                holder.relative_komentar.setVisibility(View.GONE);
//                //bekas instansiate adapter halaman awal single komentar
//            }
//
//        }
//        //end show komentar
        if(dataV.getComment().get(0) != null){

            //start bind data ke recycler single komentar
            holder.dataRecyclerSingleKomentarHalamanAwal.clear();
            holder.dataRecyclerSingleKomentarHalamanAwal.addAll(dataV.getComment());
          //  holder.adapterRecyclerSingleKomentarHalamanAwal.notifyDataSetChanged();
            holder.initRecyclerSingle(dataV.getComment());
            //end bind data ke recycler single komentar
            Log.e(MainActivity.MYTAG,"ifOnbind 256 gone");
            holder.txt_belum_ada_komentar.setVisibility(View.GONE);
            holder.relative_komentar.setVisibility(View.VISIBLE);
        }
        else if (dataV.getComment().get(0) == null){
            holder.dataRecyclerSingleKomentarHalamanAwal.clear();
            holder.initRecyclerSingle(null);
            Log.e(MainActivity.MYTAG,"else if onbind 263 visible");
            holder.txt_belum_ada_komentar.setVisibility(View.VISIBLE);
            holder.relative_komentar.setVisibility(View.GONE);
        }




        //end tambahkan komentar level1

        //Log.d("debug array komentar","komentar 1: "+dataVideo.get(0).getComment()[0]);
        holder.like_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetworkAvailable.check(context))
                toggleLike(position,MainActivity.idUserLogin, dataVideo.get(position).getVideo_id(), dataVideo.get(position).getVideo_like_id(),holder.like_video,holder.jumlah_like_video);
            }
        });

        holder.save_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSave(position,MainActivity.idUserLogin, dataVideo.get(position).getVideo_id(), dataVideo.get(position).getVideo_save_id(),holder.save_video);
            }
        });
        holder.layout_per_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SwipeVideo.class);
                String yourListAsString = new Gson().toJson(dataVideo);
                Bundle bundle = new Bundle();
                bundle.putString("data",yourListAsString);
                bundle.putInt("position",position);
                i.putExtra("bundlenya",bundle);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.getApplicationContext().startActivity(i);
            }
        });

        holder.ly_lainnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.showDialogLainnya(dataV.getVideo_id(),dataV.getVideo_title(),dataV.getPerson_id(),dataV.getPerson_name());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataVideo.size();
    }



    public class ViewHolderVideo extends RecyclerView.ViewHolder implements Komentar_Video_Level1_Adapter.KomentarLevel1{
        //deklarasi variabel komponen view
        TextView nama_pengirim,waktu_post_video,kategori_video,jumlah_like_video,jumlah_tayangan,jumlah_share_video, text_nama_video, tampilkan_komentar, komentar_video_count;
        ExoPlayerRecyclerViewAUTOPREVIEW video_thumb;
        ImageView gambar_author, download_video, like_video, share_video, save_video, gambar_thumbnail_video;
        RecyclerView recyclerKomentarVideoLevel1;
        Button btnSendBottomSheet;
        LinearLayout relative_komentar;
        TextView txt_belum_ada_komentar;
        RelativeLayout layout_per_video;
        Button btnDebug;




        ArrayList<Komentar_Video_Model> dataRecyclerSingleKomentarHalamanAwal = new ArrayList<>();

        //Start Komponen bottomsheet komentar
        Komentar_Video_Level1_Adapter commentAdapterBottomSheetLevel1;

        RecyclerView recyclerCommentInBottomSheet;

        BottomSheetDialog bottomSheetDialog,bottomSheetDialogBalasKomentar,bottomSheetDialogLainnya;
        ProgressBar pbBottomSheet;

        EditText txt_komentar_video;

        TextView txt_bottomsheet_komentar_kosong;

        ImageView send_button, gambar_komentator_video;

        ImageView gambar_komentator;

        Komentar_Video_Level1_Halaman_Awal_Adapter adapterRecyclerSingleKomentarHalamanAwal = null;

        ArrayList<Komentar_Video_Model> dataRecyclerKomentarBottomSheet;
        //End Komponen bottomsheet komentar

        public FrameLayout mediaContainer;
        public ImageView mediaCoverImage, volumeControl;
        public ProgressBar progressBar;
        public RequestManager requestManager;
        public TextView title, userHandle;
        public View parent;
        public ImageView icon_play;
        RelativeLayout ly_lainnya;
        LinearLayoutManager layoutLevel1;
        String alasan = "";

        public ViewHolderVideo(@NonNull View v) {
            super(v);
            //inisialisasi data
            nama_pengirim = v.findViewById(R.id.nama_pengirim);
            waktu_post_video = v.findViewById(R.id.waktu_post_video);
            kategori_video = v.findViewById(R.id.kategori_video);
            jumlah_like_video = v.findViewById(R.id.jumlah_like_video);
            jumlah_tayangan = v.findViewById(R.id.jumlah_tayangan);
            jumlah_share_video = v.findViewById(R.id.jumlah_share_video);
            gambar_author = v.findViewById(R.id.gambar_author_video);
            download_video = v.findViewById(R.id.download_video);
            text_nama_video = v.findViewById(R.id.text_nama_video);
            layout_per_video = v.findViewById(R.id.layout_per_video);
            gambar_thumbnail_video = v.findViewById(R.id.gambar_thumbnail_video);
            ly_lainnya = v.findViewById(R.id.ly_lainnya);

            //start code autoplay
            parent = v;
            mediaContainer = v.findViewById(R.id.mediaContainer);
            mediaCoverImage = v.findViewById(R.id.gambar_thumbnail_video);
            title = v.findViewById(R.id.tvTitle);
            userHandle = v.findViewById(R.id.tvUserHandle);
            progressBar = v.findViewById(R.id.progressBar);
            volumeControl = v.findViewById(R.id.ivVolumeControl);
            //end code autoplay
            share_video = v.findViewById(R.id.share_video);
            like_video = v.findViewById(R.id.like_video);
            save_video = v.findViewById(R.id.save_video);
            tampilkan_komentar = v.findViewById(R.id.tampilkan_komentar);
            komentar_video_count = v.findViewById(R.id.komentar_video_count);
            btnSendBottomSheet = v.findViewById(R.id.send_button);
            relative_komentar = v.findViewById(R.id.relative_komentar);
            txt_belum_ada_komentar = v.findViewById(R.id.txt_belum_ada_komentar);
            icon_play = v.findViewById(R.id.icon_play);


//            nama_video = v.findViewById(R.id.nama_video);
            recyclerKomentarVideoLevel1 = v.findViewById(R.id.recycler_komentar_video_level1);
            //setOnClickListener


            layoutLevel1 = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
            recyclerKomentarVideoLevel1.setLayoutManager(layoutLevel1);

        }

        public void initRecyclerSingle(List<Komentar_Video_Model> dataKomentar){
            Log.e(MainActivity.MYTAG,"initRecyclerSingle fandy");
            //start init recycler single komentar halaman awal
            if(adapterRecyclerSingleKomentarHalamanAwal == null){
                adapterRecyclerSingleKomentarHalamanAwal  = new Komentar_Video_Level1_Halaman_Awal_Adapter(context,dataRecyclerSingleKomentarHalamanAwal);
                recyclerKomentarVideoLevel1.setAdapter(adapterRecyclerSingleKomentarHalamanAwal);
            }
            else {
                adapterRecyclerSingleKomentarHalamanAwal.notifyDataSetChanged();
                //adapterRecyclerSingleKomentarHalamanAwal = null;
            }
            if(adapterRecyclerSingleKomentarHalamanAwal == null){
                Log.e(MainActivity.MYTAG,"adapter single null");
            }
            if(dataRecyclerSingleKomentarHalamanAwal == null){
                Log.e(MainActivity.MYTAG,"data single null");
            }
            if(layoutLevel1 == null){
                Log.e(MainActivity.MYTAG,"layout single null");
            }

            //end init recycler single komentar halaman awal
        }

        public void initCommentBottomSheet()
        {
            bottomSheetDialog = new BottomSheetDialog(context);
            View view= LayoutInflater.from(context).inflate(R.layout.sheet_dialog_komentar,null);

            recyclerCommentInBottomSheet = view.findViewById(R.id.recycler_bottomsheet_komen_video);
            LinearLayoutManager layoutLevel1BottomSheet = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
            recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1BottomSheet);

            pbBottomSheet = view.findViewById(R.id.progress_bottom_video);
            gambar_komentator_video = view.findViewById(R.id.gambar_komentator_video);

            txt_komentar_video = view.findViewById(R.id.txt_komentar_video);

            send_button = view.findViewById(R.id.send_button);

            dataRecyclerKomentarBottomSheet = new ArrayList<>();

            txt_bottomsheet_komentar_kosong = view.findViewById(R.id.txt_bottomsheet_komentar_kosong);

            gambar_komentator = view.findViewById(R.id.gambar_komentator_video);

            bottomSheetDialog.setContentView(view);
            bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    //reset adapter komentar di bottomsheet agar reload lagi di pembukaan selanjutnya
                    commentAdapterBottomSheetLevel1 = null;
                    //refresh recycler single di halaman awal video
                    getLastSingleCommentById(dataVideo.get(getAdapterPosition()).getVideo_id());
                }
            });
        }

        //start init bottomsheet lainnya
        public void initBottomSheetLainnya(String idVideoReported, String namaVideoReported, String idPenggunaReported, String namaPenggunaReported)
        {


            bottomSheetDialogLainnya = new BottomSheetDialog(context);
            View vBottomSheet= LayoutInflater.from(context).inflate(R.layout.sheet_dialog_lainnya,null);
            LinearLayout opsi_blokir = vBottomSheet.findViewById(R.id.opsi_blokir);
            LinearLayout opsi_laporkan = vBottomSheet.findViewById(R.id.opsi_laporkan);
            TextView batal = vBottomSheet.findViewById(R.id.batal);


            opsi_blokir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    start confirm dialog
                    new AlertDialog.Builder(context)
                            .setTitle("Blokir Pengguna")
                            .setMessage("Apakah kamu yakin ingin memblokir "+namaPenggunaReported+" ?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("Blokir", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    blokirPengguna(idVideoReported,namaVideoReported,idPenggunaReported,namaPenggunaReported);
                                }})
                            .setNegativeButton("Batal", null).show();
//                    end comfirm dialog
                }
            });

            opsi_laporkan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View vLaporkan= LayoutInflater.from(context).inflate(R.layout.layout_laporkan,null);
                    LinearLayout report_spam,report_tidak_menyukai, report_pornografi,report_informasi_palsu, report_ujaran_kebencian,report_kekerasan,report_haki,report_barang_ilegal,report_bunuh_diri,report_lainnya;
                    TextView batal;
                    report_spam = vLaporkan.findViewById(R.id.report_spam);
                    report_tidak_menyukai = vLaporkan.findViewById(R.id.report_tidak_menyukai);
                    report_pornografi = vLaporkan.findViewById(R.id.report_pornografi);
                    report_informasi_palsu = vLaporkan.findViewById(R.id.report_informasi_palsu);
                    report_ujaran_kebencian = vLaporkan.findViewById(R.id.report_ujaran_kebencian);
                    report_kekerasan = vLaporkan.findViewById(R.id.report_kekerasan);
                    report_haki = vLaporkan.findViewById(R.id.report_haki);
                    report_barang_ilegal = vLaporkan.findViewById(R.id.report_barang_ilegal);
                    report_bunuh_diri = vLaporkan.findViewById(R.id.report_bunuh_diri);
                    report_lainnya = vLaporkan.findViewById(R.id.report_lainnya);
                    batal = vLaporkan.findViewById(R.id.batal);

                    batal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bottomSheetDialogLainnya.dismiss();
                        }
                    });

                    report_spam.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                             alasan="Ini adalah spam";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_tidak_menyukai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Saya hanya tidak menyukainya";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_pornografi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Pornografi atau aktivitas seksual";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_informasi_palsu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Memuat informasi palsu";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_ujaran_kebencian.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Ujaran kebencian atau pelecehan";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_kekerasan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Kekerasan atau komunitas berbahaya";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_barang_ilegal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Penjualan barang ilegal atau berbahaya";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_bunuh_diri.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alasan="Bunuh diri atau melukai diri sendiri";
                            View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                        }
                    });
                    report_lainnya.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            View vKustomKirimLaporkan=LayoutInflater.from(context).inflate(R.layout.layout_kustom_laporkan,null);
//                            sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasan);
                            TextView btn_kirim_kustom_alasan = vKustomKirimLaporkan.findViewById(R.id.btn_kirim_kustom_alasan);
                            EditText edtAlasan = vKustomKirimLaporkan.findViewById(R.id.edt_alasan);
                            btn_kirim_kustom_alasan.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String alasanKustom = edtAlasan.getText().toString();
                                    if(alasanKustom.length() <= 0)
                                    {
                                        CustomToast.s(context,"Alasan tidak boleh kosong");
                                    }
                                    else
                                    {
                                        View vKirimLaporkan= LayoutInflater.from(context).inflate(R.layout.kirim_laporkan,null);
                                        sendReportVideo(SharedPrefManagerZona.getInstance(context).getid_user(),idVideoReported,idPenggunaReported,alasanKustom);
                                        bottomSheetDialogLainnya.setContentView(vKirimLaporkan);
                                    }
                                }
                            });
                            bottomSheetDialogLainnya.setContentView(vKustomKirimLaporkan);
                        }
                    });

                    bottomSheetDialogLainnya.setContentView(vLaporkan);
                }
            });

            batal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialogLainnya.dismiss();
                }
            });



            bottomSheetDialogLainnya.setContentView(vBottomSheet);

        }
        //end init bottomsheet lainnya



        public void sendReportVideo(String person_id_pengirim_report,String id_video_direport,String person_id_pemilik_id_video_direport, String alasan)
        {
            String pemilik_Video;
            if(person_id_pemilik_id_video_direport == null)
            {
                pemilik_Video = "Akun official Zona Mahasiswa";
            }
            else {
                pemilik_Video = person_id_pemilik_id_video_direport;
            }
            Log.d("SendReportVideo","SendReportVideo person_id_pengirim_report"+person_id_pengirim_report);
            Log.d("SendReportVideo","SendReportVideo id_video_direport"+id_video_direport);
            Log.d("SendReportVideo","SendReportVideo person_id_pemilik_id_video_direport"+person_id_pemilik_id_video_direport);
            Log.d("SendReportVideo","SendReportVideo alasan"+alasan);


            StringRequest strReq = new StringRequest(Request.Method.POST, URL_REPORT_VIDEO, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        Log.d(MainActivity.MYTAG,"berhasil send report wiu");
                        Log.d(MainActivity.MYTAG,"try tambah komentar ittuw");
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());


                        } else if (jObj.getString("status").equals("false")) {


                        }
                        else
                        {

                        }

                        // Check for error node in json

                    } catch (JSONException e) {
                        Log.d(MainActivity.MYTAG,"catch send blokir pengguna "+e.getMessage());
                        e.printStackTrace();
                        Log.d(MainActivity.MYTAG,"gagal send report wiu");
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(MainActivity.MYTAG,"onErrorResponse send blokir pengguna "+ error.getMessage());
                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("person_id_pengirim_report",person_id_pengirim_report);
                    params.put("id_video_direport",id_video_direport);
                    params.put("person_id_pemilik_id_video_direport",pemilik_Video);
                    params.put("alasan",alasan);

                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
            //end sendblokir Pengguna
        }


//        start removeDataVideo by position
           public void removeDataVideoByPosition(int position)
            {
                dataVideo.remove(position);
                notifyItemRemoved(position);
            }
//        end removeDataVideo by position


        // start removeDataVideoByPersonId
        public void removeDataVideoByPersonId(String personId)
        {

        }
        //end removeDataVideoByPersonId


        public void blokirPengguna(String idVideoReported, String namaVideoReported, String idPenggunaReported, String namaPenggunaReported){
            //jika idPenggunaReported == null yang dimana itu adalah Zona Mahasiswa
            if(idPenggunaReported == null)
            {
                CustomToast.s(context,"Anda tidak bisa memblokir akun "+namaPenggunaReported);
            }
            else{
                CustomToast.s(context,namaPenggunaReported+" berhasil diblokir. Kamu dapat membuka blokir kapan saja di pengaturan akun");
                // hilangkan video yang sedang diblokir
                removeDataVideoByPosition(getPosition());
                bottomSheetDialogLainnya.dismiss();
            }

            //start sendBlokirPengguna
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_BLOKIR_PENGGUNA, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        Log.d(MainActivity.MYTAG,"try tambah komentar ittuw");
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());


                        } else if (jObj.getString("status").equals("false")) {


                        }
                        else
                        {

                        }

                        // Check for error node in json

                    } catch (JSONException e) {
                        Log.d(MainActivity.MYTAG,"catch send blokir pengguna "+e.getMessage());
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(MainActivity.MYTAG,"onErrorResponse send blokir pengguna "+ error.getMessage());
                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("person_id_pemblokir",SharedPrefManagerZona.getInstance(context).getid_user());
                    params.put("person_id_diblokir",idPenggunaReported);

                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
            //end sendblokir Pengguna
        }

        private void getLastSingleCommentById(String id_video) {

            StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_SINGLE_KOMENTAR, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jObj = new JSONObject(response);
                            JSONArray jsonArrayLastComment = jObj.getJSONArray("comment");
                        if (jsonArrayLastComment != null) {
                            List<Komentar_Video_Model> lastComment = new Gson().fromJson(jsonArrayLastComment.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());
                            setDataToRecyclerViewSingleKomentar(lastComment);
                        }
                        // Check for error node in json

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //segera hapu
                    Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", MainActivity.idUserLogin);
                    params.put("id_video",id_video);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");

        }

        //start show dialog lainnya
        public void showDialogLainnya(String idVideoReported, String namaVideoReported, String idPenggunaReported, String namaPenggunaReported){

            if(namaPenggunaReported == null){
                namaPenggunaReported = "Zona Mahasiswa";
            }
            else{

            }
            initBottomSheetLainnya(idVideoReported, namaVideoReported,idPenggunaReported,namaPenggunaReported);

            bottomSheetDialogLainnya.show();
        }
        //end show dialog lainnya

        public void showDialogComment(String idVideo){
            initCommentBottomSheet();

            Glide.with(context)
                    .load(SharedPrefManagerZona.getInstance(context).geturl_voto())
                    .apply(RequestOptions.fitCenterTransform())
                    .into(gambar_komentator);


            bottomSheetDialog.show();
            refreshRecyclerViewKomentarSetelahAksiLevel1(idVideo,"awal_buka");
            send_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(CheckNetworkAvailable.check(context)) {
                        tambahKomentar(MainActivity.idUserLogin, idVideo, txt_komentar_video.getText().toString());
                        txt_komentar_video.setText("");
                        txt_komentar_video.clearFocus();
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    //  CustomToast.s(getContext(),"Komentarmu berhasil ditambah");
                }
            });
        }

        public void refreshRecyclerViewKomentarSetelahAksiLevel1(String idVideo,String jenisAksi)
        {
            //start code getCommentLevel1
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");
                            ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());

                            dataRecyclerKomentarBottomSheet.clear();
                            dataRecyclerKomentarBottomSheet.addAll(items);
                            commentAdapterBottomSheetLevel1 = new Komentar_Video_Level1_Adapter(context,dataRecyclerKomentarBottomSheet, ViewHolderVideo.this);
                            recyclerCommentInBottomSheet.setAdapter(commentAdapterBottomSheetLevel1);
                            commentAdapterBottomSheetLevel1.notifyDataSetChanged();

                            pbBottomSheet.setVisibility(View.GONE);
                        } else if (jObj.getString("status").equals("false")) {

                            txt_bottomsheet_komentar_kosong.setVisibility(View.VISIBLE);
                            pbBottomSheet.setVisibility(View.GONE);
                        }
                        else
                        {

                        }

                        // Check for error node in json

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {


                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", MainActivity.idUserLogin);
                    params.put("id_video",idVideo);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
            //end code getCommentLevel1
        }
        public void setDataToRecyclerViewSingleKomentar(List<Komentar_Video_Model> lastComment){
            if(dataRecyclerSingleKomentarHalamanAwal == null || dataRecyclerSingleKomentarHalamanAwal.size() <=0) //null. belum ada komentar sebelumnya di recycler single
            {

                Log.d(MainActivity.MYTAG,"before initRecyclerKomentarHalamanAwalAfterFirstComment");
                initRecyclerKomentarHalamanAwalAfterFirstComment(lastComment);
            }
            else {

                Log.d(MainActivity.MYTAG,"before refreshRecyclerKomentarHalamanAwal");
                refreshRecyclerKomentarHalamanAwal(lastComment);
            }
        }
        public void refreshRecyclerKomentarHalamanAwal(List<Komentar_Video_Model> lastComment)
        {
            Log.d(MainActivity.MYTAG,"inside refreshRecyclerKomentarHalamanAwal");
            dataRecyclerSingleKomentarHalamanAwal.clear();
            dataRecyclerSingleKomentarHalamanAwal.addAll(lastComment);
            adapterRecyclerSingleKomentarHalamanAwal.notifyDataSetChanged();
        }

        public void initRecyclerKomentarHalamanAwalAfterFirstComment(List<Komentar_Video_Model> lastComment)
        {
            Log.e(MainActivity.MYTAG,"initRecyclerKomentarHalamanAwalAfterFirstComment fandy");
            recyclerKomentarVideoLevel1.setAdapter(null);
            adapterRecyclerSingleKomentarHalamanAwal = null;
            dataRecyclerSingleKomentarHalamanAwal.clear();
            dataRecyclerSingleKomentarHalamanAwal.addAll(lastComment);
            Log.d(MainActivity.MYTAG,"value dataRecyclerSingleKomentarHalamanAwal "+dataRecyclerSingleKomentarHalamanAwal.get(0).getVideo_comment());
            adapterRecyclerSingleKomentarHalamanAwal = new Komentar_Video_Level1_Halaman_Awal_Adapter(context,dataRecyclerSingleKomentarHalamanAwal);
            recyclerKomentarVideoLevel1.setAdapter(adapterRecyclerSingleKomentarHalamanAwal);
      //      recyclerKomentarVideoLevel1.setVisibility(View.VISIBLE);
            relative_komentar.setVisibility(View.VISIBLE);
            Log.e(MainActivity.MYTAG,"initRecVVV 674 gone");
            txt_belum_ada_komentar.setVisibility(View.GONE);
        }

        public void tambahKomentar(String idUser, String idVideo, String komentar){
            Log.d(MainActivity.MYTAG,"tambah komentar ittuw");
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_TAMBAH_KOMENTAR, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        Log.d(MainActivity.MYTAG,"try tambah komentar ittuw");
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());

                            Log.d(MainActivity.MYTAG,"tambahKomentar() -> onResponse() -> if -> if");
                            Komentar_Video_Model lastAddedComment = items.get(0);
                            changeTotalCommentDataVideo(getAdapterPosition(),1);
                            if(commentAdapterBottomSheetLevel1!=null) {
                                addDataToRecyclerViewComment(commentAdapterBottomSheetLevel1, dataRecyclerKomentarBottomSheet, lastAddedComment , 0, recyclerCommentInBottomSheet);
                            }
                            else {
                                initDataRecyclerViewCommentOnFirstAdd(lastAddedComment);
                            }
                            //refresh recycler sehabis menambahkan komentar
                        } else if (jObj.getString("status").equals("false")) {


                        }
                        else
                        {

                        }

                        // Check for error node in json

                    } catch (JSONException e) {
                        Log.d(MainActivity.MYTAG,"catch tambah komentar ittuw "+e.getMessage());
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(MainActivity.MYTAG,"onErrorResponse tambah komentar ittuw "+ error.getMessage());
                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", idUser);
                    params.put("id_video",idVideo);
                    params.put("comment",komentar);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        }

        private void changeTotalCommentDataVideo(int position, int newValue) {
            int originalNumberOfComments = getOriginalNumberOfComments(position);
            int newNumberOfComments = calculateAndGetNewNumberOfComments(originalNumberOfComments, newValue);
            setNewNumberOfCommentsToTotalComments(position,newNumberOfComments);
        }

        private int getOriginalNumberOfComments(int position){
            return Integer.valueOf(dataVideo.get(position).getVideo_comment());
        }

        private int calculateAndGetNewNumberOfComments(int originalNumberOfComments, int newValue){
            return (originalNumberOfComments+newValue);
        }

        private void setNewNumberOfCommentsToTotalComments(int position, int newNumberOfCommentsToTotalComments)
        {
            dataVideo.get(position).setVideo_comment(String.valueOf(newNumberOfCommentsToTotalComments));
            setNewNumberOfCommentsToTextViewTotalComments(position, newNumberOfCommentsToTotalComments);
        }

        private void setNewNumberOfCommentsToTextViewTotalComments(int position, int newNumberOfCommentsToTotalComments)
        {
            String textnya = "<font color='black'>Komentar </font><font color='#d6d4d4'>"+String.valueOf(newNumberOfCommentsToTotalComments)+"</font>";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                komentar_video_count.setMovementMethod(LinkMovementMethod.getInstance());
                komentar_video_count.setText(Html.fromHtml(textnya, Html.FROM_HTML_MODE_LEGACY));
            } else {
                komentar_video_count.setMovementMethod(LinkMovementMethod.getInstance());
                komentar_video_count.setText(Html.fromHtml(textnya));
            }
        }

        public  void  addDataToRecyclerViewComment(RecyclerView.Adapter adapter, List<Komentar_Video_Model> allData, Komentar_Video_Model newData, int indeksPosisiBaru, RecyclerView recyclerView){
            Log.d(MainActivity.MYTAG,"sakjane terpanggil");
            txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
            allData.add(indeksPosisiBaru,newData);
            adapter.notifyItemInserted(indeksPosisiBaru);
            // adapter.notifyDataSetChanged();
            recyclerView.smoothScrollToPosition(indeksPosisiBaru);
        }
        //untuk tambah level 1
        private void initDataRecyclerViewCommentOnFirstAdd(Komentar_Video_Model items) {
            Log.d(MainActivity.MYTAG,"init ittuw");
            recyclerCommentInBottomSheet.setAdapter(null);
            commentAdapterBottomSheetLevel1=null;
            dataRecyclerKomentarBottomSheet.clear();
            dataRecyclerKomentarBottomSheet.add(items);
            commentAdapterBottomSheetLevel1 = new Komentar_Video_Level1_Adapter(context,dataRecyclerKomentarBottomSheet,this);
            LinearLayoutManager layoutLevel1 = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
            recyclerKomentarVideoLevel1.setLayoutManager(layoutLevel1);
            recyclerCommentInBottomSheet.setAdapter(commentAdapterBottomSheetLevel1);
            recyclerCommentInBottomSheet.setVisibility(View.VISIBLE);
            txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
        }

        @Override
        public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {

        }

        @Override
        public void balasKomentarLevel1(String idKomentarYangDibalas) {

        }


        public void setTag() {
            parent.setTag(this);
        }
    }

    private void cekDanInitIconAwalLoad(ImageView icon, Drawable drawableTidakAdaNilai, Drawable drawableAdaNilai, String idLikeString){
        if(idLikeString == null)
            icon.setImageDrawable(drawableTidakAdaNilai);
        else
            icon.setImageDrawable(drawableAdaNilai);
    }

    public interface VideoListener{
        void persiapanDownloadVideo(int position,String urlVideo,String namaVideo);
        void storeLike(String idVideo, String like);
        void shareVideo(String url);
        void storeSave(String idVideo, String save);
        void showAllComment(String idVideo);
        //method untuk passing value dari balasKomentar yang ada di Video_Adapter.java menuju Video.java
        void passingShowAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas);


    }

    private void toggleLike(int position,String idUser, String idVideo, String currentStatusLike, ImageView iconLike, TextView txtLike)
    {
        int jumlahLike = Integer.valueOf(dataVideo.get(position).getVideo_like());
        if(currentStatusLike == null) //jika sebelumnya belum ada like, set like ke adapter recycler video
        {
            //tidak ada like(0). jadikan ada like (1)
            dataVideo.get(position).setVideo_like_id("99");
            jumlahLike = jumlahLike+1;
            videoListener.storeLike(idVideo,"1");
            swapGambar(iconLike,context.getDrawable(R.drawable.love_video),150,150);
            //adapterVideo.notifyDataSetChanged();
        }
        else
        {
            dataVideo.get(position).setVideo_like_id(null);
            videoListener.storeLike(idVideo,"0");
            jumlahLike = jumlahLike-1;
            swapGambar(iconLike,context.getDrawable(R.drawable.love_fill),150,150);
            //adapterVideo.notifyDataSetChanged();
        }
        dataVideo.get(position).setVideo_like(String.valueOf(jumlahLike));
        txtLike.setText(String.valueOf(jumlahLike));
    }

    private void toggleSave(int position,String idUser, String idVideo, String currentStatusSave, ImageView iconSave){
        if(currentStatusSave == null) //jika sebelumnya belum ada like, set like ke adapter recycler video
        {
            //tidak ada like(0). jadikan ada like (1)
            dataVideo.get(position).setVideo_save_id("99");
            videoListener.storeSave(idVideo,"1");
            swapGambar(iconSave,context.getDrawable(R.drawable.ic_simpan_ajus_color),150,150);
            //adapterVideo.notifyDataSetChanged();
        }
        else
        {
            dataVideo.get(position).setVideo_save_id(null);
            videoListener.storeSave(idVideo,"0");
            swapGambar(iconSave,context.getDrawable(R.drawable.save),150,150);
            //adapterVideo.notifyDataSetChanged();
        }
    }

    private void swapGambar(ImageView imageView,Drawable drawableTujuan,int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }








}
