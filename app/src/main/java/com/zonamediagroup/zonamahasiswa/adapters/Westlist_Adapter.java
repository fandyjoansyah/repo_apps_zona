package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.R;

import com.zonamediagroup.zonamahasiswa.models.Westlist_Model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Westlist_Adapter extends RecyclerView.Adapter<Westlist_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Westlist_Model> barangdataList;
    private List<Westlist_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;


    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView tanggal, judul, kategori;
        ImageView img_thumnile, img_save;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            img_thumnile = view.findViewById(R.id.img_thumnile);
            tanggal = view.findViewById(R.id.tanggal);
            judul = view.findViewById(R.id.judul);
            kategori = view.findViewById(R.id.kategori);
            img_save = view.findViewById(R.id.img_save);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });

            img_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.Save(barangdataListFiltered.get(getAdapterPosition()));
                    final Westlist_Model produk_data = barangdataListFiltered.get(getAdapterPosition());
//                    Toast.makeText(context, "DATA "+produk_data.getGambar_user_wp(), Toast.LENGTH_SHORT).show(); // untuk mengambil nilai parameter


//                    img_save.setImageResource(R.drawable.ic_simpan_ajus);


                }
            });


        }
    }


    public Westlist_Adapter(Context context, List<Westlist_Model> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_terbaru_save, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Westlist_Model produk_data = barangdataListFiltered.get(position);
        //System.out.println("Potition " + (position + 1));

        String thumnile_post =  produk_data.getMeta_value();
//        Holder
        Glide.with(context)
                .load(thumnile_post)
                .apply(RequestOptions.centerCropTransform())
                .into(holder.img_thumnile);

        //holder.tanggal.setText(funcGetWaktu(produk_data.getPost_date()));
        holder.tanggal.setText(produk_data.getPost_date());
        holder.judul.setText(produk_data.getPost_title());
        holder.kategori.setText(produk_data.getName());


    }
    //fungsi untuk menambahkan suatu karakter pada string pada posisi tertentu
    public String addChar(String str, char ch, int position) {
        int len = str.length();
        char[] updatedArr = new char[len + 1];
        str.getChars(0, position, updatedArr, 0);
        updatedArr[position] = ch;
        str.getChars(position, len, updatedArr, position + 1);
        return new String(updatedArr);
    }

    //merubah tampilan waktu ke DD MMMM YYYY I HH:MM
    private String funcGetWaktu(String date_post)
    {
        // hilangkan ":<detik digit ke 1><detik detik digit ke 2>"
        date_post = date_post.substring(0, date_post.length() - 3);

        //tambahkan karakter pipe diantara jam dan tanggal
        date_post = addChar(date_post,'|',11);

        //tambahkan spasi setelah pipe
        date_post = addChar(date_post,' ',12);

        //Mulai penyekatan, segala yang disebelah kiri pipe | adalah tanggalBulanTahun. segala yang disebelah kanan pipe adalah jam menit
        //split karakter dengan patokan pipe |. karakter pipe harus di escape dengan 2 kali \\
        String[] parts = date_post.split("\\|");
        String tanggalBulanTahun = parts[0];
        tanggalBulanTahun = tanggalBulanTahun.trim();
        String menitJam = parts[1];
        menitJam = menitJam.trim();

        //rubah format tanggal ke dd MMMM YYYYY
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("dd MMMM yyyy");
            Date date = format1.parse(tanggalBulanTahun);
            //pakai format baru yaitu bulan ditampilkan namanya
            tanggalBulanTahun = format2.format(date);
        }
        catch(Exception e)
        {
            Log.e("ErrorZona","Terjadi Error "+e.getMessage());
        }

        String tanggalBerita = tanggalBulanTahun+" I "+menitJam;
        return tanggalBerita;
    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Westlist_Model> filteredList = new ArrayList<>();
                    for (Westlist_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getPost_title().toLowerCase().contains(charString.toLowerCase()) || row.getPost_title().contains(charSequence) || row.getPost_title().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Westlist_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Westlist_Model transaksihariinidataList);

        void Save(Westlist_Model save);
//        void Save(Recyclerview_home save);

//        void Westlist(Recyclerview_home weslist);

//        void share_voto(Home_satu_Model weslist);
    }
}
