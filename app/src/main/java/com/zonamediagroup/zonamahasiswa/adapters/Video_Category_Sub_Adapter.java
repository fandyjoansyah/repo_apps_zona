package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Video_Category_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Category_Sub_Model;

import java.util.List;

public class Video_Category_Sub_Adapter extends RecyclerView.Adapter<Video_Category_Sub_Adapter.ViewHolder> {
    List<Video_Category_Sub_Model> dataCategorySub;
    Video_Category_Sub_Adapter.VideoListener videoListener;
    Context context;
    public Video_Category_Sub_Adapter(Context context, List<Video_Category_Sub_Model> dataCategorySub, Video_Category_Sub_Adapter.VideoListener videoListener){
        this.dataCategorySub = dataCategorySub;
        this.context = context;
        this.videoListener = videoListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_category_sub,parent,false);
        return new Video_Category_Sub_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Video_Category_Sub_Model dataVC = dataCategorySub.get(position);
        Glide.with(context).load(dataCategorySub.get(position).getVideo_category_sub_image())
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.image_category_sub);

        float scale = context.getResources().getDisplayMetrics().density;
        int samping = (int) (20 * scale + 0.5f);
        int semua = (int) (5 * scale + 0.5f);

        if(position == 0){
            holder.layout_sub_category.setPadding(samping,0,semua,0);
        }else if((int) position == getItemCount()-1 ){
            holder.layout_sub_category.setPadding(semua,0,samping,0);
        }else{
            holder.layout_sub_category.setPadding(semua,0,semua,0);
        }

        holder.image_category_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoListener.changeCategoryVideo(position, "SubCategory", "", dataVC.getVideo_category_sub_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataCategorySub.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image_category_sub;
        LinearLayout layout_sub_category;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image_category_sub = itemView.findViewById(R.id.image_category_sub);
            layout_sub_category = itemView.findViewById(R.id.layout_sub_category);
        }
    }

    public interface VideoListener {
        void changeCategoryVideo(int position, String Type, String video_category_id, String video_category_sub_id);
    }
}
