package com.zonamediagroup.zonamahasiswa.adapters;

import android.animation.Animator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Recyclerview_home;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Recyclerview_home_Adapter extends RecyclerView.Adapter<Recyclerview_home_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Recyclerview_home> barangdataList;
    private List<Recyclerview_home> barangdataListFiltered;
    private ContactsAdapterListener listener;
    boolean trending = false; //merupakan trending atau bukan. default bukan (0)



    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView tanggal, judul, kategori;
        ImageView img_thumnile, img_save;
        ProgressBar pb_item_terbaru;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen
            img_thumnile = view.findViewById(R.id.img_thumnile);
            tanggal = view.findViewById(R.id.tanggal);
            judul = view.findViewById(R.id.judul);
            kategori = view.findViewById(R.id.kategori);
            img_save = view.findViewById(R.id.img_save);
            pb_item_terbaru = view.findViewById(R.id.pb_item_terbaru);

//            End Devinisi



            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()),getAdapterPosition());
                }
            });

            img_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //listener.Save(barangdataListFiltered.get(getAdapterPosition()));

                    final Recyclerview_home produk_data = barangdataListFiltered.get(getAdapterPosition());
//                    Toast.makeText(context, "DATA "+produk_data.getGambar_user_wp(), Toast.LENGTH_SHORT).show(); // untuk mengambil nilai parameter

                  //  Toast.makeText(context,"recycler home di klik",Toast.LENGTH_SHORT).show();
                  //  img_save.setImageResource(R.drawable.ic_simpan_ajus_color);

//                    final long maxCounter = 150;
//                    long diff = 100;
//
//                    new CountDownTimer(maxCounter, diff) {
//
//                        public void onTick(long millisUntilFinished) {
////                        long diff = maxCounter - millisUntilFinished;
//
//
//                        }
//
//                        public void onFinish() {
//
//
//                            img_save.setImageResource(R.drawable.ic_simpan_ajus_color);
//
//                        }
//
//                    }.start();

                    //cek apakah gambar sekarang merupakan gambar simpan / belum simpan
                    //if (img_save.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_simpan_ajus_color).getConstantState()) {
                    if(img_save.getDrawable().getConstantState().equals(img_save.getContext().getDrawable(R.drawable.ic_simpan_ajus_color).getConstantState()))
                    {
                        //img_save.setImageDrawable(context.getDrawable(R.drawable.ic_simpan_ajus));
                        funcTransisiGambar(img_save,context.getDrawable(R.drawable.ic_simpan_ajus),150,150);
                        //panggil method unsave di activity
                        listener.unSave(barangdataListFiltered.get(getAdapterPosition()),getAdapterPosition());
                    }
                    //else if(img_save.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_simpan_ajus).getConstantState())
                    else if(img_save.getDrawable().getConstantState().equals(img_save.getContext().getDrawable(R.drawable.ic_simpan_ajus).getConstantState()))
                    {
                        //img_save.setImageDrawable(context.getDrawable(R.drawable.ic_simpan_ajus_color));
                        funcTransisiGambar(img_save,context.getDrawable(R.drawable.ic_simpan_ajus_color),150,150);
                        //panggil method save di activity
                        listener.Save(barangdataListFiltered.get(getAdapterPosition()),getAdapterPosition());
                    }




                }
            });


        }
    }


    public Recyclerview_home_Adapter(Context context, List<Recyclerview_home> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
    }

    public Recyclerview_home_Adapter(Context context, List<Recyclerview_home> barangdataList, ContactsAdapterListener listener, String customRecycler) { //customRecycler adalah param optional. berisi "trending" / kosong. apabila isinya "trending" maka load layout untuk trending
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
        if(customRecycler.equals("trending"))
        {
            trending = true;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        //cek apakah trending atau bukan
        if(trending == true)
        {
             itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_terbaru_trending, parent, false);
        }
        else {
             itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_terbaru, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    //fungsi untuk menambahkan suatu karakter pada string pada posisi tertentu
    public String addChar(String str, char ch, int position) {
        int len = str.length();
        char[] updatedArr = new char[len + 1];
        str.getChars(0, position, updatedArr, 0);
        updatedArr[position] = ch;
        str.getChars(position, len, updatedArr, position + 1);
        return new String(updatedArr);
    }

    //merubah tampilan waktu ke DD MMMM YYYY I HH:MM
    private String funcGetWaktu(String date_post)
    {
        // hilangkan ":<detik digit ke 1><detik detik digit ke 2>"
        date_post = date_post.substring(0, date_post.length() - 3);

        //tambahkan karakter pipe diantara jam dan tanggal
        date_post = addChar(date_post,'|',11);

        //tambahkan spasi setelah pipe
        date_post = addChar(date_post,' ',12);

        //Mulai penyekatan, segala yang disebelah kiri pipe | adalah tanggalBulanTahun. segala yang disebelah kanan pipe adalah jam menit
        //split karakter dengan patokan pipe |. karakter pipe harus di escape dengan 2 kali \\
        String[] parts = date_post.split("\\|");
        String tanggalBulanTahun = parts[0];
        tanggalBulanTahun = tanggalBulanTahun.trim();
        String menitJam = parts[1];
        menitJam = menitJam.trim();

        //rubah format tanggal ke dd MMMM YYYYY
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("dd MMMM yyyy");
            Date date = format1.parse(tanggalBulanTahun);
            //pakai format baru yaitu bulan ditampilkan namanya
            tanggalBulanTahun = format2.format(date);
        }
        catch(Exception e)
        {
            Log.e("ErrorZona","Terjadi Error "+e.getMessage());
        }

        String tanggalBerita = tanggalBulanTahun+" I "+menitJam;
        return tanggalBerita;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Recyclerview_home produk_data = barangdataListFiltered.get(position);
        //System.out.println("Potition " + (position + 1));

        String thumnile_post =  produk_data.getMeta_value();
//        Holder
        if(thumnile_post.isEmpty()) {
            Picasso.get().load(R.drawable.ic_img_thumnile).fit().into(holder.img_thumnile, new Callback() {
                @Override
                public void onSuccess() {
                    holder.pb_item_terbaru.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    holder.pb_item_terbaru.setVisibility(View.GONE);
                }
            });
        }
        else {
            Picasso.get().load(thumnile_post).fit().into(holder.img_thumnile, new Callback() {
                @Override
                public void onSuccess() {
                    holder.pb_item_terbaru.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    holder.pb_item_terbaru.setVisibility(View.GONE);
                }
            });
        }

        //holder.tanggal.setText(produk_data.getPost_date());
        //holder.tanggal.setText(funcGetWaktu(produk_data.getPost_date()));
        holder.tanggal.setText(produk_data.getPost_date());
        holder.judul.setText(produk_data.getPost_title());
        holder.kategori.setText(produk_data.getName());

        if (produk_data.getLike().equals("0")) {
            // tidak like
            holder.img_save.setImageResource(R.drawable.ic_simpan_ajus);
        } else {
            // like
            holder.img_save.setImageResource(R.drawable.ic_simpan_ajus_color);
        }

//        Holder


    }

    private void funcTransisiGambar(ImageView imageView,Drawable drawableTujuan,int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Recyclerview_home> filteredList = new ArrayList<>();
                    for (Recyclerview_home row : barangdataList) {

//                       Filter list receler view
                        if (row.getPost_title().toLowerCase().contains(charString.toLowerCase()) || row.getPost_title().contains(charSequence) || row.getPost_title().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Recyclerview_home>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Recyclerview_home transaksihariinidataList, int position);

        void Save(Recyclerview_home save, int position);

        void unSave(Recyclerview_home save, int position);

//        void Westlist(Recyclerview_home weslist);

//        void share_voto(Home_satu_Model weslist);
    }
}
