package com.zonamediagroup.zonamahasiswa.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Video_share;
import com.zonamediagroup.zonamahasiswa.models.Video_Category_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Notif_Model;

import java.util.List;

public class Video_Notif_Adapter extends RecyclerView.Adapter<Video_Notif_Adapter.ViewHolder> {
    Context context;
    List<Video_Notif_Model> dataNotif;

    public Video_Notif_Adapter(Context context, List<Video_Notif_Model> dataNotif){
        this.dataNotif = dataNotif;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_notif,parent,false);
        return new Video_Notif_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Video_Notif_Adapter.ViewHolder holder, int position) {
        Video_Notif_Model dataN = dataNotif.get(position);
        if(dataN.getPerson_photo() != null) {
            Glide.with(context).load(dataN.getPerson_photo())
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.gambar_author_video);
        }
        Glide.with(context).load(dataN.getVideo_notification_image())
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.gambar_thumbnail_video);
        String html;
        if(dataN.getPerson_name() == null || dataN.getPerson_name().equals('0')){
            html = "<b>Zona Mahasiswa</b> " + dataN.getVideo_notification_title();
        }else {
            html = "<b>" + dataN.getPerson_name() + "</b> " + dataN.getVideo_notification_title() ;
        }
        @SuppressLint("WrongConstant") Spanned result = HtmlCompat.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        holder.nama_pengirim_and_title.setText(result);
        holder.nama_pengirim_and_title.setMovementMethod(LinkMovementMethod.getInstance());

        holder.tanggal_post_video.setText(dataN.getConvert_time());
        //start aksi klik
        Log.d(MainActivity.MYTAG,"data notif fandy size "+dataNotif.size());
        Log.d(MainActivity.MYTAG,"data notif fandy thumbnail ke  "+position+" adalah "+dataN.getVideo_thumbnail());
        Log.d(MainActivity.MYTAG,"data notif fandy title ke  "+position+" adalah "+dataN.getVideo_title());
        Log.d(MainActivity.MYTAG,"data notif fandy id video ke  "+position+" adalah "+dataN.getVideo_id());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.openVideoShare(dataN);
            }
        });
        holder.gambar_thumbnail_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.openVideoShare(dataN);
            }
        });
        holder.nama_pengirim_and_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.openVideoShare(dataN);
            }
        });
        holder.tanggal_post_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.openVideoShare(dataN);
            }
        });
        //end aksi klik
    }

    @Override
    public int getItemCount() {
        return dataNotif.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView gambar_author_video, gambar_thumbnail_video;
        TextView nama_pengirim_and_title, tanggal_post_video;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            gambar_author_video = itemView.findViewById(R.id.gambar_author_video);
            nama_pengirim_and_title = itemView.findViewById((R.id.nama_pengirim_and_title));
            gambar_thumbnail_video = itemView.findViewById(R.id.gambar_thumbnail_video);
            tanggal_post_video = itemView.findViewById(R.id.tanggal_post_video);
        }

        private Bundle prepareDataForVideoShare(Video_Notif_Model dataNotifModel){
            Bundle dataForVideoShare = new Bundle();
            dataForVideoShare.putString("video_notification_id",dataNotifModel.getVideo_notification_id());
            dataForVideoShare.putString("video_id",dataNotifModel.getVideo_id());
            dataForVideoShare.putString("person_id",dataNotifModel.getPerson_id());
            dataForVideoShare.putString("from_person_id",dataNotifModel.getFrom_person_id());
            dataForVideoShare.putString("video_notification_title",dataNotifModel.getVideo_notification_title());
            dataForVideoShare.putString("video_notification_description",dataNotifModel.getVideo_notification_description());
            dataForVideoShare.putString("video_notification_image",dataNotifModel.getVideo_notification_id());
            dataForVideoShare.putString("video_notification_type",dataNotifModel.getVideo_notification_type());
            dataForVideoShare.putString("video_notification_date",dataNotifModel.getVideo_notification_date());
            dataForVideoShare.putString("comment_id",dataNotifModel.getComment_id());
            dataForVideoShare.putString("id_user",dataNotifModel.getId_user());
            dataForVideoShare.putString("person_name",dataNotifModel.getPerson_name());
            dataForVideoShare.putString("person_given_name",dataNotifModel.getPerson_given_name());
            dataForVideoShare.putString("person_family_name",dataNotifModel.getPerson_family_name());
            dataForVideoShare.putString("person_email",dataNotifModel.getPerson_email());
            dataForVideoShare.putString("person_photo",dataNotifModel.getPerson_photo());
            dataForVideoShare.putString("person_token",dataNotifModel.getPerson_token());
            dataForVideoShare.putString("video_title",dataNotifModel.getVideo_title());
            dataForVideoShare.putString("video_deskripsi",dataNotifModel.getVideo_deskripsi());
            dataForVideoShare.putString("video_category_id",dataNotifModel.getVideo_category_id());
            dataForVideoShare.putString("video_category_sub_id",dataNotifModel.getVideo_category_sub_id());
            dataForVideoShare.putString("youtube_video_id",dataNotifModel.getYoutube_video_id());
            dataForVideoShare.putString("video_url",dataNotifModel.getVideo_url());
            dataForVideoShare.putString("video_thumbnail",dataNotifModel.getVideo_thumbnail());
            dataForVideoShare.putString("video_share",dataNotifModel.getVideo_share());
            dataForVideoShare.putString("video_like",dataNotifModel.getVideo_like());
            dataForVideoShare.putString("video_save",dataNotifModel.getVideo_save());
            dataForVideoShare.putString("video_view",dataNotifModel.getVideo_view());
            dataForVideoShare.putString("video_duration",dataNotifModel.getVideo_duration());
            dataForVideoShare.putString("video_play_total",dataNotifModel.getVideo_play_total());
            dataForVideoShare.putString("video_comment",dataNotifModel.getVideo_comment());
            dataForVideoShare.putString("video_date_created",dataNotifModel.getVideo_date_created());
            dataForVideoShare.putString("user_id",dataNotifModel.getUser_id());
            dataForVideoShare.putString("video_publish_status",dataNotifModel.getVideo_publish_status());
            dataForVideoShare.putString("video_share_url",dataNotifModel.getVideo_share_url());
            dataForVideoShare.putString("video_rate",dataNotifModel.getVideo_rate());
            dataForVideoShare.putString("convert_time",dataNotifModel.getConvert_time());
            dataForVideoShare.putString("comment_reply_id",dataNotifModel.getComment_reply_id());

            return dataForVideoShare;
        }
        private void openVideoShare(Video_Notif_Model dataNotifModel)
        {
            Intent i = new Intent(context, Video_share.class);
                i.putExtra("data_bundle_notif",prepareDataForVideoShare(dataNotifModel));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
