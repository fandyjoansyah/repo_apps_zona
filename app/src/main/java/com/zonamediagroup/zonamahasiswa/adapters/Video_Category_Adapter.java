package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Video_Category_Model;

import java.util.List;

public class Video_Category_Adapter extends RecyclerView.Adapter<Video_Category_Adapter.ViewHolder>{
    List<Video_Category_Model> dataCategory;
    Video_Category_Adapter.VideoListener videoListener;
   Context context;
   String initKategori = "1";

   public Video_Category_Adapter(Context context, List<Video_Category_Model> dataCategory, Video_Category_Adapter.VideoListener videoListener){
       this.dataCategory = dataCategory;
       this.context = context;
       this.videoListener = videoListener;
       this.initKategori = "1";
   }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_category,parent,false);
        return new Video_Category_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       Log.d(MainActivity.MYTAG,"on bind category");
       Video_Category_Model dataVC = dataCategory.get(position);
       if(dataVC.getVideo_category_id().equals(initKategori)) {
//           String img = !dataVC.getVideo_category_image_name().equals(null) ? dataVC.getVideo_category_image_name() : dataVC.getVideo_category_image_click() ;
           if (dataVC.getVideo_category_image_name_click() != null){
               Glide.with(context).load(context.getResources().getIdentifier(dataVC.getVideo_category_image_name_click(), "drawable", context.getPackageName()))
                       .apply(RequestOptions.fitCenterTransform())
                       .into(holder.image_category);
           }else {
           Glide.with(context).load(dataVC.getVideo_category_image_click())
                   .apply(RequestOptions.fitCenterTransform())
                   .into(holder.image_category);
           }
       }else{
           if (dataVC.getVideo_category_image_name() != null) {
               Glide.with(context).load(context.getResources().getIdentifier(dataVC.getVideo_category_image_name(), "drawable", context.getPackageName()))
                       .apply(RequestOptions.fitCenterTransform())
                       .into(holder.image_category);
           }else{
               Glide.with(context).load(dataVC.getVideo_category_image())
                       .apply(RequestOptions.fitCenterTransform())
                       .into(holder.image_category);
           }
       }
        float scale = context.getResources().getDisplayMetrics().density;
        int samping = (int) (20 * scale + 0.5f);
        int semua = (int) (2 * scale + 0.5f);
        if(position == 0){
            holder.layout_video_category.setPadding(samping,0,semua,0);
        }else if((int) position == getItemCount()-1 ){
            holder.layout_video_category.setPadding(semua,0,samping,0);
        }else{
            holder.layout_video_category.setPadding(semua,0,semua, 0);
        }
        holder.text_category.setText(dataVC.getVideo_category_nama());

        holder.layout_video_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initKategori = dataVC.getVideo_category_id();
                videoListener.changeCategoryVideo(0, "Category", dataVC.getVideo_category_id(), "");
              //  CustomToast.s(context,"kategori di klik : " + dataVC.getVideo_category_id());
            }
        });
    }


    @Override
    public int getItemCount() {
        return dataCategory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
    TextView text_category;
    ImageView image_category;
    LinearLayout layout_video_category;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_category = itemView.findViewById(R.id.text_category);
            image_category = itemView.findViewById(R.id.image_category);
            layout_video_category = itemView.findViewById(R.id.layout_video_category);
        }
    }

    public interface VideoListener{
       void changeCategoryVideo(int position, String Type, String video_category_id, String video_category_sub_id);
    }
}
