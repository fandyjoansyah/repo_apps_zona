package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zonamediagroup.zonamahasiswa.R;

import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class CustomShareAdapter extends RecyclerView.Adapter<CustomShareAdapter.MyView> {
    // List with String type
    private List<String> list;
    Intent shareIntent;
    PackageManager pm = null;
    Context context;

    List<ResolveInfo> launchables;
    // View Holder class which
    // extends RecyclerView.ViewHolder
    public class MyView extends RecyclerView.ViewHolder {

        // Text View
        TextView textView;

        ImageView imageView;

        // parameterised constructor for View Holder class
        // which takes the view as a parameter
        public MyView(View view)
        {
            super(view);

            // initialise TextView with id
            textView = (TextView)view
                    .findViewById(R.id.textview_ifsrv);
            imageView = (ImageView)view.findViewById(R.id.gambar_ifsrv);
        }
    }

    // Constructor for adapter class
    // which takes a list of String type
    public CustomShareAdapter(Intent shareIntent, Context context)
    {
        this.context = context;
        this.shareIntent = shareIntent;
        this.pm = context.getPackageManager();
        this.launchables=pm.queryIntentActivities(shareIntent, 0);
        Collections.sort(launchables, new ResolveInfo.DisplayNameComparator(pm));


    }


    // Override onCreateViewHolder which deals
    // with the inflation of the card layout
    // as an item for the RecyclerView.
    @Override
    public MyView onCreateViewHolder(ViewGroup parent,
                                     int viewType)
    {

        // Inflate item.xml using LayoutInflator
        View itemView
                = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_for_share_recycler_view,
                        parent,
                        false);

        // return itemView
        return new MyView(itemView);
    }

    // Override onBindViewHolder which deals
    // with the setting of different data
    // and methods related to clicks on
    // particular items of the RecyclerView.
    @Override
    public void onBindViewHolder(final MyView holder,
                                 final int position)
    {
        // Set the text of each item of
        // Recycler view with the list items
        holder.textView.setText(launchables.get(position).loadLabel(pm));
        holder.imageView.setImageDrawable(launchables.get(position).loadIcon(pm));


        //onClickListener ketika suatu list dari recycler view di klik
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"velmurugan@androidtoppers.com"});
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Hi");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Hi,This is Test");
                shareIntent.setType("text/plain");
                ResolveInfo launchable=launchables.get(holder.getAdapterPosition());
                ActivityInfo activity=launchable.activityInfo;
                ComponentName name=new ComponentName(activity.applicationInfo.packageName,
                        activity.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                view.getContext().startActivity(shareIntent);
            }
        });

    }

    // Override getItemCount which Returns
    // the length of the RecyclerView.
    @Override
    public int getItemCount()
    {
        return launchables.size();
    }
}
