package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.imageview.ShapeableImageView;
import com.zonamediagroup.zonamahasiswa.R;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.viewpager.widget.PagerAdapter;

public class Polling_ViewPagerAdapter extends PagerAdapter {
    private Context mContext;

    @Override
    public int getCount() {
        return sliderImageid.length;
    }

    private int[] sliderImageid = new int[]{
            R.drawable.share_36px, R.drawable.hati_36px,
            R.drawable.speech_bubble_36px
    };

    public Polling_ViewPagerAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView mImageView = new ImageView(mContext);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mImageView.setImageResource(sliderImageid[position]);
        container.addView(mImageView, 0);
        return mImageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }
}
