package com.zonamediagroup.zonamahasiswa.adapters;

import android.animation.Animator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Rekomendasi_Model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Rekomendasi_Adapter extends RecyclerView.Adapter<Rekomendasi_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Rekomendasi_Model> barangdataList;
    private List<Rekomendasi_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;


    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView tanggal, judul, kategori;
        ImageView img_thumnile, img_save;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            img_thumnile=view.findViewById(R.id.img_thumnile);
            tanggal=view.findViewById(R.id.tanggal);
            judul=view.findViewById(R.id.judul);
            kategori=view.findViewById(R.id.kategori);
            img_save=view.findViewById(R.id.img_save);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });

            img_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.Save(barangdataListFiltered.get(getAdapterPosition()),getAdapterPosition());

                    final Rekomendasi_Model produk_data = barangdataListFiltered.get(getAdapterPosition());
//                    Toast.makeText(context, "DATA "+produk_data.getGambar_user_wp(), Toast.LENGTH_SHORT).show(); // untuk mengambil nilai parameter


                    //cek apakah gambar sekarang merupakan gambar simpan / belum simpan
                    if (img_save.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_simpan_ajus_color).getConstantState()) {
                        //img_save.setImageDrawable(context.getDrawable(R.drawable.ic_simpan_ajus));
                        funcTransisiGambar(img_save,context.getDrawable(R.drawable.ic_simpan_ajus),150,150);
                        //panggil method unsave di activity
                        listener.unSave(barangdataListFiltered.get(getAdapterPosition()),getAdapterPosition());
                    } else if(img_save.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_simpan_ajus).getConstantState()) {
                        //img_save.setImageDrawable(context.getDrawable(R.drawable.ic_simpan_ajus_color));
                        funcTransisiGambar(img_save,context.getDrawable(R.drawable.ic_simpan_ajus_color),150,150);
                        //panggil method save di activity
                        listener.Save(barangdataListFiltered.get(getAdapterPosition()),getAdapterPosition());
                    }
//                    final long maxCounter = 150;
//                    long diff = 100;
//
//                    new CountDownTimer(maxCounter, diff) {
//
//                        public void onTick(long millisUntilFinished) {
////                        long diff = maxCounter - millisUntilFinished;
//
//
//                        }
//
//                        public void onFinish() {
//
//
//                            img_save.setImageResource(R.drawable.ic_simpan_ajus_color);
//
//                        }
//
//                    }.start();
                }
            });

        }
    }


    public Rekomendasi_Adapter(Context context, List<Rekomendasi_Model> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_terbaru_untuk_artikel_lainnya, parent, false);

        return new MyViewHolder(itemView);
    }
    //fungsi untuk menambahkan suatu karakter pada string pada posisi tertentu
    public String addChar(String str, char ch, int position) {
        int len = str.length();
        char[] updatedArr = new char[len + 1];
        str.getChars(0, position, updatedArr, 0);
        updatedArr[position] = ch;
        str.getChars(position, len, updatedArr, position + 1);
        return new String(updatedArr);
    }

    //merubah tampilan waktu ke DD MMMM YYYY I HH:MM
    private String funcGetWaktu(String date_post)
    {
        // hilangkan ":<detik digit ke 1><detik detik digit ke 2>"
        date_post = date_post.substring(0, date_post.length() - 3);

        //tambahkan karakter pipe diantara jam dan tanggal
        date_post = addChar(date_post,'|',11);

        //tambahkan spasi setelah pipe
        date_post = addChar(date_post,' ',12);

        //Mulai penyekatan, segala yang disebelah kiri pipe | adalah tanggalBulanTahun. segala yang disebelah kanan pipe adalah jam menit
        //split karakter dengan patokan pipe |. karakter pipe harus di escape dengan 2 kali \\
        String[] parts = date_post.split("\\|");
        String tanggalBulanTahun = parts[0];
        tanggalBulanTahun = tanggalBulanTahun.trim();
        String menitJam = parts[1];
        menitJam = menitJam.trim();

        //rubah format tanggal ke dd MMMM YYYYY
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("dd MMMM yyyy");
            Date date = format1.parse(tanggalBulanTahun);
            //pakai format baru yaitu bulan ditampilkan namanya
            tanggalBulanTahun = format2.format(date);
        }
        catch(Exception e)
        {
            Log.e("ErrorZona","Terjadi Error "+e.getMessage());
        }

        String tanggalBerita = tanggalBulanTahun+" I "+menitJam;
        return tanggalBerita;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Rekomendasi_Model produk_data = barangdataListFiltered.get(position);
       // System.out.println("Potition " + (position + 1));


//        Holder


        String thumnile_post = produk_data.getMeta_value();
//        Holder
        Glide.with(context)
                .load(thumnile_post)
                .apply(RequestOptions.centerCropTransform())
                .into(holder.img_thumnile);

        //holder.tanggal.setText(funcGetWaktu(produk_data.getPost_date()));
        holder.tanggal.setText(produk_data.getPost_date());
        holder.judul.setText(produk_data.getPost_title());
        holder.kategori.setText(produk_data.getName());

        if(produk_data.getLike().equals("0")){
            // tidak like
        }else{
            // like
            holder.img_save.setImageResource(R.drawable.ic_simpan_ajus_color);
        }

//        Holder

    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Rekomendasi_Model> filteredList = new ArrayList<>();
                    for (Rekomendasi_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getName().contains(charSequence) || row.getName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Rekomendasi_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Rekomendasi_Model transaksihariinidataList);



        void Save(Rekomendasi_Model rekomendasi_model, int position);

        void unSave(Rekomendasi_Model rekomendasi_model, int position);
    }

    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }
}
