package com.zonamediagroup.zonamahasiswa.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.DaftarBlokir;
import com.zonamediagroup.zonamahasiswa.Kirim_file;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.TampilGrafikPollingActivity;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Blokir_Model;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Poling_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class Blokir_Adapter extends RecyclerView.Adapter<Blokir_Adapter.MyViewHolder>
{
    private static final String URL_BUKA_BLOKIR_PENGGUNA = Server.URL_REAL_NATIVE+"Buka_blokir_pengguna";
    Context context;
    List<Blokir_Model>dataBlokir;
    String id_user_sv;


    public Blokir_Adapter(Context context, List<Blokir_Model> dataBlokir){
        this.context = context;
        this.dataBlokir = dataBlokir;
        id_user_sv = SharedPrefManagerZona.getInstance(context).getid_user();
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blokir,parent,false);
        return new Blokir_Adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Glide.with(context)
                .load(dataBlokir.get(position).getPerson_photo())
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.img_person_photo);
        holder.txt_nama_pengguna.setText(dataBlokir.get(position).getPerson_name());
        holder.btn_buka_blokir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //                    start confirm dialog
                new AlertDialog.Builder(context)
                        .setTitle("Buka Blokir Pengguna")
                        .setMessage("Membuka blokir"+holder.txt_nama_pengguna.getText()+" ?")
                        .setPositiveButton("Buka Blokir", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                bukaBlokirPengguna(id_user_sv,dataBlokir.get(position).getPerson_id(),position);
                            }})
                        .setNegativeButton("Batal", null).show();
//                    end comfirm dialog
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBlokir.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_person_photo;
        TextView txt_nama_pengguna;
        TextView btn_buka_blokir;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_person_photo = itemView.findViewById(R.id.img_person_photo);
            txt_nama_pengguna = itemView.findViewById(R.id.txt_nama_pengguna);
            btn_buka_blokir = itemView.findViewById(R.id.btn_buka_blokir);

        }

    }

    public void showLayoutEmptyIfDataEmpty(int size)
    {
        if (context instanceof DaftarBlokir) {
            if(size <= 0)
            ((DaftarBlokir) context).tidakAdaDataBlokir();
            else
                ((DaftarBlokir) context).adaDataBlokir();
        }
    }

    public void removeDataBlokirByPosition(int position)
    {
        dataBlokir.remove(position);
        notifyItemRemoved(position);
    }

    public void bukaBlokirPengguna(String personIdPemblokir, String personIdDiblokir, int position){
//        start pg
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Membuka blokir...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
//        end pg


        //start bukaBlokir pengguna
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_BUKA_BLOKIR_PENGGUNA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    Log.d(MainActivity.MYTAG,"try tambah komentar ittuw");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        removeDataBlokirByPosition(position);
                        showLayoutEmptyIfDataEmpty(dataBlokir.size());


                    } else if (jObj.getString("status").equals("false")) {


                    }
                    else
                    {

                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    Log.d(MainActivity.MYTAG,"catch send blokir pengguna "+e.getMessage());
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(MainActivity.MYTAG,"onErrorResponse send blokir pengguna "+ error.getMessage());
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("person_id_pemblokir",personIdPemblokir);
                params.put("person_id_diblokir",personIdDiblokir);

                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end bukaBlokir Pengguna
    }
}
