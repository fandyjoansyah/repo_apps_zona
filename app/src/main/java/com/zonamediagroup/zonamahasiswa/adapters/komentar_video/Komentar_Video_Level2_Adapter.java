package com.zonamediagroup.zonamahasiswa.adapters.komentar_video;

import android.animation.Animator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Komentar_Video_Level2_Adapter extends RecyclerView.Adapter<Komentar_Video_Level2_Adapter.ViewHolder> {
    Context context;
    String URL_LIKE_KOMENTAR_VIDEO_Level2 = Server.URL_REAL_VIDEO+"Video_comment_like/index_post";
    int jumlahLikeSekarang=0;
    List<Komentar_Video_Model> komentarLevel2;
    InterfaceKomentarLevel2 listenerKomentarLevel2;
    public Komentar_Video_Level2_Adapter(Context context, List<Komentar_Video_Model> komentarLevel2, InterfaceKomentarLevel2 listenerKomentarLevel2) {
        this.context = context;
        this.komentarLevel2 = komentarLevel2;
        this.listenerKomentarLevel2 = listenerKomentarLevel2;
        Log.d(MainActivity.MYTAG,"Constructor Adapter RNDD");
    }
    @NonNull
    @Override
    public Komentar_Video_Level2_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komentar_video_level2,parent,false);
        Log.d(MainActivity.MYTAG,"onCreate RNDD");
        return new Komentar_Video_Level2_Adapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Komentar_Video_Level2_Adapter.ViewHolder holder, int position) {

        if(komentarLevel2.get(position).getPerson_name() != null){
            Komentar_Video_Model dataKomentar = komentarLevel2.get(position);
            holder.tvKomentar.setText(dataKomentar.getVideo_comment());
            if(dataKomentar.getVideo_comment_like() != null)
            holder.jumlahKomentarLevel2.setText(dataKomentar.getVideo_comment_like());
            holder.tvNamaKomentator.setText(dataKomentar.getPerson_name());
            holder.tvWaktuKomentar.setText(dataKomentar.getConvert_time());
            if(dataKomentar.getVideo_comment_like_id() != null)
                holder.imgLikeLevel2.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
            Glide.with(context)
                    .load(komentarLevel2.get(position).getPerson_photo())
                    .into(holder.fotoProfilKomentarLevel2);



//            holder.imgLikeLevel2.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    jumlahLikeSekarang = Integer.valueOf(dataKomentar.getVideo_comment_like());
//                    if(holder.imgLikeLevel2.getDrawable().getConstantState().equals(holder.imgLikeLevel2.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang++;
//                        holder.jumlahKomentarLevel2.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel2.get(position).getVideo_comment_id(),"1");
//                        funcTransisiGambar(holder.imgLikeLevel2,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    else if(holder.imgLikeLevel2.getDrawable().getConstantState().equals(holder.imgLikeLevel2.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang--;
//                        holder.jumlahKomentarLevel2.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel2.get(position).getVideo_comment_id(),"0");
//                        funcTransisiGambar(holder.imgLikeLevel2,context.getDrawable(R.drawable.hati_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                }
//            });
            holder.txt_balas_komentar_video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //listenerKomentarLevel2.showAllCommentForReply(dataKomentar.getVideo_id(),dataKomentar.getVideo_comment_id(),dataKomentar.getPerson_name(), dataKomentar.getPerson_id());
                        listenerKomentarLevel2.balasKomentarLevel2(dataKomentar.getPerson_name());
                }
            });

            holder.linear_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    jumlahLikeSekarang = Integer.valueOf(holder.jumlahKomentarLevel2.getText().toString());

                    if(holder.imgLikeLevel2.getDrawable().getConstantState().equals(holder.imgLikeLevel2.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
                    {
                        jumlahLikeSekarang = jumlahLikeSekarang+1;
                        holder.jumlahKomentarLevel2.setText(String.valueOf(jumlahLikeSekarang));
                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel2.get(position).getVideo_comment_id(),"1",komentarLevel2.get(position).getVideo_id());
                        funcTransisiGambar(holder.imgLikeLevel2,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
                        jumlahLikeSekarang = 0;
                    }
                    else if(holder.imgLikeLevel2.getDrawable().getConstantState().equals(holder.imgLikeLevel2.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
                    {
                        jumlahLikeSekarang = jumlahLikeSekarang-1;
                        holder.jumlahKomentarLevel2.setText(String.valueOf(jumlahLikeSekarang));
                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel2.get(position).getVideo_comment_id(),"0",komentarLevel2.get(position).getVideo_id());
                        funcTransisiGambar(holder.imgLikeLevel2,context.getDrawable(R.drawable.hati_36px),150,150);
                        jumlahLikeSekarang = 0;
                    }
                    komentarLevel2.get(position).setVideo_comment_like(String.valueOf(jumlahLikeSekarang));

                }
            });

//            holder.imgLikeLevel2.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Log.d(MainActivity.MYTAG,"isi JKL "+holder.jumlahKomentarLevel2.getText().toString());
//                    jumlahLikeSekarang = Integer.valueOf(holder.jumlahKomentarLevel2.getText().toString());
//
//                    if(holder.imgLikeLevel2.getDrawable().getConstantState().equals(holder.imgLikeLevel2.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang+1;
//                        holder.jumlahKomentarLevel2.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel2.get(position).getVideo_comment_id(),"1");
//                        funcTransisiGambar(holder.imgLikeLevel2,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    else if(holder.imgLikeLevel2.getDrawable().getConstantState().equals(holder.imgLikeLevel2.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang-1;
//                        holder.jumlahKomentarLevel2.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel2.get(position).getVideo_comment_id(),"0");
//                        funcTransisiGambar(holder.imgLikeLevel2,context.getDrawable(R.drawable.hati_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    komentarLevel2.get(position).setVideo_comment_like(String.valueOf(jumlahLikeSekarang));
//
//                }
//            });
        }

    }

    @Override
    public int getItemCount() {
        Log.d(MainActivity.MYTAG,"komentar Level2 sizenya: "+komentarLevel2.size());
        if(komentarLevel2 == null)
        {
            return 0;
        }
        else {
            //  return 2; //tampilkan hanya 1 recyclerview saja
            return komentarLevel2.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvKomentar,jumlahKomentarLevel2,tvWaktuKomentar,tvNamaKomentator;
        ImageView fotoProfilKomentarLevel2,imgLikeLevel2;
        TextView txt_balas_komentar_video;
        LinearLayout linear_like;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKomentar = itemView.findViewById(R.id.tv_komentarvideo_level2);
            fotoProfilKomentarLevel2 = itemView.findViewById(R.id.foto_profil_komentar_level2);
            imgLikeLevel2 = itemView.findViewById(R.id.like_video_level2);
            jumlahKomentarLevel2 = itemView.findViewById(R.id.jumlah_like_komentar_level2);
            tvWaktuKomentar = itemView.findViewById(R.id.waktu_komentar_video_level2);
            tvNamaKomentator = itemView.findViewById(R.id.nama_komentator_video_level2);
            txt_balas_komentar_video = itemView.findViewById(R.id.txt_balas_komentar_video);
            linear_like = itemView.findViewById(R.id.linear_like);
            Log.d(MainActivity.MYTAG,"constructor view holder RnDD");

        }
    }

    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    private void likeKomentarVideo(String id_user, String id_comment, String like, String idVideo) {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_KOMENTAR_VIDEO_Level2, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl Video Like"+error.getMessage());

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_comment",id_comment);
                params.put("like",like);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public interface InterfaceKomentarLevel2{
        public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas);
        public void balasKomentarLevel2(String something);
    }



}

