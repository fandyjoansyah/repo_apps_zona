package com.zonamediagroup.zonamahasiswa.adapters;


import static com.zonamediagroup.zonamahasiswa.SwipeVideo.runnable;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.CheckNetworkAvailable;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.SwipeVideo;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Halaman_Awal_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SwipeVideo_Adapter extends RecyclerView.Adapter<SwipeVideo_Adapter.VideoViewHolder>{

    private static final String URL_GET_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment/index_post";
    private static final String URL_TAMBAH_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_create/index_post";
    private static final String URL_GET_SINGLE_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_last/index_post";


    public List<Video_Model> videoItems;
    Context context;
    SwipeListener swipeListener;
    MediaPlayer mediaPlayer = null;
    HashMap<Integer,VideoView> hMVideoView = new HashMap<Integer,VideoView>();
    TextView swipe_down;

    //Start Komponen bottomsheet komentar
    Komentar_Video_Level1_Adapter commentAdapterBottomSheetLevel1;

    RecyclerView recyclerCommentInBottomSheet;

    BottomSheetDialog bottomSheetDialog,bottomSheetDialogBalasKomentar;
    ProgressBar pbBottomSheet;

    EditText txt_komentar_video;
    RecyclerView recyclerKomentarVideoLevel1;

    TextView txt_bottomsheet_komentar_kosong;

    ImageView send_button, gambar_komentator_video;

    ImageView gambar_komentator;

    Komentar_Video_Level1_Halaman_Awal_Adapter adapterRecyclerSingleKomentarHalamanAwal = null;

    ArrayList<Komentar_Video_Model> dataRecyclerKomentarBottomSheet;
    //End Komponen bottomsheet komentar

    Integer sttShow = 0;

public  static  VideoView videoGlobal;

    public SwipeVideo_Adapter(List<Video_Model> videoItems,Context context,SwipeListener swipeListener) {
        this.videoItems = videoItems;
        this.context = context;
        this.swipeListener = swipeListener;
        this.sttShow = 0;
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VideoViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(
                        R.layout.item_swipe_video,
                        viewGroup,
                        false
                )
        );
    }


    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder videoViewHolder, int i) {

        Log.d(MainActivity.MYTAG,"indeks on bind video: "+i);
        if(!hMVideoView.containsKey(i))
            hMVideoView.put(i,videoViewHolder.videoView);

        videoViewHolder.setVideoData(videoItems.get(i),i);

        String textnya = "<font color='white'>"+videoItems.get(i).getVideo_deskripsi()+"</font>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            videoViewHolder.textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());
            videoViewHolder.textVideoDescription.setText(Html.fromHtml(textnya, Html.FROM_HTML_MODE_LEGACY));
        } else {
            videoViewHolder.textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());
            videoViewHolder.textVideoDescription.setText(Html.fromHtml(textnya));
        }
    }

    @Override
    public int getItemCount() {
        return videoItems.size();
    }

     class VideoViewHolder extends RecyclerView.ViewHolder implements Komentar_Video_Level1_Adapter.KomentarLevel1{
        VideoView videoView;
        TextView textVideoTitle, textVideoDescription, jumlahLike, jumlahComment,jumlahShare,txtKeteranganWaktu;
        ProgressBar videoProgressBar;
        ImageView swipeLike, swipeComment, swipeShare, backButton;
        SeekBar seekbar;
         LinearLayout linearLayoutAll, line1, line2;
         private Handler updateHandler = new Handler();
        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);
            //start code from google
            //end code from google
            videoView = itemView.findViewById(R.id.videoView);
            textVideoTitle = itemView.findViewById(R.id.textVideoTitle);
            textVideoDescription = itemView.findViewById(R.id.textVideoDescription);
            videoProgressBar = itemView.findViewById(R.id.videoProgressBar);
            swipeLike = itemView.findViewById(R.id.icLove);
            swipeComment = itemView.findViewById(R.id.icComment);
            swipeShare = itemView.findViewById(R.id.icShare);
            jumlahLike = itemView.findViewById(R.id.jumlah_like);
            jumlahComment = itemView.findViewById(R.id.jumlah_komen);
            jumlahShare = itemView.findViewById(R.id.jumlah_share_video);
            txtKeteranganWaktu = itemView.findViewById(R.id.txt_keterangan_waktu);
            seekbar = itemView.findViewById(R.id.seekBar);
            backButton = itemView.findViewById(R.id.backButton);
            linearLayoutAll = itemView.findViewById(R.id.linearLayoutAll);
            line1 = itemView.findViewById(R.id.line1);
            line2 = itemView.findViewById(R.id.line2);
            recyclerKomentarVideoLevel1 = itemView.findViewById(R.id.recycler_komentar_video_level1);

            videoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!videoView.isPlaying())
                    {
                       videoView.start();
                    }
                    else if(videoView.isPlaying())
                    {
                        videoView.pause();
                    }

                }
            });

            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    swipeListener.backVideo();
                }
            });

            textVideoDescription.setLinksClickable(true);
            textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());

            linearLayoutAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    CustomToast.s(context, "di klik");
                    if(sttShow == 0){
                        sttShow = 1;
                        line1.setVisibility(view.INVISIBLE);
                        line2.setVisibility(view.INVISIBLE);
                        textVideoTitle.setVisibility(view.INVISIBLE);
                        textVideoDescription.setVisibility(view.INVISIBLE);
                    }else{
                        sttShow = 0;
                        line1.setVisibility(view.VISIBLE);
                        line2.setVisibility(view.VISIBLE);
                        textVideoTitle.setVisibility(view.VISIBLE);
                        textVideoDescription.setVisibility(view.VISIBLE);
                    }
                }
            });
        }

     public  void setVideoData(Video_Model videoItem, int position){

            textVideoTitle.setText(videoItem.getVideo_title());
            textVideoDescription.setText(videoItem.getVideo_deskripsi());
            jumlahComment.setText(String.valueOf(videoItem.getVideo_comment()));
            txtKeteranganWaktu.setText(videoItem.getConvert_time());
            Uri uriVideo = Uri.parse(videoItem.getVideo_url());
            videoView.setVideoURI(uriVideo);
            //videoView.setVideoPath(videoItem.getVideo_url());

                //set like
                cekDanInitIconAwalLoad(swipeLike,swipeLike.getContext().getDrawable(R.drawable.love_fill),swipeLike.getContext().getDrawable(R.drawable.love_video),videoItem.getVideo_like_id());
                jumlahLike.setText(videoItem.getVideo_like());
                swipeLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        toggleLike(position,MainActivity.idUserLogin,videoItem.getVideo_id(),videoItem.getVideo_like_id(),swipeLike,jumlahLike);
                    }
                });
         Runnable updateVideoTime = new Runnable() {
             @Override
             public void run() {
                 long currentPosition = videoView.getCurrentPosition();
                 seekbar.setProgress((int) currentPosition);
                 updateHandler.postDelayed(this, 100);
             }
         };
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
                @Override
                public void onPrepared(MediaPlayer mp){
                    mp.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                        @Override
                        public void onSeekComplete(MediaPlayer mp) {

                        }
                    });
                    mediaPlayer = mp;


                    videoProgressBar.setVisibility(View.GONE);
                    mediaPlayer.start();
//                    float videoRatio = mediaPlayer.getVideoWidth() / (float) mediaPlayer.getVideoHeight();
//                    float screenRatio = videoView.getWidth() / (float) videoView.getHeight();
//                    float scale = videoRatio / screenRatio;
//                    if(scale >= 1f){
//                        videoView.setScaleX(scale);
//                    }
//                    else {
//                        videoView.setScaleY(1f / scale);
//                    }

                    seekbar.setProgress(0);
                    seekbar.setMax(videoView.getDuration());
                    updateHandler.postDelayed(updateVideoTime,10);
                }
            });
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if(position < (videoItems.size()-1)) {
                        if(SwipeVideo.notifSwipe == 0){
                            SwipeVideo.notifShow();
                            videoView.start();
                        }else{
//                            swipeListener.playNextVideo(position);
                            videoView.start();
                        }
                    }
                    else {
                        mediaPlayer.start();
                    }
                }
            });
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    return true;
                }
            });
            swipeComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CheckNetworkAvailable.check(context))
                        showDialogComment(videoItem.getVideo_id());
                }
            });
         jumlahShare.setText(videoItem.getVideo_share());
         swipeShare.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 swipeListener.shareVideoSwipe(videoItem.getVideo_share_url());
             }
         });

         seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
             @Override
             public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

             }

             @Override
             public void onStartTrackingTouch(SeekBar seekBar) {

             }

             @Override
             public void onStopTrackingTouch(SeekBar seekBar) {
                 videoView.seekTo(seekBar.getProgress());
             }
         });

        }

        //start coding komentar
        public void showDialogComment(String idVideo){
            initCommentBottomSheet();
            Glide.with(context)
                    .load(SharedPrefManagerZona.getInstance(context).geturl_voto())
                    .apply(RequestOptions.fitCenterTransform())
                    .into(gambar_komentator);


            bottomSheetDialog.show();
            refreshRecyclerViewKomentarSetelahAksiLevel1(idVideo,"awal_buka");
            send_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(CheckNetworkAvailable.check(context)) {
                        tambahKomentar(MainActivity.idUserLogin, idVideo, txt_komentar_video.getText().toString());
                        txt_komentar_video.setText("");
                        txt_komentar_video.clearFocus();
                    }
                    //  CustomToast.s(getContext(),"Komentarmu berhasil ditambah");
                }
            });
        }

         public void refreshRecyclerViewKomentarSetelahAksiLevel1(String idVideo,String jenisAksi)
         {
             //start code getCommentLevel1
             StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

                 @Override
                 public void onResponse(String response) {
                     try {
                         JSONObject jObj = new JSONObject(response);

                         if (jObj.getString("status").equals("true")) {
                             JSONArray dataArray = jObj.getJSONArray("data");
                             ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                             }.getType());

                             dataRecyclerKomentarBottomSheet.clear();
                             dataRecyclerKomentarBottomSheet.addAll(items);
                             commentAdapterBottomSheetLevel1 = new Komentar_Video_Level1_Adapter(context,dataRecyclerKomentarBottomSheet, SwipeVideo_Adapter.VideoViewHolder.this);
                             recyclerCommentInBottomSheet.setAdapter(commentAdapterBottomSheetLevel1);
                             commentAdapterBottomSheetLevel1.notifyDataSetChanged();

                             pbBottomSheet.setVisibility(View.GONE);
                         } else if (jObj.getString("status").equals("false")) {

                             txt_bottomsheet_komentar_kosong.setVisibility(View.VISIBLE);
                             pbBottomSheet.setVisibility(View.GONE);
                         }
                         else
                         {

                         }

                         // Check for error node in json

                     } catch (JSONException e) {
                         e.printStackTrace();
                     }

                 }
             }, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {


                 }
             }) {


//            parameter

                 @Override
                 protected Map<String, String> getParams() {
                     // Posting parameters to login url
                     Map<String, String> params = new HashMap<String, String>();
                     params.put("TOKEN", "qwerty");
                     params.put("id_user", MainActivity.idUserLogin);
                     params.put("id_video",idVideo);
                     return params;
                 }
             };

//        ini heandling requestimeout
             strReq.setRetryPolicy(new RetryPolicy() {
                 @Override
                 public int getCurrentTimeout() {
                     return 10000;
                 }

                 @Override
                 public int getCurrentRetryCount() {
                     return 10000;
                 }

                 @Override
                 public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                     Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                 }
             });


             // Adding request to request queue
             MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
             //end code getCommentLevel1
         }

         public void tambahKomentar(String idUser, String idVideo, String komentar){
             Log.d(MainActivity.MYTAG,"tambah komentar ittuw");
             StringRequest strReq = new StringRequest(Request.Method.POST, URL_TAMBAH_KOMENTAR, new Response.Listener<String>() {

                 @Override
                 public void onResponse(String response) {
                     try {
                         Log.d(MainActivity.MYTAG,"try tambah komentar ittuw");
                         JSONObject jObj = new JSONObject(response);

                         if (jObj.getString("status").equals("true")) {
                             JSONArray dataArray = jObj.getJSONArray("data");
                             List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                             }.getType());

                             Log.d(MainActivity.MYTAG,"tambahKomentar() -> onResponse() -> if -> if");
                             Komentar_Video_Model lastAddedComment = items.get(0);
                             changeTotalCommentDataVideo(getAdapterPosition(),1);
                             if(commentAdapterBottomSheetLevel1!=null) {
                                 addDataToRecyclerViewComment(commentAdapterBottomSheetLevel1, dataRecyclerKomentarBottomSheet, lastAddedComment , 0, recyclerCommentInBottomSheet);
                             }
                             else {
                                 initDataRecyclerViewCommentOnFirstAdd(lastAddedComment);
                             }
                         } else if (jObj.getString("status").equals("false")) {


                         }
                         else
                         {

                         }

                         // Check for error node in json

                     } catch (JSONException e) {
                         Log.d(MainActivity.MYTAG,"catch tambah komentar ittuw "+e.getMessage());
                         e.printStackTrace();
                     }

                 }
             }, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {
                     Log.d(MainActivity.MYTAG,"onErrorResponse tambah komentar ittuw "+ error.getMessage());
                 }
             }) {


//            parameter

                 @Override
                 protected Map<String, String> getParams() {
                     // Posting parameters to login url
                     Map<String, String> params = new HashMap<String, String>();
                     params.put("TOKEN", "qwerty");
                     params.put("id_user", idUser);
                     params.put("id_video",idVideo);
                     params.put("comment",komentar);
                     return params;
                 }
             };

//        ini heandling requestimeout
             strReq.setRetryPolicy(new RetryPolicy() {
                 @Override
                 public int getCurrentTimeout() {
                     return 10000;
                 }

                 @Override
                 public int getCurrentRetryCount() {
                     return 10000;
                 }

                 @Override
                 public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                     Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                 }
             });


             // Adding request to request queue
             MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
         }

         private void initDataRecyclerViewCommentOnFirstAdd(Komentar_Video_Model items) {
             Log.d(MainActivity.MYTAG,"init ittuw");
             recyclerCommentInBottomSheet.setAdapter(null);
             txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
           //error launching  List<Komentar_Video_Model> dataKomentar = new ArrayList<>();
           //error launching  dataKomentar.add(items);
             dataRecyclerKomentarBottomSheet.add(items);
             commentAdapterBottomSheetLevel1 = new Komentar_Video_Level1_Adapter(context,dataRecyclerKomentarBottomSheet,this);
             LinearLayoutManager layoutLevel1 = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
            //backup error 30 des recyclerKomentarVideoLevel1.setLayoutManager(layoutLevel1);
             recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1);
             recyclerCommentInBottomSheet.setAdapter(commentAdapterBottomSheetLevel1);
             recyclerCommentInBottomSheet.setVisibility(View.VISIBLE);
         }

         private void changeTotalCommentDataVideo(int position, int newValue) {
             int originalNumberOfComments = getOriginalNumberOfComments(position);
             int newNumberOfComments = calculateAndGetNewNumberOfComments(originalNumberOfComments, newValue);
             setNewNumberOfCommentsToTotalComments(position,newNumberOfComments);
         }
         private int getOriginalNumberOfComments(int position){
             return Integer.valueOf(videoItems.get(position).getVideo_comment());
         }

         private int calculateAndGetNewNumberOfComments(int originalNumberOfComments, int newValue){
             return (originalNumberOfComments+newValue);
         }

         private void setNewNumberOfCommentsToTotalComments(int position, int newNumberOfCommentsToTotalComments)
         {
             videoItems.get(position).setVideo_comment(String.valueOf(newNumberOfCommentsToTotalComments));
             setNewNumberOfCommentsToTextViewTotalComments(newNumberOfCommentsToTotalComments);
         }

         private void setNewNumberOfCommentsToTextViewTotalComments(int newNumberOfCommentsToTotalComments)
         {
             jumlahComment.setText(String.valueOf(newNumberOfCommentsToTotalComments));
         }

         public  void  addDataToRecyclerViewComment(RecyclerView.Adapter adapter, List<Komentar_Video_Model> allData, Komentar_Video_Model newData, int indeksPosisiBaru, RecyclerView recyclerView){
             Log.d(MainActivity.MYTAG,"sakjane terpanggil");
             txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
             allData.add(indeksPosisiBaru,newData);
             adapter.notifyItemInserted(indeksPosisiBaru);
             // adapter.notifyDataSetChanged();
             recyclerView.smoothScrollToPosition(indeksPosisiBaru);
         }
         public void initCommentBottomSheet()
         {
             bottomSheetDialog = new BottomSheetDialog(context);
             View view= LayoutInflater.from(context).inflate(R.layout.sheet_dialog_komentar,null);

             recyclerCommentInBottomSheet = view.findViewById(R.id.recycler_bottomsheet_komen_video);
             LinearLayoutManager layoutLevel1BottomSheet = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
             recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1BottomSheet);

             pbBottomSheet = view.findViewById(R.id.progress_bottom_video);
             gambar_komentator_video = view.findViewById(R.id.gambar_komentator_video);

             txt_komentar_video = view.findViewById(R.id.txt_komentar_video);

             send_button = view.findViewById(R.id.send_button);

             dataRecyclerKomentarBottomSheet = new ArrayList<>();

             txt_bottomsheet_komentar_kosong = view.findViewById(R.id.txt_bottomsheet_komentar_kosong);

             gambar_komentator = view.findViewById(R.id.gambar_komentator_video);

             bottomSheetDialog.setContentView(view);
             bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                 @Override
                 public void onDismiss(DialogInterface dialogInterface) {
                     //reset adapter komentar di bottomsheet agar reload lagi di pembukaan selanjutnya
                     commentAdapterBottomSheetLevel1 = null;
                     //refresh recycler single di halaman awal video
                     getLastSingleCommentById(videoItems.get(getAdapterPosition()).getVideo_id());
                 }
             });
         }





         private void getLastSingleCommentById(String id_video) {

             StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_SINGLE_KOMENTAR, new Response.Listener<String>() {

                 @Override
                 public void onResponse(String response) {
                     try {
                         JSONObject jObj = new JSONObject(response);
                         JSONArray jsonArrayLastComment = jObj.getJSONArray("comment");
                         if (jsonArrayLastComment != null) {
                             List<Komentar_Video_Model> lastComment = new Gson().fromJson(jsonArrayLastComment.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                             }.getType());

                         }
                         // Check for error node in json

                     } catch (JSONException e) {
                         e.printStackTrace();
                     }

                 }
             }, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {
                     //segera hapu
                     Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

                 }
             }) {


//            parameter

                 @Override
                 protected Map<String, String> getParams() {
                     // Posting parameters to login url
                     Map<String, String> params = new HashMap<String, String>();
                     params.put("TOKEN", "qwerty");
                     params.put("id_user", MainActivity.idUserLogin);
                     params.put("id_video",id_video);
                     return params;
                 }
             };

//        ini heandling requestimeout
             strReq.setRetryPolicy(new RetryPolicy() {
                 @Override
                 public int getCurrentTimeout() {
                     return 10000;
                 }

                 @Override
                 public int getCurrentRetryCount() {
                     return 10000;
                 }

                 @Override
                 public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                     Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                 }
             });


             // Adding request to request queue
             MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");

         }

         @Override
         public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {

         }

         @Override
         public void balasKomentarLevel1(String idKomentarYangDibalas) {

         }


         //end coding komentar
    }

    private  void cekDanInitIconAwalLoad(ImageView icon, Drawable drawableTidakAdaNilai, Drawable drawableAdaNilai, String idLikeString){
        if(idLikeString == null)
            icon.setImageDrawable(drawableTidakAdaNilai);
        else
            icon.setImageDrawable(drawableAdaNilai);
    }

    private  void toggleLike(int position,String idUser, String idVideo, String currentStatusLike, ImageView iconLike, TextView txtLike)
    {
        int jumlahLike = Integer.valueOf(txtLike.getText().toString());
        Log.d(MainActivity.MYTAG,"idUser TG: "+idUser);
        Log.d(MainActivity.MYTAG,"idVideo TG: "+idVideo);
        Log.d(MainActivity.MYTAG,"like TG: "+currentStatusLike);
        int jumlahLikeDataCurrentVideo = Integer.valueOf(videoItems.get(position).getVideo_like());
        Log.d(MainActivity.MYTAG,"jumlahLikeDataCurrentVideo initial "+jumlahLikeDataCurrentVideo);
        if(currentStatusLike == null) //jika sebelumnya belum ada like, set like ke adapter recycler video
        {
            //tidak ada like(0). jadikan ada like (1)
            videoItems.get(position).setVideo_like_id("99");
            jumlahLikeDataCurrentVideo = jumlahLikeDataCurrentVideo+1;
            Log.d(MainActivity.MYTAG,"jumlahLikeDataCurrentVideo tambah "+jumlahLikeDataCurrentVideo);
            videoItems.get(position).setVideo_like(String.valueOf(jumlahLikeDataCurrentVideo));
            swipeListener.storeLikeSwipe(idVideo,"1");
            swapGambar(iconLike,context.getDrawable(R.drawable.love_video),150,150);
            jumlahLike++;
            //adapterVideo.notifyDataSetChanged();
        }
        else
        {
            videoItems.get(position).setVideo_like_id(null);
            jumlahLikeDataCurrentVideo = jumlahLikeDataCurrentVideo-1;
            Log.d(MainActivity.MYTAG,"jumlahLikeDataCurrentVideo kurang "+jumlahLikeDataCurrentVideo);
            videoItems.get(position).setVideo_like(String.valueOf(jumlahLikeDataCurrentVideo));
            swipeListener.storeLikeSwipe(idVideo,"0");
            swapGambar(iconLike,context.getDrawable(R.drawable.love_fill),150,150);
            jumlahLike--;
            //adapterVideo.notifyDataSetChanged();
        }


        txtLike.setText(String.valueOf(jumlahLike));
    }

    private void swapGambar(ImageView imageView,Drawable drawableTujuan,int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    public interface SwipeListener{
        void storeLikeSwipe(String idVideo, String like);
        void showAllCommentOnSwipe(String idVideo);
        void shareVideoSwipe(String url);
        void backVideo();
        void playNextVideo(int currentPosition);

    }

    @Override
    public int getItemViewType(int position) {
        //start timer
        SwipeVideo.endTimer();
        SwipeVideo.runnable = new Runnable() {
            @Override
            public void run() {
                if(videoGlobal!=null)
                    SwipeVideo.showLogX(videoItems.get(position).getVideo_id(),MainActivity.idUserLogin,hMVideoView.get(position).getCurrentPosition());
                    SwipeVideo.tambahPlay(videoItems.get(position).getVideo_id(),MainActivity.idUserLogin,hMVideoView.get(position).getCurrentPosition(),SwipeVideo.delay);
                SwipeVideo.handler.postDelayed(this, SwipeVideo.delay);
            }
        };
        SwipeVideo.handler.postDelayed(runnable, SwipeVideo.delay);
        //end timer

        return super.getItemViewType(position);
    }

}