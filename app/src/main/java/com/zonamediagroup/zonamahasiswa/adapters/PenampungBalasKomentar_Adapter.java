package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zonamediagroup.zonamahasiswa.R;

import java.util.List;

public class PenampungBalasKomentar_Adapter extends RecyclerView.Adapter<PenampungBalasKomentar_Adapter.ViewHolder> {
    Context context;
    List<Komentar_Adapter> dataBalasKomentarTambahan;
    public PenampungBalasKomentar_Adapter(Context context,List<Komentar_Adapter> dataBalasKomentarTambahan){
        this.context = context;
        this.dataBalasKomentarTambahan = dataBalasKomentarTambahan;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komentar,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return dataBalasKomentarTambahan.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
