package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KategoriForhatModel;

import java.util.ArrayList;

public class PilihKategoriForhatInTulisAdapter extends RecyclerView.Adapter<PilihKategoriForhatInTulisAdapter.MyKategoriForhatViewholder> {
    ArrayList<KategoriForhatModel> listKategoriForhat;
    Context context;
    Animation fade_in;
    PilihKategoriForhatInTulisAdapterListener kategoriForhatAdapterListener;
    int positionAdapterRadioSelected = -1;
    RadioButton radioButtonSelected = null;
    KategoriForhatModel kategoriForhatSelected = null;
    private void initAnimation(){
        fade_in = AnimationUtils.loadAnimation(context, R.anim.fade_in);
    }
    public PilihKategoriForhatInTulisAdapter(Context context, ArrayList<KategoriForhatModel> listKategoriForhat, PilihKategoriForhatInTulisAdapterListener  kategoriForhatAdapterListener){
        this.listKategoriForhat = listKategoriForhat;
        this.context = context;
        this.kategoriForhatAdapterListener = kategoriForhatAdapterListener;
        initAnimation();
    }
    @NonNull
    @Override
    public MyKategoriForhatViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutKategoriForhat = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori_forhat, parent, false);
        MyKategoriForhatViewholder myKategoriForhatViewholder = new MyKategoriForhatViewholder(layoutKategoriForhat);
        return myKategoriForhatViewholder;
    }

    @Override
    public void onBindViewHolder(MyKategoriForhatViewholder holder, int position) {
        holder.setTextRadioButton(listKategoriForhat.get(position).getNama_kategori());
    }

    @Override
    public int getItemCount() {
        return listKategoriForhat.size();
    }

    public class MyKategoriForhatViewholder extends RecyclerView.ViewHolder{
        RadioButton item_kategori_forhat;
        public MyKategoriForhatViewholder(@NonNull View itemView) {
            super(itemView);
            findViewByIdAllComponent();
            setListenerAllComponent();
        }
        private void findViewByIdAllComponent(){
            item_kategori_forhat = itemView.findViewById(R.id.item_kategori_forhat);
        }

        private void setListenerAllComponent(){
            item_kategori_forhat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mekanismeToggleRadioButton(item_kategori_forhat);
                }
            });
        }

        private void mekanismeToggleRadioButton(RadioButton clicked_radio_button) {
            if(positionAdapterRadioSelected < 0 && radioButtonSelected == null) //jika belum ada radio yang terpilih
            {
                positionAdapterRadioSelected = getAdapterPosition();
                kategoriForhatSelected = listKategoriForhat.get(getAdapterPosition());
                radioButtonSelected = clicked_radio_button;
            }
            else if(positionAdapterRadioSelected == getAdapterPosition() && radioButtonSelected == clicked_radio_button) //jika radio button yang terpilih sebelumnya merupakan radio yang sekarang di klik, maka uncheck radio ini.
            {
                positionAdapterRadioSelected = -1;
                kategoriForhatSelected = null;
                radioButtonSelected.setChecked(false);
                radioButtonSelected = null;
            }
            else //jika radio baru yang terpilih tidak sama dengan radio yang sudah terpilih sebelumnya
            {
                radioButtonSelected.setChecked(false); /*uncheck radio yang sebelumnya*/
                positionAdapterRadioSelected = getAdapterPosition();
                kategoriForhatSelected = listKategoriForhat.get(getAdapterPosition());
                radioButtonSelected = clicked_radio_button;
            }
            kategoriForhatAdapterListener.toggleKategoriForhatSelected(kategoriForhatSelected);
        }




        private void setTextRadioButton(String newText)
        {
            item_kategori_forhat.setText(newText);
        }
    }

    public interface PilihKategoriForhatInTulisAdapterListener{
        public void toggleKategoriForhatSelected(KategoriForhatModel kategoriForhatSelected);
    }



}