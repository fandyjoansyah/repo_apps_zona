package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Trending_Model;

import java.util.ArrayList;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Trending_Adapter extends RecyclerView.Adapter<Trending_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Trending_Model> barangdataList;
    private List<Trending_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;


    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView judul;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            judul = view.findViewById(R.id.judul);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public Trending_Adapter(Context context, List<Trending_Model> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_terbaru, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Trending_Model produk_data = barangdataListFiltered.get(position);
       // System.out.println("Potition " + (position + 1));


//        Holder


        holder.judul.setText(produk_data.getPost_title());

//        Holder

    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Trending_Model> filteredList = new ArrayList<>();
                    for (Trending_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getName().contains(charSequence) || row.getName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Trending_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Trending_Model transaksihariinidataList);
    }
}
