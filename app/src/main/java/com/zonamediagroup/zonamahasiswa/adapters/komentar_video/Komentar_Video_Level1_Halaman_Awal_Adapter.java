package com.zonamediagroup.zonamahasiswa.adapters.komentar_video;

import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Komentar_Video_Level1_Halaman_Awal_Adapter extends RecyclerView.Adapter<Komentar_Video_Level1_Halaman_Awal_Adapter.ViewHolder> implements Komentar_Video_Level2_Adapter.InterfaceKomentarLevel2 {
    Context context;
    String URL_LIKE_KOMENTAR_VIDEO_LEVEL1 = Server.URL_REAL_VIDEO+"Video_comment_like/index_post";
String URL_KOMENTAR_LEVEL_2 = Server.URL_REAL_VIDEO+"Video_comment/index_post";
    private static final String URL_BALAS_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_reply_create/index_post";
    int jumlahLikeSekarang=0;
    List<Komentar_Video_Model> komentarLevel1;

    List<Komentar_Video_Model> arrKom = null;


    //tipe parent adalah tipe dari induk Komentar_Video_Level1_Adapter. apabila 0 maka halaman yang tidak menampilkan lihat balasan. apabila 1 maka halaman yang menampilkan lihat balasan
    int tipeParent=0;
    BottomSheetDialog bottomSheetDialogBalasKomentar;





  public Komentar_Video_Level1_Halaman_Awal_Adapter(Context context, List<Komentar_Video_Model> komentarLevel1) {
        this.context = context;
        this.komentarLevel1 = komentarLevel1;
        //tipe parent adalah tipe dari induk Komentar_Video_Level1_Adapter. apabila 0 maka halaman yang tidak menampilkan lihat balasan. apabila 1 maka halaman yang menampilkan lihat balasan

    }
    @NonNull
    @Override
    public Komentar_Video_Level1_Halaman_Awal_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komentar_video_level1_halaman_awal,parent,false);
        return new Komentar_Video_Level1_Halaman_Awal_Adapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Komentar_Video_Level1_Halaman_Awal_Adapter.ViewHolder holder, int position) {
            Log.d(MainActivity.MYTAG,"debug disini woy "+komentarLevel1.get(position).getVideo_comment());
            Komentar_Video_Model dataKomentar = komentarLevel1.get(position);
            holder.dataKomentar = dataKomentar;
            holder.tvKomentar.setText(dataKomentar.getVideo_comment());
            holder.jumlahKomentarLevel1.setText(dataKomentar.getVideo_comment_like());
            holder.tvWaktuKomentar.setText(dataKomentar.getVideo_comment_date_created());
            holder.tvNamaKomentator.setText(dataKomentar.getPerson_name());
            holder.tvWaktuKomentar.setText(dataKomentar.getConvert_time());
            if(dataKomentar.getVideo_comment_like_id() != null)
                holder.imgLikeLevel1.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
            else
                holder.imgLikeLevel1.setImageDrawable(context.getDrawable(R.drawable.hati_36px));

            Glide.with(context)
                    .load(komentarLevel1.get(position).getPerson_photo())
                    .into(holder.fotoProfilKomentarLevel1);

        holder.linear_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumlahLikeSekarang = Integer.valueOf(holder.jumlahKomentarLevel1.getText().toString());

                if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
                {
                    jumlahLikeSekarang = jumlahLikeSekarang+1;
                    holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
                    likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"1");
                    funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
                    jumlahLikeSekarang = 0;
                }
                else if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
                {
                    jumlahLikeSekarang = jumlahLikeSekarang-1;
                    holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
                    likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"0");
                    funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_36px),150,150);
                    jumlahLikeSekarang = 0;
                }
                komentarLevel1.get(position).setVideo_comment_like(String.valueOf(jumlahLikeSekarang));
            }
        });

//            holder.imgLikeLevel1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    jumlahLikeSekarang = Integer.valueOf(holder.jumlahKomentarLevel1.getText().toString());
//
//                    if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang+1;
//                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"1");
//                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    else if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang-1;
//                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"0");
//                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    komentarLevel1.get(position).setVideo_comment_like(String.valueOf(jumlahLikeSekarang));
//
//                }
//            });

            //start tambahkan komentar level 2
            //FANDY TODO: dapatkan komentar level 2 by id level 1
            Log.d(MainActivity.MYTAG,"pemanggil level2: "+dataKomentar.getVideo_comment());
            //end tambahkan komentar level 2






    }



//    public void getKategori(){
//        StringRequest strReq = new StringRequest(Request.Method.POST, URL_KOMENTAR_LEVEL_2, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jObj = new JSONObject(response);
//
//                    if (jObj.getString("status").equals("true")) {
//                        JSONArray dataArray = jObj.getJSONArray("data");
//
//                        List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
//                        }.getType());
//
//                        //start load komentar level 2
//                        if(items.size() > 0) { //jika ada balas komentar, dan merupakan tipeParent yg menampilkan  balas komentar
//
//                            //cek tipeParent
//                            switch(tipeParent)
//                            {
//                                case 0 :
//                                    tampil_komen_level2.setText(items.size() + " balasan");
//                                    break;
//                                case 1 :
//                                    tampil_komen_level2.setText("Lihat " + items.size() + " balasan");
//                                    break;
//                            }
//                            tampil_komen_level2.setVisibility(View.VISIBLE);
//                            Komentar_Video_Level2_Adapter komentarLevel2Adapter = new Komentar_Video_Level2_Adapter(context, items);
//
//                            LinearLayoutManager layoutLevel1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
//                            recyclerViewLevel2.setLayoutManager(layoutLevel1);
//                            recyclerViewLevel2.setAdapter(komentarLevel2Adapter);
//                            //end load komentar level 2
//                        }
//
//                    } else if (jObj.getString("status").equals("false")) {
//
//                    }
//
//                } catch (JSONException e) {
//                    // JSON error
//
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//
//
////            parameter
//
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting parameters to login url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("TOKEN", "qwerty");
//                params.put("id_user", id_user);
//                params.put("id_video",id_video);
//                params.put("id_comment",id_comment);
//                return params;
//            }
//        };
//
////        ini heandling requestimeout
//        strReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 10000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 10000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
////                eror_show();
//                Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());
//
//            }
//        });
//
//
//        // Adding request to request queue
//        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
//    }

    @Override
    public int getItemCount() {
        if(komentarLevel1 == null)
        {
            Log.d(MainActivity.MYTAG,"getItemCount KOmentarVideo Level 1 Halaman Awal NULL ");
            return 0;
        }
        else {
            Log.d(MainActivity.MYTAG,"getItemCount KOmentarVideo Level 1 Halaman Awal NOT NULL. SIZE: "+komentarLevel1.size());
            //  return 2; //tampilkan hanya 1 recyclerview saja
            return komentarLevel1.size();
        }
    }

    //override interface nya Komentar_Video_Level2_Adapter
    @Override
    public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {
    }

    @Override
    public void balasKomentarLevel2(String something) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements Komentar_Video_Level2_Adapter.InterfaceKomentarLevel2 {
        TextView tvKomentar,jumlahKomentarLevel1,tvWaktuKomentar,tvNamaKomentator;
        ImageView fotoProfilKomentarLevel1,imgLikeLevel1;
        LinearLayout layout_komen_video_level2, linear_like;
        RecyclerView recyclerLevel2;

        Komentar_Video_Level2_Adapter komentarLevel2Adapter;
        Komentar_Video_Model dataKomentar;
        public  ArrayList<Komentar_Video_Model> dataKomentarLevel2 = new ArrayList<>();


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKomentar = itemView.findViewById(R.id.tv_komentarvideo_level1);
            fotoProfilKomentarLevel1 = itemView.findViewById(R.id.foto_profil_komentar_level1);
            imgLikeLevel1 = itemView.findViewById(R.id.like_video_level1);
            jumlahKomentarLevel1 = itemView.findViewById(R.id.jumlah_komentar_level1);
            tvWaktuKomentar = itemView.findViewById(R.id.waktu_komentar_video_level1);
            tvNamaKomentator = itemView.findViewById(R.id.nama_komentator_video_level1);
            recyclerLevel2 = itemView.findViewById(R.id.recycler_komen_video_level2);
            layout_komen_video_level2 = itemView.findViewById(R.id.layout_komen_video_level2);
            linear_like = itemView.findViewById(R.id.linear_like);
        }

        //start percobaan2
        public void tampilDialogBalasKomentar(String idUser, String idVideo,String idKomentarYangDibalas, String idUserYangDibalas, String namaPenggunaLevel2YangDibalas,String namaPenggunaYangDibalas)
        {
            bottomSheetDialogBalasKomentar = new BottomSheetDialog(context);
            View view= LayoutInflater.from(context).inflate(R.layout.sheet_balas_komentar,null);
            EditText edt_balas_komentar;
            ImageView btn_balas_komentar, gambar_komentator_video;
            TextView tujuan_balas_komentar;

            edt_balas_komentar = view.findViewById(R.id.edt_balas_komentar);
            btn_balas_komentar = view.findViewById(R.id.btn_balas_komentar);
            gambar_komentator_video = view.findViewById(R.id.gambar_komentator_video);
            Glide.with(context)
                    .load(SharedPrefManagerZona.getInstance(context).geturl_voto())
                    .into(gambar_komentator_video);
            tujuan_balas_komentar = view.findViewById(R.id.tujuan_balas_komentar);

            tujuan_balas_komentar.setText(namaPenggunaYangDibalas);
            if(!namaPenggunaLevel2YangDibalas.equals(""))
            {
                edt_balas_komentar.setText("@"+namaPenggunaLevel2YangDibalas);
            }

            btn_balas_komentar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    balasKomentar(idUser,idVideo,edt_balas_komentar.getText().toString(),idKomentarYangDibalas,idUserYangDibalas);
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            });
            edt_balas_komentar.requestFocus();


            bottomSheetDialogBalasKomentar.setContentView(view);
            bottomSheetDialogBalasKomentar.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {

                }
            });
            bottomSheetDialogBalasKomentar.show();
        }
        private void getKomentarLevel2ById(String id_user, String id_video, String id_comment, RecyclerView recyclerViewLevel2,TextView tampil_komen_level2) {
            Log.d(MainActivity.MYTAG,"PAY ATTENTION. id_video: "+id_video);
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_KOMENTAR_LEVEL_2, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");

                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());

                            //start load komentar level 2
                            if(items.size() > 0) { //jika ada balas komentar, dan merupakan tipeParent yg menampilkan  balas komentar

                                for(Komentar_Video_Model komentar:items)
                                {
                                    Log.d(MainActivity.MYTAG,"PAY ATTENTION. komentar: "+komentar.getVideo_comment());
                                }

                                dataKomentarLevel2.clear();
                                dataKomentarLevel2.addAll(items);

                                komentarLevel2Adapter = new Komentar_Video_Level2_Adapter(context, dataKomentarLevel2,ViewHolder.this);

                                LinearLayoutManager layoutLevel1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                                recyclerViewLevel2.setLayoutManager(layoutLevel1);
                                recyclerViewLevel2.setAdapter(komentarLevel2Adapter);

                                //end load komentar level 2
                            }

                        } else if (jObj.getString("status").equals("false")) {

                        }

                    } catch (JSONException e) {
                        // JSON error

                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", id_user);
                    params.put("id_video",id_video);
                    params.put("id_comment",id_comment);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");


        }
        public void balasKomentar(String idUser, String idVideo, String komentar, String idKomentarYangDibalas, String idUserYangDibalas){
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_BALAS_KOMENTAR, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sAC try");
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONObject dataObject = jObj.getJSONObject("data");
                            Komentar_Video_Model items = new Gson().fromJson(dataObject.toString(), new TypeToken<Komentar_Video_Model>() {
                            }.getType());
                            //start percobaan
                            if(komentarLevel2Adapter!=null)
                            {
                                addDataToRecyclerViewCommentSubLevel(komentarLevel2Adapter, dataKomentarLevel2, items, 0, recyclerLevel2);
                            }
                            else {
                                initDataRecyclerViewCommentOnFirstAdd(items);
                            }
                            //end percobaan
                            //  addDataToRecyclerViewCommentSubLevel();
                        } else if (jObj.getString("status").equals("false")) {
                            //segera hapus
                            Log.d(MainActivity.MYTAG,"sac try if else");

                        }
                        else
                        {
                            //segera hapus
                            Log.d(MainActivity.MYTAG,"sac try else");
                        }

                        // Check for error node in json

                    } catch (JSONException e) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sAC catchX");
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", idUser);
                    params.put("id_video",idVideo);
                    params.put("comment",komentar);
                    params.put("id_comment",idKomentarYangDibalas);
                    params.put("id_user_reply",idUserYangDibalas);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        }

        public  void  addDataToRecyclerViewCommentSubLevel(RecyclerView.Adapter adapter, List<Komentar_Video_Model> allData, Komentar_Video_Model newData, int indeksPosisiBaru, RecyclerView recyclerView){
            allData.add(indeksPosisiBaru,newData);
                adapter.notifyItemInserted(indeksPosisiBaru);
                recyclerView.smoothScrollToPosition(indeksPosisiBaru);
        }
        //end percobaan2

        private void initDataRecyclerViewCommentOnFirstAdd(Komentar_Video_Model items) {
            //recyclerLevel2.setAdapter(null);
            List<Komentar_Video_Model> dataKomentar = new ArrayList<>();
            dataKomentar.add(items);
            Komentar_Video_Level2_Adapter adapterAdd = new Komentar_Video_Level2_Adapter(context,dataKomentar,this);
            LinearLayoutManager layoutLevel2 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            recyclerLevel2.setLayoutManager(layoutLevel2);
            recyclerLevel2.setAdapter(adapterAdd);
            layout_komen_video_level2.setVisibility(View.VISIBLE);
            recyclerLevel2.setVisibility(View.VISIBLE);


        }

        @Override
        public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {

        }

        @Override
        public void balasKomentarLevel2(String something) {

        }


    }

    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    private void likeKomentarVideo(String id_user, String id_comment, String like) {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_KOMENTAR_VIDEO_LEVEL1, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl Video Like"+error.getMessage());

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_comment",id_comment);
                params.put("like",like);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }




}
