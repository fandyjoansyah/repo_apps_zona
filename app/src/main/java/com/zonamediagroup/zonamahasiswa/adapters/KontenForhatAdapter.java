package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdView;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.ForhatFragment.ForhatHome;
import com.zonamediagroup.zonamahasiswa.ForhatTulisActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.ServerForhat;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.models.KeteranganKontenHabisModel;
import com.zonamediagroup.zonamahasiswa.models.PengenalanForhatModel;
import com.zonamediagroup.zonamahasiswa.models.TulisCurhatanModel;
import com.zonamediagroup.zonamahasiswa.models.forhat_models.KontenForhatRecyclerView_Model;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class KontenForhatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Object> listDataKontenForhat;
    Context context;
    String id_user_sv;
    ListKontenForhatListener listKontenForhatListener;
    private static final int ITEM_TYPE_PENGENALAN_FORHAT = 0;
    private static final int ITEM_TYPE_TULIS_CURHATAN = 1;
    private static final int ITEM_TYPE_KONTEN = 2;
    private static final int ITEM_TYPE_BANNER_AD = 3;
    private static final int ITEM_TYPE_KETERANGAN_KONTEN_HABIS = 4;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType)
        {
            case ITEM_TYPE_PENGENALAN_FORHAT:
                //Inflate pengenalan forhat container
                View pengenalanForhatLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pengenalan_forhat, parent, false);

                //Create View Holder
                MyPengenalanForhatViewHolder myPengenalanForhatViewHolder = new MyPengenalanForhatViewHolder(pengenalanForhatLayoutView);
                return myPengenalanForhatViewHolder;
            case ITEM_TYPE_TULIS_CURHATAN:
                //Inflate tulis curhatan container
                View tulisCurhatanLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tulis_curhatan, parent, false);

                //Create View Holder
                MyTulisCurhatanHolder myTulisCurhatanHolder = new MyTulisCurhatanHolder(tulisCurhatanLayoutView);
                return myTulisCurhatanHolder;
            case ITEM_TYPE_BANNER_AD:
                //Inflate ad banner container
                View bannerLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads_recyclerview, parent, false);

                //Create View Holder
                MyAdViewHolder myAdViewHolder = new MyAdViewHolder(bannerLayoutView);

                return myAdViewHolder;
            case ITEM_TYPE_KETERANGAN_KONTEN_HABIS:
                View keteranganKontenHabisLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_konten_habis, parent, false);
                //Create View Holder
                MyKeteranganKontenHabisHolder myKeteranganKontenHabisHolder = new MyKeteranganKontenHabisHolder(keteranganKontenHabisLayoutView);
                return myKeteranganKontenHabisHolder;
            default:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_konten_forhat,parent,false);
                return new KontenForhatAdapter.KontenForhatViewHolder(view);

        }

    }

    public KontenForhatAdapter(Context context, ArrayList<Object> listDataKontenForhat, ListKontenForhatListener listKontenForhatListener){
        this.context = context;
        this.listDataKontenForhat = listDataKontenForhat;
        this.listKontenForhatListener = listKontenForhatListener;
        Log.d("28_juni_2022","data size: "+listDataKontenForhat.size());
        id_user_sv = SharedPrefManagerZona.getInstance(context).getid_user();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("28_juni_2022","oBVH");
        int viewType = getItemViewType(position);
        switch (viewType)
        {
            case ITEM_TYPE_PENGENALAN_FORHAT:
                if (listDataKontenForhat.get(position) instanceof PengenalanForhatModel)
                {
                    Log.d("7_juli_2022","pengenalan forhat");
                }
            case ITEM_TYPE_TULIS_CURHATAN:
                if (listDataKontenForhat.get(position) instanceof TulisCurhatanModel)
                {
                    Log.d("7_juli_2022","tulis curhatan");
                }
            case ITEM_TYPE_KETERANGAN_KONTEN_HABIS:
                if (listDataKontenForhat.get(position) instanceof KeteranganKontenHabisModel)
                {
                    Log.d("7_juli_2022","keterangan konten habis");
                }
            case ITEM_TYPE_BANNER_AD:
                if (listDataKontenForhat.get(position) instanceof AdView)
                {
                    MyAdViewHolder bannerHolder = (MyAdViewHolder) holder;
                    AdView adView = (AdView) listDataKontenForhat.get(position);
                    ViewGroup adCardView = (ViewGroup) bannerHolder.itemView;
                    // The AdViewHolder recycled by the RecyclerView may be a different
                    // instance than the one used previously for this position. Clear the
                    // AdViewHolder of any subviews in case it has a different
                    // AdView associated with it, and make sure the AdView for this position doesn't
                    // already have a parent of a different recycled AdViewHolder.
                    if (adCardView.getChildCount() > 0)
                    {
                        adCardView.removeAllViews();
                    }
                    if (adView.getParent() != null)
                    {
                        ((ViewGroup) adView.getParent()).removeView(adView);
                    }

                    // Add the banner ad to the ad view.
                    adCardView.addView(adView);
                }
            case ITEM_TYPE_KONTEN:
            default:
                if(listDataKontenForhat.get(position) instanceof KontenForhatRecyclerView_Model)
                {
                    KontenForhatViewHolder kontenForhatViewHolder = (KontenForhatViewHolder) holder;
                    kontenForhatViewHolder.setDataForhatToComponents((KontenForhatRecyclerView_Model) listDataKontenForhat.get(position));
                }
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(listDataKontenForhat.get(position) instanceof TulisCurhatanModel)
        {
            return ITEM_TYPE_TULIS_CURHATAN;
        }
        if(listDataKontenForhat.get(position) instanceof PengenalanForhatModel)
        {
            return ITEM_TYPE_PENGENALAN_FORHAT;
        }
        else if(listDataKontenForhat.get(position) instanceof KeteranganKontenHabisModel)
        {
            return ITEM_TYPE_KETERANGAN_KONTEN_HABIS;
        }
        else if (listDataKontenForhat.get(position) instanceof KontenForhatRecyclerView_Model)
        {
            return ITEM_TYPE_KONTEN;
        } else
        {
            return (position % ForhatHome.ITEMS_PER_AD == 0) ? ITEM_TYPE_BANNER_AD : ITEM_TYPE_KONTEN;
        }
    }

    @Override
    public int getItemCount() {
        return listDataKontenForhat.size();
    }

    public class KontenForhatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView img_foto_profil;
        TextView txt_nama_pengguna, txt_waktu, judul_curhat, isi_curhat, jumlah_like_forhat,jumlah_comment_forhat,jumlah_share_forhat;
        ImageView img_utama,icon_like_forhat;
        RelativeLayout btn_like,btn_comment,btn_share;
        int totalLike,totalComment,totalShare;
        String user_like;
        String idKonten;
        String slug;



        private int decreaseValueByOne(int value)
        {

            return value-1;
        }

        private int increaseValueByOne(int value)
        {
            return value+1;
        }






        public KontenForhatViewHolder(@NonNull View itemView) {
            super(itemView);
            findViewByIdAllComponents(itemView);
            btn_like.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        private void findViewByIdAllComponents(View itemView)
        {
            img_foto_profil = itemView.findViewById(R.id.img_foto_profil);
            txt_nama_pengguna = itemView.findViewById(R.id.txt_nama_pengguna);
            txt_waktu = itemView.findViewById(R.id.txt_waktu);
            judul_curhat = itemView.findViewById(R.id.judul_curhat);
            isi_curhat = itemView.findViewById(R.id.isi_curhat);
            img_utama = itemView.findViewById(R.id.img_utama);
            btn_like = itemView.findViewById(R.id.btn_like);
            btn_comment = itemView.findViewById(R.id.btn_comment);
            btn_share = itemView.findViewById(R.id.btn_share);
            jumlah_like_forhat = itemView.findViewById(R.id.jumlah_like_forhat);
            jumlah_comment_forhat = itemView.findViewById(R.id.jumlah_comment_forhat);
            jumlah_share_forhat = itemView.findViewById(R.id.jumlah_share_forhat);
            icon_like_forhat = itemView.findViewById(R.id.icon_like_forhat);
        }

        private void setListenerAllComponent(){

        }

        private void testToggleLike(){
            Log.d("28_juni_2022","testToggleLike");
            if(icon_like_forhat.getColorFilter()!=null)
            {
                setStateAdaLikeDariUserSekarang();
            }
            else
            {
                setStateTidakAdaLikeDariUserSekarang();
            }
        }

        private void set_img_foto_profil(String fotoProfile,String avatar){
            String imgDestination;
            if(fotoProfile != null)
            {
                imgDestination = fotoProfile;
            }
            else
            {
                imgDestination = avatar;
            }
            Log.d("28_juni_2022","ifp: "+fotoProfile);
            Glide.with(context)
                    .load(imgDestination)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(img_foto_profil);
        }

        private void set_txt_nama_pengguna(String value){
            txt_nama_pengguna.setText(value);
        }
        private void set_txt_waktu(String value){
            txt_waktu.setText(value);
        }
        private void set_judul_curhat(String value){
            Log.d("28_juni_2022","judul curht: "+value);
            judul_curhat.setText(value);
        }
        private void set_isi_curhat(String value){
            /*jika value != "", maka tambahkan baca selengkapnya*/
            if(!value.equals(""))
            {
                String bacaSelengkapnya = "<span style='color: #0d6efd;'> <u>Baca selengkapnya</u></span>";
                isi_curhat.setText(Html.fromHtml(value+bacaSelengkapnya));
            }
        }
        private void set_jumlah_like_forhat(int value){
            jumlah_like_forhat.setText(String.valueOf(value));
        }
        private void set_jumlah_comment_forhat(int value){
            jumlah_comment_forhat.setText(String.valueOf(value));
        }
        private void set_jumlah_share_forhat(int value){
            jumlah_share_forhat.setText(String.valueOf(value));
        }
        private void set_img_utama(String value){
            Glide.with(context)
                    .load(value)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(img_utama);
        }


        /* set data ke komponen ketika awal mendapatkan data dari server*/
        private void setDataForhatToComponents(KontenForhatRecyclerView_Model dataKontenForhat){
            //sebelum launching kalau bisa img_foto_profil jangan pakai yang PATEN

            /*sementara comment untuk percobaan optimasi*/
            set_img_foto_profil(dataKontenForhat.getFoto_user(),dataKontenForhat.getAvatar());

            /*sementara comment untuk percobaan optimasi*/
            set_img_utama(ServerForhat.URL_IMG_LOCATION+dataKontenForhat.getCover());

            set_txt_nama_pengguna(dataKontenForhat.getNama_user());
            set_txt_waktu(dataKontenForhat.getCreated_at());
            set_judul_curhat(dataKontenForhat.getJudul_curhatan());
            set_isi_curhat(dataKontenForhat.getIsi_curhatan());
            set_jumlah_like_forhat(dataKontenForhat.getTotal_like());
            set_jumlah_comment_forhat(dataKontenForhat.getTotal_komentar());
            set_jumlah_share_forhat(dataKontenForhat.getTotal_share());
            idKonten = dataKontenForhat.getId();
            slug = dataKontenForhat.getSlug();
           // Log.d("29_juni_2022","judul: "+dataKontenForhat.getJudul_curhatan());
          //  Log.d("29_juni_2022","user_like: "+dataKontenForhat.getUser_like());
            user_like = dataKontenForhat.getUser_like();
            if(checkIfCurrentUserHasLike(user_like))
            {
                setStateAdaLikeDariUserSekarang();
            }
            totalLike = dataKontenForhat.getTotal_like();
           // Log.d("29_juni_2022","total_like: "+dataKontenForhat.getTotal_like());
            totalComment = dataKontenForhat.getTotal_komentar();
          //  Log.d("29_juni_2022","total_komentar: "+dataKontenForhat.getTotal_komentar());
            totalShare = dataKontenForhat.getTotal_share();
           // Log.d("29_juni_2022","total_share: "+dataKontenForhat.getTotal_share());
        }

        private boolean checkIfCurrentUserHasLike(String user_like){
            if(user_like!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void setStateAdaLikeDariUserSekarang(){
            icon_like_forhat.setColorFilter(ContextCompat.getColor(context,R.color.ada_like_forhat));
        }

        private void modifyLikeValueLocally(int newValue){
            totalLike = newValue;
            KontenForhatRecyclerView_Model objKontenForhat = (KontenForhatRecyclerView_Model)listDataKontenForhat.get(getAdapterPosition());
            objKontenForhat.setTotal_like(totalLike);
        }

        private void upateLikeValueOnServer(){

        }

        private void toggleLike()
        {
            Log.d("29_juni_2022","toggleLike()");
            if(user_like == null)
            {
                user_like = "ada_like";
                Log.d("29_juni_2022","user_like if: "+user_like);
                Log.d("29_juni_2022","totalLike beforeIncrease if: "+totalLike);
                modifyLikeValueLocally(increaseValueByOne(totalLike));
                Log.d("29_juni_2022","totalLike if: "+totalLike);
                setStateAdaLikeDariUserSekarang();
            }
            else
            {
                user_like = null;
                Log.d("29_juni_2022","user_like else: "+user_like);
                Log.d("29_juni_2022","totalLike beforeDecrease else: "+totalLike);
                modifyLikeValueLocally(decreaseValueByOne(totalLike));
                Log.d("29_juni_2022","totalLike else: "+totalLike);
                setStateTidakAdaLikeDariUserSekarang();
            }
            upateLikeValueOnServer();
            set_jumlah_like_forhat(totalLike);
            listKontenForhatListener.like(id_user_sv,idKonten);
        }



        private void setStateTidakAdaLikeDariUserSekarang(){
            icon_like_forhat.setColorFilter(null);
        }

        private void setStateAdaLikeDariUserSekarangWithAnim(){

        }

        private void setStateTidakAdaLikeDariUserSekarangWithAnim(){

        }


        @Override
        public void onClick(View view) {
            int idComponent = view.getId();
            switch (idComponent)
            {
                case R.id.btn_like:
                    Log.d("28_juni_2022","like clicked on "+getAdapterPosition());
                    toggleLike();
                    break;
                case R.id.item_forhat_recycler:
                    Log.d("29_juni_2022","recycler clicked. judul: "+slug);
                    listKontenForhatListener.openBacaKonten(slug);
            }
        }
    }



    public interface ListKontenForhatListener{
        void like(String idUser, String idKonten);
        void openBacaKonten(String slug);
    }

    private void hapusDataRecyclerView(int position)
    {
        listDataKontenForhat.remove(position);
        notifyItemRemoved(position);
    }


    //Banner Ad View Holder
    class MyAdViewHolder extends RecyclerView.ViewHolder
    {
        MyAdViewHolder(View itemView)
        {
            super(itemView);
        }
    }

    //Pengenalan Forhat View Holder
    class MyPengenalanForhatViewHolder extends RecyclerView.ViewHolder
    {
        ImageView ic_close_pengenalan;
        MyPengenalanForhatViewHolder(View itemView)
        {
            super(itemView);
            findViewByIdAllComponents(itemView);
            setListenerAllComponents();
        }
        private void findViewByIdAllComponents(View itemView){
            ic_close_pengenalan = itemView.findViewById(R.id.ic_close_pengenalan);
        }
        private void setListenerAllComponents(){
            ic_close_pengenalan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    hapusDataRecyclerView(getAdapterPosition());
                }
            });
        }

    }

    //Tulis Curhatan View Holder
    class MyTulisCurhatanHolder extends RecyclerView.ViewHolder
    {
        MyTulisCurhatanHolder(View itemView)
        {
            super(itemView);
            setListenerAllComponents(itemView);
        }
        private void setListenerAllComponents(View itemView){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, ForhatTulisActivity.class));
                }
            });
        }
    }

    //Keterangan Konten Habis View Holder
    class MyKeteranganKontenHabisHolder extends RecyclerView.ViewHolder
    {
        MyKeteranganKontenHabisHolder(View itemView)
        {
            super(itemView);
        }
    }
}
