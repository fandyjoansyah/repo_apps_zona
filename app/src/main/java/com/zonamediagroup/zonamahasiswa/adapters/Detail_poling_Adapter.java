package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Detail_poling_Model;


import java.util.ArrayList;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Detail_poling_Adapter extends RecyclerView.Adapter<Detail_poling_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Detail_poling_Model> barangdataList;
    private List<Detail_poling_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;



    private boolean sudahPernahVote = false;

    int selected_position = -1;

    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView total_vote;
        ShapeableImageView img_tokoh;
        RadioButton ly_poling;
        //TextView txt_poling;
        TextView txt_pilihan;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            total_vote = view.findViewById(R.id.total_vote);
            img_tokoh = view.findViewById(R.id.img_tokoh);

            txt_pilihan = view.findViewById(R.id.txt_pilihan);

            ly_poling = view.findViewById(R.id.ly_poling);
         //   txt_poling = view.findViewById(R.id.txt_poling);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public Detail_poling_Adapter(Context context, List<Detail_poling_Model> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
        //Log.d("debug_16_september","constructor detail_poling_adapter");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_poling, parent, false);
        Log.d("debug_16_september","ON Create View");
        //cek apakah dari awal sudah ada data vote
        for(int i=0;i<barangdataList.size();i++)
        {
            if(barangdataList.get(i).getSelected() == true)
            {
                sudahPernahVote = true;
            }
        }
        if(sudahPernahVote == true){
            itemView.findViewById(R.id.img_tokoh).setEnabled(false);
            itemView.findViewById(R.id.ly_poling).setEnabled(false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       // Log.d("debug_16_september","selected position : "+selected_position);
        //cek value id_detail_polling_sudah_vote ada di dataset barangDataList. apabila ada maka barangDataList ke i tersebut menjadi selected_position
        if(barangdataList.get(position).getSelected() == true)
        {
            selected_position = position;
        }
        Log.d("debug_16_september","ON Bind");

        //aktifkan radioButton apabila index dari radioButton tersebut = postiion
        holder.ly_poling.setChecked(position == selected_position);


        final Detail_poling_Model produk_data = barangdataListFiltered.get(position);
        //Log.d("DebugZona","ukuran barangDataListFiltered adalah : "+barangdataListFiltered.size()+" sekarang berada di indeks ke : "+position);
        // System.out.println("Potition " + (position + 1));


//        Holder


      //  holder.total_vote.setText(" " + produk_data.getTotal_vote() + " Vote");
        //Log.d("DebugZona","On Bind produk_data : "+produk_data.getTotal_vote());
        Glide.with(context)
                .load(produk_data.getImg_tokoh())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.img_tokoh);
//        Holder

        View.OnClickListener aksiPilihVote =new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //jika sudah pernah seleksi, maka hapus seleksi lama
                if(selected_position != -1)
                {
                    barangdataList.get(selected_position).setSelected(false);
                }
                selected_position = position;
                notifyDataSetChanged();
                listener.onContactSelected(barangdataListFiltered.get(position));
            }
        };

        holder.img_tokoh.setOnClickListener(aksiPilihVote);
        holder.ly_poling.setOnClickListener(aksiPilihVote);

        if (selected_position == position) {
            // do your stuff here like
            //Change selected item background color
          //  holder.txt_poling.setTextColor(Color.parseColor("#DD2A3F"));
//            holder.ly_poling.setBackgroundResource(R.drawable.rounded_detail_poling);
         //   holder.ly_poling.setBackgroundResource(R.drawable.rounded_disable);
        } else {
            // do your stuff here like
            //Change  unselected item background color
          //  holder.txt_poling.setTextColor(Color.parseColor("#6c757d"));
          //  holder.ly_poling.setBackgroundResource(R.drawable.rounded_disable);
        }
        Log.d("bug_aneh","position : "+position);
       holder.txt_pilihan.setText(barangdataListFiltered.get(position).getNama_tokoh());
    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Detail_poling_Model> filteredList = new ArrayList<>();
                    for (Detail_poling_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getNama_tokoh().toLowerCase().contains(charString.toLowerCase()) || row.getNama_tokoh().contains(charSequence) || row.getNama_tokoh().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Detail_poling_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Detail_poling_Model transaksihariinidataList);
    }


}
