package com.zonamediagroup.zonamahasiswa.adapters.komentar_video;

import android.animation.Animator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Komentar_Video_Level1_AdapterVERSILAMA extends RecyclerView.Adapter<Komentar_Video_Level1_AdapterVERSILAMA.ViewHolder> {
    Context context;
    String URL_LIKE_KOMENTAR_VIDEO_LEVEL1 = "http://103.145.226.115/~lihatwe1/zonavideo/api-video/Video_comment_like/index_post";
    int jumlahLikeSekarang=0;
    List<Komentar_Video_Model> komentarLevel1;
    public Komentar_Video_Level1_AdapterVERSILAMA(Context context, List<Komentar_Video_Model> komentarLevel1) {
        this.context = context;
        this.komentarLevel1 = komentarLevel1;
        Log.d(MainActivity.MYTAG,"Constructor Adapter RNDD");
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(com.zonamediagroup.zonamahasiswa.R.layout.item_komentar_video_level1_unused,parent,false);
        Log.d(MainActivity.MYTAG,"onCreate RNDD");
        return new Komentar_Video_Level1_AdapterVERSILAMA.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if(komentarLevel1.get(position).getPerson_name() != null){
            Komentar_Video_Model dataKomentar = komentarLevel1.get(position);
            holder.tvKomentar.setText(dataKomentar.getVideo_comment());
            holder.jumlahKomentarLevel1.setText(dataKomentar.getVideo_comment_like());
            holder.tvWaktuKomentar.setText(dataKomentar.getVideo_comment_date_created());
            holder.tvNamaKomentator.setText(dataKomentar.getPerson_name());
            holder.tvWaktuKomentar.setText(dataKomentar.getConvert_time());
            if(dataKomentar.getVideo_comment_like_id() != null)
                holder.imgLikeLevel1.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));

            Glide.with(context)
                    .load(komentarLevel1.get(position).getPerson_photo())
                    .into(holder.fotoProfilKomentarLevel1);



            holder.imgLikeLevel1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jumlahLikeSekarang = Integer.valueOf(dataKomentar.getVideo_comment_like());
                    if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
                    {
                       jumlahLikeSekarang = jumlahLikeSekarang++;
                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"1");
                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
                        jumlahLikeSekarang = 0;
                    }
                    else if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
                    {
                        jumlahLikeSekarang = jumlahLikeSekarang--;
                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"0");
                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_36px),150,150);
                        jumlahLikeSekarang = 0;
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if(komentarLevel1 == null)
        {
            return 0;
        }
        else {
          //  return 2; //tampilkan hanya 1 recyclerview saja
            return komentarLevel1.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvKomentar,jumlahKomentarLevel1,tvWaktuKomentar,tvNamaKomentator;
        ImageView fotoProfilKomentarLevel1,imgLikeLevel1;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKomentar = itemView.findViewById(com.zonamediagroup.zonamahasiswa.R.id.tv_komentarvideo_level1);
            fotoProfilKomentarLevel1 = itemView.findViewById(R.id.foto_profil_komentar_level1);
            imgLikeLevel1 = itemView.findViewById(R.id.like_video_level1);
            jumlahKomentarLevel1 = itemView.findViewById(R.id.jumlah_komentar_level1);
            tvWaktuKomentar = itemView.findViewById(R.id.waktu_komentar_video_level1);
            tvNamaKomentator = itemView.findViewById(R.id.nama_komentator_video_level1);
            Log.d(MainActivity.MYTAG,"constructor view holder RnDD");

        }
    }

    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    private void likeKomentarVideo(String id_user, String id_comment, String like) {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_KOMENTAR_VIDEO_LEVEL1, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl Video Like"+error.getMessage());

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_comment",id_comment);
                params.put("like",like);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }
}
