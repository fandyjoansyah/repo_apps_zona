package com.zonamediagroup.zonamahasiswa.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Video_save;
import com.zonamediagroup.zonamahasiswa.Video_share;
import com.zonamediagroup.zonamahasiswa.models.Video_Notif_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Save_Model;

import java.util.List;

public class Video_Save_Adapter extends RecyclerView.Adapter<Video_Save_Adapter.ViewHolder> {
    Context context;
    List<Video_Save_Model> dataSave;
    Video_Save_Adapter.viewListener viewListener;

    public Video_Save_Adapter(Context context, List<Video_Save_Model> dataSave, Video_Save_Adapter.viewListener viewListener){
        this.context = context;
        this.dataSave = dataSave;
        this.viewListener = viewListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_save,parent,false);
        return new Video_Save_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Video_Save_Model dataS = dataSave.get(position);

        holder.img_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewListener.storeSave(position, dataS.getVideo_id(), "0");
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.openVideoShare(dataS);
            }
        });
        Glide.with(context).load(dataS.getVideo_thumbnail())
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.img_thumnile);

        holder.text_date.setText(dataS.getVideo_save_date());
        holder.text_title.setText(dataS.getVideo_title());
        if(dataS.getVideo_category_sub_nama() != null){
            holder.text_category.setText(dataS.getVideo_category_sub_nama());
        }else{
            holder.text_category.setText(dataS.getVideo_category_nama());
        }
    }

    @Override
    public int getItemCount() {
        return dataSave.size();
    }

    public interface viewListener {
        void storeSave(Integer position, String idVideo, String save);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_save, img_thumnile;
        TextView text_date, text_title, text_category;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_save = itemView.findViewById(R.id.img_save);
            img_thumnile = itemView.findViewById(R.id.img_thumnile);
            text_date = itemView.findViewById(R.id.text_date);
            text_title = itemView.findViewById(R.id.text_title);
            text_category = itemView.findViewById(R.id.text_category);
        }
        //start copy from notify
        private Bundle prepareDataForVideoShare(Video_Save_Model dataS){
            Bundle dataForVideoShare = new Bundle();
            dataForVideoShare.putString("video_notification_type","save");
          dataForVideoShare.putString("video_category_nama",dataS.getVideo_category_nama());
            dataForVideoShare.putString("video_id",dataS.getVideo_id());
            dataForVideoShare.putString("video_category_id",dataS.getVideo_category_id());
            dataForVideoShare.putString("video_save_date",dataS.getVideo_save_date());
            dataForVideoShare.putString("video_title",dataS.getVideo_title());
            dataForVideoShare.putString("video_category_sub_nama",dataS.getVideo_category_sub_nama());
            dataForVideoShare.putString("video_category_nama",dataS.getVideo_category_nama());
            dataForVideoShare.putString("video_deskripsi",dataS.getVideo_deskripsi());
            dataForVideoShare.putString("video_save_id",dataS.getVideo_save_id());
            dataForVideoShare.putString("video_url",dataS.getVideo_url());
            return dataForVideoShare;
        }
        private void openVideoShare(Video_Save_Model dataNotifModel)
        {
            Intent i = new Intent(context, Video_share.class);
            i.putExtra("data_bundle_notif",prepareDataForVideoShare(dataNotifModel));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
        //end copy from notify
    }

}
