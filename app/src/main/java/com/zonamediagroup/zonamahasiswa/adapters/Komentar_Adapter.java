package com.zonamediagroup.zonamahasiswa.adapters;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.LongDef;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Bank_Tag;
import com.zonamediagroup.zonamahasiswa.CheckNetworkAvailable;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.interfaces.PopUpTambahBalasKomentarArtikel;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Model;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Komentar_Adapter extends RecyclerView.Adapter<Komentar_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Komentar_Model> barangdataList;
    private List<Komentar_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;
    private String idUserSekarang;
    public static final String TAG = Bank_Tag.BALAS_KOMENTAR;
    int jumlahLike;
    static String tag_json_obj = "json_obj_req";

    //START URL
    private static String URL_BALAS_KOMENTAR = Server.URL_REAL+"Post_sub_komentar";
    private static String URL_GET_BALAS_KOMENTAR = Server.URL_REAL+"Dapatkan_sub_komentar_by_id_komentar";
    private static String URL_LIKE_UNLIKE_SUB_KOMENTAR = Server.URL_REAL+"Menyukai_sub_komentar";
    private static String URL_hapus_sub_komentar = Server.URL_REAL+"Delete_sub_komentar";

    //END URL

    Activity activity;

    final CharSequence[] itemsCurUser = {"Salin", "Hapus"};
    final CharSequence[] itemsOtherUser = {"Salin"};
    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder implements PopUpTambahBalasKomentarArtikel, SubKomentar_Adapter.ContactsAdapterListenerSubKomentar {

        CircleImageView img_komentator;

        public String getIdIndukKomentar() {
            return idIndukKomentar;
        }

        public void setIdIndukKomentar(String idIndukKomentar) {
            this.idIndukKomentar = idIndukKomentar;
        }

        private String idIndukKomentar;

        public TextView nama_komentator, komentar, hapus, txt_waktu_komen;

        private ImageView btnLikeKomentar;
        private TextView tvJumlahLikeKomentar;

        LinearLayout ly_hor, ly_percobaan;

        //start component dialog balas komentar
        BottomSheetDialog bottomSheetDialogBalasKomentar;
        EditText edt_balas_komentar;
        ImageView btn_balas_komentar, gambar_komentator_artikel;
        TextView tujuan_balas_komentar;
        View viewDialog;
        ProgressBar loading_balas_komentar;
        RelativeLayout relative_layout_sheet_balas_komentar;
        //end component dialog balas komentar

        //start component untuk tampil balas komentar
        RecyclerView recycler_sub_komentar;
        SubKomentar_Adapter adapterSubKomentar;
        RecyclerView.LayoutManager layoutManagerRecyclerSubKomentar;
        TextView txt_lihat_balas_komentar_artikel,txt_sembunyikan_balas_komentar_artikel;
        //end component untuk tampil balas komentar

        TextView txt_balas_komentar;

        List<Komentar_Model> dataSubKomentar;

        List<Komentar_Model> dataRecentAdd;

        int totalSubKomentar;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen
            ly_percobaan = view.findViewById(R.id.ly_percobaan);
            nama_komentator = view.findViewById(R.id.nama_komentator);
            komentar = view.findViewById(R.id.komentar);
            img_komentator=view.findViewById(R.id.img_komentator);
            btnLikeKomentar = view.findViewById(R.id.btn_like_komentar);
            tvJumlahLikeKomentar = view.findViewById(R.id.txt_jumlah_like_komentar);
            txt_waktu_komen = view.findViewById(R.id.txt_waktu_komen);
            txt_balas_komentar = view.findViewById(R.id.txt_balas_komentar);
            txt_lihat_balas_komentar_artikel = view.findViewById(R.id.txt_lihat_balas_komentar_artikel);
            txt_sembunyikan_balas_komentar_artikel = view.findViewById(R.id.txt_sembunyikan_balas_komentar_artikel);
            ly_hor = view.findViewById(R.id.ly_hor);
            //start init recycler sub komentar
            recycler_sub_komentar = view.findViewById(R.id.recycler_sub_komentar);
            layoutManagerRecyclerSubKomentar = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            recycler_sub_komentar.setLayoutManager(layoutManagerRecyclerSubKomentar);
            recycler_sub_komentar.setItemAnimator(new DefaultItemAnimator());
            //end init recycler view sub komentar
            //start init dialog balas komentar
            viewDialog= LayoutInflater.from(context).inflate(R.layout.sheet_balas_komentar,null);
            edt_balas_komentar = viewDialog.findViewById(R.id.edt_balas_komentar);
            btn_balas_komentar = viewDialog.findViewById(R.id.btn_balas_komentar);
            gambar_komentator_artikel = viewDialog.findViewById(R.id.gambar_komentator_video);
            tujuan_balas_komentar = viewDialog.findViewById(R.id.tujuan_balas_komentar);
            loading_balas_komentar = viewDialog.findViewById(R.id.loading_balas_komentar);
            relative_layout_sheet_balas_komentar = viewDialog.findViewById(R.id.relative_layout_sheet_balas_komentar);
            bottomSheetDialogBalasKomentar = new BottomSheetDialog(context);
            bottomSheetDialogBalasKomentar.setContentView(viewDialog);
            //end init dialog balas komentar

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });

            txt_lihat_balas_komentar_artikel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stateShowBalasKomentar();
                }
            });

            txt_sembunyikan_balas_komentar_artikel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stateHideBalasKomentar();
                }
            });

            dataSubKomentar = new ArrayList<>();
            dataRecentAdd = new ArrayList<>();
        }


        @Override
        public void showReplyCommentDialog(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"showReplyCommentDialog()");
            bottomSheetDialogBalasKomentar.show();
        }

        @Override
        public void setDataToReplyCommentDialog(String namaPenggunaYangDibalas, String idKomentarYangDibalas, String idUserYangMembalas){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"openReplyCommentDialog()");
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : namaPenggunaYangDibalas: "+namaPenggunaYangDibalas);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : idKomentarYangDibalas: "+idKomentarYangDibalas);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : idUserYangMembalas: "+idUserYangMembalas);


            Glide.with(context)
                    .load(SharedPrefManagerZona.getInstance(context).geturl_voto())
                    .into(gambar_komentator_artikel);
            tujuan_balas_komentar.setText(namaPenggunaYangDibalas);

            btn_balas_komentar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CheckNetworkAvailable.check(context)) {
                        //check apakah komentar kosong.
                        if(edt_balas_komentar.getText().toString().equals("")){
                            CustomToast.s(context,"Komentar tidak boleh kosong");
                        }
                        else {
                            kirimBalasKomentar(edt_balas_komentar.getText().toString(), idKomentarYangDibalas, idUserYangMembalas);
                            //bottomSheetDialogBalasKomentar.dismiss();
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            Log.d(Bank_Tag.BALAS_KOMENTAR, "get GLOBAL : viewDialog: " + viewDialog);
                            imm.hideSoftInputFromWindow(viewDialog.getWindowToken(), 0);
                        }
                    }
                }
            });
            edt_balas_komentar.requestFocus();


            bottomSheetDialogBalasKomentar.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {

                }
            });
        }

        @Override
        public void kirimBalasKomentar(String komentar, String idKomentarYangDibalas, String idUserPembalas){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"kirimBalasKomentar()");
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : komentar : "+komentar);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : idKomentarYangDibalas : "+idKomentarYangDibalas);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : idUserPembalas : "+idUserPembalas);
            stateStillCommenting();

            StringRequest strReq = new StringRequest(Request.Method.POST, URL_BALAS_KOMENTAR, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    stateFinishCommenting();
                    clearEditTextBalasKomentar();
                    try {
                        Log.d(TAG,"BALAS_KOMENTAR try");
                        Log.d(TAG,"id_komentar/idKomentarYangDibalas: "+idKomentarYangDibalas);
                        Log.d(TAG,"id_user/idUserPembalas : "+idUserPembalas);
                        Log.d(TAG,"komentar : "+komentar);

                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONObject dataObject = jObj.getJSONObject("item_terbaru");
                            Komentar_Model items = new Gson().fromJson(dataObject.toString(), new TypeToken<Komentar_Model>() {
                            }.getType());
                            int positionInsert = 0;
                            addItemToSubKomentar(items,positionInsert);
                            scrollSubKomentarToCertainPosition(positionInsert);
                            setTextTxtLihatBalasanKomentar();
                            stateShowBalasKomentar();

                        } else if (jObj.getString("status").equals("false")) {


                        }
                        else
                        {

                        }
                        // Check for error node in json

                    } catch (JSONException e) {
                        Log.d(TAG,"BALAS_KOMENTAR catch "+e.getMessage());
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG,"BALAS_KOMENTAR onErrorREsponse "+error.getMessage());
                    stateFinishCommenting();
                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_komentar",idKomentarYangDibalas);
                    params.put("id_user",idUserPembalas);
                    params.put("komentar",komentar);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        }

        private void getBalasKomentarById(String id_komentar, String id_current_user){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"getBalasKomentarById()");
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : id_komentar : "+id_komentar);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : id_current_user : "+id_current_user);

            StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_BALAS_KOMENTAR, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());


                        } else if (jObj.getString("status").equals("false")) {


                        }
                        else
                        {

                        }
                        // Check for error node in json

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_komentar",id_komentar);
                    params.put("id_current_user",id_current_user);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        }
        public List<Komentar_Model> getDataSubKomentar() {
            return dataSubKomentar;
        }

        public void setDataSubKomentar(List<Komentar_Model> dataSubKomentar) {
            Log.d(TAG, "setDataSubKomentar: ");
            this.dataSubKomentar.clear();
            this.dataSubKomentar.addAll(dataSubKomentar);
        }

        //pasang data sub komentar ke adapter sub komentar
        public void setDataSubKomentarToAdapterSubKomentar(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"setDataSubKomentarToAdapterSubKomentar()");

            adapterSubKomentar = new SubKomentar_Adapter(context,dataSubKomentar,idIndukKomentar,this);
            setAdapterRecyclerSubKomentar();
            //start sementara comment
//            if(checkIfAdapterRecyclerViewSubKomentarIsNull())
//            {
//                //adapter masih null, maka proses inisialisasi adapter dijalankan
//                adapterSubKomentar = new SubKomentar_Adapter(context,dataSubKomentar,idIndukKomentar,this);
//                setAdapterRecyclerSubKomentar();
//            }
//            else
//            {
//                //adapter tidak null, maka dan notifyDataChanged saja
//                notifyAdapterSubKomentar();
//            }
            //end sementara comment

        }

        private void notifyAdapterSubKomentar(){
            Log.d(TAG, "notifyAdapterSubKomentar: ");
            adapterSubKomentar.notifyDataSetChanged();
        }

        public void setAdapterRecyclerSubKomentar(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"setAdapterRecyclerSubKomentar()");
            //jika ada adapter dari item komentar level 1 sebelumnya, maka clear dulu
            recycler_sub_komentar.setAdapter(adapterSubKomentar);

        }

        public boolean checkIfAdapterRecyclerViewSubKomentarIsNull(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"checkIfAdapterRecyclerViewSubKomentarIsNull()");
            if(adapterSubKomentar == null)
            {
                return true;
            }
            else{
                return false;
            }
        }



        public boolean checkIfLayoutManagerRecyclerViewSubKomentarIsNull(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"checkIfLayoutManagerRecyclerViewSubKomentarIsNull()");
            if(layoutManagerRecyclerSubKomentar == null)
            {
                return true;
            }
            else{
                return false;
            }
        }

        public boolean checkIfRecyclerViewSubKomentarIsNull(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"checkIfRecyclerViewSubKomentarIsNull()");
            if(recycler_sub_komentar == null)
            {
                return true;
            }
            else{
                return false;
            }
        }

        private boolean checkIfDataSubKomentarAvailable(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"checkIfDataSubKomentarAvailable()");
            if(dataSubKomentar.size() <=0 || dataSubKomentar == null)
            {
                return false;
            }
            else {
                return true;
            }
        }

        //fungsi ini untuk mengatur berbagai hal apabila subKomentar ada. (hide komponen tertentu, show komponen tertentu, dll)
        public void showAllAtributesOfSubKomentarIfSubKomentarExists(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"showAllAtributesOfSubKomentarIfSubKomentarExists()");
            boolean isDataSubKomentarAvailable = checkIfDataSubKomentarAvailable();
            if(isDataSubKomentarAvailable == true){ //ada data sub komentar
                showTxtLihatBalasKomentarArtikel();
                setTextTxtLihatBalasanKomentar();
            }
            else{
                hideTxtLihatBalasKomentarArtikel();
            }
        }

        private void showTxtLihatBalasKomentarArtikel(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"showTxtLihatBalasKomentarArtikel()");
            txt_lihat_balas_komentar_artikel.setVisibility(View.VISIBLE);
        }
        private void hideTxtLihatBalasKomentarArtikel(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"hideTxtLihatBalasKomentarArtikel()");
            txt_lihat_balas_komentar_artikel.setVisibility(View.GONE);
        }

        private void showRecyclerViewSubKomentar(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"showRecyclerViewSubKomentar()");
            recycler_sub_komentar.setVisibility(View.VISIBLE);
        }
        private void hideRecyclerViewSubKomentar(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"hideRecyclerViewSubKomentar()");
            recycler_sub_komentar.setVisibility(View.GONE);
        }
        private void showTxtSembunyikanBalasKomentar(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"showTxtSembunyikanBalasKomentar()");
            txt_sembunyikan_balas_komentar_artikel.setVisibility(View.VISIBLE);
        }
        private void hideTxtSembunyikanBalasKomentar(){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"hideTxtSembunyikanBalasKomentar()");
            txt_sembunyikan_balas_komentar_artikel.setVisibility(View.GONE);
        }

        private void setDataRecentAdd(Komentar_Model lastAddedItem){
            Log.d(Bank_Tag.BALAS_KOMENTAR,"setDataRecentAdd()");
            dataRecentAdd.add(lastAddedItem);
        }

        private void showLoadingBalasKomentar(){
            Log.d(TAG, "showLoadingBalasKomentar: ");
            loading_balas_komentar.setVisibility(View.VISIBLE);
        }

        private void hideLoadingBalasKomentar(){
            Log.d(TAG, "hideLoadingBalasKomentar: ");
            loading_balas_komentar.setVisibility(View.GONE);
        }
        private void showLinearLayoutSheetBalasKomentar(){
            Log.d(TAG, "showLinearLayoutSheetBalasKomentar: ");
            relative_layout_sheet_balas_komentar .setVisibility(View.VISIBLE);
        }

        private void hideLinearLayoutSheetBalasKomentar(){
            Log.d(TAG, "hideLinearLayoutSheetBalasKomentar: ");
            relative_layout_sheet_balas_komentar .setVisibility(View.INVISIBLE);
        }

        private String getTotalSubKomentar(){
            Log.d(TAG, "getTotalSubKomentar: ");
            return String.valueOf(dataSubKomentar.size());
        }

        private void addItemToSubKomentar(Komentar_Model komentarBaru, int position){
            Log.d(TAG, "addItemToSubKomentar: ");
            //jika adapter belum terset, maka set dulu ke data Sub Komentar
            if(checkIfAdapterRecyclerViewSubKomentarIsNull())
            {
                adapterSubKomentar = new SubKomentar_Adapter(context,dataSubKomentar,idIndukKomentar,this);
                setAdapterRecyclerSubKomentar();
            }
            else {

                //start temp
                for(int i=0;i<dataSubKomentar.size();i++){
                    Log.d(TAG,"dirty. data Sub Komentar ke : "+i+" adalah"+dataSubKomentar.get(i).getKomentar());
                }
                    //end temp
                dataSubKomentar.add(position, komentarBaru);
                notifyAdapterSubKomentar();
                for(int i=0;i<dataSubKomentar.size();i++){
                    Log.d(TAG,"dirty. addItemToSubKomentar(): ");
                    Log.d(TAG,"dirty. Komentar: "+dataSubKomentar.get(i).getKomentar());
                    Log.d(TAG,"dirty. : idKomentar"+dataSubKomentar.get(i).getId_komentar());
                    Log.d(TAG,"dirty. : idSubKomentar"+dataSubKomentar.get(i).getId_sub_komentar());
                    Log.d(TAG,"dirty. : waktu komentar"+dataSubKomentar.get(i).getWaktu_komentar());
                    Log.d(TAG,"dirty. : sub komentar"+dataSubKomentar.get(i).getSub_komentar());
                    Log.d(TAG,"dirty. : jumlahLike"+dataSubKomentar.get(i).getJumlah_like());
                    Log.d(TAG,"dirty. : idUser"+dataSubKomentar.get(i).getId_user());
                    Log.d(TAG,"dirty. : person name"+dataSubKomentar.get(i).getPerson_name());
                    Log.d(TAG,"dirty. : personPhoto"+dataSubKomentar.get(i).getPerson_photo());
                    Log.d(TAG,"dirty. : getAdaLikeCurrentUser"+dataSubKomentar.get(i).getAda_like_current_user());
                }

            }
        }

        private void scrollSubKomentarToCertainPosition(int position){
            Log.d(TAG, "scrollSubKomentarToCertainPosition: ");
            recycler_sub_komentar.smoothScrollToPosition(position);
        }


        private void setTextTxtLihatBalasanKomentar(){
            Log.d(TAG, "setTextTxtLihatBalasanKomentar: ");
            if(dataSubKomentar.size()<=0 || dataSubKomentar == null){
               txt_lihat_balas_komentar_artikel.setText("");
               stateNoBalasKomentar();
            }
            else if(dataSubKomentar.size() == 1)
            {
                txt_lihat_balas_komentar_artikel.setText("Lihat balasan komentar");
            }
            else if (dataSubKomentar.size() > 1)
            {
                txt_lihat_balas_komentar_artikel.setText("Lihat "+getTotalSubKomentar()+" balasan komentar");
            }
        }

        private void stateShowBalasKomentar(){
            showRecyclerViewSubKomentar();
            showTxtSembunyikanBalasKomentar();
            hideTxtLihatBalasKomentarArtikel();
        }

        private void stateHideBalasKomentar(){
            hideRecyclerViewSubKomentar();
            hideTxtSembunyikanBalasKomentar();
            showTxtLihatBalasKomentarArtikel();
        }

        private void stateNoBalasKomentar(){
            hideRecyclerViewSubKomentar();
            hideTxtSembunyikanBalasKomentar();
            hideTxtLihatBalasKomentarArtikel();
        }


        @Override
        public void onContactSelected(Komentar_Model transaksihariinidataList) {

        }

        @Override
        public void hapusSubKomentarDiKlik(String idSubKomentar, String idUserSekarang, int position) {
            setTextTxtLihatBalasanKomentar();
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_sub_komentar, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Register Error: " + error.getMessage());
                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", idUserSekarang);
                    params.put("id_sub_komentar", idSubKomentar);
                    return params;
                }
            };


            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                    Log.e(TAG, "VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);


        }

        @Override
        public void likeKomentarOrUnlikeKomentarLevel2(String idKomentar, String idUserSekarang) {
            Log.d("debugKomentar","idKomentar = "+idKomentar);
            Log.d("debugKomentar","idUserSekarang = "+idUserSekarang);

            StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_UNLIKE_SUB_KOMENTAR, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Register Error: " + error.getMessage());

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("person_id", idUserSekarang);
                    params.put("id_sub_komentar", idKomentar);
                    return params;
                }
            };


            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {



                    Log.e(TAG, "VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");


        }

        @Override
        public void kirimBalasKomentarFromSubKomentarAdapter(String komentar, String idKomentarYangDibalas, String idUserPembalas) {
            kirimBalasKomentar(komentar,idKomentarYangDibalas,idUserPembalas);
        }


        private void stateStillCommenting() {
            showLoadingBalasKomentar();
            hideLinearLayoutSheetBalasKomentar();
        }

        private void stateFinishCommenting(){
            hideLoadingBalasKomentar();
            showLinearLayoutSheetBalasKomentar();
            bottomSheetDialogBalasKomentar.dismiss();
        }

        private void clearEditTextBalasKomentar() {
            edt_balas_komentar.setText("");
        }
    }


    public Komentar_Adapter(Context context, List<Komentar_Model> barangdataList, ContactsAdapterListener listener, String idUserSekarang, Activity activity) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
        this.idUserSekarang = idUserSekarang;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_komentar, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Komentar_Model produk_data = barangdataListFiltered.get(position);
       // System.out.println("Potition " + (position + 1));


        holder.setIdIndukKomentar(barangdataListFiltered.get(position).getId_komentar());

//        Holder

        //ada like dari current user. (menentukan true atau tidak nya suatu data memiliki like dari current user dilakukan di API.)
        if(produk_data.getAda_like_current_user() == true)
        {
            holder.btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
        }
        else
        {
            holder.btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_36px));
        }
        int jumlahLike = Integer.valueOf(produk_data.getJumlah_like());
        if(jumlahLike > 0)
        {
            holder.tvJumlahLikeKomentar.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tvJumlahLikeKomentar.setVisibility(View.GONE);
        }
        Log.d("debugKomentar","value boolean = "+produk_data.getKomentar());
        holder.btnLikeKomentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               suka(holder.tvJumlahLikeKomentar,holder.btnLikeKomentar,produk_data,position);
            }
        });
        holder.tvJumlahLikeKomentar.setText(produk_data.getJumlah_like());
        holder.nama_komentator.setText(produk_data.getPerson_name());
//        holder.komentar.setText(produk_data.getKomentar());
        String textnya = produk_data.getKomentar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.komentar.setMovementMethod(LinkMovementMethod.getInstance());
            holder.komentar.setText(Html.fromHtml(textnya, Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.komentar.setMovementMethod(LinkMovementMethod.getInstance());
                holder.komentar.setText(Html.fromHtml(textnya));
        }


        holder.txt_waktu_komen.setText(produk_data.getWaktu_komentar());
        //jika id_user sekarang = id_user ke i, maka tampilkan menu hapus dari komentar ke i tersebut

        //builder untuk dialog klik tahan
        AlertDialog.Builder builderTahan = new AlertDialog.Builder(context);


        if(idUserSekarang.equals(produk_data.getId_user()))
        {
            //start block klik tahan pengguna sendiri
            builderTahan.setItems(itemsCurUser, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    manageKlikTahan(itemsCurUser[item],holder.komentar,position,produk_data);
                }
            });
            //end block klik tahan pengguna sendiri
        }
        else
        {
            //start block klik tahan pengguna lain
            builderTahan.setItems(itemsOtherUser, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    manageKlikTahan(itemsOtherUser[item],holder.komentar,position,produk_data);
                }
            });
            //end block klik tahan pengguna lain

        }
        //start onLongClick
        //Long Press
        holder.ly_hor.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog alert = builderTahan.create();
                alert.show();
                return true;
            }
        });
        //end onLongClick


        Glide.with(context)
                .load(produk_data.getPerson_photo())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.img_komentator);

        holder.txt_balas_komentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        holder.setDataToReplyCommentDialog(produk_data.getPerson_name(),produk_data.getId_komentar(),MainActivity.idUserLogin);
                holder.showReplyCommentDialog();
            }
        });

        //start sesi set data recyclerSubKomentar
        //set data sub Komentar to list subKomentar view holder
        holder.setDataSubKomentar(barangdataListFiltered.get(position).getSub_komentar());
        //set data list subKomentar view holder to adapterRecyclerSubKomentar
        holder.setDataSubKomentarToAdapterSubKomentar();
        //end sesi set data recyclerSubKomentar

        //start control ada data subKomentar atau tidak
        holder.showAllAtributesOfSubKomentarIfSubKomentarExists();
        //end control ada data subKomentar atau tidak

    }

    public void suka(TextView tvJumlahLikeKomentar, ImageView btnLikeKomentar, Komentar_Model produk_data, int position)
    {
        jumlahLike = Integer.valueOf(tvJumlahLikeKomentar.getText().toString());
        listener.likeKomentarOrUnlikeKomentar(produk_data.getId_komentar(),idUserSekarang);
        if (btnLikeKomentar.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.hati_36px).getConstantState()) {
            btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
            //melakukan call
            //jika ada like baru. beri efek like nambah
            jumlahLike = jumlahLike+1;
            tvJumlahLikeKomentar.setText(String.valueOf(jumlahLike));
            barangdataListFiltered.get(position).setJumlah_like(String.valueOf(jumlahLike));
            barangdataListFiltered.get(position).setAda_like_current_user(true);

        } else if(btnLikeKomentar.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.hati_nyala_36px).getConstantState()){
            btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_36px));
            //jika ada unlike baru. beri efek like berkurang
            jumlahLike = jumlahLike-1;
            tvJumlahLikeKomentar.setText(String.valueOf(jumlahLike));
            barangdataListFiltered.get(position).setJumlah_like(String.valueOf(jumlahLike));
            barangdataListFiltered.get(position).setAda_like_current_user(false);
        }
        setTextLike(tvJumlahLikeKomentar);
    }

    public void setTextLike(TextView tvJumlahLikeKomentar)
    {
        jumlahLike = Integer.valueOf(tvJumlahLikeKomentar.getText().toString());
        if(jumlahLike > 0)
            tvJumlahLikeKomentar.setVisibility(View.VISIBLE);
        else
            tvJumlahLikeKomentar.setVisibility(View.INVISIBLE);
    }

    public void manageKlikTahan(CharSequence item, TextView tvKomentar, int position, Komentar_Model produk_data)
    {
        if ("Salin".equals(item)) {
            salin(tvKomentar);
        } else if ("Hapus".equals(item)) {
            hapus(produk_data,position);
        }
    }

    public void salin(TextView tvKomentar)
    {
        ClipboardManager cm = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setText(tvKomentar.getText().toString());
        Toast.makeText(context, "Text disalin", Toast.LENGTH_SHORT).show();
    }

    public void hapus(Komentar_Model produk_data, int position)
    {
        //dialog konfirmasi hapus
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle("Hapus Komentar");
        builder.setMessage("Yakin komentar akan dihapus?");

        builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //jika pengguna memilih ini, maka lanjut hapus komentar
                listener.hapusDiKlik(produk_data.getId_komentar(), produk_data.getId_user());
                barangdataListFiltered.remove(position);
                notifyItemRemoved(position);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Komentar_Model> filteredList = new ArrayList<>();
                    for (Komentar_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getKomentar().toLowerCase().contains(charString.toLowerCase()) || row.getKomentar().contains(charSequence) || row.getKomentar().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Komentar_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Komentar_Model transaksihariinidataList);
        void hapusDiKlik(String idKomentar, String idUserSekarang);
        void likeKomentarOrUnlikeKomentar(String idKomentar, String idUserSekarang);

    }
}
