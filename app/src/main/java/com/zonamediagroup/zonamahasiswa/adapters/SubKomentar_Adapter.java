package com.zonamediagroup.zonamahasiswa.adapters;

import static com.android.volley.VolleyLog.TAG;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Bank_Tag;
import com.zonamediagroup.zonamahasiswa.CheckNetworkAvailable;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.interfaces.PopUpTambahBalasKomentarArtikel;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Model;

import com.zonamediagroup.zonamahasiswa.adapters.Komentar_Adapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SubKomentar_Adapter extends RecyclerView.Adapter<SubKomentar_Adapter.ViewHolder> {
    Context context;
    List<Komentar_Model> dataSubKomentar;
    String idUserSekarang;
    String idIndukKomentar;
    int jumlahLike;
    public static final String TAG = Bank_Tag.BALAS_KOMENTAR;
    private SubKomentar_Adapter.ContactsAdapterListenerSubKomentar listenerSubKomentarAdapter;


    final CharSequence[] itemsCurUser = {"Salin", "Hapus"};
    final CharSequence[] itemsOtherUser = {"Salin"};
    public  SubKomentar_Adapter(Context context, List<Komentar_Model> dataSubKomentar, String idIndukKomentar,SubKomentar_Adapter.ContactsAdapterListenerSubKomentar listenerSubKomentarAdapter){
        this.context = context;
        this.dataSubKomentar = dataSubKomentar;
        idUserSekarang = MainActivity.idUserLogin;
        this.idIndukKomentar = idIndukKomentar;
        this.listenerSubKomentarAdapter = listenerSubKomentarAdapter;
        for(int i=0;i<dataSubKomentar.size();i++){
            Log.d(TAG,"dirty. constructorSubKomentarAdapter(): ");
            Log.d(TAG,"dirty. Komentar: "+dataSubKomentar.get(i).getKomentar());
            Log.d(TAG,"dirty. : idKomentar"+dataSubKomentar.get(i).getId_komentar());
            Log.d(TAG,"dirty. : idSubKomentar"+dataSubKomentar.get(i).getId_sub_komentar());
            Log.d(TAG,"dirty. : waktu komentar"+dataSubKomentar.get(i).getWaktu_komentar());
            Log.d(TAG,"dirty. : sub komentar"+dataSubKomentar.get(i).getSub_komentar());
            Log.d(TAG,"dirty. : jumlahLike"+dataSubKomentar.get(i).getJumlah_like());
            Log.d(TAG,"dirty. : idUser"+dataSubKomentar.get(i).getId_user());
            Log.d(TAG,"dirty. : person name"+dataSubKomentar.get(i).getPerson_name());
            Log.d(TAG,"dirty. : personPhoto"+dataSubKomentar.get(i).getPerson_photo());
            Log.d(TAG,"dirty. : getAdaLikeCurrentUser"+dataSubKomentar.get(i).getAda_like_current_user());
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komentar,parent,false);
        return new SubKomentar_Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Komentar_Model produk_data = dataSubKomentar.get(position);
        // System.out.println("Potition " + (position + 1));




//        Holder

        //ada like dari current user. (menentukan true atau tidak nya suatu data memiliki like dari current user dilakukan di API.)
        if(produk_data.getAda_like_current_user() == true)
        {
            Log.d(TAG,"dirty. if 116");
             holder.btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
        }
        else
        {
            Log.d(TAG,"dirty. else 121");
            holder.btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_36px));
        }

        //jika ada like lebih dari 0, maka tampilkan tvJumlahLike
        Log.e(Bank_Tag.BALAS_KOMENTAR,"produk_data.getJumlah_like sub komen: "+produk_data.getJumlah_like());
        if(Integer.valueOf(produk_data.getJumlah_like()) > 0 && produk_data.getJumlah_like() != null)
        {
            holder.tvJumlahLikeKomentar.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tvJumlahLikeKomentar.setVisibility(View.GONE);
        }
        Log.d("debugKomentar","value boolean = "+produk_data.getKomentar());
        holder.btnLikeKomentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suka(holder.tvJumlahLikeKomentar,holder.btnLikeKomentar,produk_data,position);
            }
        });
        holder.tvJumlahLikeKomentar.setText(produk_data.getJumlah_like());
        holder.nama_komentator.setText(produk_data.getPerson_name());
        //holder.komentar.setText(produk_data.getKomentar());
        String textnya = produk_data.getKomentar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.komentar.setMovementMethod(LinkMovementMethod.getInstance());
            holder.komentar.setText(Html.fromHtml(textnya, Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.komentar.setMovementMethod(LinkMovementMethod.getInstance());
            holder.komentar.setText(Html.fromHtml(textnya));
        }

        holder.txt_waktu_komen.setText(produk_data.getWaktu_komentar());
        //jika id_user sekarang = id_user ke i, maka tampilkan menu hapus dari komentar ke i tersebut

        //builder untuk dialog klik tahan
        AlertDialog.Builder builderTahan = new AlertDialog.Builder(context);

        Log.d(TAG,"dirty. subKomentar_Adapter getId_user() "+produk_data.getId_user());
        Log.d(TAG,"dirty. subKomentar_Adapter idUserSekarang "+idUserSekarang);
        if(idUserSekarang.equals(produk_data.getId_user()))
        {
            //start block klik tahan pengguna sendiri
            builderTahan.setItems(itemsCurUser, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    manageKlikTahan(itemsCurUser[item],holder.komentar,position,produk_data);
                }
            });
            //end block klik tahan pengguna sendiri
        }
        else
        {
            //start block klik tahan pengguna lain
            builderTahan.setItems(itemsOtherUser, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {

                    manageKlikTahan(itemsOtherUser[item],holder.komentar,position,produk_data);
                }
            });
            //end block klik tahan pengguna lain

        }
        //start onLongClick
        //Long Press
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog alert = builderTahan.create();
                alert.show();
                return false;
            }
        });
        //end onLongClick


        Glide.with(context)
                .load(produk_data.getPerson_photo())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.img_komentator);

        holder.txt_balas_komentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.setDataToReplyCommentDialog(produk_data.getPerson_name(),produk_data.getId_komentar(),MainActivity.idUserLogin);
                holder.showReplyCommentDialog();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSubKomentar.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements PopUpTambahBalasKomentarArtikel {

        CircleImageView img_komentator;

        public TextView nama_komentator, komentar, hapus, txt_waktu_komen;

        private ImageView btnLikeKomentar;
        private TextView tvJumlahLikeKomentar;

        //start component dialog balas komentar
        BottomSheetDialog bottomSheetDialogBalasKomentar;
        EditText edt_balas_komentar;
        ImageView btn_balas_komentar, gambar_komentator_artikel;
        TextView tujuan_balas_komentar;
        View viewDialog;
        ProgressBar loading_balas_komentar;
        //end component dialog balas komentar

        //start component untuk tampil balas komentar
        RecyclerView recycler_sub_komentar;
        SubKomentar_Adapter adapterSubKomentar;
        RecyclerView.LayoutManager layoutManagerRecyclerSubKomentar;
        RelativeLayout relative_layout_sheet_balas_komentar;
        //end component untuk tampil balas komentar

        TextView txt_balas_komentar;


        public ViewHolder(@NonNull View view) {
            super(view);

//            definisi komponen

            nama_komentator = view.findViewById(R.id.nama_komentator);
            komentar = view.findViewById(R.id.komentar);
            img_komentator=view.findViewById(R.id.img_komentator);
            btnLikeKomentar = view.findViewById(R.id.btn_like_komentar);
            tvJumlahLikeKomentar = view.findViewById(R.id.txt_jumlah_like_komentar);
            txt_waktu_komen = view.findViewById(R.id.txt_waktu_komen);
            txt_balas_komentar = view.findViewById(R.id.txt_balas_komentar);
            //start init recycler sub komentar
            recycler_sub_komentar = view.findViewById(R.id.recycler_sub_komentar);
            layoutManagerRecyclerSubKomentar = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            recycler_sub_komentar.setLayoutManager(layoutManagerRecyclerSubKomentar);
            recycler_sub_komentar.setItemAnimator(new DefaultItemAnimator());
            //end init recycler view sub komentar
            //start init dialog balas komentar
            viewDialog= LayoutInflater.from(context).inflate(R.layout.sheet_balas_komentar,null);
            edt_balas_komentar = viewDialog.findViewById(R.id.edt_balas_komentar);
            btn_balas_komentar = viewDialog.findViewById(R.id.btn_balas_komentar);
            gambar_komentator_artikel = viewDialog.findViewById(R.id.gambar_komentator_video);
            tujuan_balas_komentar = viewDialog.findViewById(R.id.tujuan_balas_komentar);
            loading_balas_komentar = viewDialog.findViewById(R.id.loading_balas_komentar);
            relative_layout_sheet_balas_komentar = viewDialog.findViewById(R.id.relative_layout_sheet_balas_komentar);
            bottomSheetDialogBalasKomentar = new BottomSheetDialog(context);
            bottomSheetDialogBalasKomentar.setContentView(viewDialog);
            //end init dialog balas komentar

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    Log.d(Bank_Tag.BALAS_KOMENTAR,"besar getAdapterPosition() "+getAdapterPosition());
                    listenerSubKomentarAdapter.onContactSelected(dataSubKomentar.get(getAdapterPosition()));
                }
            });

        }

        @Override
        public void showReplyCommentDialog() {
            Log.d(Bank_Tag.BALAS_KOMENTAR,"showReplyCommentDialog()");
            bottomSheetDialogBalasKomentar.show();
        }

        @Override
        public void setDataToReplyCommentDialog(String namaPenggunaYangDibalas, String idKomentarYangDibalas, String idUserYangMembalas) {
            Log.d(Bank_Tag.BALAS_KOMENTAR,"openReplyCommentDialog()");
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : namaPenggunaYangDibalas: "+namaPenggunaYangDibalas);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : idKomentarYangDibalas: "+idKomentarYangDibalas);
            Log.d(Bank_Tag.BALAS_KOMENTAR,"input : idUserYangMembalas: "+idUserYangMembalas);


            Glide.with(context)
                    .load(SharedPrefManagerZona.getInstance(context).geturl_voto())
                    .into(gambar_komentator_artikel);
            tujuan_balas_komentar.setText(namaPenggunaYangDibalas);

            edt_balas_komentar.setText("@"+namaPenggunaYangDibalas+" ");

            btn_balas_komentar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CheckNetworkAvailable.check(context)) {
                        //check apakah komentar kosong.
                        if(edt_balas_komentar.getText().toString().equals("")){
                            CustomToast.s(context,"Komentar tidak boleh kosong");
                        }
                        else {
                            listenerSubKomentarAdapter.kirimBalasKomentarFromSubKomentarAdapter(edt_balas_komentar.getText().toString(), idKomentarYangDibalas, idUserYangMembalas);
                            //bottomSheetDialogBalasKomentar.dismiss();
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            Log.d(Bank_Tag.BALAS_KOMENTAR, "get GLOBAL : viewDialog: " + viewDialog);
                            imm.hideSoftInputFromWindow(viewDialog.getWindowToken(), 0);
                            bottomSheetDialogBalasKomentar.dismiss();
                        }
                    }
                }
            });
            edt_balas_komentar.requestFocus();


            bottomSheetDialogBalasKomentar.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {

                }
            });
        }

        @Override
        public void kirimBalasKomentar(String komentar, String idKomentarYangDibalas, String idUserPembalas) {

        }

    }

    public void manageKlikTahan(CharSequence item, TextView tvKomentar, int position, Komentar_Model produk_data)
    {
        if ("Salin".equals(item)) {
            salin(tvKomentar);
        } else if ("Hapus".equals(item)) {
            hapus(produk_data,position);
        }
    }

    public void salin(TextView tvKomentar)
    {
        ClipboardManager cm = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setText(tvKomentar.getText().toString());
        Toast.makeText(context, "Text disalin", Toast.LENGTH_SHORT).show();
    }

    public void hapus(Komentar_Model produk_data, int position)
    {

        //dialog konfirmasi hapus
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Hapus Komentar");
        builder.setMessage("Yakin komentar akan dihapus?");

        builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dataSubKomentar.remove(position);
                notifyDataSetChanged();
                //jika pengguna memilih ini, maka lanjut hapus komentar
                listenerSubKomentarAdapter.hapusSubKomentarDiKlik(produk_data.getId_sub_komentar(),idUserSekarang,position);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void suka(TextView tvJumlahLikeKomentar, ImageView btnLikeKomentar, Komentar_Model produk_data, int position)
    {
        Log.d(TAG,"textview like: "+tvJumlahLikeKomentar.getText().toString()+" like datamodel: "+produk_data.getJumlah_like());
        jumlahLike = Integer.valueOf(tvJumlahLikeKomentar.getText().toString());
        listenerSubKomentarAdapter.likeKomentarOrUnlikeKomentarLevel2(produk_data.getId_sub_komentar(),idUserSekarang);
        if (btnLikeKomentar.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.hati_36px).getConstantState()) {
         btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
            //melakukan call
            //jika ada like baru. beri efek like nambah
            jumlahLike = jumlahLike+1;
            tvJumlahLikeKomentar.setText(String.valueOf(jumlahLike));
            dataSubKomentar.get(position).setJumlah_like(String.valueOf(jumlahLike));
            dataSubKomentar.get(position).setAda_like_current_user(true);

        } else if(btnLikeKomentar.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.hati_nyala_36px).getConstantState()){
            btnLikeKomentar.setImageDrawable(context.getDrawable(R.drawable.hati_36px));
            //jika ada unlike baru. beri efek like berkurang
            jumlahLike = jumlahLike-1;
            tvJumlahLikeKomentar.setText(String.valueOf(jumlahLike));
            dataSubKomentar.get(position).setJumlah_like(String.valueOf(jumlahLike));
            dataSubKomentar.get(position).setAda_like_current_user(false);
        }
        setTextLike(tvJumlahLikeKomentar);
    }

    public void setTextLike(TextView tvJumlahLikeKomentar)
    {
        jumlahLike = Integer.valueOf(tvJumlahLikeKomentar.getText().toString());
        if(jumlahLike > 0)
            tvJumlahLikeKomentar.setVisibility(View.VISIBLE);
        else
            tvJumlahLikeKomentar.setVisibility(View.INVISIBLE);
    }

    public interface ContactsAdapterListenerSubKomentar {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Komentar_Model transaksihariinidataList);
        void hapusSubKomentarDiKlik(String idSubKomentar, String idUserSekarang, int position);
        void likeKomentarOrUnlikeKomentarLevel2(String idSubKomentar, String idUserSekarang);
        void kirimBalasKomentarFromSubKomentarAdapter(String komentar, String idKomentarYangDibalas, String idUserPembalas);
    }
}
