package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.models.Download_Model;

import java.util.ArrayList;
import java.util.List;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Download_Adapter extends RecyclerView.Adapter<Download_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Download_Model> barangdataList;
    private List<Download_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;


    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView judul_download;
        ImageView img_thumnile;
        TextView txt_ekstensi;
        RelativeLayout ly_ekstensi, containerThumbnail;
        ImageView gambarSegitiga;




        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            judul_download = view.findViewById(R.id.judul_download);
            img_thumnile=view.findViewById(R.id.img_thumnile);

            ly_ekstensi = view.findViewById(R.id.ly_ekstensi_download);

            containerThumbnail = view.findViewById(R.id.container_img_thumnile);
            gambarSegitiga = view.findViewById(R.id.gambar_segitiga);
            txt_ekstensi = view.findViewById(R.id.txt_ekstensi);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public Download_Adapter(Context context, List<Download_Model> barangdataList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_download, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Download_Model produk_data = barangdataListFiltered.get(position);
       // System.out.println("Potition " + (position + 1));



//        Holder


        holder.judul_download.setText(produk_data.getTittle_download());

        Glide.with(context)
                .load(produk_data.getImg_download())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.img_thumnile);

//        Holder
        //set Text ekstensi
        String ekstensi = getEkstensi(produk_data.getUrl_file());
        holder.txt_ekstensi.setText(ekstensi);
        holder.txt_ekstensi.setTextSize(TypedValue.COMPLEX_UNIT_SP,10);

        //set color layout ekstensi
//        switch(ekstensi){
//            case "pptx":
//                //holder.ly_ekstensi.setBackgroundColor(Color.parseColor("#f56949"));
//                holder.ly_ekstensi.setBackgroundColor(Color.parseColor("#FFF15F4E"));
//                holder.gambarSegitiga.setVisibility(View.GONE);
//                holder.containerThumbnail.setPadding(0,0,0,0);
//                break;
//            case "docx":
//                holder.ly_ekstensi.setBackgroundColor(Color.parseColor("#002a3e"));
//                holder.gambarSegitiga.setVisibility(View.VISIBLE);
//                break;
//            case "pdf":
//                //holder.ly_ekstensi.setBackgroundColor(Color.parseColor("#002a3e"));
//                holder.ly_ekstensi.setBackgroundColor(Color.parseColor("#FFCEAC"));
//                holder.gambarSegitiga.setVisibility(View.VISIBLE);
//                break;
//        }
        holder.ly_ekstensi.setBackgroundColor(Color.parseColor("#FFF15F4E"));
        holder.gambarSegitiga.setVisibility(View.GONE);
        holder.containerThumbnail.setPadding(0,0,0,0);

    }

    private String getEkstensi(String url)
    {
        return url.substring(url.lastIndexOf('.')+1);
    }



    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Download_Model> filteredList = new ArrayList<>();
                    for (Download_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getTittle_download().toLowerCase().contains(charString.toLowerCase()) || row.getTittle_download().contains(charSequence) || row.getTittle_download().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Download_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Download_Model transaksihariinidataList);
    }
}
