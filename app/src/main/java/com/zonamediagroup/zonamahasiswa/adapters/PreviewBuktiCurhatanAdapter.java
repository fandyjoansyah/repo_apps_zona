package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zonamediagroup.zonamahasiswa.R;

import java.util.ArrayList;
import java.util.List;

public class PreviewBuktiCurhatanAdapter extends RecyclerView.Adapter<PreviewBuktiCurhatanAdapter.PreviewBuktiCurhatanViewHolder> {

    ArrayList<Uri> dataUri;
    Context context;
    PreviewBuktiCurhatanListener previewBuktiCurhatanListener;

    public PreviewBuktiCurhatanAdapter(ArrayList<Uri> dataUri, Context context,PreviewBuktiCurhatanListener previewBuktiCurhatanListener)
    {
        this.dataUri = dataUri;
        this.context = context;
        this.previewBuktiCurhatanListener = previewBuktiCurhatanListener;
    }

    @NonNull
    @Override
    public PreviewBuktiCurhatanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview_bukti_curhatan, parent, false);
        return new PreviewBuktiCurhatanViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull PreviewBuktiCurhatanViewHolder holder, int position) {
       holder.initFromOnBindViewHolder(dataUri.get(position));
    }

    @Override
    public int getItemCount() {
        return dataUri.size();
    }

    public class PreviewBuktiCurhatanViewHolder extends RecyclerView.ViewHolder {
        ImageView delete_cover,img_cover_preview,icon_media;

        View itemView;
        Uri uri;
        public PreviewBuktiCurhatanViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            findViewByIdAllComponent();
            setListenerAllComponent();
          //  setTextTvDummy();
        }

        private void initFromOnBindViewHolder(Uri uri)
        {
            this.uri = uri;
            addThumbnailToImageView(this.uri);
            mekanismeSetIconVideo();
        }

        private void mekanismeSetIconVideo(){
            String ekstensi = previewBuktiCurhatanListener.getEkstensiFromActivity(uri);
            if(ekstensi.equals("mp4"))
                setIconVideo();
        }

        private void setIconVideo(){
            Glide
                    .with(context)
                    .load(R.drawable.ic_bukti_video)
                    .into(icon_media);
        }


        private void findViewByIdAllComponent(){
            delete_cover = itemView.findViewById(R.id.delete_cover);
            img_cover_preview = itemView.findViewById(R.id.img_cover_preview);
            icon_media = itemView.findViewById(R.id.icon_media);
        }


        private void setListenerAllComponent(){
            delete_cover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeData(getAdapterPosition());
                }
            });

        }

        private void removeData(int index){
            dataUri.remove(index);
            notifyDataSetChanged();
        }

        private void addThumbnailToImageView(Uri uriBukti){
            Log.d("9_agustus_2022","path uriBukti: "+uriBukti.getPath());
            Log.d("8_agustus_2022","addThumbnailToImageView()");
            Glide
                    .with(context)
                    .load(uriBukti)
                    .into(img_cover_preview);
        }
    }

    public interface PreviewBuktiCurhatanListener{
       public String getEkstensiFromActivity(Uri uri);
    }
}
