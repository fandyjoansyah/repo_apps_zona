package com.zonamediagroup.zonamahasiswa.adapters.komentar_video;

import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.CheckNetworkAvailable;
import com.zonamediagroup.zonamahasiswa.CurrentActiveVideoData;
import com.zonamediagroup.zonamahasiswa.CustomToast;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Komentar_Video_Level1_Adapter extends RecyclerView.Adapter<Komentar_Video_Level1_Adapter.ViewHolder> implements Komentar_Video_Level2_Adapter.InterfaceKomentarLevel2 {
    Context context;
    String URL_LIKE_KOMENTAR_VIDEO_LEVEL1 = Server.URL_REAL_VIDEO+"Video_comment_like/index_post";
String URL_KOMENTAR_LEVEL_2 = Server.URL_REAL_VIDEO+"Video_comment/index_post";
    private static final String URL_BALAS_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_reply_create/index_post";
    int jumlahLikeSekarang=0;
    List<Komentar_Video_Model> komentarLevel1;

    List<Komentar_Video_Model> arrKom = null;
    KomentarLevel1 listenerKomentarLevel1;

    //tipe parent adalah tipe dari induk Komentar_Video_Level1_Adapter. apabila 0 maka halaman yang tidak menampilkan lihat balasan. apabila 1 maka halaman yang menampilkan lihat balasan
    //int tipeParent=0;
    BottomSheetDialog bottomSheetDialogBalasKomentar;
    String targetKomentarInduk;
    String targetKomentarAnak;
    int indeksCheck;
    RecyclerView recyclerViewMain;




  public Komentar_Video_Level1_Adapter(Context context, List<Komentar_Video_Model> komentarLevel1, KomentarLevel1 listenerKomentarLevel1) {
        this.context = context;
        this.komentarLevel1 = komentarLevel1;
        this.listenerKomentarLevel1 = listenerKomentarLevel1;
        cekKomentarSingleVideo();
        //tipe parent adalah tipe dari induk Komentar_Video_Level1_Adapter. apabila 0 maka halaman yang tidak menampilkan lihat balasan. apabila 1 maka halaman yang menampilkan lihat balasan

    }
    @NonNull
    @Override
    public Komentar_Video_Level1_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komentar_video_level1,parent,false);
        return new Komentar_Video_Level1_Adapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Komentar_Video_Level1_Adapter.ViewHolder holder, int position) {
        if(komentarLevel1.get(position).getPerson_name() != null){
            Komentar_Video_Model dataKomentar = komentarLevel1.get(position);
            holder.dataKomentar = dataKomentar;
            holder.tvKomentar.setText(dataKomentar.getVideo_comment());
            holder.jumlahKomentarLevel1.setText(dataKomentar.getVideo_comment_like());
            holder.tvWaktuKomentar.setText(dataKomentar.getVideo_comment_date_created());
            holder.tvNamaKomentator.setText(dataKomentar.getPerson_name());
            holder.tvWaktuKomentar.setText(dataKomentar.getConvert_time());
            if(dataKomentar.getVideo_comment_like_id() != null)
                holder.imgLikeLevel1.setImageDrawable(context.getDrawable(R.drawable.hati_nyala_36px));
            else
                holder.imgLikeLevel1.setImageDrawable(context.getDrawable(R.drawable.hati_36px));

            Glide.with(context)
                    .load(komentarLevel1.get(position).getPerson_photo())
                    .into(holder.fotoProfilKomentarLevel1);

            holder.linear_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    jumlahLikeSekarang = Integer.valueOf(holder.jumlahKomentarLevel1.getText().toString());

                    if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
                    {
                        jumlahLikeSekarang = jumlahLikeSekarang+1;
                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"1", komentarLevel1.get(position).getPerson_id(),komentarLevel1.get(position).getVideo_id());
                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
                        jumlahLikeSekarang = 0;
                    }
                    else if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
                    {
                        jumlahLikeSekarang = jumlahLikeSekarang-1;
                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"0", komentarLevel1.get(position).getPerson_id(),komentarLevel1.get(position).getVideo_id());
                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_36px),150,150);
                        jumlahLikeSekarang = 0;
                    }
                    komentarLevel1.get(position).setVideo_comment_like(String.valueOf(jumlahLikeSekarang));
                }
            });

//            holder.imgLikeLevel1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Log.e(MainActivity.MYTAG,"error klik like level 1 "+holder.jumlahKomentarLevel1.getText().toString());
//                    jumlahLikeSekarang = Integer.valueOf(holder.jumlahKomentarLevel1.getText().toString());
//
//                    if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang+1;
//                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"1");
//                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_nyala_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    else if(holder.imgLikeLevel1.getDrawable().getConstantState().equals(holder.imgLikeLevel1.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
//                    {
//                        jumlahLikeSekarang = jumlahLikeSekarang-1;
//                        holder.jumlahKomentarLevel1.setText(String.valueOf(jumlahLikeSekarang));
//                        likeKomentarVideo(MainActivity.idUserLogin,komentarLevel1.get(position).getVideo_comment_id(),"0");
//                        funcTransisiGambar(holder.imgLikeLevel1,context.getDrawable(R.drawable.hati_36px),150,150);
//                        jumlahLikeSekarang = 0;
//                    }
//                    komentarLevel1.get(position).setVideo_comment_like(String.valueOf(jumlahLikeSekarang));
//
//                }
//            });
            holder.txt_balas_komentar_video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.tampilDialogBalasKomentar(MainActivity.idUserLogin, dataKomentar.getVideo_id(),  dataKomentar.getVideo_comment_id(), dataKomentar.getPerson_id(),"",dataKomentar.getPerson_name());
                }
            });
            //start tambahkan komentar level 2
            //FANDY TODO: dapatkan komentar level 2 by id level 1
            Log.d(MainActivity.MYTAG,"pemanggil level2: "+dataKomentar.getVideo_comment());
            holder.getKomentarLevel2ById(MainActivity.idUserLogin,dataKomentar.getVideo_id(),dataKomentar.getVideo_comment_id(),holder.tampil_komen_level2);
            //end tambahkan komentar level 2

                holder.tampil_komen_level2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.showLevel2();
                    }
                });


            holder.hide_komen_video_level2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.hideLevel2();
                }
            });

        }


    }




    @Override
    public int getItemCount() {
        if(komentarLevel1 == null)
        {
            return 0;
        }
        else {
            //  return 2; //tampilkan hanya 1 recyclerview saja
            return komentarLevel1.size();
        }
    }

    //override interface nya Komentar_Video_Level2_Adapter
    @Override
    public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {
        listenerKomentarLevel1.showAllCommentForReply(idVideo,idKomentarYangDibalas,namaKomentatorYangDibalas,idUserGoogleYangDibalas);
    }

    @Override
    public void balasKomentarLevel2(String something) {

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerViewMain = recyclerView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements Komentar_Video_Level2_Adapter.InterfaceKomentarLevel2 {
        TextView tvKomentar,jumlahKomentarLevel1,tvWaktuKomentar,tvNamaKomentator;
        ImageView fotoProfilKomentarLevel1,imgLikeLevel1;
        TextView tampil_komen_level2,hide_komen_video_level2;
        LinearLayout layout_komen_video_level2, linear_like;
        RecyclerView recyclerLevel2;
        TextView txt_balas_komentar_video;
        Komentar_Video_Level2_Adapter komentarLevel2Adapter;
        Komentar_Video_Model dataKomentar;
        public  ArrayList<Komentar_Video_Model> dataKomentarLevel2 = new ArrayList<>();


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKomentar = itemView.findViewById(com.zonamediagroup.zonamahasiswa.R.id.tv_komentarvideo_level1);
            fotoProfilKomentarLevel1 = itemView.findViewById(R.id.foto_profil_komentar_level1);
            imgLikeLevel1 = itemView.findViewById(R.id.like_video_level1);
            jumlahKomentarLevel1 = itemView.findViewById(R.id.jumlah_komentar_level1);
            tvWaktuKomentar = itemView.findViewById(R.id.waktu_komentar_video_level1);
            tvNamaKomentator = itemView.findViewById(R.id.nama_komentator_video_level1);
            recyclerLevel2 = itemView.findViewById(R.id.recycler_komen_video_level2);
            tampil_komen_level2 = itemView.findViewById(R.id.tampil_komen_level2);
            layout_komen_video_level2 = itemView.findViewById(R.id.layout_komen_video_level2);
            hide_komen_video_level2 = itemView.findViewById(R.id.hide_komen_video_level2);
            txt_balas_komentar_video = itemView.findViewById(R.id.txt_balas_komentar_video);
            linear_like = itemView.findViewById(R.id.linear_like);
        }

        //start percobaan2
        public void tampilDialogBalasKomentar(String idUser, String idVideo,String idKomentarYangDibalas, String idUserYangDibalas, String namaPenggunaLevel2YangDibalas,String namaPenggunaYangDibalas)
        {
            Log.d(MainActivity.MYTAG,"tampilDialogBalasKomentar(). idUserYangDibalas: "+idUserYangDibalas);
            bottomSheetDialogBalasKomentar = new BottomSheetDialog(context);
            View view= LayoutInflater.from(context).inflate(R.layout.sheet_balas_komentar,null);
            EditText edt_balas_komentar;
            ImageView btn_balas_komentar, gambar_komentator_video;
            TextView tujuan_balas_komentar;


            edt_balas_komentar = view.findViewById(R.id.edt_balas_komentar);
            btn_balas_komentar = view.findViewById(R.id.btn_balas_komentar);
            gambar_komentator_video = view.findViewById(R.id.gambar_komentator_video);
            Glide.with(context)
                    .load(SharedPrefManagerZona.getInstance(context).geturl_voto())
                    .into(gambar_komentator_video);
            tujuan_balas_komentar = view.findViewById(R.id.tujuan_balas_komentar);

            tujuan_balas_komentar.setText(namaPenggunaYangDibalas);
            if(!namaPenggunaLevel2YangDibalas.equals(""))
            {
                edt_balas_komentar.setText("@"+namaPenggunaLevel2YangDibalas);
            }

            btn_balas_komentar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CheckNetworkAvailable.check(context)) {
                        balasKomentar(idUser, idVideo, edt_balas_komentar.getText().toString(), idKomentarYangDibalas, idUserYangDibalas);
                        bottomSheetDialogBalasKomentar.dismiss();
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            });
            edt_balas_komentar.requestFocus();


            bottomSheetDialogBalasKomentar.setContentView(view);
            bottomSheetDialogBalasKomentar.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {

                }
            });
            bottomSheetDialogBalasKomentar.show();
        }

        private void getKomentarLevel2ById(String id_user, String id_video, String id_comment,TextView tampil_komen_level2) {
            Log.d(MainActivity.MYTAG,"PAY ATTENTION. id_video: "+id_video);
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_KOMENTAR_LEVEL_2, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");

                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());

                            //start load komentar level 2
                            if(items.size() > 0) {

                                tampil_komen_level2.setText("Lihat " + items.size() + " balasan");
                                for(Komentar_Video_Model komentar:items)
                                {
                                    Log.d(MainActivity.MYTAG,"PAY ATTENTION. komentar: "+komentar.getVideo_comment());
                                }

                                dataKomentarLevel2.clear();
                                dataKomentarLevel2.addAll(items);
                                hideLevel2();

                                komentarLevel2Adapter = new Komentar_Video_Level2_Adapter(context, dataKomentarLevel2,ViewHolder.this);

                                LinearLayoutManager layoutLevel1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                                recyclerLevel2.setLayoutManager(layoutLevel1);
                                recyclerLevel2.setAdapter(komentarLevel2Adapter);

                                //end load komentar level 2

                                checkTargetKomentarLevel2(dataKomentarLevel2,komentarLevel2Adapter);
                            }

                        } else if (jObj.getString("status").equals("false")) {

                        }

                    } catch (JSONException e) {
                        // JSON error

                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", id_user);
                    params.put("id_video",id_video);
                    params.put("id_comment",id_comment);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");


        }

        private void checkTargetKomentarLevel2(ArrayList<Komentar_Video_Model> dataKomentarLevel2,Komentar_Video_Level2_Adapter komentarLevel2Adapter){
            if(targetKomentarInduk != null){
                if(getAdapterPosition() != -1) { //untuk njagani kondisi awal dimana getAdapterPosition nilainya adalah -1
                    if (komentarLevel1.get(getAdapterPosition()).getVideo_comment_id().equals(targetKomentarInduk)) {
                        recyclerViewMain.scrollToPosition(getAdapterPosition());
                        if (targetKomentarAnak != null) {
                            showLevel2();
                            for (int i = 0; i < dataKomentarLevel2.size(); i++) {
                                if (dataKomentarLevel2.get(i).getVideo_comment_id().equals(targetKomentarAnak)) {
                                    recyclerLevel2.scrollToPosition(komentarLevel2Adapter.getItemCount() - 1);
                                    //layoutLevel1.scrollToPosition(i);
                                    indeksCheck = i;
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            recyclerLevel2.scrollToPosition(komentarLevel2Adapter.getItemCount() - 1);
                                        }
                                    }, 200);
                                }
                            }
                        } else {
                            showLevel2();
                        }

                    }
                }
            }
            //end check target komentar
        }
        public void balasKomentar(String idUser, String idVideo, String komentar, String idKomentarYangDibalas, String idUserYangDibalas){
            Log.e(MainActivity.MYTAG,"launching balas komentar"+idUserYangDibalas);
            StringRequest strReq = new StringRequest(Request.Method.POST, URL_BALAS_KOMENTAR, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sAC try");
                        JSONObject jObj = new JSONObject(response);

                        if (jObj.getString("status").equals("true")) {
                            JSONArray dataArray = jObj.getJSONArray("data");
                            List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                            }.getType());
                            Komentar_Video_Model singleItems = items.get(0);
                            //start percobaan
                            if(komentarLevel2Adapter!=null)
                            {
                                addDataToRecyclerViewCommentSubLevel(komentarLevel2Adapter, dataKomentarLevel2, singleItems, 0, recyclerLevel2);
                            }
                            else {
                                initDataRecyclerViewCommentOnFirstAdd(singleItems);
                            }
                            //end percobaan
                            //  addDataToRecyclerViewCommentSubLevel();
                        } else if (jObj.getString("status").equals("false")) {
                            //segera hapus
                            Log.e(MainActivity.MYTAG,"balasKomentar:onResponse:try:elseIf");
                            Log.d(MainActivity.MYTAG,"sac try if else");

                        }
                        else
                        {
                            Log.e(MainActivity.MYTAG,"balasKomentar:onResponse:try:else");
                            //segera hapus
                            Log.d(MainActivity.MYTAG,"sac try else");
                        }

                        // Check for error node in json

                    } catch (JSONException e) {
                        Log.e(MainActivity.MYTAG,"balasKomentar:onResponse:catch: "+e.getMessage());
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(MainActivity.MYTAG,"balasKomentar:onErrorResponse "+error.getMessage());

                }
            }) {


//            parameter

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TOKEN", "qwerty");
                    params.put("id_user", idUser);
                    params.put("id_video",idVideo);
                    params.put("comment",komentar);
                    params.put("id_comment",idKomentarYangDibalas);
                    params.put("id_user_reply",idUserYangDibalas);
                    return params;
                }
            };

//        ini heandling requestimeout
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                    Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

                }
            });


            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        }

        public  void  addDataToRecyclerViewCommentSubLevel(RecyclerView.Adapter adapter, List<Komentar_Video_Model> allData, Komentar_Video_Model newData, int indeksPosisiBaru, RecyclerView recyclerView){
            Log.e(MainActivity.MYTAG,"addDataToRecyclerViewCommentSubLevel");
            allData.add(indeksPosisiBaru,newData);
                adapter.notifyItemInserted(indeksPosisiBaru);
                recyclerView.smoothScrollToPosition(indeksPosisiBaru);
        }
        //end percobaan2

        private void initDataRecyclerViewCommentOnFirstAdd(Komentar_Video_Model items) {
            Log.e(MainActivity.MYTAG,"initDataRecyclerViewCommentOnFirstAdd");
            dataKomentarLevel2.add(items);
            komentarLevel2Adapter = new Komentar_Video_Level2_Adapter(context,dataKomentarLevel2,this);
            LinearLayoutManager layoutLevel2 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            recyclerLevel2.setLayoutManager(layoutLevel2);
            recyclerLevel2.setAdapter(komentarLevel2Adapter);
            showLevel2();
            recyclerLevel2.setVisibility(View.VISIBLE);


        }

        @Override
        public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {

        }

        @Override
        public void balasKomentarLevel2(String namaPenggunaLevel2YangDibalas) {
            //samakan dengan aksi holder.txt_balas_komentar_video.setOnClickListener...
            String namaTujuanBalas = namaPenggunaLevel2YangDibalas;
            tampilDialogBalasKomentar(MainActivity.idUserLogin, dataKomentar.getVideo_id(),  dataKomentar.getVideo_comment_id(), dataKomentar.getPerson_id(), namaPenggunaLevel2YangDibalas,namaTujuanBalas);
        }

        private void showLevel2(){
            layout_komen_video_level2.setVisibility(View.VISIBLE);
            tampil_komen_level2.setVisibility(View.GONE);
        }
        private void hideLevel2(){
            layout_komen_video_level2.setVisibility(View.GONE);
            tampil_komen_level2.setVisibility(View.VISIBLE);
        }
    }

    private void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    private void likeKomentarVideo(String id_user, String id_comment, String like, String personId, String idVideo) {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_KOMENTAR_VIDEO_LEVEL1, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl Video Like"+error.getMessage());

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", id_user);
                params.put("id_comment",id_comment);
                params.put("like",like);
                params.put("user_id_liked",personId);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "VolleyError Error KVLA: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public interface KomentarLevel1{
        public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas);
        public void balasKomentarLevel1(String idKomentarYangDibalas);
    }

    public void cekKomentarSingleVideo(){
      if(CurrentActiveVideoData.targetKomentarInduk != null) {
          this.targetKomentarInduk = CurrentActiveVideoData.targetKomentarInduk;
          CurrentActiveVideoData.targetKomentarInduk = null;
      }
      if(CurrentActiveVideoData.targetKomentarAnak != null) {
            this.targetKomentarAnak = CurrentActiveVideoData.targetKomentarAnak;
            CurrentActiveVideoData.targetKomentarAnak = null;
        }
    }
}
