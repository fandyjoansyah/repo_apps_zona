package com.zonamediagroup.zonamahasiswa.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.TampilGrafikPollingActivity;
import com.zonamediagroup.zonamahasiswa.models.Poling_Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import info.androidhive.recyclerviewsearch.R;

/**
 * Created by ravi on 16/11/17.
 */

public class Poling_Adapter extends RecyclerView.Adapter<Poling_Adapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Poling_Model> barangdataList;
    private List<Poling_Model> barangdataListFiltered;
    private ContactsAdapterListener listener;
    public String tanggalSekarang = "";



    //    Chace

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public boolean polingSudahBerakhir = false;

        public TextView tittle_poling;
        public TextView hasilPolling;
        public TextView txtBerakhirPada;
        ImageView img_poling;


        public MyViewHolder(View view) {
            super(view);

//            definisi komponen

            tittle_poling = view.findViewById(R.id.tittle_poling);
            img_poling=view.findViewById(R.id.img_poling);
            hasilPolling = view.findViewById(R.id.hasil_polling);
            txtBerakhirPada = view.findViewById(R.id.txt_berakhir_pada);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(barangdataListFiltered.get(getAdapterPosition()));
                }
            });

            hasilPolling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Poling_Model poling_model = barangdataListFiltered.get(getAdapterPosition());
                    Log.d("DebugZona",poling_model.getTiitle_poling());

                    //panggil TampilGrafikPollingActivity dengan mempassing id_polling
                    Intent intent = new Intent(view.getContext(), TampilGrafikPollingActivity.class);
                    intent.putExtra("idPolling", poling_model.getId_poling());
                    intent.putExtra("titlePolling", poling_model.getTiitle_poling());
                    intent.putExtra("status_polling", polingSudahBerakhir);

                    context.startActivity(intent);
                }
            });
        }
    }


    public Poling_Adapter(Context context, List<Poling_Model> barangdataList, ContactsAdapterListener listener, String tanggalSekarang) {
        this.context = context;
        this.listener = listener;
        this.barangdataList = barangdataList;
        this.barangdataListFiltered = barangdataList;
        this.tanggalSekarang = tanggalSekarang;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_poling, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Poling_Model produk_data = barangdataListFiltered.get(position);
       // System.out.println("Potition " + (position + 1));


//        Holder

        //start bandingkan waktu
        String waktuSelesaiPolling = produk_data.getPoling_end();
        String waktuSekarangPolling = produk_data.getWaktu_sekarang();

        try {
            Date waktuSelesai = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(waktuSelesaiPolling);
            Date waktuSekarang = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(waktuSekarangPolling);
          if (waktuSekarang.compareTo(waktuSelesai) > 0) {
                //waktu sekarang lebih dari waktu selesai
                Log.d("DebugPolling","didalam if ");
                final int paddingBottom = holder.hasilPolling.getPaddingBottom(), paddingLeft = holder.hasilPolling.getPaddingLeft();
                final int paddingRight = holder.hasilPolling.getPaddingRight(), paddingTop = holder.hasilPolling.getPaddingTop();
                holder.txtBerakhirPada.setText("Polling telah berakhir");
                holder.hasilPolling.setBackground(context.getResources().getDrawable(R.drawable.rounded_lihat_hasil_voting_biru));
                holder.hasilPolling.setPadding(paddingLeft,paddingTop,paddingRight,paddingBottom);
                holder.hasilPolling.setTextColor(Color.parseColor("#FFFFFF"));
                holder.polingSudahBerakhir = true;
            } else if (waktuSekarang.compareTo(waktuSelesai) <= 0) {
                //waktu sekarang sebelum atau sama dengan waktuSelesai
                Log.d("DebugPolling","didalam else if ");
                holder.txtBerakhirPada.setText("Berakhir pada "+funcWaktuFormatter(produk_data.getPoling_end()));
                holder.polingSudahBerakhir = false;
            }
            Log.d("19 september 2021","value boolean : "+holder.polingSudahBerakhir);

        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("DebugPolling","catch"+e.getMessage());
        }

        //end bandingkan waktu
        holder.tittle_poling.setText(""+produk_data.getTiitle_poling());
        Log.d("DebugPolling","dari luar "+produk_data.getPoling_end());
        Glide.with(context)
                .load(produk_data.getImg_poling())
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.img_poling);

//        Holder

    }

    @Override
    public int getItemCount() {
        return barangdataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    barangdataListFiltered = barangdataList;
                } else {
                    List<Poling_Model> filteredList = new ArrayList<>();
                    for (Poling_Model row : barangdataList) {

//                       Filter list receler view
                        if (row.getTiitle_poling().toLowerCase().contains(charString.toLowerCase()) || row.getTiitle_poling().contains(charSequence) || row.getTiitle_poling().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    barangdataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = barangdataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                barangdataListFiltered = (ArrayList<Poling_Model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        /**
         * @param transaksihariinidataList
         */
        void onContactSelected(Poling_Model transaksihariinidataList);
    }

    //method untuk mendapatkan tanggal dan bulan
    private String funcWaktuFormatter(String waktuDariDb)
    {
        String tanggalBulan = "";
        //rubah format tanggal ke dd MMMM YYYYY
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("d MMMM");
            Date date = format1.parse(waktuDariDb);
            Date currentTime = Calendar.getInstance().getTime();

            //Toast.makeText(context.getApplicationContext(), currentTime.toString(),Toast.LENGTH_SHORT).show();
            //pakai format baru yaitu bulan ditampilkan namanya
            tanggalBulan = format2.format(date);
        }
        catch(Exception e)
        {
            Log.e("ErrorZona","Terjadi Error "+e.getMessage());
        }
        return tanggalBulan;
    }

    public interface InterHasilPollingListener
    {
        void onHasilPollingClick(Poling_Model detailPolling);
    }
}
