package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Komentar_Adapter;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.ApiClient;
import com.zonamediagroup.zonamahasiswa.komponen_retrofit.InterfaceApi;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Komentar extends AppCompatActivity implements Komentar_Adapter.ContactsAdapterListener {

  String judul, id_post, total_komentar_s;

  ImageView kirim_komentar;

  TextView tittle_komentar, total_komentar, isikomentar;

  RecyclerView receler_komen;
  private List<Komentar_Model> receler_komenModels;
  private Komentar_Adapter receler_komenAdapter;
  private String URL_home = Server.URL_REAL + "Get_komentar_v2";

  private String URL_komentar = Server.URL_REAL + "Post_komentar";

  private String URL_hapus_komentar = Server.URL_REAL + "Delete_komentar";

  private String URL_LIKE_UNLIKE_KOMENTAR = Server.URL_REAL + "Menyukai_komentar";


  private static final String TAG = Home.class.getSimpleName();
  static String tag_json_obj = "json_obj_req";

  String id_user_sv;
  String komenx;



  // loading
  ImageView image_animasi_loading;
  LinearLayout loding_layar;

  // eror layar
  LinearLayout eror_layar;
  TextView coba_lagi;
  int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

  ImageView search;
  ImageView saveok;

  ImageView back;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.page_komentar_dua);

    id_user_sv = SharedPrefManagerZona.getInstance(this).getid_user();

    terimadata();

    kirim_komentar = findViewById(R.id.kirim_komentar);
    isikomentar = findViewById(R.id.isikomentar);


    tittle_komentar = findViewById(R.id.tittle_komentar);
    total_komentar = findViewById(R.id.total_komentar);

    total_komentar.setText(total_komentar_s);
    tittle_komentar.setText(judul);

    back = findViewById(R.id.back);

    // kirim komentar
    kirim_komentar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        komenx = isikomentar.getText().toString();

        if (komenx.trim().equals("")) {
          //Toast.makeText(Komentar.this, "Isi Komentar", Toast.LENGTH_SHORT).show();
        } else {
          // post ke server
          // id_post id_user komentar
          loading_show();

          isikomentar.onEditorAction(EditorInfo.IME_ACTION_DONE);
// Call API
          post_komentar(id_user_sv, id_post, komenx);


        }
      }
    });


    // Komentar
    receler_komen = findViewById(R.id.receler_satu_komentar);
    receler_komenModels = new ArrayList<>();
    receler_komenAdapter = new Komentar_Adapter(Komentar.this, receler_komenModels, this, id_user_sv, this);
    RecyclerView.LayoutManager mLayoutManagerkategoriv = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
    receler_komen.setLayoutManager(mLayoutManagerkategoriv);
    receler_komen.setItemAnimator(new DefaultItemAnimator());
    receler_komen.setAdapter(receler_komenAdapter);


//        Loading
    loding_layar = findViewById(R.id.loding_layar);
    image_animasi_loading = findViewById(R.id.image_animasi_loading);

//        eror
    eror_layar = findViewById(R.id.eror_layar);
    coba_lagi = findViewById(R.id.coba_lagi);

    coba_lagi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        eror_dismiss();
        loading_show();

        if (eror_param == 1) {
          komentar(id_post);
          //komentarAdvanced(id_post);
        } else if (eror_param == 2) {
// Call API
          post_komentar(id_user_sv, id_post, komenx);
        }


      }
    });

    search = findViewById(R.id.search);
    saveok = findViewById(R.id.saveok);

    // action
    saveok.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Komentar.this, Save.class);
        startActivity(intent);
      }
    });

    search.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Komentar.this, Search.class);
        startActivity(intent);
      }
    });

    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        String a = total_komentar.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", a);
        setResult(100, returnIntent);
        finish();
      }
    });

    eror_dismiss();
    loading_show();

//        Call API
    komentar(id_post);
    //komentarAdvanced(id_post);
  }


  private void komentarAdvanced(String id_post)
  {
      Log.d("debug-retrofit","id user: "+id_user_sv);
      Log.d("debug-retrofit","id_post: "+id_post);

    InterfaceApi api = ApiClient.getClient().create(InterfaceApi.class);
    Call <retrofit2.Response<String>> call = api.getKomentar("qwerty",id_post,id_user_sv);
    call.enqueue(new Callback <retrofit2.Response<String>>() {
      @Override
      public void onResponse(Call <retrofit2.Response<String>> call, retrofit2.Response <retrofit2.Response<String>> response) {
        //start
        Log.d("RetrofitX", "onResponseX: ");
        try {
//          JSONObject hasilRespon = new JSONObject(response.body());
          Toast.makeText(getApplicationContext(), "res: " + response.raw().toString(), Toast.LENGTH_SHORT).show();
        }
        catch(Exception e)
        {
          Toast.makeText(getApplicationContext(), "gagal: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        //end
      }

      @Override
      public void onFailure(Call <retrofit2.Response<String>> call, Throwable t) {
          Toast.makeText(getApplicationContext(), "onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
      }
    });
  }


  private void komentar(String id_post) {

Log.d("komentar7nov","k7n: "+id_post);
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data REKOMENDASI");

          if (jObj.getString("status").equals("true")) {

            JSONArray dataArray = jObj.getJSONArray("item_terbaru");


            List<Komentar_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Model>>() {
            }.getType());

            total_komentar.setText(String.valueOf(items.size()));

            // adding contacts to contacts list
            receler_komenModels.clear();
            receler_komenModels.addAll(items);
            // refreshing recycler view
            receler_komenAdapter.notifyDataSetChanged();
            loading_dismiss();


          } else if (jObj.getString("status").equals("false")) {


            loading_dismiss();
          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          eror_param = 1;
          eror_show();

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());

        eror_param = 1;
        eror_show();

      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_post", id_post);
        params.put("id_user", id_user_sv);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {


        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  public void terimadata() {
    Bundle data = getIntent().getExtras();
    id_post = data.getString("id_post");
    judul = data.getString("judul");
    total_komentar_s = data.getString("total_komentar");
  }


  @Override
  public void onContactSelected(Komentar_Model komen) {

  }


  @Override
  public void hapusDiKlik(String idKomentar, String idUserSekarang) {
    //kurangi jumlah total komentar di tampilan textview
    int jumlahKomentar = Integer.valueOf(total_komentar.getText().toString());
    jumlahKomentar--;
    String jumlahKomentarAkhir = String.valueOf(jumlahKomentar);
    total_komentar.setText(jumlahKomentarAkhir);


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_komentar, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {
      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());
        eror_param = 2;
        eror_show();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", idUserSekarang);
        params.put("id_komentar", idKomentar);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {


        eror_param = 2;
        eror_show();

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);


  }

  @Override
  public void likeKomentarOrUnlikeKomentar(String idKomentar, String idUserSekarang) {
    Log.d("debugKomentar","idKomentar = "+idKomentar);
    Log.d("debugKomentar","idUserSekarang = "+idUserSekarang);

    StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_UNLIKE_KOMENTAR, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {
      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());
        eror_param = 2;
        eror_show();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("person_id", idUserSekarang);
        params.put("id_komentar", idKomentar);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {


        eror_param = 2;
        eror_show();

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);


  }


  private void post_komentar(String id_user, String id_post, String komentar) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_komentar, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println(jObj.getString("status").equals("true"));

          if (jObj.getString("status").equals("true")) {

            isikomentar.setText("");
            komentar(id_post);
            //komentarAdvanced(id_post);


          } else if (jObj.getString("status").equals("false")) {

            loading_dismiss();

          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error
          eror_param = 2;
          eror_show();

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());

        eror_param = 2;
        eror_show();

      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        params.put("komentar", komentar);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {


        eror_param = 2;
        eror_show();

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }


  // loading dan eror
  public void loading_show() {
    loding_layar.setVisibility(View.VISIBLE);
  }

  public void loading_dismiss() {

    loding_layar.setVisibility(View.GONE);
  }

  public void eror_show() {
    eror_layar.setVisibility(View.VISIBLE);
  }

  public void eror_dismiss() {

    eror_layar.setVisibility(View.GONE);
  }


  @Override
  public void onBackPressed() {
    Log.d("CDA", "onBackPressed Called");

    String a = total_komentar.getText().toString();

    Intent returnIntent = new Intent();
    returnIntent.putExtra("result", a);
    setResult(100, returnIntent);
    finish();
  }

}