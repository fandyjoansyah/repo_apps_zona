package com.zonamediagroup.zonamahasiswa;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;

public class Splesccreen_apps extends AppCompatActivity {


    private int waktu_loading = 1500; //4000=4 detik

    //    loading
    ProgressBar progressBar;
    Sprite doubleBounce;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splesccreen2);

        //        Progres bar
        progressBar = (ProgressBar) findViewById(R.id.spin_kit_buku);
        doubleBounce = new ThreeBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);


// auto
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //setelah loading maka akan langsung berpindah ke home activity
                Boolean a = SharedPrefManagerZona.getInstance(getApplicationContext()).getsesion();

                if (a) {

                    jalanterus();

                } else {

                    Intent intent = new Intent(Splesccreen_apps.this, WelcomeActivity.class);
                    startActivity(intent);
                }


            }
        }, waktu_loading);


    }

    public void jalanterus() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                Intent i = new Intent(Splesccreen_apps.this, MainActivity.class);

                startActivity(i); // menghubungkan activity splashscren ke main activity dengan intent
                //jeda selesai Splashscreen
                finish();
            }
        }, 0); //3000 L = 3 detik
    }

}
