package com.zonamediagroup.zonamahasiswa;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Rekomendasi_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Rekomendasi_Model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Detail_by_id extends AppCompatActivity implements Rekomendasi_Adapter.ContactsAdapterListener {

  String id_post, post_content, humnile_post, title_post, name_categori, date_post, name_auth, gambar_author, link, id_kategori;

  private String URL_dapatkan_like_dan_totalLike_dan_totalKomentar = Server.URL_REAL + "Get_like_totalLike_totalComment";
  ImageView img_thumnile, img_author;

  int allLike, allComment;

  TextView judul_berita, judul_kategori, tanggal_berita, nama_author;
  WebView conten2;

  private String URL_set_unset_like = Server.URL_REAL + "Menyukai_artikel";

  ImageView imageLike;

  TextView labelTulisanSimpan;

  ImageView iconSimpan;

  private static final String URL_cek_save_di_server = Server.URL_REAL + "Cek_simpan_by_idpost";

  String adaSimpan = "0"; //respon dari server 1 = ada like. 0 = tidak ada like


  private String URL_hapus_save = Server.URL_REAL + "Delete_westlist";


  //3 menu dibawah (komentar, like, share)
  LinearLayout bottomKomentar;
  LinearLayout bottomLike;
  LinearLayout bottomShare;

  TextView totalLike,totalComment;

  String id_user_sv;

  Intent intentDariLuar;

  private static final String TAG = Home.class.getSimpleName();
  static String tag_json_obj = "json_obj_req";
  private String URL_post_view = Server.URL_REAL + "Rekomendasi_post_by_id";

  private String URL_total_komentar = Server.URL_REAL + "Total_komentar";

  RecyclerView receler_home;
  private List<Rekomendasi_Model> receler_homeModels;
  private Rekomendasi_Adapter receler_homeAdapter;
  private String URL_home = Server.URL_REAL + "Rekomendasi_post_by_id_dengan_waktu";

  private String URL_home_save = Server.URL_REAL + "Save_like";


  LinearLayout detail_komentar;
  TextView jumlah_komentar;
  String total_komentar;

  //var untuk menghandle baca juga
  boolean bacaJuga = false;


  // loading
  ImageView image_animasi_loading;
  LinearLayout loding_layar;

  // eror layar
  LinearLayout eror_layar;
  TextView coba_lagi;
  int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

  ImageView search;
  ImageView saveok;
  String slug="";

  LinearLayout btnSimpan;

  ImageView back;

  LinearLayout ly_share, ly_save;
  ImageView img_simpan_detail;

  static final int LAUNCH_SECOND_ACTIVITY = 1;

  boolean disimpan = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.page_detail);
    //intent dari luar
    intentDariLuar = getIntent();

    terimadata();




    // komponen set
    conten2 = findViewById(R.id.conten2);
    img_thumnile = findViewById(R.id.img_thumnile);
    img_author = findViewById(R.id.img_author);
    judul_berita = findViewById(R.id.judul_berita);
    judul_kategori = findViewById(R.id.judul_kategori);
    tanggal_berita = findViewById(R.id.tanggal_berita);
    nama_author = findViewById(R.id.nama_author);
    labelTulisanSimpan = findViewById(R.id.label_tulisan_simpan);

    //bottom navigation view manual memakai linear layout
    bottomKomentar = findViewById(R.id.custom_navbottom_komentar);
    bottomLike = findViewById(R.id.custom_navbottom_like);
    bottomShare = findViewById(R.id.custom_navbottom_share);
    imageLike = findViewById(R.id.img_like);
    btnSimpan = findViewById(R.id.btn_simpan);

    //ads
    AdView mAdView =  findViewById(R.id.adview_detail);
    AdRequest adRequest = new AdRequest.Builder().build();

    mAdView.loadAd(adRequest);
    //btnSimpan.setVisibility(View.GONE);

    totalLike = findViewById(R.id.total_like_rounded);
    totalComment = findViewById(R.id.total_komentar_rounded);

    // Komentar
    detail_komentar = findViewById(R.id.detail_komentar);
    jumlah_komentar = findViewById(R.id.jumlah_komentar);

    iconSimpan = findViewById(R.id.icon_simpan);



    // set Komponen

    // post view
    id_user_sv = SharedPrefManagerZona.getInstance(this).getid_user();

    funcDapatkanLike(id_user_sv,id_post);

    // rekomendasi
    receler_home = findViewById(R.id.receler_satu_rekomendasi);
    receler_homeModels = new ArrayList<>();
    receler_homeAdapter = new Rekomendasi_Adapter(Detail_by_id.this, receler_homeModels, this);
    RecyclerView.LayoutManager mLayoutManagerkategoriv = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
    receler_home.setLayoutManager(mLayoutManagerkategoriv);
    receler_home.setItemAnimator(new DefaultItemAnimator());
    receler_home.setAdapter(receler_homeAdapter);


    detail_komentar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {


        Intent intent = new Intent(Detail_by_id.this, Komentar.class);

        intent.putExtra("judul", title_post);
        intent.putExtra("id_post", id_post);
        intent.putExtra("total_komentar", total_komentar);
        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);

      }
    });

    System.out.println("INI ID POST " + id_post);
//        total_komentar(id_post);

    back = findViewById(R.id.back);
    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        finish();
      }
    });


    ly_share = findViewById(R.id.ly_share);
    ly_share.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        // share
        try {
          Intent shareIntent = new Intent(Intent.ACTION_SEND);
          shareIntent.setType("text/plain");
          shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Zona Mahasiswa");
          String shareMessage = link + "\n\n" + title_post + BuildConfig.APPLICATION_ID + "\n\n";
          shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
          startActivity(Intent.createChooser(shareIntent, "Share"));
        } catch (Exception e) {
          //e.toString();
        }
      }
    });

    img_simpan_detail=findViewById(R.id.img_simpan_detail);
    ly_save=findViewById(R.id.ly_save);
    ly_save.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        // save ke server
        img_simpan_detail.setImageResource(R.drawable.ic_simpan_ajus_color);
        save_like(id_user_sv, id_post);
      }
    });


//        Loading
    loding_layar = findViewById(R.id.loding_layar);
    image_animasi_loading = findViewById(R.id.image_animasi_loading);

//        eror
    eror_layar = findViewById(R.id.eror_layar);
    coba_lagi = findViewById(R.id.coba_lagi);

    coba_lagi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        eror_dismiss();
        loading_show();
// Call API
        //Segera hapus
        Log.d("important_log_sub","rekom1");
        rekomendasi(id_post, id_user_sv);

      }
    });

    search = findViewById(R.id.search);
    saveok = findViewById(R.id.saveok);

    // action
    saveok.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Detail_by_id.this, Save.class);

        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
      }
    });

    search.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Detail_by_id.this, Search.class);

        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
      }
    });

    eror_dismiss();
    loading_show();

// Call API
    Post_view(id_user_sv, id_post);

    if(bacaJuga == true)
    {
      //jika merupakan baca juga, maka get idpost dulu lalu tampilkan
      getIdPostBySlugAndRunRekomendasi(slug);
    }
    //start temp coding 25 april. use if success
    else {
      cekSaveByIdPost(id_user_sv, id_post);
    }
    //end temp coding 25 april. use if success
// Call API


    //start bagian code onClickListener custom buttomNavigationView(by Layout)
    //onclick ketiga menu bawah (komentar, like, share)
    bottomKomentar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        //komentar disini
        Intent intent = new Intent(Detail_by_id.this, Komentar.class);

        intent.putExtra("judul", title_post);
        intent.putExtra("id_post", id_post);
        intent.putExtra("total_komentar", total_komentar);
        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
      }
    });

    bottomLike.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        funcSetOrUnsetLike(id_user_sv,id_post);
      //  if (imageLike.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.hati_36px).getConstantState()) {
       if (imageLike.getDrawable().getConstantState().equals(imageLike.getContext().getDrawable(R.drawable.hati_36px).getConstantState()))
       {
          imageLike.setImageDrawable(getDrawable(R.drawable.hati_nyala_36px));
          //jika ada like baru. beri efek like nambah
          allLike = allLike+1;
          //cek apakah like sudah 99? apabila 99 dan ketambahan like maka menjadi 99+
          if(allLike > 99){
            totalLike.setText("99+");
            totalLike.setVisibility(View.VISIBLE);
          }
          else if(allLike == 1){
            totalLike.setVisibility(View.VISIBLE);
            totalLike.setText(String.valueOf(allLike));
          }
          else{
            totalLike.setText(String.valueOf(allLike));
            totalLike.setVisibility(View.VISIBLE);
          }
          //totalLike.setText(String.valueOf(allLike));
        }
       //else if(imageLike.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.hati_nyala_36px).getConstantState())
        else if (imageLike.getDrawable().getConstantState().equals(imageLike.getContext().getDrawable(R.drawable.hati_nyala_36px).getConstantState()))
        {
          imageLike.setImageDrawable(getDrawable(R.drawable.hati_36px));
          //jika ada unlike baru. beri efek like berkurang
          allLike = allLike-1;
          //cek apakah unlike dari user menyebabkan 0. apabila 0 sembunyikan textview rounded like
          if(allLike < 1){
            totalLike.setVisibility(View.INVISIBLE);
          }
          else if(allLike > 99)
          {

          }
          else{
            totalLike.setText(String.valueOf(allLike));
          }
          //totalLike.setText(String.valueOf(allLike));
        }
      }
    });

    bottomShare.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        //share disini
        try {
          Intent shareIntent = new Intent(Intent.ACTION_SEND);
          shareIntent.setType("text/plain");
          shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Zona Mahasiswa");
          String linkBagi = "";
          if(link.contains("https://zonamahasiswa.id")){
            linkBagi = link;
          }
          else
          {
            linkBagi = "https://zonamahasiswa.id/"+link;
          }
          String shareMessage = title_post + "\n\n"+ linkBagi;
          shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
          startActivity(Intent.createChooser(shareIntent, "Share"));
        } catch (Exception e) {
          //e.toString();
        }
      }
    });
    //end bagian code onClickListener custom buttomNavigationView(by Layout)

    btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (disimpan == false)
        {
         btnFormatAdaSimpan();
        }
        else
        {
         btnFormatTidakAdaSimpan();
        }

      }
    });
    funcCekSave();
  }

  private void getIdPostBySlugAndRunRekomendasi(String slug) {
    String URL_POST_BY_SLUG = Server.URL_REAL+"Get_id_post_by_slug_native?TOKEN=qwerty&slug=";
    StringRequest strReq = new StringRequest(Request.Method.GET, URL_POST_BY_SLUG+slug, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          JSONArray dataArray = jObj.getJSONArray("id_post");
          id_post = dataArray.getJSONObject(0).getString("id");
          Log.d("24 april 2022","id_post oyisiapmantap "+id_post);
          //setelah idPost didapat, jalankan rekomendasi()
          //Segera hapus
          Log.d("important_log_sub","rekom3");
          rekomendasi(id_post,id_user_sv);
        } catch (Exception e) {
          // JSON error
          //loading_dismiss();

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());
          //loading_dismiss ();

//                dismissdialog();
      }
    }) {




    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    //loading_dismiss();
  }

  public void terimadata() {
    Bundle data = intentDariLuar.getExtras();
    if(intentDariLuar.hasExtra("SLUG_BACA_JUGA"))
    {
       bacaJuga = true;
       slug = data.getString("SLUG_BACA_JUGA");
    }
    else
    {
      bacaJuga = false;
    }
    id_post = data.getString("id_post");

  }



  private void Post_view(String id_user, String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_post_view, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data Global");


          if (jObj.getString("status").equals("true")) {

            System.out.println("SAVE VIEW SUKSES");


          } else if (jObj.getString("status").equals("false")) {

            System.out.println("SAVE VIEW SUKSES");

          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());



      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }


  private void rekomendasi(String id_post, String id_user) {
    //SEGERA HAPUS
    Log.d("15oktober","Checkpoint rekomendasi. id_post = "+id_post);
    Log.d("17september","id post : "+id_post);
    Log.d("17september","id user : "+id_user);

    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {
        Log.e("fandy_tracing_error_25_april","onResponse dbi kepanggil");
        //SEGERA HAPUS
        Log.d("15oktober","onResponse rekom");
        try {
          Log.e("fandy_tracing_error_25_april","try dbi kepanggil");

          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data REKOMENDASI");

          //SEGERA HAPUS
          Log.d("15oktober","before stat true");
          if (jObj.getString("status").equals("true")) {
            //SEGERA HAPUS
            Log.d("15oktober","after stat true");
            // get data Post index 0
            JSONArray data_post = jObj.getJSONArray("data_post");
            JSONObject jsonObject1 = data_post.getJSONObject(0);

            post_content = jsonObject1.optString("post_content");
            humnile_post = jsonObject1.optString("meta_value");
           // Log.d("7_oktober","thumbnile post: "+humnile_post);
            title_post = jsonObject1.optString("post_title");
            //title_post = "ini judul";
            name_categori = jsonObject1.optString("name");
            date_post = jsonObject1.optString("post_date");
            name_auth = jsonObject1.optString("user_login");
            gambar_author = jsonObject1.optString("gambar_user_wp");
            link =jsonObject1.optString("guid");
            String id_author = jsonObject1.optString("post_author");
            id_kategori = jsonObject1.optString("term_id");
            adaSimpan = jsonObject1.optString("like");
            //segera hapus
            Log.d("18_oktober","title post: "+title_post);
            Log.d("18_oktober","adaSimpan: "+adaSimpan);

            //keperluan debug. jangan hapus, komen saja bila tidak perlu
            Log.d("important_log","id_usernya: "+id_user);
            Log.d("important_log","id_postnya: "+id_post);
            funcCekSave();


            //set kabeh
            //nama_author.setText("author");
            nama_author.setText(name_auth);
            judul_kategori.setText(name_categori);
            tanggal_berita.setText(date_post);
            judul_berita.setText(title_post);

            Glide.with(getApplicationContext())
                    .load(humnile_post)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(img_thumnile);


            Glide.with(getApplicationContext())
                    .load(gambar_author)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(img_author);

            conten2.loadData(funcOlahData(post_content), "text/html; charset=utf-8", "UTF-8");
            conten2.setWebViewClient(new WebViewClient(){
                                       @Override
                                       public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                                         if(urlNewString.contains("zonamahasiswa.id")){
                                           String slug = getSlugByUrl(urlNewString);
                                           Intent intent = new Intent(Detail_by_id.this, Detail_by_id.class);
                                           intent.putExtra("SLUG_BACA_JUGA", slug);
                                           intent.putExtra("id_post", id_post);
                                           finish();
                                           startActivity(intent);
                                         }
                                         else
                                         {
                                           Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(urlNewString));
                                           startActivity(i);
                                         }
                                         return true;
                                       }
            });
            // item terbaru

            loading_dismiss();
            //SEGERA HAPUS
            Log.d("15oktober","Checkpoint oyi");

            JSONArray dataArray = jObj.getJSONArray("item_terbaru");
//                        dataArray.remove(0); // remove index 0

            List<Rekomendasi_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Rekomendasi_Model>>() {
            }.getType());

            receler_homeModels.clear();
            receler_homeModels.addAll(items);
            Log.d("17september","isi title : "+receler_homeModels.get(0).getPost_title());
            Log.d("17september","isi likeX : "+receler_homeModels.get(0).getLike());

            receler_homeAdapter.notifyDataSetChanged();
            //SEGERA HAPUS
            Log.d("15oktober","judul: "+receler_homeModels.get(1).getLike());

            // total komentar
            total_komentar = jObj.getString("total_komentar");
            jumlah_komentar.setText(total_komentar);


            // get data

          }


          // Check for error node in json

        } catch (JSONException e) {
          Log.e("fandy_tracing_error_25_april","catch dbi kepanggi: "+e.getMessage());

          // JSON error
          Log.e("fandy tracing error 25 april 2022","fte line 715");
          eror_show();

          e.printStackTrace();
        }
        //loading_dismiss();
      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e("fandy_tracing_error_25_april","onErrorResponse dbi kepanggil karena "+error.getMessage());

        //SEGERA HAPUS
        Log.d("15oktober","Checkpoint error karena: "+error.getMessage());
        Log.e(TAG, "Register Error: " + error.getMessage());
        loading_dismiss();
        Log.e("fandy tracing error 25 april 2022","fte line 730");
        eror_show();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {
        Log.e("fandy tracing error 25 april 2022","fte line 763");
        eror_show();
        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  private void total_komentar(String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_total_komentar, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {
//                Log.e(TAG, "Produk Response: " + response.toString());


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data TOTAL KOMENTAR");


          if (jObj.getString("status").equals("true")) {


            total_komentar = jObj.getString("item_terbaru");
            jumlah_komentar.setText(total_komentar);

          } else if (jObj.getString("status").equals("false")) {
            jumlah_komentar.setText("0");

          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());


//                dismissdialog();
      }
    }) {


//            reques

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_post", id_post);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  @Override
  public void onContactSelected(Rekomendasi_Model itemsatu) {

    Intent intent = new Intent(Detail_by_id.this, Detail.class);
    intent.putExtra("id_post", itemsatu.getID());
    intent.putExtra("post_content", itemsatu.getPost_content());
    intent.putExtra("humnile_post", itemsatu.getMeta_value());
    intent.putExtra("title_post", itemsatu.getPost_title());
    intent.putExtra("name_categori", itemsatu.getName());
    intent.putExtra("date_post", itemsatu.getPost_date());
    intent.putExtra("name_auth", itemsatu.getUser_login());
    intent.putExtra("img_author", itemsatu.getGambar_user_wp());
    intent.putExtra("link", itemsatu.getGuid());
    intent.putExtra("id_kategori", itemsatu.getTerm_id());
    intent.putExtra("save", itemsatu.getLike());
    intent.putExtra("id_author", itemsatu.getPost_author());

    Log.d("17september","get like dari onContactSelectedListener"+itemsatu.getLike());

    startActivity(intent);

  }

  @Override
  public void Save(Rekomendasi_Model save, int position) {
    String id_post_sv = save.getID();
    save_like(id_user_sv, id_post_sv);
    receler_homeModels.get(position).setLike("1");
  }

  @Override
  public void unSave(Rekomendasi_Model rekomendasi_model, int position) {
    String id_post_sv = rekomendasi_model.getID();
    delete_simpan(id_user_sv, id_post_sv);
    receler_homeModels.get(position).setLike("0");
  }


  // loading dan eror
  public void loading_show() {
    loding_layar.setVisibility(View.VISIBLE);
  }

  public void loading_dismiss() {

    loding_layar.setVisibility(View.GONE);
  }

  public void eror_show() {
    eror_layar.setVisibility(View.VISIBLE);
  }

  public void eror_dismiss() {

    eror_layar.setVisibility(View.GONE);
  }

  // result
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == LAUNCH_SECOND_ACTIVITY) {
      if (resultCode == 100) {
// retrun value dari komentar.class
        String result = data.getStringExtra("result");
        System.out.println("RESUL " + result);

        //jika total komentar dari komentar.class = 0, maka setText ""
        if(result.equals("0"))
        {
          jumlah_komentar.setText("");
          totalComment.setText("");
        }
        else
        {
          jumlah_komentar.setText(result);
          totalComment.setText(result);
        }
      }
      if (resultCode == Activity.RESULT_OK) {
        //SEGERA HAPUS
        Log.d("15oktober","Checkpoint 1 id_user: "+id_user_sv+", idPost: "+id_post);
        cekSaveByIdPost(id_user_sv,id_post);
      }

    }

  } //onActivityResult


  private void save_like(String id_user, String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data SAVE LIKE");


          if (jObj.getString("status").equals("true")) {


          } else if (jObj.getString("status").equals("false")) {


          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());


//                dismissdialog();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }





  //fungsi ini untuk mendapatkan apakah ada like dari pengguna yang sedang login dan untuk post yang sedang dibuka
  private void funcDapatkanLike(String id_user, String id_post) {

    Log.d("NotifikasiDapatkanLike","id_user = "+id_user);
    Log.d("NotifikasiDapatkanLike","id_post = "+id_post);

    StringRequest strReq = new StringRequest(Request.Method.POST, URL_dapatkan_like_dan_totalLike_dan_totalKomentar, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          Log.d("DebugLike","id_user : "+id_user);
          Log.d("DebugLike","id_post : "+id_post);
          int like = jObj.getJSONObject("data").getInt("resultLike"); //0 tidak ada like. 1 ada like
          allLike = jObj.getJSONObject("data").getInt("resultAllLike");
          allComment = jObj.getJSONObject("data").getInt("resultAllComment");
          if(like == 1) //jika ada like
          {
            imageLike.setImageDrawable(getDrawable(R.drawable.hati_nyala_36px));
          }
          //totalLike.setText(String.valueOf(allLike));
          //totalComment.setText(String.valueOf(allComment));
          // Check for error node in json

          //start cek comment awal load
          if(allComment > 0 && allComment < 99){ //jika all like berada diantara 0 dan 99
            totalComment.setText(String.valueOf(allComment));
          }
          else if(allComment > 99)
          {
            totalComment.setText("99+");
          }

          //end cek comment awal load


          //start cek comment awal load
          if(allComment > 0 && allComment < 99){ //jika all like berada diantara 0 dan 99
            totalComment.setText(String.valueOf(allComment));
          }
          else if(allComment > 99)
          {
            totalComment.setText("99+");
          }

          //end cek comment awal load


        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());


//                dismissdialog();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  //fungsi untuk cek ada like atau tidak untuk button simpan di tengah artikel
  public void funcCekSave()
  {
    Log.d("debug_24_september","funcCekSave()");
    if(adaSimpan.equals("1"))
    {
      Log.d("debug_24_september","funcCekSave() if");
      btnSimpan.setBackground(ContextCompat.getDrawable(Detail_by_id.this,R.drawable.background_button_simpan_sudah_disimpan));
      labelTulisanSimpan.setTextColor(ContextCompat.getColor(Detail_by_id.this,R.color.black));
      labelTulisanSimpan.setText("Disimpan");
      disimpan = true;
      iconSimpan.setImageResource(R.drawable.ic_simpan_hitam);
    }
  }


  private String funcOlahData(String post_content)
  {
    //hapus link href ke zonamahasiswa.id
    Log.d("DebugZona","below = replace");
    String hapusHref = post_content.replaceAll("<a href=\"http://zonamahasiswa.id/\">zonamahasiswa.id</a>","<strong><span class='link_tidak_aktif'>Zona Mahasiswa</span></strong>");
    String hapusHref2 = hapusHref.replaceAll("<a href=\"http://zonamahasiswa.id\">zonamahasiswa.id</a>","<strong><span class='link_tidak_aktif'>Zona Mahasiswa</span></strong>");
    String hapusHref3 = hapusHref2.replaceAll("<a href=\"http://zonamahasiswa.id\">zonamahasiswa.id </a>","<strong><span class='link_tidak_aktif'>Zona Mahasiswa</span></strong>");

    //hapus comentar tag wordpress
    String hapusKomentarWp = hapusHref3.replaceAll("(?s)<!--.*?-->","");



    //tambahkan class wp-block-quote ke tag blockquote
    //tambahkan style untuk respon html artikel
    String tambahanCss = "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
            "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
            "<link href=\"https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap\" rel=\"stylesheet\">\n" +
            "\n" +
            "\n" +
            "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
            "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
            "<link href=\"https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap\" rel=\"stylesheet\"> \n" +
            "\n" +
            "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
            "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
            "<link href=\"https://fonts.googleapis.com/css2?family=Noto+Sans&family=Noto+Sans+KR:wght@900&display=swap\" rel=\"stylesheet\"> \n" +
            "<style>\n" +
            "p\n" +
            "{\n" +
            "font-family: 'Noto Sans', sans-serif;\n" +
            "text-align: left;\n" +
            "line-height:1.6;\n" +
            "font-size:15px;\n" +
            "}\n" +
            "a{\n" +
            "    text-decoration: none;\n" +
            "}\n" +
            "blockquote{\n" +
            "    font-size: 14px;\n" +
            "    border-left:2px outset blue;\n" +
            "    padding: 1px 10px 1px 10px;\n" +
            "    margin-left:0;\n" +
            "    margin-right:0;\n" +
            "}\n" +
            "\n" +
            "\n" +
            "blockquote p\n" +
            "{\n" +
            "font-family: 'Noto Sans KR', sans-serif;\n" +
            "font-weight:800;\n" +
            "text-align:left;\n" +
            "line-height:1.3;\n" +
            "}\n" +
            "blockquote p a\n" +
            "{\n" +
            "    color:rgba(5,172,233);\n" +
            "}\n" +
            "h4\n" +
            "{\n" +
            "    font-size:17px;\n" +
            "    font-family: 'Noto Sans KR', sans-serif;\n" +
            "}\n" +
            "img\n" +
            "{\n" +
            "    width:100%;\n" +
            "    height:200px;\n" +
            "    margin-left: auto;\n" +
            "    margin-right: auto;\n" +
            "    border-radius: 15px;\n" +
            "    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.3);\n" +
            "    margin-bottom:3px;\n" +
            "}\n" +
            "figure\n" +
            "{\n" +
            "    margin-left:0px;\n" +
            "    display: block;\n" +
            "    width:100%;\n" +
            "}\n" +
            "figcaption\n" +
            "{\n" +
            "    text-align: center;\n" +
            "    margin-top: 5px;\n" +
            "    font-size: 12px;\n" +
            "    color: rgb(53, 49, 49);\n" +
            "}\n" +
            "p .link_tidak_aktif{\n" +
            "    color: rgb(46, 46, 173);\n" +
            "}\n" +
            "body{\n" +
            "    background-color:rgb(250,250,250);\n" +
            "}\n" +
            "</style>";
    hapusKomentarWp = tambahanCss+hapusKomentarWp;
    Log.d("ZonaDebug", "isi : "+hapusKomentarWp);
    return hapusKomentarWp;
  }


  //fungsi untuk like atau unlike post
  private void funcSetOrUnsetLike(String id_user, String id_post)
  {
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_set_unset_like, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          Log.d("DebugLike","id_user : "+id_user);
          Log.d("DebugLike","id_post : "+id_post);
          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());


//                dismissdialog();
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  //fungsi untuk mendapatkan slug dari url
  private String getSlugByUrl(String urlNewString) {
    //dapatkan semua karakter sesudah kataPatokan
    String kataPatokan = "zonamahasiswa.id/";
    String slug = urlNewString.substring(urlNewString.lastIndexOf(kataPatokan) + kataPatokan.length());
    // Log.d("SLUG","slug: "+slug);
    return slug;
  }





  private void delete_simpan(String id_user, String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data SAVE LIKE");


          if (jObj.getString("status").equals("true")) {


          } else if (jObj.getString("status").equals("false")) {


          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());



      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  private void add_simpan(String id_user, String id_post) {
    //keperluan debug. jangan hapus, komen saja bila tidak perlu
    //Log.d("important_log","id_user add simpan: "+id_user);
    //Log.d("important_log","id_post add simpan: "+id_post);
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data SAVE LIKE");


          if (jObj.getString("status").equals("true")) {
            //segera hapus
            Log.d("important_log_sub","status save detail_by_id true");
          } else if (jObj.getString("status").equals("false")) {
            //segera hapus
            Log.d("important_log_sub","status save detail_by_id false");
          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error
          //segera hapus
          Log.d("important_log_sub","exception save");

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());
        //segera hapus
        Log.d("important_log_sub","exception save");
      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });

    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }


  //fungsi untuk cek apakah user masih ada save atau tidak, dan merubah button save di dalam artikel. dipanggil setelah user menekan back dari list simpan ke activity ini dan user menghapus salah satu artikel di list
  private void cekSaveByIdPost(String idUser,String idPost)
  {

    loading_show();
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_cek_save_di_server, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {
        //Segera hapus
        Log.d("important_log_sub","rekom4");
          rekomendasi(idPost, idUser);
        try {
          JSONObject jObj = new JSONObject(response);
          if(jObj.getBoolean("status") == true)
          {
          }
          else
          {
            btnFormatTidakAdaSimpan();
          }


        } catch (JSONException e) {
          // JSON error

          e.printStackTrace();
        }
      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        //Segera hapus
        Log.d("important_log_sub","rekom5");
          rekomendasi(idPost, idUser);
        Log.e(TAG, "Register Error: " + error.getMessage());

      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", idUser);
        params.put("id_post", idPost);
        return params;
      }
    };


    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);

  }

  public void btnFormatAdaSimpan(){
    btnSimpan.setBackground(ContextCompat.getDrawable(Detail_by_id.this,R.drawable.background_button_simpan_sudah_disimpan));
    labelTulisanSimpan.setTextColor(ContextCompat.getColor(Detail_by_id.this,R.color.black));
    labelTulisanSimpan.setText("Disimpan");
    disimpan = true;
    iconSimpan.setImageResource(R.drawable.ic_simpan_hitam);
    Toast.makeText(Detail_by_id.this,"Artikel Disimpan",Toast.LENGTH_SHORT).show();
    add_simpan(id_user_sv,id_post);
  }

  public void btnFormatTidakAdaSimpan(){
    btnSimpan.setBackground(ContextCompat.getDrawable(Detail_by_id.this,R.drawable.background_button_simpan));
    labelTulisanSimpan.setTextColor(Color.parseColor("#F0AA9C9C"));
    labelTulisanSimpan.setText("Simpan");
    disimpan = false;
    iconSimpan.setImageResource(R.drawable.ic_simpan_abu_abu);
    delete_simpan(id_user_sv,id_post);
  }

  @Override
  protected void onResume() {
    super.onResume();
//    cekSaveByIdPost(id_user_sv,id_post);


  }
}