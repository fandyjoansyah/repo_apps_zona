package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.FadingCircle;

import org.json.JSONObject;

public class LupaPassword extends AppCompatActivity {
    TextView error_for_edt_email_reset_password, tv_keterangan_btn_request;
    EditText edt_email_reset_password;
    RelativeLayout btn_request;
    ImageView img_centang_request;
    Animation fade_in;
    ProgressBar progress_btn_request;
    private static final String URL_CHECK_GMAIL_ON_SERVER = Server.URL_DAFTAR_MASUK+"Check_gmail_on_server";
    private static final String URL_REQUEST_CHANGE_PASSWORD = Server.URL_DAFTAR_MASUK+"Request_change_password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_lupa_password);
        findViewByIdForAllComponent();
        setListenerAllComponent();
        initAnimation();
        initAndroidNetworking();
    }

    private void initAndroidNetworking() {
        AndroidNetworking.initialize(getApplicationContext());
    }

    private void aktifkanButtonLoading(){
        progress_btn_request.startAnimation(fade_in);
        progress_btn_request.setVisibility(View.VISIBLE);
        tv_keterangan_btn_request.startAnimation(fade_in);
        tv_keterangan_btn_request.setText("Mengirim tautan...");
        nonAktifkanListenerButtonKirim();
    }

    private void nonaktifkanButtonLoading(){
        progress_btn_request.startAnimation(fade_in);
        progress_btn_request.setVisibility(View.GONE);
        tv_keterangan_btn_request.startAnimation(fade_in);
        tv_keterangan_btn_request.setText("Daftar");
        aktifkanListenerButtonKirim();
    }

    private void rubahButtonMenjadiBerhasilRequest(){
        progress_btn_request.setVisibility(View.GONE);
        img_centang_request.startAnimation(fade_in);
        img_centang_request.setVisibility(View.VISIBLE);
        tv_keterangan_btn_request.startAnimation(fade_in);
        tv_keterangan_btn_request.setText("Tautan telah dikirim");
        btn_request.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#31EA4E")));
        //disable setOnClickListener
        nonAktifkanListenerButtonKirim();
    }

    private void initAnimation() {
        //init animation
        fade_in = AnimationUtils.loadAnimation(LupaPassword.this,R.anim.fade_in);

    }

    private void setListenerAllComponent() {
      aktifkanListenerButtonKirim();
    }

    private void mekanismeKirimEmailResetPassword() {
        aktifkanButtonLoading();
        hideTextViewError(error_for_edt_email_reset_password);
        if(validasiEmailResetPassword()!=true)
        {

        }
        else {
            cekApakahEmailSudahTerdaftar(edt_email_reset_password.getText().toString());
        }
    }

    private void cekApakahEmailSudahTerdaftar(String email) {

        AndroidNetworking.post(URL_CHECK_GMAIL_ON_SERVER)
                .addBodyParameter("TOKEN","qwerty")
                .addBodyParameter("gmail",email)
                .setTag("cek email ada atau tidak ada")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean sudahAdaDiserver = response.getBoolean("status");
                            if (sudahAdaDiserver) {
                                sendRequestChangePassword(email);
                            } else {
                                CustomToast.makeCustomToast(LupaPassword.this,"Email belum terdaftar",CustomToast.JENIS_TOAST_ERROR,Toast.LENGTH_SHORT,true);
                                showErrorTextView(error_for_edt_email_reset_password,"Email belum terdaftar");
                            }
                        }
                        catch(Exception e)
                        {
                            CustomToast.makeCustomToast(LupaPassword.this,"Mohon maaf terjadi kesalahan",CustomToast.JENIS_TOAST_WARNING,Toast.LENGTH_SHORT,true);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //Log.d("LupaPassword:cekApakahEmailSudahTerdaftar:onError:","LupaPassword:cekApakahEmailSudahTerdaftar:onError: "+anError.getErrorDetail());
                        CustomToast.makeCustomToast(LupaPassword.this,"Terjadi kesalahan, coba lagi nanti atau periksa koneksi internetmu",CustomToast.JENIS_TOAST_ERROR,Toast.LENGTH_SHORT,true);

                    }
                });
    }

    private void sendRequestChangePassword(String email){
        AndroidNetworking.post(URL_REQUEST_CHANGE_PASSWORD)
                .addBodyParameter("TOKEN","qwerty")
                .addBodyParameter("email",email)
                .setTag("kirim request reset password")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            nonaktifkanButtonLoading();
                            boolean berhasilRequestChange = response.getBoolean("status");
                            if (berhasilRequestChange) {
                                //berhasil request change password
                                rubahButtonMenjadiBerhasilRequest();
                                showPopUpBerhasilResetPassword();
                            } else {
                                //gagal request change password
                                CustomToast.makeCustomToast(LupaPassword.this,"Gagal mengirim tautan",CustomToast.JENIS_TOAST_ERROR,Toast.LENGTH_SHORT,true);
                            }
                        }
                        catch(Exception e)
                        {
                            nonaktifkanButtonLoading();
                            CustomToast.makeCustomToast(LupaPassword.this,"Mohon maaf terjadi kesalahan",CustomToast.JENIS_TOAST_WARNING,Toast.LENGTH_SHORT,true);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        nonaktifkanButtonLoading();
                        //Log.d("LupaPassword:cekApakahEmailSudahTerdaftar:onError:","LupaPassword:cekApakahEmailSudahTerdaftar:onError: "+anError.getErrorDetail());
                        CustomToast.makeCustomToast(LupaPassword.this,"Terjadi kesalahan, coba lagi nanti atau periksa koneksi internetmu",CustomToast.JENIS_TOAST_ERROR,Toast.LENGTH_SHORT,true);

                    }
                });
    }


    private void showPopUpBerhasilResetPassword() {
        AlertDialog.Builder dialogBuilder;
        AlertDialog dialog;
        Button btn_kembali;
        RelativeLayout relative_popup_reset_password;
        dialogBuilder = new AlertDialog.Builder(LupaPassword.this);
        final View vPopUp = getLayoutInflater().inflate(R.layout.popup_reset_password_berhasil,null);


        btn_kembali = vPopUp.findViewById(R.id.btn_kembali_reset);
        relative_popup_reset_password = vPopUp.findViewById(R.id.relative_popup_berhasil_daftar);

        dialogBuilder.setView(vPopUp);
        dialog = dialogBuilder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btn_kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
               Runnable r = new Runnable() {
                   @Override
                   public void run() {
                       onBackPressed();
                   }
               };
                new Delay().delayTask(500,r);
            }
        });

    }



    private boolean checkValidasiInputKosong(EditText edt){
        if(edt.getText().toString().trim().length() == 0)
        {
            return false;
        }
        return true;
    }

    private boolean validasiEmailResetPassword() {
        if(!checkValidasiInputKosong(edt_email_reset_password))
        {
            showErrorTextView(error_for_edt_email_reset_password,"Email tidak boleh kosong");
            return false;
        }
        else if(!checkValidasiInputEmail(edt_email_reset_password.getText().toString())){
            showErrorTextView(error_for_edt_email_reset_password,"Format penulisan email salah");
            return false;
        }
        return true;
    }

    private void aktifkanListenerButtonKirim(){
        btn_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanismeKirimEmailResetPassword();
            }
        });
    }

    private void nonAktifkanListenerButtonKirim(){
        btn_request.setOnClickListener(null);
    }

    private boolean checkValidasiInputEmail(String target){
        if(Patterns.EMAIL_ADDRESS.matcher(target).matches())
            return true;
        else
            return false;
    }

    private void showErrorTextView(TextView tv, String pesanError) {
        nonaktifkanButtonLoading();
        tv.startAnimation(fade_in);
        tv.setVisibility(View.VISIBLE);
        tv.setText(pesanError);
    }

    private void hideTextViewError(TextView tv){
        tv.setVisibility(View.GONE);
    }

    private void findViewByIdForAllComponent() {
        edt_email_reset_password = findViewById(R.id.edt_email_reset_password);

        error_for_edt_email_reset_password = findViewById(R.id.error_for_edt_email_reset_password);


        btn_request = findViewById(R.id.btn_request);
         img_centang_request = findViewById(R.id.img_centang_request);

        progress_btn_request = findViewById(R.id.progress_btn_request);
        tv_keterangan_btn_request = findViewById(R.id.tv_keterangan_btn_request);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_transition_slide_from_left,R.anim.activity_transition_slide_to_right);
    }
}