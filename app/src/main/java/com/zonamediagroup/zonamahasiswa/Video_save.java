package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Notif_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Save_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Video_Notif_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Save_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Video_save extends AppCompatActivity implements Video_Save_Adapter.viewListener {

    private static final String URL_SAVE_VIDEO = Server.URL_REAL_VIDEO+"Video_save/index_post";
    private static final String URL_SAVE_VIDEO_CREATE = Server.URL_REAL_VIDEO+"Video_save_create/index_post";

    LinearLayout loding_layar;
    ImageView icon_back_save;
    RecyclerView recycle_save;
    TextView ket_save_kosong;

    List<Video_Save_Model> dataSaveVideo;
    Video_Save_Adapter adapterSave;

    boolean adaPerubahanSave = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_video_save);

        loding_layar = findViewById(R.id.loding_layar);
        recycle_save = findViewById(R.id.recycle_save);
        ket_save_kosong = findViewById(R.id.ket_save_kosong);
        dataSaveVideo = new ArrayList<>();

        //start init reyclerview
        LinearLayoutManager layoutRecyclerSave = new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        adapterSave = new Video_Save_Adapter(getApplicationContext(), dataSaveVideo, Video_save.this);
        recycle_save.setLayoutManager(layoutRecyclerSave);
        recycle_save.setAdapter(adapterSave);
        //end init recyclerview


        icon_back_save = findViewById(R.id.icon_back_save);
        icon_back_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getSave();
    }

    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {
        loding_layar.setVisibility(View.GONE);
    }

    private void getSave() {
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_SAVE_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {

                        JSONArray dataArray = jObj.getJSONArray("data");
                        dataSaveVideo.clear();

                        List<Video_Save_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Video_Save_Model>>() {
                        }.getType());

                        dataSaveVideo.addAll(items);
                        adapterSave.notifyDataSetChanged();

                        loading_dismiss();

                        if(dataSaveVideo != null) {
                            recycle_save.setVisibility(View.VISIBLE);
                            ket_save_kosong.setVisibility(View.GONE);
                        }else{
                            recycle_save.setVisibility(View.GONE);
                            ket_save_kosong.setVisibility(View.VISIBLE);
                        }

                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();
                        recycle_save.setVisibility(View.GONE);
                        ket_save_kosong.setVisibility(View.VISIBLE);
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    e.printStackTrace();
                    Log.d(MainActivity.MYTAG,"kateg cach "+e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
                Log.d(MainActivity.MYTAG,"kateg onErrorRsponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
//                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void storeSave(Integer position, String idVideo, String save)
    {
        saveDataToArrayListUnsave(idVideo);
        int saveAngka = Integer.valueOf(save);
        int videoIdAngka = Integer.valueOf(idVideo);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_SAVE_VIDEO_CREATE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        getSave();
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();
//                        CustomToast.l(getContext(),"Gagal set like");
                    }

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
//                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                params.put("save",save);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
//                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void saveDataToArrayListUnsave(String idVideo){
        if(CurrentActiveVideoData.dataSavesDeleted == null)
            CurrentActiveVideoData.initDataVideosSavesDeleted();

        CurrentActiveVideoData.dataSavesDeleted.add(idVideo);
    }

    private void backToPreviousIntent(boolean adaPerubahan){
        if(adaPerubahan) //ada aksi hapus siman
        {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
        else //tidak ada aksi hapus simpan
            {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        backToPreviousIntent(adaPerubahanSave);
    }


}