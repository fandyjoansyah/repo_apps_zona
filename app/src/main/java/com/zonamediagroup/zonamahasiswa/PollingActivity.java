package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Poling_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.ViewPagerAdapter;
import com.zonamediagroup.zonamahasiswa.models.Data_ViewPager_Model;
import com.zonamediagroup.zonamahasiswa.models.Kategori_Model;
import com.zonamediagroup.zonamahasiswa.models.Poling_Model;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZona;
import com.zonamediagroup.zonamahasiswa.tooltip.ToolTipsZonaController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class PollingActivity extends AppCompatActivity implements Poling_Adapter.ContactsAdapterListener {
    //start init all variabel
    RecyclerView receler_poling;
    private List<Poling_Model> receler_polingModels;
    //untuk gambar viewpager
    private List<Data_ViewPager_Model> dataViewPagerModel;
    private Poling_Adapter receler_polingAdapter;
    String tanggalSekarang = "";
    final String JUMLAH_MAKSIMUM_VIEWPAGER = "3";
    TabLayout tabViewPager;

    private static final String TAG = Home.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";
    private String URL_home = Server.URL_REAL + "Get_poling/?TOKEN=qwerty&jumlahData=" + JUMLAH_MAKSIMUM_VIEWPAGER;

    //viewpager
    ViewPager mViewPager;

    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok;

    //start code untuk moving viewpager
    int currentPage = 0;
    int totalPage = 0;
    Timer timer;
    final long DELAY_MS = 400;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3900; // time in milliseconds between successive task executions.
    //end code untuk moving viewpager


    //    iklan
    //    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/5224354917";
    private static String AD_UNIT_ID = "";
    private static final long COUNTER_TIME = 10;

    private InterstitialAd interstitialAd;

    private int coinCount;

    private CountDownTimer countDownTimer;
    private boolean gameOver;
    private boolean gamePaused;

    private RewardedAd rewardedAd;

    private long timeRemaining;
    boolean isLoading;
    Kategori_Model item_dua;

    private InterstitialAd iklan;

    String id;
    private ToolTipsZona[] arrToolTipsPolling;
    private ArrayList<ToolTipsZona> listToolTipsPolling;
    SharedPreferences pref;

    //    iklan end
    //end init all variabel
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_polling_activity);
    //  start copy from init method from Polling fragment
        get_poling();
        mViewPager = findViewById(R.id.view_pager_polling);
        receler_poling = findViewById(R.id.receler_poling);
        receler_polingModels = new ArrayList<>();
        dataViewPagerModel = new ArrayList<>();
        receler_polingAdapter = new Poling_Adapter(PollingActivity.this, receler_polingModels, this, tanggalSekarang);
        LinearLayoutManager mLayoutManagerkategori;
        mLayoutManagerkategori = new LinearLayoutManager(PollingActivity.this, RecyclerView.VERTICAL, false);
        receler_poling.setLayoutManager(mLayoutManagerkategori);
        receler_poling.setItemAnimator(new DefaultItemAnimator());
        receler_poling.setAdapter(receler_polingAdapter);
        pref = getApplicationContext().getSharedPreferences("zonamhs", 0); // 0 - for private mode
        setShowTimeForShowIklanVideoOnBottomNavigationPolling();

//        Loading
        loding_layar = findViewById(R.id.loding_layar);
        image_animasi_loading = findViewById(R.id.image_animasi_loading);


//        eror
        eror_layar = findViewById(R.id.eror_layar);
        coba_lagi = findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();
// Call API
                get_poling();

            }
        });


        search = findViewById(R.id.search);
        saveok = findViewById(R.id.saveok);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PollingActivity.this, Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PollingActivity.this, Search.class);
                startActivity(intent);
            }
        });
        //loading_show();
        //start call iklan
//        loading_dismiss();
//        createInterstitial();
//        showInterstitial();


        //end call iklan

    // ende copy from init method from Polling fragment

//
    }


    private void showIklan()
    {
        //start iklan gambar
        MobileAds.initialize(PollingActivity.this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}});
        iklan = new InterstitialAd(PollingActivity.this);
        iklan.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        iklan.loadAd(new AdRequest.Builder().build());
        iklan.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {

            }
            @Override
            public void onAdLoaded() {
                iklan.show();
                get_poling();
                Log.d("iklan_grafik","successful load");
            }
            @Override
            public void onAdFailedToLoad(int errorCode) {
                get_poling();
                Log.d("iklan_grafik","failed load");
            } });

        //end iklan gambar
    }

    private void get_poling() {


        StringRequest strReq = new StringRequest(Request.Method.GET, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(jObj.getString("status"));

                    System.out.println("Data Global");


                    if (jObj.getString("status").equals("true")) {


                        JSONArray dataArray = jObj.getJSONArray("data");
                        JSONArray dataViewPagerJsonArray = jObj.getJSONArray("data_viewpager");


                        List<Poling_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Poling_Model>>() {
                        }.getType());


                        //untuk viewpager
                        List<Data_ViewPager_Model> itemsViewPager = new Gson().fromJson(dataViewPagerJsonArray.toString(), new TypeToken<List<Data_ViewPager_Model>>() {
                        }.getType());

                        receler_polingModels.clear();
                        receler_polingModels.addAll(items);


                        receler_polingAdapter.notifyDataSetChanged();

                        dataViewPagerModel.addAll(itemsViewPager);
                        totalPage = dataViewPagerModel.size();

                        ViewPagerAdapter vpAdapter = new ViewPagerAdapter(PollingActivity.this, dataViewPagerModel);
                        mViewPager.setAdapter(vpAdapter);

                        loading_dismiss();

                        //start code viewpager gerak sendiri
                        final Handler handler = new Handler();
                        final Runnable Update = new Runnable() {
                            public void run() {
                                Log.d("2_oktober", "currentPage: " + currentPage);

                                mViewPager.setCurrentItem(currentPage);
                                currentPage++;
                                if (currentPage == totalPage) {
                                    currentPage = 0;
                                }
                            }
                        };

                        timer = new Timer(); // This will create a new Thread
                        timer.schedule(new TimerTask() { // task to be scheduled
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, DELAY_MS, PERIOD_MS);
                        //end code viewpager gerak sendiri

                        //start listener viewpager

                        mViewPager.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {

                                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                                    timer.cancel();
                                }
                                return false;
                            }
                        });

                        doToolTipsPolling();
                        //end listener viewpager

                    } else if (jObj.getString("status").equals("false")) {

                        loading_dismiss();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_show();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                eror_show();
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                loading_dismiss();
                eror_show();
                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public void onContactSelected(Poling_Model poling) {

        Intent intent = new Intent(PollingActivity.this, Detail_poling.class);
        intent.putExtra("id_poling", poling.getId_poling());
        intent.putExtra("tiitle_poling", poling.getTiitle_poling());
        intent.putExtra("img_poling", poling.getImg_poling());
        intent.putExtra("poling_start", poling.getPoling_start());
        intent.putExtra("poling_end", poling.getPoling_end());
        intent.putExtra("waktu_sekarang", poling.getWaktu_sekarang());

        startActivity(intent);

    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

    public void createInterstitial() {
        interstitialAd = new InterstitialAd(PollingActivity.this);
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712"); // Ganti sesuai dengan kode interstitial ads anda
        loadInterstitial();
    }

    public void loadInterstitial() {
        AdRequest interstitialRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(interstitialRequest);
    }

    public void showInterstitial() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();

            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // not call show interstitial ad from here
                }

                @Override
                public void onAdClosed() {
                    loadInterstitial();

                    // get_poling();
                }
            });
        } else {
            loadInterstitial();
            // get_poling();
        }
    }

//    private void initToolTipsPolling()
//    {
//        ToolTipsZona tooltipsPolling1 =  new ToolTipsZona(PollingActivity.this,"Yuk vote pilihanmu sekarang sebelum waktu polling berakhir","Selesai",R.id.receler_poling,true,R.layout.tooltip_small,R.id.tv_text,R.id.btn_next, Gravity.TOP,"Polling","");
//        arrToolTipsPolling = new ToolTipsZona[]{tooltipsPolling1};
//    }
//
//    private void runToolTipsPolling()
//    {
//        new ToolTipsZonaController().setMultipleToolTips(arrToolTipsPolling);
//    }

    private void doToolTipsPolling()
    {
        if(PollingActivity.this != null) {
            if (pref.contains("pollingToolTips") == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
            {
                initToolTipsPolling();
                runToolTipsPolling();
                pref.edit().putInt("pollingToolTips", 1).commit();
            }
        }
    }

    private void initToolTipsPolling()
    {
        String[] pesanToolTips = {"Yuk vote pilihanmu sekarang sebelum waktu polling berakhir"};
        String[] textButtonToolTips = {"Selesai"};
        int[] gravityToolTips = {Gravity.TOP};
        int[] targetToolTips = {R.id.receler_poling};

        listToolTipsPolling = new ArrayList<ToolTipsZona>();
//        List<Tooltips_Model> allToolTips = MainActivity.allToolTips;
        //jCOunter yang akan dipakai sebagai index oleh local array
        int jCounter=0;
        for(int i=0;i<1;i++)
        {
//            if( allToolTips.get(i).getFragment().equals("polling"))
//            {
            listToolTipsPolling.add(new ToolTipsZona(PollingActivity.this,
                    pesanToolTips[jCounter],
                    textButtonToolTips[jCounter],
                    targetToolTips[jCounter],
                    true,
                    R.layout.tooltip_small,
                    R.id.tv_text,
                    R.id.btn_next,
                    gravityToolTips[jCounter],
                    "polling",
                    "polling"));
            jCounter++;
//            }
        }
    }

    private void runToolTipsPolling()
    {
        new ToolTipsZonaController().setMultipleToolTips(listToolTipsPolling);
    }

    //    start iklan bottom navigation per x menit
//atur waktu tampil iklan agar tampil hanya setelah x menit, lalu show iklan
    public void setShowTimeForShowIklanVideoOnBottomNavigationPolling(){
        Log.d("fandy_29_april","ssT bottom navigation polling");
        //start iklan
        int intervalMenit = RuleValueAplikasi.durasiIklan;
        String labelSharedPref = "waktu_iklan_bottom_navigation_polling_terakhir";
        //start pref
        if (pref.contains(labelSharedPref) == false) //jika belum ada sharedpreferences key tertentu, maka lakukan init
        {
            //jika tidak ada sharedpref iklan tampil terakhir, maka sharedpref iklan tampil terakhir = now
            pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
            showIklanVideoOnBottomNavigationPolling();
        }
        else //jika sudah ada waktu iklan terakhir
        {
            String waktuTerakhirIklanTampil = pref.getString(labelSharedPref,"");
            String waktuSekarang = IklanInterstitial.getCurrentDateTime();
            long selisihWaktu = IklanInterstitial.substractTwoDateTimes(waktuTerakhirIklanTampil,waktuSekarang);
            Log.d("2 Februari","2feb selisih waktu: "+selisihWaktu);
            if(selisihWaktu > intervalMenit)
            {
                Log.d("fandy_29_april","ssT  if 237 visited");
                showIklanVideoOnBottomNavigationPolling();
                pref.edit().putString(labelSharedPref,IklanInterstitial.getCurrentDateTime()).commit();
                Log.d("2 Februari","2feb ifku: ");
            }
            else {
                Log.d("fandy_29_april","ssT  else 243 visited");
                Log.d("2 Februari","2feb elseku: ");
            }
        }
        //end pref
        //end iklan

    }

    //show iklan bottomNavigationKategori
    public void showIklanVideoOnBottomNavigationPolling(){
        IklanInterstitial.iklanVideo.show();
    }
//    end iklan bottom navigation per x menit


}