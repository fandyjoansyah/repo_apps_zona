package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Video_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RnDKomentarVideo extends AppCompatActivity {
    private static final String URL_GET_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment/index_post";
    RecyclerView recyclerKomentar;
    List<Komentar_Video_Model>dataKomentar;
    Komentar_Video_Level1_Adapter adapterKomentar;

    //untuk video
    String url_file;
    String nama_file;
    String ekstensiFile;
    String nextIdVideo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rn_dkomentar_video);
//
//        //inisialisasi widget
//        recyclerKomentar = findViewById(R.id.rnd_recycler_level1);
//
//        //komponen recycler view
//        dataKomentar = new ArrayList<>();
//        adapterKomentar = new Komentar_Video_Level1_Adapter(getApplicationContext(),dataKomentar,1,this);
//
//        //setup recycler view
//        LinearLayoutManager mLayoutManagerVideo;
//        mLayoutManagerVideo = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
//        recyclerKomentar.setLayoutManager(mLayoutManagerVideo);
//        recyclerKomentar.setItemAnimator(new DefaultItemAnimator());
//        recyclerKomentar.setAdapter(adapterKomentar);
//
//
//
//
//        getKomentarLevel1("103641696102986163134","25");
    }

//    private void getKomentarLevel1(String id_user,String id_video) {
//
//
//        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jObj = new JSONObject(response);
//                    //segera hapus fu
//                    Log.d(MainActivity.MYTAG,"getKomentarLevel1 onResponse try");
//
//                    if (jObj.getString("status").equals("true")) {
//                        //set nextVideo yang akan diload awal di next page
//                        nextIdVideo = jObj.getString("next_item");
//                        JSONArray dataArray = jObj.getJSONArray("data");
//
//                        //Log.d("Response VideoX","vbgvbg: "+dataArray.getJSONObject(0).getJSONArray("comment").getJSONObject(0).get("video_comment"));
//                        List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
//                        }.getType());
//
//                        dataKomentar.addAll(items);
//                        adapterKomentar.notifyDataSetChanged();
//
//                        Log.d(MainActivity.MYTAG,"RndKomentar: "+items.get(0).getVideo_comment());
//
//                    } else if (jObj.getString("status").equals("false")) {
//
//                        //item telah habis
//
//                    }
//
//                    // Check for error node in json
//
//                } catch (JSONException e) {
//                    //segera hapus fu
//                    Log.d(MainActivity.MYTAG,"getKomentarLevel1 onResponse catch");
//                    // JSON error
//                    Log.d(MainActivity.MYTAG,"catch response getALl");
//
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //segera hapus fu
//                Log.d(MainActivity.MYTAG,"onErrorResponse");
//
//
//            }
//        }) {
//
//
////            parameter
//
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting parameters to login url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("TOKEN", "qwerty");
//                params.put("id_user", id_user);
//                params.put("id_video",id_video);
//                return params;
//            }
//        };
//
////        ini heandling requestimeout
//        strReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 10000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 10000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
////                eror_show();
//                Log.e(MainActivity.MYTAG, "VolleyError Error: " + error.getMessage());
//
//            }
//        });
//
//
//        // Adding request to request queue
//        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
//    }
//
//    @Override
//    public void persiapanDownloadVideo(int position, String urlVideo, String namaVideo) {
//
//    }
//
//    @Override
//    public void storeLike(String idVideo, String like) {
//
//    }
//
//    @Override
//    public void shareVideo(String url) {
//
//    }
//
//    @Override
//    public void showAllComment(String idVideo) {
//
//    }
//
//    @Override
//    public void passingShowAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {
//
//    }
//
//
//    @Override
//    public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {
//
//    }
}