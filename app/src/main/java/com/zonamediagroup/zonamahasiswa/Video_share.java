package com.zonamediagroup.zonamahasiswa;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Fragment.Video;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.SwipeVideo_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Adapter;
import com.zonamediagroup.zonamahasiswa.adapters.komentar_video.Komentar_Video_Level1_Halaman_Awal_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Komentar_Video_Model;
import com.zonamediagroup.zonamahasiswa.models.Video_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Video_share extends AppCompatActivity implements SwipeVideo_Adapter.SwipeListener, Komentar_Video_Level1_Adapter.KomentarLevel1{
    BottomSheetDialog bottomSheetDialog;
    private static final String URL_SHARE_VIDEO = Server.URL_REAL_VIDEO+"Video_save/index_post";
    private static final String URL_HALAMAN_UTAMA_VIDEO = Server.URL_HALAMAN_UTAMA_VIDEO;
    private static final String URL_LIKE_VIDEO = Server.URL_REAL_VIDEO+"Video_like/index_post";
    private static final String URL_GET_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment/index_post";
    private static final String URL_GET_SINGLE_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_last/index_post";

    private static final String URL_TAMBAH_KOMENTAR = Server.URL_REAL_VIDEO+"Video_comment_create/index_post";

    VideoView videoView;
    TextView textVideoTitle, textVideoDescription, jumlahLike, jumlahComment,jumlahShare,txtKeteranganWaktu;
    ProgressBar videoProgressBar;
    ImageView swipeLike, swipeComment, swipeShare, backButton, send_button;
    SeekBar seekbar;
    RecyclerView recyclerCommentInBottomSheet;
    ProgressBar pbBottomSheet;
    EditText txt_komentar_video;
    ArrayList<Komentar_Video_Model> dataRecyclerKomentarBottomSheet;
    Komentar_Video_Level1_Adapter commentAdapterLevel1;
    String video_id = "0";
    String video_like_id = null;
    String video_share_url = "0";

    //Start Komponen bottomsheet komentar
    Komentar_Video_Level1_Adapter commentAdapterBottomSheetLevel1;


    RecyclerView recyclerKomentarVideoLevel1;

    TextView txt_bottomsheet_komentar_kosong;

    ImageView  gambar_komentator_video;

    ImageView gambar_komentator;

    Komentar_Video_Level1_Halaman_Awal_Adapter adapterRecyclerSingleKomentarHalamanAwal = null;

    //End Komponen bottomsheet komentar

    Video_Model videoItem;
    LinearLayout linearLayoutAll, line1, line2;
    Integer sttShow = 0;

    public Bundle getBundleFromNotificationVideo() {
        return bundleFromNotificationVideo;
    }

    public void setBundleFromNotificationVideo(Bundle bundleFromNotificationVideo) {
        this.bundleFromNotificationVideo = bundleFromNotificationVideo;
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    Bundle bundleFromNotificationVideo;


    String namaVideo;
    String idUserLogin = "103641696102986163134";
    String tipeNotifikasi="";
    private Handler updateHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_video_share);
        idUserLogin = SharedPrefManagerZona.getInstance(this).getid_user();

        videoView = findViewById(R.id.videoView);
        textVideoTitle = findViewById(R.id.textVideoTitle);
        textVideoDescription = findViewById(R.id.textVideoDescription);
        videoProgressBar = findViewById(R.id.videoProgressBar);
        swipeLike = findViewById(R.id.icLove);
        swipeComment = findViewById(R.id.icComment);
        swipeShare = findViewById(R.id.icShare);
        jumlahLike = findViewById(R.id.jumlah_like);
        jumlahComment = findViewById(R.id.jumlah_komen);
        jumlahShare = findViewById(R.id.jumlah_share_video);
        txtKeteranganWaktu = findViewById(R.id.txt_keterangan_waktu);
        seekbar = findViewById(R.id.seekBar);
        backButton = findViewById(R.id.backButton);
        linearLayoutAll = findViewById(R.id.linearLayoutAll);
        line1 = findViewById(R.id.line1);
        line2 = findViewById(R.id.line2);

        //cek siapakah pemanggilnya. share video atau notifikasi
        //jika pemanggil adalah dari notifikasi
        if(isVideoNotif() == true)
        {
            Bundle bundleFromNotification = getBundleFromNotificationVideo();

            if(bundleFromNotification.getString("video_notification_type").equals("reply"))
            {
                tipeNotifikasi = "reply";
                CurrentActiveVideoData.targetKomentarInduk = bundleFromNotification.getString("comment_id");
                CurrentActiveVideoData.targetKomentarAnak = bundleFromNotification.getString("comment_reply_id");
                //notif bertipe reply
            }
            else if(bundleFromNotification.getString("video_notification_type").equals("like")){
                tipeNotifikasi = "like";
                CurrentActiveVideoData.targetKomentarInduk = bundleFromNotification.getString("comment_id");
                //notif bertipe like
            }
            else if(bundleFromNotification.getString("video_notification_type").equals("save")){

            }
            String vid = bundleFromNotification.getString("video_id");
            video_id = vid;
            String idKategori = bundleFromNotification.getString("video_category_id");
            Log.d(MainActivity.MYTAG,"video share. id video: "+vid);
            Log.d(MainActivity.MYTAG,"video share. thumbnail video: "+bundleFromNotificationVideo.getString("video_thumbnail"));
            Log.d(MainActivity.MYTAG,"video share. title video: "+bundleFromNotificationVideo.getString("video_title"));
            createVideoShare(vid,idKategori,tipeNotifikasi);
        }
        else {
            //jika pemanggil adalah dari selain notifikasi (share)
            Uri data = getIntent().getData();
            if (data != null) {
                if (!data.getQueryParameter("id").equals("0") && !data.getQueryParameter("shareid").equals("0")) {
                    String vid = data.getQueryParameter("id");
                    video_id = vid;
                    String vshareid = data.getQueryParameter("shareid");
                    storeShare(vid, vshareid);
                    createVideoShare(vid,"",tipeNotifikasi);
                } else {
                    onBackPressed();
                    //            CustomToast.s(getApplicationContext(), "data tidak ada");
                }
            }
            else{
                //non share, non save non notif (semisal membuka Video_share video dari pop up)
                if(getIntent().getStringExtra("id_video") != null)
                {
                    String vid = getIntent().getStringExtra("id_video");
                    createVideoShare(vid,"",tipeNotifikasi);
                }

            }
        }

        linearLayoutAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    CustomToast.s(context, "di klik");
                if(sttShow == 0){
                    sttShow = 1;
                    line1.setVisibility(view.INVISIBLE);
                    line2.setVisibility(view.INVISIBLE);
                    textVideoTitle.setVisibility(view.INVISIBLE);
                    textVideoDescription.setVisibility(view.INVISIBLE);
                }else{
                    sttShow = 0;
                    line1.setVisibility(view.VISIBLE);
                    line2.setVisibility(view.VISIBLE);
                    textVideoTitle.setVisibility(view.VISIBLE);
                    textVideoDescription.setVisibility(view.VISIBLE);
                }
            }
        });
    }

    private void highlightSpecifiedComment(String komentarInduk, String komentarPembalas) {

    }

    private void createVideoShare(String vid,String id_kategori, String tipeNotifikasi)
    {
        getAllVideo(vid,id_kategori);
        initCommentBottomSheet();
        LinearLayoutManager layoutLevel1BottomSheet = new LinearLayoutManager(Video_share.this, RecyclerView.VERTICAL, false);
        recyclerCommentInBottomSheet
                .setLayoutManager(layoutLevel1BottomSheet);

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!videoView.isPlaying()) {
                    videoView.start();
                } else if (videoView.isPlaying()) {
                    videoView.pause();
                }

            }
        });

        textVideoDescription.setLinksClickable(true);
        textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());


        videoView.start();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if(tipeNotifikasi.equals("like") || tipeNotifikasi.equals("reply")){ //jika tipenotifikasi berupa like / komentar, maka arahkan ke spesifik komentar
            showDialogComment(vid);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra("video",true);
        startActivity(i);

    }

    private boolean isVideoNotif(){
        Bundle bundle = getIntent().getBundleExtra("data_bundle_notif");
        if(bundle == null)
            return false;
        else {
            setBundleFromNotificationVideo(bundle);
            return true;
        }
    }



    public void storeShare(String idVideo, String shareid)
    {
        int videoIdAngka = Integer.valueOf(idVideo);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_SHARE_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals("true")) {
//                            jumlah_share_video.setText(jObj.getString("count_share"));

                    } else if (jObj.getString("status").equals("false")) {
//                        CustomToast.l(context, "");
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d("mantap djiwa","onErrorResponse "+error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
//                eror_show();

            }
        }) {
            //            parameter
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUserLogin);
                params.put("id_video",idVideo);
                params.put("id_share",shareid);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
//                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    private void cekDanInitIconAwalLoad(ImageView icon, Drawable drawableTidakAdaNilai, Drawable drawableAdaNilai, String idLikeString){
        if(idLikeString == null || idLikeString.equals("null")) {
            icon.setImageDrawable(drawableTidakAdaNilai);

        }
        else if(idLikeString != null){
            icon.setImageDrawable(drawableAdaNilai);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private  void toggleLike(int position, String idUser, String idVideo, String currentStatusLike, ImageView iconLike, TextView txtLike)
    {
        int jumlahLike = Integer.valueOf(txtLike.getText().toString());
        Log.d(MainActivity.MYTAG,"idUser TG: "+idUser);
        Log.d(MainActivity.MYTAG,"idVideo TG: "+idVideo);
        Log.d(MainActivity.MYTAG,"like TG: "+currentStatusLike);
        if(currentStatusLike == null) //jika sebelumnya belum ada like, set like ke adapter recycler video
        {
            //tidak ada like(0). jadikan ada like (1)
            storeLikeSwipe(idVideo,"1");
            swapGambar(iconLike,getDrawable(R.drawable.love_video),150,150);
            jumlahLike++;
            video_like_id = "1";
            //adapterVideo.notifyDataSetChanged();
        }
        else
        {
            storeLikeSwipe(idVideo,"0");
            swapGambar(iconLike,getDrawable(R.drawable.love_fill),150,150);
            jumlahLike--;
            video_like_id = null;
            //adapterVideo.notifyDataSetChanged();
        }
        txtLike.setText(String.valueOf(jumlahLike));
    }

    private void swapGambar(ImageView imageView,Drawable drawableTujuan,int durasiFadeIn, int durasiFadeOut)
    {
        //start coba animasi
        imageView.animate()
                .alpha(0f)
                .setDuration(durasiFadeIn)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        imageView.setImageDrawable(drawableTujuan);
                        imageView.animate().alpha(1).setDuration(durasiFadeOut);
                    }
                    @Override
                    public void onAnimationCancel(Animator animator) { }
                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
        //end coba animasi
    }

    public void storeLikeSwipe(String idVideo, String like) {
        int likeAngka = Integer.valueOf(like);
        int videoIdAngka = Integer.valueOf(idVideo);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_LIKE_VIDEO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                    } else if (jObj.getString("status").equals("false")) {

                    }

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUserLogin);
                params.put("id_video",idVideo);
                params.put("like",like);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    public void shareVideoSwipe(String url) {

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        //shareIntent.putExtra(Intent.EXTRA_SUBJECT,  " https://play.google.com/store/apps/details?id=com.zonamediagroup.zonamahasiswa.app");
        String app_url = url;
        shareIntent.putExtra(Intent.EXTRA_TEXT, app_url);
        startActivity(Intent.createChooser(shareIntent, "Share via"));
    }

    @Override
    public void backVideo() {

    }

    @Override
    public void playNextVideo(int currentPosition) {

    }




    public void showAllCommentOnSwipe(String idVideo) {

        Log.d(MainActivity.MYTAG,"CALLER FROM Video. idVideo: "+idVideo);
        bottomSheetDialog.show();
        //start code getCommentLevel1
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC try");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONArray dataArray = jObj.getJSONArray("data");
                        ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());
                        dataRecyclerKomentarBottomSheet.clear();
                        dataRecyclerKomentarBottomSheet.addAll(items);
                        commentAdapterLevel1 = new Komentar_Video_Level1_Adapter(getApplicationContext(),dataRecyclerKomentarBottomSheet, (Komentar_Video_Level1_Adapter.KomentarLevel1) getApplicationContext());
                        recyclerCommentInBottomSheet.setAdapter(commentAdapterLevel1);
                        commentAdapterLevel1.notifyDataSetChanged();
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"komen pertama: "+items.get(0).getVideo_comment());
                        pbBottomSheet.setVisibility(View.GONE);
                    } else if (jObj.getString("status").equals("false")) {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try if else");

                    }
                    else
                    {
                        //segera hapus
                        Log.d(MainActivity.MYTAG,"sac try else");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    //segera hapus
                    Log.d(MainActivity.MYTAG,"sAC catch");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapus
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end code getCommentLevel1
        txt_komentar_video.setHint("");
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahKomentar(MainActivity.idUserLogin, idVideo, txt_komentar_video.getText().toString());
            }
        });
    }



    private void getAllVideo(String id_video, String id_kategori) {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_HALAMAN_UTAMA_VIDEO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                      //  CustomToast.s(getApplicationContext()," okokok");

//                        get data video
                        JSONArray dataArray = jObj.getJSONArray("data");
                        namaVideo = dataArray.getJSONObject(0).getString("video_title");
                        JSONObject dataVideo = dataArray.getJSONObject(0);
                        List<Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Video_Model>>() {
                        }.getType());
                        videoItem = items.get(0);
                     //   CustomToast.s(getApplicationContext(),"datanyadisini"+dataVideo.getString("video_title"));
                        textVideoTitle.setText(dataVideo.getString("video_title"));
                        jumlahComment.setText(String.valueOf(dataVideo.getString("video_comment")));
                        txtKeteranganWaktu.setText(dataVideo.getString("convert_time"));
                        videoView.setVideoPath(dataVideo.getString("video_url"));
                        video_id = dataVideo.getString("video_id");
                        if(dataVideo.getString("video_like_id") != null)
                        {

                        }
                        if(dataVideo.getString("video_like_id").equals("null"))
                        {

                        }
                        else
                        {
                            video_like_id = dataVideo.getString("video_like_id");
                        }

                        Log.d(MainActivity.MYTAG,"onok2 ae. video_like_id: "+dataVideo.getString("video_like_id"));
                        Log.d(MainActivity.MYTAG,"onok2 ae. video_like_id: "+dataVideo.getString("video_id"));
                        Log.d(MainActivity.MYTAG,"onok2 ae. video_like_id: "+dataVideo.getString("video_title"));

                        video_share_url = dataVideo.getString("video_share_url");
//                        set like
                        jumlahLike.setText(dataVideo.getString("video_like"));
                        Log.d(MainActivity.MYTAG,"onok2 ae. video_like: "+dataVideo.getString("video_like"));

                        String textnya = "<font color='white'>"+dataVideo.getString("video_deskripsi")+"</font>";
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());
                            textVideoDescription.setText(Html.fromHtml(textnya, Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());
                            textVideoDescription.setText(Html.fromHtml(textnya));
                        }

                        cekDanInitIconAwalLoad(swipeLike,swipeLike.getContext().getDrawable(R.drawable.love_fill),swipeLike.getContext().getDrawable(R.drawable.love_video),video_like_id);

                        swipeLike.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                toggleLike(0,MainActivity.idUserLogin,video_id,video_like_id,swipeLike,jumlahLike);
                            }
                        });
                        Runnable updateVideoTime = new Runnable() {
                            @Override
                            public void run() {
                                long currentPosition = videoView.getCurrentPosition();
                                seekbar.setProgress((int) currentPosition);
                                updateHandler.postDelayed(this, 100);
                            }
                        };
                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
                            @Override
                            public void onPrepared(MediaPlayer mp){
                                videoProgressBar.setVisibility(View.GONE);
                                mp.start();
                                float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                                float screenRatio = videoView.getWidth() / (float) videoView.getHeight();
                                float scale = videoRatio / screenRatio;
                                if(scale >= 1f){
                                    videoView.setScaleX(scale);
                                }
                                else {
                                    videoView.setScaleY(1f / scale);
                                }

                                seekbar.setProgress(0);
                                seekbar.setMax(videoView.getDuration());
//                                updateHandler.postDelayed(updateVideoTime,100);
                            }
                        });
                        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                mediaPlayer.start();
                            }
                        });
                        swipeComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(CheckNetworkAvailable.check(Video_share.this))
                                    showDialogComment(video_id);
                            }
                        });
                        jumlahShare.setText(dataVideo.getString("video_share"));
                        swipeShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                shareVideoSwipe(video_share_url);
                            }
                        });

                        textVideoDescription.setLinksClickable(true);
                        textVideoDescription.setMovementMethod(LinkMovementMethod.getInstance());

                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
                            @Override
                            public void onPrepared(MediaPlayer mp){
                                videoProgressBar.setVisibility(View.GONE);
                                mp.start();
                                float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                                float screenRatio = videoView.getWidth() / (float) videoView.getHeight();
                                float scale = videoRatio / screenRatio;
                                if(scale >= 1f){
                                    videoView.setScaleX(scale);
                                }
                                else {
                                    videoView.setScaleY(1f / scale);
                                }

                                seekbar.setProgress(0);
                                seekbar.setMax(videoView.getDuration());
                                updateHandler.postDelayed(updateVideoTime,100);
                            }
                        });
                    } else if (jObj.getString("status").equals("false")) {
                        CustomToast.s(getApplicationContext(),"Gagal mengambil video ");
                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    Log.d(MainActivity.MYTAG,"catch response getALl");
                    e.printStackTrace();
               //     CustomToast.s(getApplicationContext(),"error catch");
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Register Error: " + error.getMessage());
                Log.d(MainActivity.MYTAG,"onErrorResponse getALl "+error.getMessage());
//                eror_show();
            //    CustomToast.s(getApplicationContext(),"error respon "+error.getMessage());
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUserLogin);
                params.put("id_video",id_video);
                params.put("video_category_id",id_kategori);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
//                Log.e(TAG, "VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    @Override
    public void showAllCommentForReply(String idVideo, String idKomentarYangDibalas, String namaKomentatorYangDibalas, String idUserGoogleYangDibalas) {

    }

    @Override
    public void balasKomentarLevel1(String idKomentarYangDibalas) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //start coding komentar
    public void showDialogComment(String idVideo){
        initCommentBottomSheet();
        Glide.with(Video_share.this)
                .load(SharedPrefManagerZona.getInstance(Video_share.this).geturl_voto())
                .apply(RequestOptions.fitCenterTransform())
                .into(gambar_komentator);


        bottomSheetDialog.show();
        refreshRecyclerViewKomentarSetelahAksiLevel1(idVideo,"awal_buka");
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetworkAvailable.check(Video_share.this)) {
                    tambahKomentar(MainActivity.idUserLogin, idVideo, txt_komentar_video.getText().toString());
                    txt_komentar_video.setText("");
                    txt_komentar_video.clearFocus();
                }
                //  CustomToast.s(getContext(),"Komentarmu berhasil ditambah");
            }
        });
    }

    public void refreshRecyclerViewKomentarSetelahAksiLevel1(String idVideo,String jenisAksi)
    {
        //start code getCommentLevel1
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONArray dataArray = jObj.getJSONArray("data");
                        ArrayList<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());

                        dataRecyclerKomentarBottomSheet.clear();
                        dataRecyclerKomentarBottomSheet.addAll(items);
                        commentAdapterBottomSheetLevel1 = new Komentar_Video_Level1_Adapter(Video_share.this,dataRecyclerKomentarBottomSheet, Video_share.this);
                        recyclerCommentInBottomSheet.setAdapter(commentAdapterBottomSheetLevel1);
                        commentAdapterBottomSheetLevel1.notifyDataSetChanged();

                        pbBottomSheet.setVisibility(View.GONE);
                    } else if (jObj.getString("status").equals("false")) {

                        txt_bottomsheet_komentar_kosong.setVisibility(View.VISIBLE);
                        pbBottomSheet.setVisibility(View.GONE);
                    }
                    else
                    {

                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",idVideo);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
        //end code getCommentLevel1
    }

    public void tambahKomentar(String idUser, String idVideo, String komentar){
        Log.d(MainActivity.MYTAG,"tambah komentar ittuw");
        StringRequest strReq = new StringRequest(Request.Method.POST, URL_TAMBAH_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    Log.d(MainActivity.MYTAG,"try tambah komentar ittuw");
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {
                        JSONArray dataArray = jObj.getJSONArray("data");
                        List<Komentar_Video_Model> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());

                        Log.d(MainActivity.MYTAG,"tambahKomentar() -> onResponse() -> if -> if");
                        Komentar_Video_Model lastAddedComment = items.get(0);
                        changeTotalCommentDataVideo(1);
                        if(commentAdapterBottomSheetLevel1!=null) {
                            addDataToRecyclerViewComment(commentAdapterBottomSheetLevel1, dataRecyclerKomentarBottomSheet, lastAddedComment , 0, recyclerCommentInBottomSheet);
                        }
                        else {
                            initDataRecyclerViewCommentOnFirstAdd(lastAddedComment);
                        }
                        //lanjut disini. refresh recycler sehabis menambahkan komentar
                    } else if (jObj.getString("status").equals("false")) {


                    }
                    else
                    {

                    }

                    // Check for error node in json

                } catch (JSONException e) {
                    Log.d(MainActivity.MYTAG,"catch tambah komentar ittuw "+e.getMessage());
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(MainActivity.MYTAG,"onErrorResponse tambah komentar ittuw "+ error.getMessage());
            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", idUser);
                params.put("id_video",idVideo);
                params.put("comment",komentar);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");
    }

    private void initDataRecyclerViewCommentOnFirstAdd(Komentar_Video_Model items) {
        Log.d(MainActivity.MYTAG,"init ittuw");
        recyclerCommentInBottomSheet.setAdapter(null);
        txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
//  error launching      List<Komentar_Video_Model> dataKomentar = new ArrayList<>();
//  error launching      dataKomentar.add(items);
        dataRecyclerKomentarBottomSheet.add(items);
        commentAdapterBottomSheetLevel1 = new Komentar_Video_Level1_Adapter(Video_share.this,dataRecyclerKomentarBottomSheet,this);
        LinearLayoutManager layoutLevel1 = new LinearLayoutManager(Video_share.this,RecyclerView.VERTICAL,false);
        recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1);
        recyclerCommentInBottomSheet.setAdapter(commentAdapterBottomSheetLevel1);
        recyclerCommentInBottomSheet.setVisibility(View.VISIBLE);
    }

    private void changeTotalCommentDataVideo(int newValue) {
        int originalNumberOfComments = getOriginalNumberOfComments();
        int newNumberOfComments = calculateAndGetNewNumberOfComments(originalNumberOfComments, newValue);
        setNewNumberOfCommentsToTotalComments(newNumberOfComments);
    }
    private int getOriginalNumberOfComments(){
        return Integer.valueOf(videoItem.getVideo_comment());
    }

    private int calculateAndGetNewNumberOfComments(int originalNumberOfComments, int newValue){
        return (originalNumberOfComments+newValue);
    }

    private void setNewNumberOfCommentsToTotalComments(int newNumberOfCommentsToTotalComments)
    {
        videoItem.setVideo_comment(String.valueOf(newNumberOfCommentsToTotalComments));
        setNewNumberOfCommentsToTextViewTotalComments(newNumberOfCommentsToTotalComments);
    }

    private void setNewNumberOfCommentsToTextViewTotalComments(int newNumberOfCommentsToTotalComments)
    {
        jumlahComment.setText(String.valueOf(newNumberOfCommentsToTotalComments));
    }

    public  void  addDataToRecyclerViewComment(RecyclerView.Adapter adapter, List<Komentar_Video_Model> allData, Komentar_Video_Model newData, int indeksPosisiBaru, RecyclerView recyclerView){
        Log.d(MainActivity.MYTAG,"sakjane terpanggil");
        txt_bottomsheet_komentar_kosong.setVisibility(View.GONE);
        allData.add(indeksPosisiBaru,newData);
        adapter.notifyItemInserted(indeksPosisiBaru);
        // adapter.notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(indeksPosisiBaru);
    }
    public void initCommentBottomSheet()
    {
        bottomSheetDialog = new BottomSheetDialog(Video_share.this);
        View view= LayoutInflater.from(Video_share.this).inflate(R.layout.sheet_dialog_komentar,null);

        recyclerCommentInBottomSheet = view.findViewById(R.id.recycler_bottomsheet_komen_video);
        LinearLayoutManager layoutLevel1BottomSheet = new LinearLayoutManager(Video_share.this,RecyclerView.VERTICAL,false);
        recyclerCommentInBottomSheet.setLayoutManager(layoutLevel1BottomSheet);

        pbBottomSheet = view.findViewById(R.id.progress_bottom_video);
        gambar_komentator_video = view.findViewById(R.id.gambar_komentator_video);

        txt_komentar_video = view.findViewById(R.id.txt_komentar_video);

        send_button = view.findViewById(R.id.send_button);

        dataRecyclerKomentarBottomSheet = new ArrayList<>();

        txt_bottomsheet_komentar_kosong = view.findViewById(R.id.txt_bottomsheet_komentar_kosong);

        gambar_komentator = view.findViewById(R.id.gambar_komentator_video);

        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //reset adapter komentar di bottomsheet agar reload lagi di pembukaan selanjutnya
                commentAdapterBottomSheetLevel1 = null;
                //refresh recycler single di halaman awal video
                getLastSingleCommentById(videoItem.getVideo_id());
            }
        });
    }





    private void getLastSingleCommentById(String id_video) {

        StringRequest strReq = new StringRequest(Request.Method.POST, URL_GET_SINGLE_KOMENTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray jsonArrayLastComment = jObj.getJSONArray("comment");
                    if (jsonArrayLastComment != null) {
                        List<Komentar_Video_Model> lastComment = new Gson().fromJson(jsonArrayLastComment.toString(), new TypeToken<List<Komentar_Video_Model>>() {
                        }.getType());

                    }
                    // Check for error node in json

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //segera hapu
                Log.d(MainActivity.MYTAG,"sAC onErrorResponse");

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");
                params.put("id_user", MainActivity.idUserLogin);
                params.put("id_video",id_video);
                return params;
            }
        };

//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

//                eror_show();
                Log.e(MainActivity.MYTAG, "sAC VolleyError Error: " + error.getMessage());

            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, "json_obj_req");

    }

    //end coding komentar
}