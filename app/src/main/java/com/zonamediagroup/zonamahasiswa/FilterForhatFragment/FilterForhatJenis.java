package com.zonamediagroup.zonamahasiswa.FilterForhatFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.zonamediagroup.zonamahasiswa.R;

public class FilterForhatJenis extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.include_filter_forhat_jenis, container, false);
    }
}