package com.zonamediagroup.zonamahasiswa;

import android.provider.ContactsContract;
import android.util.Log;
import android.util.Patterns;

public class EmailValidator {
    public static boolean checkValidasiInputEmail(String target){
        if(Patterns.EMAIL_ADDRESS.matcher(target).matches())
            return true;
        else
            return false;
    }

    public static boolean checkValidasiDomainAllowed(String target)
    {
        Log.d("16_juni_2022","CVDA called");
        if(target.contains("@gmail.com"))
        {
            return true;
        }
        else if(target.contains("@yahoo.com"))
        {
            return true;
        }
        else if(target.contains("@yahoo.co.id"))
        {
            return true;
        }
        else if(target.contains("@outlook.com"))
        {
            return true;
        }
        else if(target.contains("@hotmail.com"))
        {
            return true;
        }
        else if(target.contains("@icloud.com"))
        {
            return true;
        }
        else if(target.contains(".sch.id"))
        {
            return true;
        }
        else if(target.contains(".go.id"))
        {
            return true;
        }
        else if(target.contains(".ac.id"))
        {
            return true;
        }
        else if(target.contains(".id"))
        {
            return true;
        }
        else if(target.contains(".net"))
        {
            return true;
        }
        else if(target.contains(".co.id"))
        {
            return true;
        }
        else if(target.contains(".com"))
        {
            return true;
        }
        else if(target.contains(".info"))
        {
            return true;
        }
        else if(target.contains(".org"))
        {
            return true;
        }
        else if(target.contains(".tv"))
        {
            return true;
        }
        else if(target.contains(".in"))
        {
            return true;
        }
        else if(target.contains(".agency"))
        {
            return true;
        }
        else if(target.contains(".club"))
        {
            return true;
        }
        else if(target.contains(".biz"))
        {
            return true;
        }
        else if(target.contains(".edu"))
        {
            return true;
        }
        else if(target.contains(".name"))
        {
            return true;
        }
        else if(target.contains(".travel"))
        {
            return true;
        }
        else{
            Log.d("16_juni_2022","else");
            return false;
        }
    }
}
