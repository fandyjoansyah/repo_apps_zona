package com.zonamediagroup.zonamahasiswa.tooltip;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;

import com.zonamediagroup.zonamahasiswa.R;

public class ToolTipsZona {
   public Activity activity;
  public  String pesan, textTombol;
   public int idViewAnchor;
   public boolean arrow;
   public int resLayoutToolTips;
   public View viewAnchor;
   public Context context;

    public Activity getActivity() {
        return activity;
    }

    public String getPesan() {
        return pesan;
    }

    public String getTextTombol() {
        return textTombol;
    }

    public int getIdViewAnchor() {
        return idViewAnchor;
    }

    public boolean isArrow() {
        return arrow;
    }

    public int getResLayoutToolTips() {
        return resLayoutToolTips;
    }

    public int getIdTextViewTooltips() {
        return idTextViewTooltips;
    }

    public int getIdItemClick() {
        return idItemClick;
    }

    public int getGravity() {
        return gravity;
    }

    public String getNamaFragment() {
        return namaFragment;
    }

 public   int idTextViewTooltips;
 public   int idItemClick;
 public   int gravity;
 public   String namaFragment="";

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public void setTextTombol(String textTombol) {
        this.textTombol = textTombol;
    }

    public void setIdViewAnchor(int idViewAnchor) {
        this.idViewAnchor = idViewAnchor;
    }

    public void setArrow(boolean arrow) {
        this.arrow = arrow;
    }

    public void setResLayoutToolTips(int resLayoutToolTips) {
        this.resLayoutToolTips = resLayoutToolTips;
    }

    public View getViewAnchor() {
        return viewAnchor;
    }

    public void setViewAnchor(View viewAnchor) {
        this.viewAnchor = viewAnchor;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setIdTextViewTooltips(int idTextViewTooltips) {
        this.idTextViewTooltips = idTextViewTooltips;
    }

    public void setIdItemClick(int idItemClick) {
        this.idItemClick = idItemClick;
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    public void setNamaFragment(String namaFragment) {
        this.namaFragment = namaFragment;
    }

    public String getNamaToolTips() {
        return namaToolTips;
    }

    public void setNamaToolTips(String namaToolTips) {
        this.namaToolTips = namaToolTips;
    }

    public String namaToolTips;


    public ToolTipsZona(Activity activity,String pesan, String textTombol, int idViewAnchor,boolean arrow, int resLayoutToolTips, int idTextViewTooltips, int idItemClick, int gravity, String namaFragment,String namaToolTips){
        this.activity = activity;
        this.pesan = pesan;
        this.idViewAnchor = idViewAnchor;
        this.arrow = arrow;
        this.resLayoutToolTips = resLayoutToolTips;
        this.idTextViewTooltips = idTextViewTooltips;
        this.idItemClick = idItemClick;
        this.gravity = gravity;
        this.textTombol = textTombol;
        if(namaFragment != null && !namaFragment.equals(""))
            this.namaFragment = namaFragment;
    }

    public ToolTipsZona(Context context,String pesan, String textTombol, View viewAnchor,boolean arrow, int resLayoutToolTips, int idTextViewTooltips, int idItemClick, int gravity, String namaFragment,String namaToolTips){
        this.context = context;
        this.pesan = pesan;
        this.viewAnchor = viewAnchor;
        this.arrow = arrow;
        this.resLayoutToolTips = resLayoutToolTips;
        this.idTextViewTooltips = idTextViewTooltips;
        this.idItemClick = idItemClick;
        this.gravity = gravity;
        this.textTombol = textTombol;
        if(namaFragment != null && !namaFragment.equals(""))
            this.namaFragment = namaFragment;
    }




}
