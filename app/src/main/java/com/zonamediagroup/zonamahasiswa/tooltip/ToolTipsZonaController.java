package com.zonamediagroup.zonamahasiswa.tooltip;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.zonamediagroup.zonamahasiswa.Fragment.Home;
import com.zonamediagroup.zonamahasiswa.MainActivity;
import com.zonamediagroup.zonamahasiswa.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.ToLongBiFunction;

public class ToolTipsZonaController {
    Activity activity;
    ToolTipsZona[] arrToolTips;
    int indeksGlobal;
    private List<ToolTipsZona> listToolTips;


    public ToolTipsZonaController(){

    }

    //fungsi yang datanya memakai array
//    public  void jalankanToolTips(int indeks){
//        indeksGlobal = indeks;
//        activity = arrToolTips[indeksGlobal].activity;
//        final SimpleTooltip tooltip = new SimpleTooltip.Builder(activity)
//                .anchorView(activity.findViewById(arrToolTips[indeksGlobal].idViewAnchor))
//                .text(arrToolTips[indeksGlobal].pesan)
//                .gravity(arrToolTips[indeksGlobal].gravity)
//                .showArrow(arrToolTips[indeksGlobal].arrow)
//                .dismissOnOutsideTouch(false)
//                .dismissOnInsideTouch(false)
//                .modal(true)
//                .animated(true)
//                .transparentOverlay(false)
//                .overlayMatchParent(false)
//                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
//                .overlayOffset(0)
//                .animationDuration(2000)
//                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
//                .contentView(arrToolTips[indeksGlobal].resLayoutToolTips, R.id.tv_text)
//                .focusable(true)
//                .build();
//
//        tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(tooltip.isShowing())
//                {
//                    tooltip.dismiss();
//                    if(indeksGlobal + 1 < arrToolTips.length)
//                    {
//                        indeksGlobal = indeksGlobal+1;
//                        //jalankan tooltips array selanjutnya
//                        jalankanToolTips(indeksGlobal);
//                    }
//
//            }
//            }
//        });
//
//        tooltip.show();
//
//        //send data ke server kalau sudah pernah tampil
//        updateToolTipsToServer(null,null);
//    }

    public  void jalankanToolTips(int indeks){
        indeksGlobal = indeks;
        activity = listToolTips.get(indeksGlobal).activity;
        final SimpleTooltip tooltip = new SimpleTooltip.Builder(activity)
                .anchorView(activity.findViewById(listToolTips.get(indeksGlobal).idViewAnchor))
                .text(listToolTips.get(indeksGlobal).pesan)
                .gravity(listToolTips.get(indeksGlobal).gravity)
                .showArrow(listToolTips.get(indeksGlobal).arrow)
                .dismissOnOutsideTouch(false)
                .dismissOnInsideTouch(false)
                .modal(true)
                .animated(true)
                .transparentOverlay(false)
                .overlayMatchParent(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR_RADIUS)
                .overlayOffset(0)
                .animationDuration(2000)
                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
                .contentView(listToolTips.get(indeksGlobal).resLayoutToolTips, R.id.tv_text)
                .focusable(true)
                .build();
        Button btn = tooltip.findViewById(R.id.btn_next);
        btn.setText(listToolTips.get(indeks).textTombol);
        tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tooltip.isShowing())
                {
                    tooltip.dismiss();
                    if(indeksGlobal + 1 < listToolTips.size())
                    {
                        indeksGlobal = indeksGlobal+1;
                        //jalankan tooltips array selanjutnya
                        jalankanToolTips(indeksGlobal);
                    }

                }
            }
        });

        tooltip.show();

        //send data ke server kalau sudah pernah tampil
        updateToolTipsToServer(null,null);
    }

    private void updateToolTipsToServer(String idUser, String idToolTips) {

    }


    public  void jalankanToolTipsAnchorView(int indeks){
        indeksGlobal = indeks;

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(listToolTips.get(indeks).context)
                .anchorView(listToolTips.get(indeks).viewAnchor)
                .text(listToolTips.get(indeks).pesan)
                .gravity(listToolTips.get(indeks).gravity)
                .showArrow(listToolTips.get(indeks).arrow)
                .dismissOnOutsideTouch(false)
                .dismissOnInsideTouch(false)
                .modal(true)
                .animated(true)
                .transparentOverlay(false)
//                .overlayWindowBackgroundColor(Color.BLACK)
                .overlayMatchParent(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR_RADIUS)
                .overlayOffset(0)
                .animationDuration(2000)
                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
                .contentView(listToolTips.get(indeks).resLayoutToolTips, R.id.tv_text)
                .focusable(true)
                .build();
       Button btn = tooltip.findViewById(R.id.btn_next);
       btn.setText(listToolTips.get(indeks).textTombol);
        tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tooltip.isShowing())
                {
                    tooltip.dismiss();
                    if(indeksGlobal + 1 < listToolTips.size())
                    {
                        indeksGlobal = indeksGlobal+1;
                        //jalankan tooltips array selanjutnya
                        jalankanToolTips(indeksGlobal);
                    }

                }
            }
        });

        tooltip.show();

    }



    public void setMultipleToolTips(ToolTipsZona[] arrToolTips) {
        this.arrToolTips = arrToolTips;
        this.jalankanToolTips(0); //menjalankan tooltips dimulai dari indeks ke 0
    }

    public void setMultipleToolTips(List<ToolTipsZona> listToolTips) {
        this.listToolTips = listToolTips;
        this.jalankanToolTips(0); //menjalankan tooltips dimulai dari indeks ke 0
    }

    public void setMultipleToolTipsAnchorView(ToolTipsZona[] arrToolTips) {
        this.arrToolTips = arrToolTips;
        this.jalankanToolTipsAnchorView(0); //menjalankan tooltips dimulai dari indeks ke 0
    }
    public void setMultipleToolTipsAnchorView(List<ToolTipsZona> listToolTips) {
        this.listToolTips = listToolTips;
        this.jalankanToolTipsAnchorView(0); //menjalankan tooltips dimulai dari indeks ke 0
    }

    public  void jalankanSingleToolTips(ToolTipsZona ttz){
        activity = ttz.activity;
        Log.d(MainActivity.MYTAG,"id view anchor: "+ttz.idViewAnchor);
         SimpleTooltip tooltip = new SimpleTooltip.Builder(activity)
                .anchorView(activity.findViewById(ttz.idViewAnchor))
                .text(ttz.pesan)
                .gravity(ttz.gravity)
                .showArrow(ttz.arrow)
                .dismissOnOutsideTouch(false)
                .dismissOnInsideTouch(false)
                .modal(true)
                .animated(true)
                .transparentOverlay(false)
//                 .overlayWindowBackgroundColor(Color.BLACK)
                .overlayMatchParent(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR_RADIUS)
                .overlayOffset(0)
                .animationDuration(2000)
                .animationPadding(SimpleTooltipUtils.pxFromDp(0))
                .contentView(ttz.resLayoutToolTips, R.id.tv_text)
                .focusable(true)
                .build();

        tooltip.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tooltip.isShowing())
                {
                    tooltip.dismiss();

                }
            }
        });

        tooltip.show();

    }
}


