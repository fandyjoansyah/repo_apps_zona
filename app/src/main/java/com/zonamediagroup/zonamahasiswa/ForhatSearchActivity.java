package com.zonamediagroup.zonamahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.apmem.tools.layouts.FlowLayout;

import java.util.HashMap;
import java.util.Map;


public class ForhatSearchActivity extends AppCompatActivity implements View.OnClickListener, BottomSheetDialogFilterForhat.BottomSheetFilterForhatListener{

    EditText edt_cari;
    FlowLayout area_selected_filter;
    ImageView filter_pencarian,btn_back,btn_delete_edt_cari, close_tag_postingan,close_tag_jenis,close_tag_kategori;
    LinearLayout ly_selected_filter_kategori,ly_selected_filter_jenis,ly_selected_filter_postingan;
    BottomSheetDialogFilterForhat bSDFF;
    HashMap<String,String> hashMapActivity;
    TextView tv_jenis_selected,tv_kategori_selected,tv_postingan_selected;
    String tahunTerpilihActivity="";
    String bulanTerpilihActivity="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_forhat_search);
        initHashMapActivity();
        findViewByIdAllComponent();
        setListenerAllComponent();

    }

    private void initHashMapActivity(){
        hashMapActivity = new HashMap<>();
    }


    private void initBottomSheetObject(HashMap hashMapActivity,String bulanTerpilihActivity,String tahunTerpilihActivity){
        bSDFF = new BottomSheetDialogFilterForhat();
    }

    private void nullifiedBottomSheetObject(){
        bSDFF = null;
    }
    private void findViewByIdAllComponent() {
        filter_pencarian = findViewById(R.id.filter_pencarian);
        area_selected_filter = findViewById(R.id.area_selected_filter);
        btn_back = findViewById(R.id.btn_back);
        btn_delete_edt_cari = findViewById(R.id.btn_delete_edt_cari);
        edt_cari = findViewById(R.id.edt_cari);
        close_tag_postingan = findViewById(R.id.close_tag_postingan);
        close_tag_jenis = findViewById(R.id.close_tag_jenis);
        close_tag_kategori = findViewById(R.id.close_tag_kategori);
        ly_selected_filter_kategori = findViewById(R.id.ly_selected_filter_kategori);
        ly_selected_filter_jenis = findViewById(R.id.ly_selected_filter_jenis);
        ly_selected_filter_postingan = findViewById(R.id.ly_selected_filter_postingan);
        tv_jenis_selected = findViewById(R.id.tv_jenis_selected);
        tv_kategori_selected = findViewById(R.id.tv_kategori_selected);
        tv_postingan_selected = findViewById(R.id.tv_postingan_selected);
    }

    private void setListenerAllComponent() {
        filter_pencarian.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_delete_edt_cari.setOnClickListener(this);
        edt_cari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mekanismeTypingOnEdtCari();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        close_tag_postingan.setOnClickListener(this);
        close_tag_jenis.setOnClickListener(this);
        close_tag_kategori.setOnClickListener(this);
    }

    private void mekanismeTypingOnEdtCari(){
        if(edt_cari.getText().toString()!=null&&!edt_cari.getText().toString().equals(""))
        {
            showBtnDeleteEdtCari();
        }
        else
        {
            hideBtnDeleteEdtCari();
        }
    }

    private void deleteEdtCari(){
        if(edt_cari.getText().toString()!=null&&!edt_cari.getText().toString().equals(""))
        {
            edt_cari.setText("");
        }
    }

    private void showBtnDeleteEdtCari(){
        btn_delete_edt_cari.setVisibility(View.VISIBLE);
    }
    private void hideBtnDeleteEdtCari(){
        btn_delete_edt_cari.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id)
        {
            case R.id.filter_pencarian:
                showBottomSheetFilter();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_delete_edt_cari:
                deleteEdtCari();
                hideBtnDeleteEdtCari();
                break;
            case R.id.close_tag_postingan:
                hideTagFilterPostingan();
                break;
            case R.id.close_tag_jenis:
                hideTagFilterJenis();
                break;
            case R.id.close_tag_kategori:
                hideTagFilterKategori();
                break;

        }
    }

    private void showTagFilterMainPage(View tagLayout)
    {
        if(tagLayout.getVisibility()!=View.VISIBLE)
        {
            tagLayout.setVisibility(View.VISIBLE);
        }
    }



    private void hideTagFilterMainPage(View tagLayout){
        if(tagLayout.getVisibility() == View.VISIBLE)
        {
            tagLayout.setVisibility(View.GONE);
        }
    }

    private void setText_tv_jenis_selected(String text){
        tv_jenis_selected.setText(text);
    }

    private void setText_tv_postingan_selected(String text){
        tv_postingan_selected.setText(text);
    }

    private void setText_tv_kategori_selected(String text){
        tv_kategori_selected.setText(text);
    }


    private void showTagFilterJenis(){
        ly_selected_filter_jenis.setVisibility(View.VISIBLE);
    }

    private void showTagFilterPostingan(){
        ly_selected_filter_postingan.setVisibility(View.VISIBLE);
    }

    private void showTagFilterKategori(){
        ly_selected_filter_kategori.setVisibility(View.VISIBLE);
    }

    private void hideTagFilterJenis(){
        if(ly_selected_filter_jenis.getVisibility() == View.VISIBLE)
        {
            ly_selected_filter_jenis.setVisibility(View.GONE);
        }
    }

    private void hideTagFilterPostingan(){
        if(ly_selected_filter_postingan.getVisibility() == View.VISIBLE)
        {
            ly_selected_filter_postingan.setVisibility(View.GONE);
        }
    }

    private void hideTagFilterKategori(){
        if(ly_selected_filter_kategori.getVisibility() == View.VISIBLE)
        {
            ly_selected_filter_kategori.setVisibility(View.GONE);
        }
    }


    /*bottom sheet filter pencarian*/
    private void showBottomSheetFilter() {
        initBottomSheetObject(hashMapActivity,bulanTerpilihActivity,tahunTerpilihActivity);
        bSDFF.show(getSupportFragmentManager(),"bottomsheet filter");
    }



    private void mekanismeShowTagFilterActivityByHashMap(){
        for (Map.Entry<String,String> entry : hashMapActivity.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            switch(value)
            {
                case MenuFilterForhatManager.SUBFILTER_CURHATAN:
                case MenuFilterForhatManager.SUBFILTER_TANGGAPAN:
                {
                    showTagFilterJenis();
                    setText_tv_jenis_selected(key+": "+value);
                }
                break;
                case MenuFilterForhatManager.SUBFILTER_SEMUA:
                case MenuFilterForhatManager.SUBFILTER_TERBARU:
                case MenuFilterForhatManager.SUBFILTER_TERPOPULER:
                {
                    showTagFilterPostingan();
                    setText_tv_postingan_selected(key+": "+value);
                }
            }
        }
    }



    @Override
    public void sendDataBottomSheetToForhatSearchActivity(HashMap hashMapBottomSheet, String bulanTerpilih, String tahunTerpilih) {

    }

    @Override
    public void sendDataBottomSheetToForhatSearchActivity() {

    }
}