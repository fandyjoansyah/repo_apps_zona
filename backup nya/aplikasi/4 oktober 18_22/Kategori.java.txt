package com.zonamediagroup.zonamahasiswa.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Download_file;
import com.zonamediagroup.zonamahasiswa.Post_by_kategori;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Save;
import com.zonamediagroup.zonamahasiswa.Search;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.Kategori_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Kategori_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Kategori extends Fragment implements Kategori_Adapter.ContactsAdapterListener {

    private String URL_home = Server.URL_REAL + "Kategori_fix/?TOKEN=qwerty";

    private static final String TAG = Kategori.class.getSimpleName();
    static String tag_json_obj = "json_obj_req";

    // item kedua
    RecyclerView receler_dua;
    private List<Kategori_Model> duaModels;
    private Kategori_Adapter duaAdapter;


    // loading
    ImageView image_animasi_loading;
    LinearLayout loding_layar;

    // eror layar
    LinearLayout eror_layar;
    TextView coba_lagi;
    int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

    ImageView search;
    ImageView saveok;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.page_kategori, container, false);

        init(root);

        return root;
    }

    void init(View root) {


//        item KATEGORI
        receler_dua = root.findViewById(R.id.receler_dua);
        duaModels = new ArrayList<>();
        duaAdapter = new Kategori_Adapter(getContext(), duaModels, this);
        RecyclerView.LayoutManager mLayoutManagerkategorix = new GridLayoutManager(getActivity(), 4);
        receler_dua.setLayoutManager(mLayoutManagerkategorix);
        receler_dua.setItemAnimator(new DefaultItemAnimator());
        receler_dua.setAdapter(duaAdapter);


//        Loading
        loding_layar = root.findViewById(R.id.loding_layar);
        image_animasi_loading = root.findViewById(R.id.image_animasi_loading);

//        eror
        eror_layar = root.findViewById(R.id.eror_layar);
        coba_lagi = root.findViewById(R.id.coba_lagi);

        coba_lagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eror_dismiss();
                loading_show();

                get_kategori();

            }
        });

        search = root.findViewById(R.id.search);
        saveok = root.findViewById(R.id.saveok);

        // action
        saveok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Save.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Search.class);
                startActivity(intent);
            }
        });

        eror_dismiss();
        loading_show();

//        Call API
        get_kategori();

    }

    private void get_kategori() {


        StringRequest strReq = new StringRequest(Request.Method.GET, URL_home, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("true")) {


                        JSONArray dataArray2 = jObj.getJSONArray("item_kedua");

                        List<Kategori_Model> itemsdua = new Gson().fromJson(dataArray2.toString(), new TypeToken<List<Kategori_Model>>() {
                        }.getType());


                        duaModels.clear();
                        duaModels.addAll(itemsdua);
                        duaAdapter.notifyDataSetChanged();


                        // add one index last
                        String dataArray = new JSONArray()
                                .put(new JSONObject()
                                        .put("id_kategori", "yogi")
                                        .put("id_categori_wp", "yogi")
                                        .put("nama_kategori", "Template Makalah, Skripsi, PPT")
                                        .put("gambar_kategori", "https://zonamahasiswa.id/wp-content/uploads/apps_icon/v1/templatemakalah.png")
                                        .put("warna_kategori", "#5286EC")
                                )

                                .toString();

                        List<Kategori_Model> items_dobel = new Gson().fromJson(dataArray, new TypeToken<List<Kategori_Model>>() {
                        }.getType());

                        duaModels.addAll(items_dobel);
                        //start swap position
                        Collections.swap(duaModels,(duaModels.size()-1),(duaModels.size()-2));
                        //end swap position

                        duaAdapter.notifyDataSetChanged();

                        loading_dismiss();
                    } else if (jObj.getString("status").equals("false")) {
                        loading_dismiss();

                    }


                    // Check for error node in json

                } catch (JSONException e) {
                    // JSON error
                    eror_show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                eror_show();

            }
        }) {


//            parameter

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("TOKEN", "qwerty");

                return params;
            }
        };


//        ini heandling requestimeout
        strReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

                Log.e(TAG, "VolleyError Error: " + error.getMessage());
//                eror_show();
            }
        });

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    @Override
    public void onContactSelected(Kategori_Model itemdua) {
        
        String id = itemdua.getId_categori_wp();

        if (id.equals("yogi")) {
            Intent intent = new Intent(getActivity(), Download_file.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), Post_by_kategori.class);
            intent.putExtra("id_kategori", itemdua.getId_kategori());
            intent.putExtra("id_categori_wp", itemdua.getId_categori_wp());
            intent.putExtra("nama_kategori", itemdua.getNama_kategori());

            intent.putExtra("gambar_kategori", itemdua.getGambar_kategori());
            intent.putExtra("warna_kategori", itemdua.getWarna_kategori());

            startActivity(intent);
        }


    }


    // loading dan eror
    public void loading_show() {
        loding_layar.setVisibility(View.VISIBLE);
    }

    public void loading_dismiss() {

        loding_layar.setVisibility(View.GONE);
    }

    public void eror_show() {
        eror_layar.setVisibility(View.VISIBLE);
    }

    public void eror_dismiss() {

        eror_layar.setVisibility(View.GONE);
    }

}


