package com.zonamediagroup.zonamahasiswa.Fragment;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zonamediagroup.zonamahasiswa.Detail;
import com.zonamediagroup.zonamahasiswa.R;
import com.zonamediagroup.zonamahasiswa.Save;
import com.zonamediagroup.zonamahasiswa.Search;
import com.zonamediagroup.zonamahasiswa.Server;
import com.zonamediagroup.zonamahasiswa.SharedPrefManagerZona;
import com.zonamediagroup.zonamahasiswa.Shop_buku.MyApplication;
import com.zonamediagroup.zonamahasiswa.adapters.HorizontalKategoriAdapter;
import com.zonamediagroup.zonamahasiswa.adapters.Recyclerview_home_Adapter;
import com.zonamediagroup.zonamahasiswa.models.Kategori_post_Model;
import com.zonamediagroup.zonamahasiswa.models.Recyclerview_home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Home extends Fragment implements Recyclerview_home_Adapter.ContactsAdapterListener, HorizontalKategoriAdapter.HorizontalContactsAdapterListener {

  protected Activity mActivity;

  LinearLayout layout_terbaru, layout_trending;
   RecyclerView receler_home, receler_home_trending;
   private List<Recyclerview_home> receler_homeModels;

  //temp
   private List<Recyclerview_home> receler_pertamaModels;

   private Recyclerview_home_Adapter receler_homeAdapter, receler_pertamaAdapter, receler_homeTrendingAdapter;

  //temp
   private List<Kategori_post_Model> receler_home_horizontal_data;

  //temp
   HorizontalKategoriAdapter receler_horizontal_adapter;

  //temp
    RecyclerView receler_horizontal, receler_pertama;

  NestedScrollView nestedscrol;
  boolean scrool = false;
  int page = 1;

  //jika 0 maka membuat recycler pertama bisa mendapatkan notifydatasetchanged
  //jika 1 maka membuat recycler pertama tidak bisa mendapatkan notifydatasetchanged
  int notifyRecyclerPertama = 0; 


  private static final String TAG = Home.class.getSimpleName();
  static final String tag_json_obj = "json_obj_req";
  private String URL_home = Server.URL_REAL + "Home_fix";
  //private static final String URL_home = Server.URL_REAL + "Home_fix_develop";


  private static final String URL_home_trending = Server.URL_REAL + "Trending";

  private static final String URL_home_save = Server.URL_REAL + "Save_like";

  // home post atas index 0
   ImageView img_thumnile_home;
   TextView tanggal_home, judul_home, kategori_home;

   RelativeLayout rl_loading_thumbnile_home;

  private static final String URL_hapus_save = Server.URL_REAL + "Delete_westlist";

  //temp
  static int JUMLAH_DATA_RECYCLERVIEW_AWAL = 4; //jumlah post yang ingin ditampilkan di recycler awal

  // loading
   ImageView image_animasi_loading;
   LinearLayout loding_layar;

  // eror layar
   LinearLayout eror_layar;
   TextView coba_lagi;
   int eror_param = 0; // eror_param =1 => terbaru | eror_param = 2 => trending

  //    loading bar
   ProgressBar progressBar;
   Sprite doubleBounce;
    LinearLayout layoutloading_bar;

  // data index 0
  String json_ID, json_post_author, json_post_date, json_post_title, json_guid, json_post_content,
          json_meta_value, json_user_login, json_name, json_term_id, json_gambar_user_wp, json_like_wp;

   LinearLayout liniar_index0;
   ImageView save_index0;

  // id_user from data sesion
  String id_user_sv;

   ProgressBar pb_home;
   ImageView search;
   ImageView saveok;

   LinearLayout ly_terbaru, ly_trending;
   Boolean ly_aja = true;
   TextView txt_terbaru, txt_trending;

  // param retrun ke save
  static final int LAUNCH_SECOND_ACTIVITY = 1;

  //parameter dari layout single artikel terbaru teratas, ketika memanggil detail.class
  static final int REQUEST_CODE_SINGLE_ARTIKEL_UTAMA = 19;

  Boolean data_kembali = false;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.page_home, container, false);

    init(root);
    return root;
  }

  void init(View root) {

    // linear layout terbaru trending
    ly_terbaru = root.findViewById(R.id.ly_terbaru);
    ly_trending = root.findViewById(R.id.ly_trending);
    txt_terbaru = root.findViewById(R.id.txt_terbaru);
    txt_trending = root.findViewById(R.id.txt_trending);
    pb_home = root.findViewById(R.id.pb_home);
    AdView mAdView = (AdView) root.findViewById(R.id.adView);
    AdRequest adRequest = new AdRequest.Builder().build();

    mAdView.loadAd(adRequest);
    id_user_sv = SharedPrefManagerZona.getInstance(mActivity).getid_user();

//        Nested
    nestedscrol = root.findViewById(R.id.nestedscrol);

//        Save
    saveok = root.findViewById(R.id.saveok);

//        Loading
    loding_layar = root.findViewById(R.id.loding_layar);
    image_animasi_loading = root.findViewById(R.id.image_animasi_loading);

//        eror
    eror_layar = root.findViewById(R.id.eror_layar);
    coba_lagi = root.findViewById(R.id.coba_lagi);

//loading gambar thumbnail
    rl_loading_thumbnile_home = root.findViewById(R.id.rl_loading_thumbnile_home);

//        Loading bar
    progressBar = (ProgressBar) root.findViewById(R.id.spin_kit_loading);
    doubleBounce = new ThreeBounce();
    progressBar.setIndeterminateDrawable(doubleBounce);
    layoutloading_bar = root.findViewById(R.id.layoutloading_bar);

//       home post atas index 0

    img_thumnile_home = root.findViewById(R.id.img_thumnile_home);
    tanggal_home = root.findViewById(R.id.tanggal_home);
    judul_home = root.findViewById(R.id.judul_home);
    kategori_home = root.findViewById(R.id.kategori_home);

    search = root.findViewById(R.id.search);

    save_index0 = root.findViewById(R.id.save_index0);

    save_index0.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        //if (save_index0.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_simpan_ajus_color).getConstantState()) {
          if(save_index0.getDrawable().getConstantState().equals(save_index0.getContext().getDrawable(R.drawable.ic_simpan_ajus_color).getConstantState()))
        {
          funcTransisiGambar(save_index0,getContext().getDrawable(R.drawable.ic_simpan_ajus),150,150);
          delete_simpan(id_user_sv, json_ID);
          json_like_wp = "0";
        }
          //else if(save_index0.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_simpan_ajus).getConstantState()) {
      else  if(save_index0.getDrawable().getConstantState().equals(save_index0.getContext().getDrawable(R.drawable.ic_simpan_ajus).getConstantState()))
          {
//          // save post to API
//         // save_index0.setImageDrawable(getContext().getDrawable(R.drawable.ic_simpan_ajus_color));
          funcTransisiGambar(save_index0,getContext().getDrawable(R.drawable.ic_simpan_ajus_color),150,150);
//// Call Api
          save_like(id_user_sv, json_ID);
            json_like_wp = "1";
        }
      }
    });

    layout_trending = root.findViewById(R.id.layout_trending);
    layout_terbaru = root.findViewById(R.id.layout_terbaru);
    liniar_index0 = root.findViewById(R.id.liniar_index0);

    liniar_index0.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), Detail.class);
        intent.putExtra("id_post", json_ID);
        intent.putExtra("post_content", json_post_content);
        intent.putExtra("humnile_post", "http://zonamahasiswa.id/wp-content/uploads/" + json_meta_value);
        intent.putExtra("title_post", json_post_title);
        intent.putExtra("name_categori", json_name);
        intent.putExtra("date_post", json_post_date);
        intent.putExtra("name_auth", json_user_login);
        intent.putExtra("img_author", json_gambar_user_wp);
        intent.putExtra("link", json_guid);
        intent.putExtra("id_kategori", json_term_id);
        intent.putExtra("id_author", json_post_author);
        intent.putExtra("save", json_like_wp);

        //sementara komen. hapus bila alternatif work
        //startActivity(intent);

        startActivityForResult(intent,REQUEST_CODE_SINGLE_ARTIKEL_UTAMA);

      }
    });


    receler_home = root.findViewById(R.id.receler_home);

    receler_home_trending = root.findViewById(R.id.receler_home_trending);

    //temp
    receler_horizontal = root.findViewById(R.id.receler_home_horizontal);

    //temp
    receler_pertama = root.findViewById(R.id.receler_pertama);

    // terbaru
    receler_homeModels = new ArrayList<>();

    //temp
    receler_pertamaModels = new ArrayList<>();

    //temp
    receler_home_horizontal_data = new ArrayList<>();

    receler_homeAdapter = new Recyclerview_home_Adapter(getContext(), receler_homeModels, this);

    //receler trending
    receler_homeTrendingAdapter = new Recyclerview_home_Adapter(getContext(), receler_homeModels, this,"trending");


    LinearLayoutManager mLayoutManagerkategori;
    mLayoutManagerkategori = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
    receler_home.setLayoutManager(mLayoutManagerkategori);
    receler_home.setItemAnimator(new DefaultItemAnimator());
    receler_home.setAdapter(receler_homeAdapter);


    //start inisialisasi recycler trending
    LinearLayoutManager mLayoutManagerTrending;
    mLayoutManagerTrending = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
    receler_home_trending.setLayoutManager(mLayoutManagerTrending);
    receler_home_trending.setItemAnimator(new DefaultItemAnimator());
    receler_home_trending.setAdapter(receler_homeTrendingAdapter);
    //end inisialisasi recycler trending
    Log.d("recelerhome","setAdapter..");


    //temp start inisialisasi recycler pertama
    //temp
    receler_pertamaAdapter = new Recyclerview_home_Adapter(getContext(), receler_pertamaModels, this);
    LinearLayoutManager mLayoutManagerVertical;
    mLayoutManagerVertical = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
    receler_pertama.setLayoutManager(mLayoutManagerVertical);
    receler_pertama.setItemAnimator(new DefaultItemAnimator());
    receler_pertama.setAdapter(receler_pertamaAdapter);
    //temp end inisialisasi recycler pertama



    //temp start inisialisasi horizontal recycler
    receler_horizontal_adapter = new HorizontalKategoriAdapter(getContext(),receler_home_horizontal_data,this);
    LinearLayoutManager mLayoutManagerHorizontal;
    mLayoutManagerHorizontal = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
    receler_horizontal.setLayoutManager(mLayoutManagerHorizontal);
    receler_horizontal.setItemAnimator(new DefaultItemAnimator());
    receler_horizontal.setAdapter(receler_horizontal_adapter);
    //temp end inisialisasi horizontal recycler


    // action
    saveok.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), Save.class);
        startActivityForResult(intent, 1);

      }
    });

    nestedscrol.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
      @Override
      public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
          Log.d("ScrollViewChild","jumlah child = "+v.getChildCount());
          scrool = true;

//                    loading_show();
          loading_bar_show();
          page++;


          if (ly_aja) {
            // terbaru
//                        Call API
            //segera hapus
            Log.d("get_homeduaDebug","gethome1");
            get_homedua(String.valueOf(page), id_user_sv);
          } else {
            // trending
//                        Call API
            get_trending(String.valueOf(page), id_user_sv);
          }


        }
      }
    });

    search.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), Search.class);
        startActivityForResult(intent, 1);
      }
    });

    ly_terbaru.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        if (ly_aja) {
          // true
        } else {
          loading_show();


          terbaru_aktiv();

          // false
          ly_aja = true;
          page = 1;
          receler_homeModels.clear(); // hapus recelerview

          //temp
          receler_pertamaModels.clear();

          liniar_index0.setVisibility(View.VISIBLE);
          layout_terbaru.setVisibility(View.VISIBLE);
          layout_trending.setVisibility(View.GONE);
          //buat notifyRecyclerPertama = 0 agar adapter dari recycler view pertama kembali mendapatkan aksi notifyDataSetChanged
          notifyRecyclerPertama = 0;
          //segera hapus
          Log.d("get_homeduaDebug","gethome2");
          get_homedua(String.valueOf(page), id_user_sv);
        }

      }
    });

    ly_trending.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        if (ly_aja) {


          loading_show();


          trending_aktiv();

          // true
          ly_aja = false;
          page = 1;
          receler_homeModels.clear(); // hapus recelerview

          //temp
          receler_pertamaModels.clear();
          liniar_index0.setVisibility(View.GONE);
          layout_terbaru.setVisibility(View.GONE);
          layout_trending.setVisibility(View.VISIBLE);

          // post
          get_trending(String.valueOf(page), id_user_sv);

        } else {

        }

      }
    });

    coba_lagi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        eror_dismiss();
        loading_show();


        if (eror_param == 1) {
          // terbaru
          //segera hapus
          Log.d("get_homeduaDebug","gethome3");
          get_homedua(String.valueOf(page), id_user_sv);

        } else if (eror_param == 2) {
          // trending

          get_trending(String.valueOf(page), id_user_sv);
        }
      }
    });


    eror_dismiss();

    loading_show();


//        Call API
    //segera hapus
    Log.d("get_homeduaDebug","gethome4");
    get_homedua(String.valueOf(page), id_user_sv);
  }


  private void get_homedua(String halamanaktif, String id_user) {

Log.d("funcGetHome","get_homedua");
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {

        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));

          System.out.println("Data Global");


          if (jObj.getString("status").equals("true")) {


            if (data_kembali == true) {
              receler_homeModels.clear();

              //temp
              receler_pertamaModels.clear();

              // refreshing recycler view
              receler_homeAdapter.notifyDataSetChanged();

              //temp
              receler_pertamaAdapter.notifyDataSetChanged();

              data_kembali = false;
            }

            JSONArray dataArray = jObj.getJSONArray("item_terbaru");

            //temp. horizontal article
            JSONArray horizontalArray = jObj.getJSONArray("item_horizontal");


            if (page == 1) {

// get respons API get Index 0
              JSONObject jsonObject1 = dataArray.getJSONObject(0);
              json_ID = jsonObject1.optString("ID");
              json_post_author = jsonObject1.optString("post_author");
              json_post_date = jsonObject1.optString("post_date");
              json_post_title = jsonObject1.optString("post_title");
              json_guid = jsonObject1.optString("guid");
              json_post_content = jsonObject1.optString("post_content");
              json_meta_value = jsonObject1.optString("meta_value");
              json_user_login = jsonObject1.optString("user_login");
              json_name = jsonObject1.optString("name");
              json_term_id = jsonObject1.optString("term_id");
              json_gambar_user_wp = jsonObject1.optString("gambar_user_wp");
              json_like_wp = jsonObject1.optString("like");
              //segera hapus
              Log.d("json_like_wp","isi json_like_wp: "+json_like_wp);
//                            Cek condition ? jika Wishlist maka merubah icon
              if (json_like_wp.equals("1")) {
                save_index0.setImageResource(R.drawable.ic_simpan_ajus_color);
                //segera hapus
                Log.d("json_like_wp","isi json_like_wp: IFF");

              } else {
                // save post to API
                save_index0.setImageResource(R.drawable.ic_simpan_ajus);
                //segera hapus
                Log.d("json_like_wp","isi json_like_wp: ELSEE");

              }

              // di set ke tampilan
              Glide.with(mActivity)
                      .load("http://zonamahasiswa.id/wp-content/uploads/" + json_meta_value)
                      .apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.ic_img_thumnile).centerCrop())
                      .centerCrop()
                      .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                          pb_home.setVisibility(View.GONE);
                          return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                          pb_home.setVisibility(View.GONE);
                          return false;
                        }
                      })
                      .into(img_thumnile_home);
              //hide loading, show image
              //rl_loading_thumbnile_home.setVisibility(View.GONE);
              //img_thumnile_home.setVisibility(View.VISIBLE);

              //tanggal_home.setText(json_post_date);
              tanggal_home.setText(funcGetWaktu(json_post_date));
              judul_home.setText(json_post_title);
              kategori_home.setText(json_name);

              dataArray.remove(0); // remove index 0
            } else {

            }


            List<Recyclerview_home> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Recyclerview_home>>() {
            }.getType());

            List<Kategori_post_Model> itemsHorizontal = new Gson().fromJson(horizontalArray.toString(), new TypeToken<List<Kategori_post_Model>>() {
            }.getType());
            //temp. delete if work (3 line below)
            //receler_homeModels.addAll(items);
            //Log.d("recelerhome","dari volley");
            //receler_homeAdapter.notifyDataSetChanged();

            funcBagiDataRecelerVertical(JUMLAH_DATA_RECYCLERVIEW_AWAL,items,itemsHorizontal);

            if (scrool) {
              scrool = false;
              loading_bar_dismiss();
            } else {

              loading_dismiss();
            }


          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error
          eror_param = 1;

          loading_dismiss();
          eror_show();
          Log.d("HomeError","error 1 "+e.getMessage());

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());

        eror_param = 1;

        loading_dismiss();
        eror_show();
        Log.d("HomeError","error 2");

      }
    }) {


//            reques parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("halamanaktif", halamanaktif);
        params.put("id_user", id_user);
        return params;
      }
    };

//        Heandling Error

    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());


      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  //fungsi untuk menambahkan suatu karakter pada string pada posisi tertentu
  public String addChar(String str, char ch, int position) {
    int len = str.length();
    char[] updatedArr = new char[len + 1];
    str.getChars(0, position, updatedArr, 0);
    updatedArr[position] = ch;
    str.getChars(position, len, updatedArr, position + 1);
    return new String(updatedArr);
  }

  //merubah tampilan waktu ke DD MMMM YYYY I HH:MM
  private String funcGetWaktu(String date_post)
  {
    // hilangkan ":<detik digit ke 1><detik detik digit ke 2>"
    date_post = date_post.substring(0, date_post.length() - 3);

    //tambahkan karakter pipe diantara jam dan tanggal
    date_post = addChar(date_post,'|',11);

    //tambahkan spasi setelah pipe
    date_post = addChar(date_post,' ',12);

    //Mulai penyekatan, segala yang disebelah kiri pipe | adalah tanggalBulanTahun. segala yang disebelah kanan pipe adalah jam menit
    //split karakter dengan patokan pipe |. karakter pipe harus di escape dengan 2 kali \\
    String[] parts = date_post.split("\\|");
    String tanggalBulanTahun = parts[0];
    tanggalBulanTahun = tanggalBulanTahun.trim();
    String menitJam = parts[1];
    menitJam = menitJam.trim();

    //rubah format tanggal ke dd MMMM YYYYY
    try {
      SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
      SimpleDateFormat format2 = new SimpleDateFormat("dd MMMM yyyy");
      Date date = format1.parse(tanggalBulanTahun);
      //pakai format baru yaitu bulan ditampilkan namanya
      tanggalBulanTahun = format2.format(date);
    }
    catch(Exception e)
    {
      Log.e("ErrorZona","Terjadi Error "+e.getMessage());
    }

    String tanggalBerita = tanggalBulanTahun+" I "+menitJam;
    return tanggalBerita;
  }

  private void get_trending(String halamanaktif, String id_user) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home_trending, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
          System.out.println(jObj.getString("status"));


          if (jObj.getString("status").equals("true")) {

            JSONArray dataArray = jObj.getJSONArray("item_terbaru");



            List<Recyclerview_home> items = new Gson().fromJson(dataArray.toString(), new TypeToken<List<Recyclerview_home>>() {
            }.getType());



            //temp delete if work (2 line below)
            receler_homeModels.addAll(items);
            //receler_homeAdapter.notifyDataSetChanged();
            receler_homeTrendingAdapter.notifyDataSetChanged();

            if (scrool) {
              scrool = false;
              loading_bar_dismiss();
            } else {

              loading_dismiss();
            }


          }


          // Check for error node in json

        } catch (JSONException e) {
          // JSON error
          eror_param = 2;

          loading_dismiss();
          eror_show();
          Log.d("HomeError","error 3 "+e.getMessage());

          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());

        eror_param = 2;

        loading_dismiss();
        eror_show();
        //Log.d("HomeError","error 4");

      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("halamanaktif", halamanaktif);
        params.put("id_user", id_user);
        return params;
      }
    };

// Heandling error

    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());
        eror_param = 2;

        loading_dismiss();
        eror_show();
        Log.d("HomeError","error 5");
      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.mActivity = activity;
  }

  /**
   * If you use the support 23 library, the above method will prompt outdated, obsessive-compulsive partners, you can use the following method instead
   */
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    this.mActivity = (Activity) context;
  }

  @Override
  public void onContactSelected(Recyclerview_home itemsatu, int position) {

//        Select recelerview
    Intent intent = new Intent(getActivity(), Detail.class);
    intent.putExtra("id_post", itemsatu.getID());
    intent.putExtra("post_content", itemsatu.getPost_content());
    intent.putExtra("humnile_post", "http://zonamahasiswa.id/wp-content/uploads/" + itemsatu.getMeta_value());
    intent.putExtra("title_post", itemsatu.getPost_title());
    intent.putExtra("name_categori", itemsatu.getName());
    intent.putExtra("date_post", itemsatu.getPost_date());
    intent.putExtra("name_auth", itemsatu.getUser_login());
    intent.putExtra("img_author", itemsatu.getGambar_user_wp());
    intent.putExtra("link", itemsatu.getGuid());

      intent.putExtra("save", itemsatu.getLike());


    intent.putExtra("id_author", itemsatu.getPost_author());
    intent.putExtra("id_kategori", itemsatu.getTerm_id());

    //SEMENTARA KOMEN
    //startActivity(intent);

    startActivityForResult(intent,1);

    //SEGERA HAPUS
    Toast.makeText(getContext(),"onCOntactSelected "+position,Toast.LENGTH_SHORT).show();
  }

  @Override
  public void Save(Recyclerview_home save, int position) {

    String id_post_sv = save.getID();
    save_like(id_user_sv, id_post_sv);
    //agar artikel ketika dibuka di detail.class juga menjadi ada simpan
    save.setLike("1");
  }

  @Override
  public void unSave(Recyclerview_home model, int position) {
    String id_post_sv = model.getID();
    delete_simpan(id_user_sv, id_post_sv);
   //agar artikel ketika dibuka di detail.class juga menjadi tidak ada simpan
    model.setLike("0");
  }



  public void loading_bar_show() {
    layoutloading_bar.setVisibility(View.VISIBLE);
  }

  public void loading_bar_dismiss() {
    layoutloading_bar.setVisibility(View.INVISIBLE);
  }

  private void save_like(String id_user, String id_post) {


    StringRequest strReq = new StringRequest(Request.Method.POST, URL_home_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
//          System.out.println(jObj.getString("status"));
//
//          System.out.println("Data SAVE LIKE");




        } catch (JSONException e) {
          // JSON error


          e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());


      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }


  // loading dan eror
  public void loading_show() {
    loding_layar.setVisibility(View.VISIBLE);
  }

  public void loading_dismiss() {

    loding_layar.setVisibility(View.GONE);
  }

  public void eror_show() {
    eror_layar.setVisibility(View.VISIBLE);
  }

  public void eror_dismiss() {

    eror_layar.setVisibility(View.GONE);
  }

  //    set bacground terbaru
  public void terbaru_aktiv() {
    txt_trending.setBackgroundResource(R.drawable.rounded_disable);
    txt_trending.setTextColor(ContextCompat.getColor(getContext(), R.color.black));

    txt_terbaru.setBackgroundResource(R.drawable.rounded_aktif);
    txt_terbaru.setTextColor(ContextCompat.getColor(getContext(), R.color.white));

  }

  //    ser bacground trending
  public void trending_aktiv() {
    txt_trending.setBackgroundResource(R.drawable.rounded_aktif);
    txt_trending.setTextColor(ContextCompat.getColor(getContext(), R.color.white));

    txt_terbaru.setBackgroundResource(R.drawable.rounded_disable);
    txt_terbaru.setTextColor(ContextCompat.getColor(getContext(), R.color.black));

  }

  private void delete_simpan(String id_user, String id_post) {
    StringRequest strReq = new StringRequest(Request.Method.POST, URL_hapus_save, new Response.Listener<String>() {

      @Override
      public void onResponse(String response) {


        try {
          JSONObject jObj = new JSONObject(response);
//          System.out.println(jObj.getString("status"));
//
//          System.out.println("Data SAVE LIKE");



          // Check for error node in json

        } catch (JSONException e) {
          // JSON error


            e.printStackTrace();
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Register Error: " + error.getMessage());



      }
    }) {


//            parameter

      @Override
      protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
        params.put("TOKEN", "qwerty");
        params.put("id_user", id_user);
        params.put("id_post", id_post);
        return params;
      }
    };

//        ini heandling requestimeout
    strReq.setRetryPolicy(new RetryPolicy() {
      @Override
      public int getCurrentTimeout() {
        return 10000;
      }

      @Override
      public int getCurrentRetryCount() {
        return 10000;
      }

      @Override
      public void retry(VolleyError error) throws VolleyError {

        Log.e(TAG, "VolleyError Error: " + error.getMessage());

      }
    });


    // Adding request to request queue
    MyApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
  }

  // cal back result
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //  Handle activity result here
    if (requestCode == LAUNCH_SECOND_ACTIVITY) {

      if (resultCode == Activity.RESULT_OK) { //apabila ada aksi back dari save, dan ada save yang dihapus

        notifyRecyclerPertama = 0; //agar recycler yang pertama di notif ulang

        data_kembali = true;

        page = 1;

        loading_show();

        //SEGERA HAPUS
        Toast.makeText(getContext(),"ifx home ok",Toast.LENGTH_SHORT).show();

        if (ly_aja) {
          // terbaru
          //segera hapus
          Log.d("get_homeduaDebug","gethome5");
          get_homedua(String.valueOf(page), id_user_sv);
        } else {
          // trending
          get_trending(String.valueOf(page), id_user_sv);
        }

      }
      if (resultCode == Activity.RESULT_CANCELED) { //apabila ada aksi back dari save, namun tidak ada save yang dihapus
        // Write your code if there's no result
        //SEGERA HAPUS
        Toast.makeText(getContext(),"ifx home cancel",Toast.LENGTH_SHORT).show();
      }
    }
    //jika request code dari single artikel di menu terbaru
    else if(requestCode == REQUEST_CODE_SINGLE_ARTIKEL_UTAMA)
    {
      if (resultCode == Activity.RESULT_OK) {
        //segera hapus
        Toast.makeText(getContext(),"perubahan save single artikel",Toast.LENGTH_SHORT).show();
        loading_show();
        //segera hapus
        Log.d("get_homeduaDebug","gethome6");
        get_homedua(String.valueOf(page), id_user_sv);
      }
      else if(resultCode == Activity.RESULT_CANCELED){
        //segera hapus
        Toast.makeText(getContext(),"TIDAK ada perubahan save single artikel",Toast.LENGTH_SHORT).show();
      }

    }
    else{
      //SEGERA HAPUS
      Toast.makeText(getContext(),"another request code",Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onContactSelectedHorizontal(Kategori_post_Model transaksihariinidataList) {
    funcTampilkanArtikelHorizontal(transaksihariinidataList);
  }

  //method untuk membagi data antar 2 recycler view. n data di recycler awal, sisanya di recycler utama
  public void funcBagiDataRecelerVertical(int jumlahData,List<Recyclerview_home> dataAwal, List<Kategori_post_Model> dataHorizontal)
  {
    Log.d("BagiData","bagiDataTerpanggil");
    List<Recyclerview_home> dataRecyclerPertama = new ArrayList<>();
    List<Recyclerview_home> dataRecyclerKedua = new ArrayList<>();
    for(int i=0;i<jumlahData;i++)
    {
      dataRecyclerPertama.add(i, dataAwal.get(i));
    }
    for(int j=0;j<jumlahData;j++)
    {
      dataAwal.remove(0);

    }

    dataRecyclerKedua = dataAwal;

    if(notifyRecyclerPertama == 0)
    {
      notifyRecyclerPertama = 1;
      //recycler pertama load pertama kali saja
      receler_pertamaModels.addAll(dataRecyclerPertama);
      receler_pertamaAdapter.notifyDataSetChanged();


      //recycler horizontal load pertama kali saja
      receler_home_horizontal_data.addAll(dataHorizontal);
      receler_horizontal_adapter.notifyDataSetChanged();
    }



    receler_homeModels.addAll(dataRecyclerKedua);
    Log.d("fandy","receler_homeModels ukuran: "+receler_homeModels.size());
    receler_homeAdapter.notifyDataSetChanged();



  }

  //fungsi untuk menampilkan artikel horizontal
  //fungsi ini untuk memanggil artikel yang akan dibaca
  public boolean funcTampilkanArtikelHorizontal(Kategori_post_Model itemsatu)
  {
    Intent intent = new Intent(getContext(), Detail.class);
    intent.putExtra("id_post", itemsatu.getID());
    intent.putExtra("post_content", itemsatu.getPost_content());
    intent.putExtra("humnile_post", "http://zonamahasiswa.id/wp-content/uploads/" + itemsatu.getMeta_value());
    intent.putExtra("title_post", itemsatu.getPost_title());
    //Log.d("CerbungZona","isi save : "+itemsatu.getLike());
    intent.putExtra("save", itemsatu.getLike());
    intent.putExtra("name_categori", itemsatu.getName());
    intent.putExtra("date_post", itemsatu.getPost_date());
    intent.putExtra("name_auth", itemsatu.getUser_login());
    //Log.d("FandyDebug","name auth: "+itemsatu.getUser_login());
    intent.putExtra("id_author", itemsatu.getPost_author());
    //Log.d("DEBUGZONA_VOLLEY_POST_BY_KATEGORI",itemsatu.getPost_author());

    intent.putExtra("img_author", itemsatu.getGambar_user_wp());
    intent.putExtra("link", itemsatu.getGuid());
    intent.putExtra("id_kategori", itemsatu.getTerm_id());
    startActivity(intent);
    return true;
  }

  public void funcTransisiGambar(ImageView imageView, Drawable drawableTujuan, int durasiFadeIn, int durasiFadeOut)
  {
    //start coba animasi
    imageView.animate()
            .alpha(0f)
            .setDuration(durasiFadeIn)
            .setListener(new Animator.AnimatorListener() {
              @Override
              public void onAnimationStart(Animator animator) { }
              @Override
              public void onAnimationEnd(Animator animator) {
                imageView.setImageDrawable(drawableTujuan);
                imageView.animate().alpha(1).setDuration(durasiFadeOut).setListener(new Animator.AnimatorListener() {
                  @Override
                  public void onAnimationStart(Animator animator) {

                  }

                  @Override
                  public void onAnimationEnd(Animator animator) {
                    imageView.clearAnimation();
                  }

                  @Override
                  public void onAnimationCancel(Animator animator) {

                  }

                  @Override
                  public void onAnimationRepeat(Animator animator) {

                  }
                });
              }
              @Override
              public void onAnimationCancel(Animator animator) { }
              @Override
              public void onAnimationRepeat(Animator animator) { }
            });

    //end coba animasi
  }




}

